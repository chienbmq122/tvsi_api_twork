﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVSI.Crm.API.Lib.Utilities
{
    public class StringeeToken
    {
        public static string GenerateToken(string userId)
        {
            var tokenHandler = new JwtSecurityTokenHandler();


            var keySecret = "MHJ1QTdTS3ZIclBiZlN2dUthNkczNWFHNFU1WDJEUWs=";
            var sid = "SK7jqgLyZGZdJjrh7iNlebxMv2DkqQhhF";


            var key = Encoding.UTF8.GetBytes(keySecret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Expires = DateTime.UtcNow.AddDays(365),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateJwtSecurityToken(tokenDescriptor);
            token.Header.Remove("typ");
            token.Header.Remove("alg");
            token.Header.Remove("cty");
            token.Header.Add("typ", "JWT");
            token.Header.Add("alg", "HS256");
            token.Header.Add("cty", "stringee-api;v=1");


            token.Payload.Add("jti", sid + "-" + Guid.NewGuid().ToString());
            token.Payload.Add("iss", sid);
            token.Payload.Add("rest_api", true);
            token.Payload.Add("userId", userId);


            return tokenHandler.WriteToken(token);
        }
    }
}
