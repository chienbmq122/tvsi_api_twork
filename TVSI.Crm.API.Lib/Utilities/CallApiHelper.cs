using System.Net;
using System.Text;
using Newtonsoft.Json;
using TVSI.Crm.API.Lib.Models.Response.Customer;

namespace TVSI.Crm.API.Lib.Utilities;

public class CallApiHelper
{
    public static GetImageCardId GetImageCardId(string cardId, object model,string apiBond)
    {
        try
        {
            using (var client = new HttpClient())
            {
                var json = JsonConvert.SerializeObject(model); 
                var body = new StringContent(json, Encoding.UTF8, "application/json");
                using (var response = client.PostAsync(apiBond + "Ekyc/CI_CA_GetImageByCardID", body).Result)
                {

                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        var result = response.Content.ReadAsStringAsync().Result;
                        var dataAccount = JsonConvert.DeserializeObject<GetImageCardId>(result);
                        if (dataAccount!.RetCode == "000")
                        {
                            if (!string.IsNullOrEmpty(dataAccount.RetData.Front_Image) && !string.IsNullOrEmpty(dataAccount.RetData.Back_Image))
                            {
                                dataAccount.RetData.Front_Image = dataAccount.RetData.Front_Image.Replace("data:image/jpg;base64,", "");
                                dataAccount.RetData.Back_Image = dataAccount.RetData.Back_Image.Replace("data:image/jpg;base64,", "");
                                return dataAccount;
                            }
                        }
                    }
                    return null!;
                }

            }
        }
        catch (Exception e)
        {
            return null!;
        }

    }
}