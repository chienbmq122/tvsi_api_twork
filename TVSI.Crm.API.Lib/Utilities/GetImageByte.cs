using System.Net;

namespace TVSI.Crm.API.Lib.Utilities;

public static class GetImageByte
{
    public static byte[]? GetImageByteUrl(string url)
    {
        Stream stream = null;
        byte[] buf;
        try
        {
            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(url);

            HttpWebResponse response = (HttpWebResponse)req.GetResponse();
            stream = response.GetResponseStream();

            using (MemoryStream ms = new MemoryStream())
            {
                stream.CopyTo(ms);
                buf = ms.ToArray();
            }

            stream.Close();
            response.Close();
            return buf;

        }
        catch (Exception exp)
        {
            buf = null;
        }
        return buf;
    }
}