﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVSI.Crm.API.Lib.Utilities
{
    public class ActivityConst
    {
        public static string EncriptMd5(string content)
        {
            byte[] textBytes = System.Text.Encoding.Default.GetBytes(content);
            try
            {
                System.Security.Cryptography.MD5CryptoServiceProvider cryptHandler;
                cryptHandler = new System.Security.Cryptography.MD5CryptoServiceProvider();
                byte[] hash = cryptHandler.ComputeHash(textBytes);
                string ret = "";
                foreach (byte a in hash)
                {
                    ret += a.ToString("x2");
                }
                return ret;
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }
        }

        public static string RandomNumber(int length = 6)
        {
            //Initiate objects & vars  
            byte[] seed = Guid.NewGuid().ToByteArray();
            Random random = new Random(BitConverter.ToInt32(seed, 0));
            int randNumber = 0;
            //Loop ‘length’ times to generate a random number or character
            String randomNumber = "";
            for (int i = 0; i < length; i++)
            {
                randNumber = random.Next(48, 58);
                randomNumber = randomNumber + (char)randNumber;
                //append random char or digit to randomNumber string

            }
            return randomNumber;
        }
    }
}
