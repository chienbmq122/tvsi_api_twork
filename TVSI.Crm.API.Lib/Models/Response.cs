﻿using System.ComponentModel.DataAnnotations;
using TVSI.TWork.API.Common.Enums;
using TVSI.TWork.API.Common.ExtensionMethods;

namespace TVSI.Crm.API.Lib.Models;

public class Response<T>
{
    [Required]
    public string UserName { get; set; }
    public string Code { get; set; } = "-1";
    public string? Message { get; set; }
    public IEnumerable<T>? Data { get; set; } = null;
}


public class ResponseSingle<T>
{
    [Required]
    public string UserName { get; set; }
    public string Code { get; set; } = "-1";
    public string? Message { get; set; }
    public T? Data { get; set; } = default;
}