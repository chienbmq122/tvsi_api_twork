﻿namespace TVSI.Crm.API.Lib.Models
{
    public class LanguageItem
    {
        public string? ItemCode { get; set; }
        public string? Vietnamese { get; set; }
        public string? English { get; set; }
    }
}
