﻿namespace TVSI.Crm.API.Lib.Models;

public class SystemError
{
    public string Code { get; set; } = "E999";
    public string? Message { get; set; } = "Lỗi hệ thống" + "(E999)";
}