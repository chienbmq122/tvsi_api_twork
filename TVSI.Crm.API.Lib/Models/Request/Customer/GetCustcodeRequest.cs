﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVSI.Crm.API.Lib.Models.Request.Customer
{
    public class GetCustcodeRequest : BaseRequest
    {
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public string CustCode { get; set; }
    }
}
