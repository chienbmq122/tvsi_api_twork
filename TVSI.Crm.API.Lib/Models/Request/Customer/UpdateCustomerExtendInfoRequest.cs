using System.ComponentModel.DataAnnotations;
using TVSI.Crm.API.Lib.Models.Request.Account;

namespace TVSI.Crm.API.Lib.Models.Request.Customer;

public class UpdateCustomerExtendInfoRequest : BaseRequest
{
    private List<ShareHolderInfo>? _shareHolderInfo;
    private List<ExtendCust>? _extendCust;


    /// <summary>
    /// Tên KH
    /// </summary>
    public string? FullName { get; set; }
    
    /// <summary>
    /// Mã kh
    /// </summary>
    [Required,MinLength(6),MaxLength(6)]
    public string? Custcode { get; set; }
    
    /// <summary>
    /// địa chỉ KH
    /// </summary>
    public string? Address { get; set; }
    
    /// <summary>
    /// Thông tin cổ đông
    /// </summary>
    public bool IsShareHolder { get; set; }

    public List<ShareHolderInfo>? ShareHolderInfo
    {
        get => _shareHolderInfo;
        set => _shareHolderInfo = value;
    }
    
    public List<ExtendCust>? ExtendCust
    {
        get => _extendCust;
        set => _extendCust = value;
    }

    /*/// <summary>
    /// Nghề nghiệp
    /// </summary>
    public int JobId { get; set; }

        
    /// <summary>
    /// Nơi làm việc
    /// </summary>
    public int WorkPlaceId { get; set; }
        
    
    /// <summary>
    /// Khả năng tài chính
    /// </summary>
    public int FinancialCapabilityId { get; set; }  
        
        
    /// <summary>
    /// Kinh nghiệm đầu tư
    /// </summary>
    public int InvestmentExperienceId  { get; set; }   
        
        
        
    /// <summary>
    /// đã tham gia trái phiếu
    /// </summary>
    public int JoinedBondId { get; set; }
        
        
        
    /// <summary>
    /// Khả năng chấp nhận rủi ro
    /// </summary>
    public int RiskAcceptId { get; set; }
        

    /// <summary>
    /// Sở thích đầu tư
    /// </summary>
    public int HobbyInvestmentId  { get; set; }
        

        
    /// <summary>
    /// Có quan hệ với sale
    /// </summary>
    public int RelationshipSaleId { get; set; } 
        

    /// <summary>
    /// Lưu ý khi nói chuyện
    /// </summary>
    public int NoteTalkingId  { get; set; }  
         

        
    /// <summary>
    /// Kiến thức, Kinh nghiệm
    /// </summary>
    public int KnowledExpId { get; set; }  
        

        
    /// <summary>
    /// Quan điểm đầu tư
    /// </summary>
    public int InvestmentPersId { get; set; }  
        

        
    /// <summary>
    /// Nguồn khai thác
    /// </summary>
    public int SourceId { get; set; } */
}