namespace TVSI.Crm.API.Lib.Models.Request.Customer;

public class CustomerOnlineDetailInfoRequest : BaseRequest
{
    /// <summary>
    /// Id hồ sơ
    /// </summary>
    public int Id { get; set; }
}