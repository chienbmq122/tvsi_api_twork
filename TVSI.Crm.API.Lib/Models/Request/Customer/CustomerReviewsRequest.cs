namespace TVSI.Crm.API.Lib.Models.Request.Customer;

public class CustomerReviewsRequest : BaseRequest
{
    public string? CustCode { get; set; }
}