using System.ComponentModel.DataAnnotations;

namespace TVSI.Crm.API.Lib.Models.Request.Customer;

public class UpdateKycCustomerInfoRequest : BaseRequest
{

    /// <summary>
    /// Tên KH
    /// </summary>
    public string? FullName { get; set; }
    
    /// <summary>
    /// Mã kh
    /// </summary>
    [Required,MinLength(6),MaxLength(6)]
    public string? Custcode { get; set; }


    public List<int>? KycId { get; set; }
    
    
    /// <summary>
    /// Mục tiêu tài chính ngắn hạn
    /// </summary>
    public string? FinancialTaget { get; set; }
    
    
    /// <summary>
    /// Đặc điểm cần lưu ý
    /// </summary>
    public string? OtherNote { get; set; } 
    
    /// <summary>
    /// Sản phẩm sẽ giới thiệu
    /// </summary>
    public string? ProductIntro { get; set; }

    /*
    /// <summary>
    /// Thời gian nắm giữ sự kiện
    /// </summary>
    public int EventHoldingTimeId { get; set; }
    
    /// <summary>
    /// Thời gian theo dõi thị trường
    /// </summary>
    public int MarketTimeId { get; set; }
    
    
    /// <summary>
    /// Phân loại KH
    /// </summary>
    public int CustomerTypeId { get; set; }
    

    /// <summary>
    /// Thời gian đầu tư tại công ty ck khác
    /// </summary>
    private int InvestSecuritiesCompanyId { get; set; } 
    
    /// <summary>
    /// Mức độ kinh nghiệm
    /// </summary>
    private int ExpLevelId { get; set; }

    /// <summary>
    /// Kênh đầu tư
    /// </summary>
    private int InvestChannelId { get; set; }
    
    /// <summary>
    /// Nội dung kênh đầu tư
    /// </summary>
    private int ContentInvestChannelId { get; set; }
    
    /// <summary>
    /// Khẩu vị rủi ro
    /// </summary>
    private int RiskInterestsId { get; set; }
    
    
    /// <summary>
    /// Mục đính tài chính
    /// </summary>
    private int FinancialPurposeId { get; set; }
    
    /// <summary>
    /// Khóa học tham gia
    /// </summary>
    private int CourseJoinedId { get; set; }
    */



}

public class CourseJoined
{
    public string? Name { get; set; }
    public int Value { get; set; } 
}
/// <summary>
/// Hội thảo đã tham gia
/// </summary>
public class SeminarJoined
{
    public string? Name { get; set; }
    public int Value { get; set; } 
}
/// <summary>
/// sản phẩm đã tham gia
/// </summary>
public class ProductJoined
{
    public string? Name { get; set; }
    public int Value { get; set; } 
}

/// <summary>
/// Bản tin muốn nhận
/// </summary>
public class NewsJoined
{
    public string? Name { get; set; }
    public int Value { get; set; } 
}