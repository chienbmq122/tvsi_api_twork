﻿using System.ComponentModel.DataAnnotations;

namespace TVSI.Crm.API.Lib.Models.Request.Customer;


/// <summary>
/// Thông tin Request danh sách KH MTK
/// </summary>
public class CustomerOnlineListRequest : BaseRequest
{
    /// <summary>
    /// Từ khóa tìm kiếm : số CMT/CCCD
    /// </summary>
    public string? CardId { get; set; } 
    
    /// <summary>
    /// Từ khóa tìm kiếm : mã Khách hàng
    /// </summary>
    public string? CustCode { get; set; } 
    
    
    /// <summary>
    /// Từ khóa tìm kiếm : Họ và tên
    /// </summary>
    public string? FullName { get; set; }
    
    
    /// <summary>
    /// Từ khóa tìm kiếm : số điện thoại KH
    /// </summary>
    public string? Phone { get; set; }
    
    /// <summary>
    /// Từ khóa tìm kiếm : mã nhân viên tư vấn SaleID
    /// </summary>
    public string? SaleID { get; set; }  
    
    /// <summary>
    /// Từ khóa tìm kiếm : Kênh mở tài khoản
    /// </summary>
    public int OpenSource { get; set; } 
    
    /// <summary>
    /// Từ khóa tìm kiếm : Trạng thái tk
    /// </summary>
    public string? AccountStatus { get; set; } 
    
    /// <summary>
    /// Từ khóa tìm kiếm : trạng thái hồ sơ
    /// </summary>
    public string? ProfileStatus { get; set; } 
    
    /// <summary>
    /// Từ khóa tìm kiếm : trạng thái Tương tác
    /// </summary>
    public string? ActionStatus { get; set; }

    /// <summary>
    /// Thứ tự trang (tính từ 1)
    /// </summary>
    [Required]
    public int PageIndex { get; set; }

    /// <summary>
    /// Số bản ghi cần lấy trên một trang
    /// </summary>
    [Required]
    public int PageSize { get; set; }
}