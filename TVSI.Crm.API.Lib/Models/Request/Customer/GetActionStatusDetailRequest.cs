namespace TVSI.Crm.API.Lib.Models.Request.Customer;

public class GetActionStatusDetailRequest : BaseRequest
{
    public int Id { get; set; }
}