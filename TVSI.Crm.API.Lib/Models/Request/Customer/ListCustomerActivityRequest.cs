﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVSI.Crm.API.Lib.Models.Request.Customer
{
    public class ListCustomerActivityRequest : BaseRequest
    {
        public string CustCode { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
    }
}
