namespace TVSI.Crm.API.Lib.Models.Request.Customer;

public class CheckValidateCustCodeRequest : BaseRequest
{ 
    public string? Custcode { get; set; }
    public int CustomerType { get; set; }
}