namespace TVSI.Crm.API.Lib.Models.Request.Customer;

public class ConfirmEkycRequest : BaseRequest
{
    /// <summary>
    /// Kênh mở tài khoản
    /// </summary>
    public int OpenSource { get; set; }
    
    /// <summary>
    /// Code Kích hoạt
    /// </summary>
    public string? ConfirmCode { get; set; }
}
