namespace TVSI.Crm.API.Lib.Models.Request.Customer;

public class CancelCustomerInfoRequest : BaseRequest
{
    /// <summary>
    /// Id can huy
    /// </summary>
    public int Id { get; set; }
}