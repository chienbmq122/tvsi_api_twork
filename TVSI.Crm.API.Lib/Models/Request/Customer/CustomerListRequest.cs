using System.ComponentModel.DataAnnotations;

namespace TVSI.Crm.API.Lib.Models.Request.Customer;


/// <summary>
/// Quan ly KH -> danh sách KH
/// </summary>
public class CustomerListRequest : BaseRequest
{
    /// <summary>
    /// Từ khóa tìm kiếm : mã Khách hàng
    /// </summary>
    public string? Custcode { get; set; } 
    
    
    /// <summary>
    /// Từ khóa tìm kiếm : Họ và tên
    /// </summary>
    public string? FullName { get; set; }
    
    
    /// <summary>
    /// Từ khóa tìm kiếm : số điện thoại KH
    /// </summary>
    public string? Phone { get; set; } 
    
    /// <summary>
    /// Từ khóa tìm kiếm : số Email KH
    /// </summary>
    public string? Email { get; set; }
    
    
    /// <summary>
    /// Từ khóa tìm kiếm : Loại hình KH 
    /// </summary>
    public int CustomerType { get; set; }
    
    /// <summary>
    /// Trạng thái hoạt động
    /// </summary>
    public int Status { get; set; }
    
    /// <summary>
    /// Thứ tự trang (tính từ 1)
    /// </summary>
    [Required]
    public int PageIndex { get; set; }

    /// <summary>
    /// Số bản ghi cần lấy trên một trang
    /// </summary>
    [Required]
    public int PageSize { get; set; }
    
}