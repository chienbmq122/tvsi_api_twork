using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using TVSI.Crm.API.Lib.Models.Request.Account;

namespace TVSI.Crm.API.Lib.Models.Request.Customer;

public class UpdateCustomerActionStatusRequest : BaseRequest
{
    [Required]
    public int Id { get; set; }
    public string? ActionStatus { get; set; }
    public string? Description { get; set; }
    public string? SaleAssign { get; set; }

}