namespace TVSI.Crm.API.Lib.Models.Request.Customer;

public class CustomerDetailRequest : BaseRequest
{

    /// <summary>
    /// Mã khách hàng
    /// </summary>
    public string? Custcode { get; set; }
}