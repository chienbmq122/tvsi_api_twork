using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using TVSI.Crm.API.Lib.Models.Request.Account;

namespace TVSI.Crm.API.Lib.Models.Request.Customer;

public class ConvertCustomerToAccountRequest : BaseRequest,IValidatableObject
{
    private List<BankInfo>? _bankInfo;

    public List<BankInfo>? BankInfo
    {
        get => _bankInfo;
        set => _bankInfo = value;
    }

    [Description("Mã khác hàng")] public string? Custcode { get; set; } // Mã khách hàng

    [Description("Họ và tên")] public string? FullName { get; set; } // Họ tên      

    [Description("Ngày sinh")] public string? BirthDay { get; set; } // ngày sinh

    [Description("giới tính")] public int Gender { get; set; } // giới tính 1 là nam, 2 là nữ  , 3 ko xác định

    [Description("Số CMT/CCCD")] public string? CardId { get; set; } // số CMT/CCCD

    [Description("Ngày cấp CMT/CCCD")] public string? IssueDate { get; set; } // ngày cấp CMT/CCCD

    [Description("Nơi cấp CMT/CCCD")] public string? IssuePlace { get; set; } // nơi cấp

    [Description("Địa chỉ")] public string? Address { get; set; } // địa chỉ

    [Description("Sale ID")] public string? SaleID { get; set; } // saleID


    [Description("Trạng thái hợp đồng")]
    public int ContractStatus { get; set; } // trạng thái hồ sơ, 1 đã hoàn thiện, 2 nợ hồ sơ

    [Description("Đăng ký margin")] public bool AccountMargin { get; set; }
    public long CreditLimit { get; set; } // Hạn mức dư nợ
    public int? DebtTerm { get; set; } = null; // Thời hạn nợ hợp đồng
    public int? DepositRate { get; set; } // Tỷ lệ ký quỹ


    [Description("ẢNH CMT/CCCD MẶT TRƯỚC")]
    public string? ImageFront { get; set; }

    [Description("ẢNH CMT/CCCD MẶT SAU")] public string? ImageBack { get; set; }

    [Description("ẢNH xác thực")] public string? ImageFace { get; set; }

    [Description("Đại diện TVSI")] public string? ManTvsi { get; set; }

    [Description("Chức vụ")] public string? ManPosition { get; set; }

    [Description("giấy ủy quyền")] public string? ManAnPower { get; set; }

    [Description("Ngày cấp giấy ủy quyền")]
    public string? ManDateAnPower { get; set; }

    [Description("Số CMND ủy quyền")] public string? ManCardId { get; set; }

    [Description("Ngày cấp CMT/CCCD")] public string? ManIssueDate { get; set; }

    [Description("Nơi cấp CMT/CCCD ủy quyền")]
    public string? ManIssuePlace { get; set; }

    [Description("Phương thức nhận KQGD SMS")]
    public bool TradingResultSms { get; set; }

    [Description("Phương thức nhận KQGD Email")]
    public bool TradingResultEmail { get; set; }

    [Description("Số điện thoại 01 ")] public string? Phone01 { get; set; }

    [Description("Số điện thoại 02 ")] public string? Phone02 { get; set; }

    [Description("Email")] public string? Email { get; set; }


    [Description("Thông tin ngân hàng")] public bool Info01 { get; set; }
    public bool Info02 { get; set; }
    public bool Info03 { get; set; }
    public bool Info04 { get; set; }
    public bool Info05 { get; set; }
    public bool Info06 { get; set; }
    public bool Info07 { get; set; }


    public string TradingResultMethod
    {
        get
        {
            var result = "";
            if (TradingResultSms)
                result += "1";
            if (TradingResultEmail)
                result += "2";
            return result;
        }
    }

    public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
    {
            if (ContractStatus == 2 && AccountMargin)
            {
                    if (CreditLimit == 0)
                            yield return new ValidationResult($"Field is Required {CreditLimit}",
                                    new[] { nameof(CreditLimit) });
                    if (DebtTerm == 0)
                            yield return new ValidationResult($"Field is Required {DebtTerm}",
                                    new[] { nameof(DebtTerm) });
                    if (DepositRate == 0)
                            yield return new ValidationResult($"Field is Required {DepositRate}",
                                    new[] { nameof(DepositRate) });
            }

            if (TradingResultEmail)
                    if (string.IsNullOrEmpty(Email))
                            yield return new ValidationResult($"Field is Required {Email}", new[] { nameof(Email) });
            if (TradingResultSms)
                    if (string.IsNullOrEmpty(Phone01))
                            yield return new ValidationResult($"Field is Required {Phone01}",
                                    new[] { nameof(Phone01) });

            if (ContractStatus == 2)
            {
                    if (string.IsNullOrEmpty(ImageFront))
                            yield return new ValidationResult($"Field is Required {ImageFront}",
                                    new[] { nameof(ImageFront) });
                    if (string.IsNullOrEmpty(ImageBack))
                            yield return new ValidationResult($"Field is Required {ImageBack}",
                                    new[] { nameof(ImageBack) });
            }

            if (BankInfo?.Count > 0)
            {
                    for (int i = 0; i < BankInfo?.Count; i++)
                    {
                            if (!string.IsNullOrEmpty(BankInfo?[i].BankNo) ||
                                !string.IsNullOrEmpty(BankInfo?[i].SubBranchNo) ||
                                !string.IsNullOrEmpty(BankInfo?[i].BankAccountNo))
                            {
                                    if (string.IsNullOrEmpty(BankInfo?[i].BankAccountNo))
                                            yield return new ValidationResult($"Field is Required BankAccountNo",
                                                    new[] { nameof(BankInfo) });
                                    if (string.IsNullOrEmpty(BankInfo?[i].BankNo))
                                            yield return new ValidationResult($"Field is Required BankNo",
                                                    new[] { nameof(BankInfo) });
                                    if (string.IsNullOrEmpty(BankInfo?[i].SubBranchNo))
                                            yield return new ValidationResult($"Field is Required SubBranchNo",
                                                    new[] { nameof(BankInfo) });
                            }
                    }
            }
    }
}