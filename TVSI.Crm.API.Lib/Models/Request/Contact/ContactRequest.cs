﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVSI.Crm.API.Lib.Models.Request.Contact
{
    public class ContactRequest
    {
        public class ContactStaffRequest : BaseRequest
        {
            public int PageIndex { get; set; }
            public int PageSize { get; set; }
            public string? KeySearch { get; set; }
            public string? CustCode { get; set; }
            public string? Phone { get; set; }
        }

        public class ContactinfoCustRequest : BaseRequest
        {
            public string? CustCode { get; set; }
        }

        public class ContactinfoLeadRequest : BaseRequest
        {
            public int LeadID { get; set; }
        }

        public class ContactinfoStaffRequest : BaseRequest
        {
            public string? StaffID { get; set; }
        }
    }
}
