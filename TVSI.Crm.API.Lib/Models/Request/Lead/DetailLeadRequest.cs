﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVSI.Crm.API.Lib.Models.Request.Lead
{
    public class DetailLeadRequest : BaseRequest
    {
        public int LeadID { get; set; }
    }
}
