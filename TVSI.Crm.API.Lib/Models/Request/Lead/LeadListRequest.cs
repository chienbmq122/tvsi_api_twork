﻿namespace TVSI.Crm.API.Lib.Models.Request.Lead;

/// <summary>
/// Thông tin Request danh sách KH tiềm năng
/// </summary>
public class LeadListRequest : BaseRequest
{
    public string? Name { get; set; }
    public string? Phone { get; set; }
    public string? Email { get; set; }
    public int LeadSourceID { get; set; }
    public int ProfileType { get; set; }

    /// <summary>
    /// Trạng thái - 0:Thêm mới, 1:Đã được Assign, 2:Đã được chuyển thành Khách hàng, -1:Đã đóng, 99:Đã xóa
    /// </summary>
    public int Status { get; set; }

    /// <summary>
    /// Thứ tự trang (tính từ 1)
    /// </summary>
    public int PageIndex { get; set; }

    /// <summary>
    /// Số bản ghi cần lấy trên một trang
    /// </summary>
    public int PageSize { get; set; }
}