﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVSI.Crm.API.Lib.Models.Request.Lead
{
    public class DeleteLead : BaseRequest
    {
        public int LeadID { get; set; }

        public string Reason { get; set; }
    }
}
