﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVSI.Crm.API.Lib.Models.Request.Lead
{
    public class UpdateLeadRequest : BaseRequest
    {
        public string LeadName { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public int ProfileType { get; set; }
        public int Status { get; set; }
        public int BranchID { get; set; }
        public string AssignUser { get; set; }
        public string Description { get; set; }
        public int LeadSourceID { get; set; }
        public int LeadID { get; set; }
        public List<CreateAttributeUser> Attributes { get; set; }
    }
}
