using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using TVSI.Crm.API.Lib.Models.Request.Account;

namespace TVSI.Crm.API.Lib.Models.Request.Account;

public class UpdateAccountStatusRequest : BaseRequest
{
    [Required]
    public int Id { get; set; }
    public bool IsConfirm { get; set; }
    public string? AccountStatus { get; set; }
    public string? SaleId { get; set; }
    public string? AddContract { get; set; }
    public string? Description { get; set; }
}