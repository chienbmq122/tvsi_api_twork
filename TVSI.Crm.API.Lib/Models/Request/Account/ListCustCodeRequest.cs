﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVSI.Crm.API.Lib.Models.Request.Account
{
    public class GetCustcodeRequestAccount : BaseRequest
    {
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public string CustCode { get; set; }
    }
}
