using System.ComponentModel.DataAnnotations;

namespace TVSI.Crm.API.Lib.Models.Request.Account;

public class OpenIndividualDomesticAdditionalAccountRequest : BaseRequest
{
    
    ///
    /// <summary>
    /// Mã khách hàng
    /// </summary>
    [Required]
    public string? CustomerCode { get; set; }

    ///
    /// <summary>
    /// Họ tên
    /// </summary>
    [Required]
    public string? FullName { get; set; }


    /// <summary>
    /// Ngày sinh
    /// </summary>
    [Required]
    public string? BirthDay { get; set; }

    /// <summary>
    /// giới tính
    /// </summary>
    public int? Gender { get; set; }

    /// <summary>
    /// Số CMT/CCCD
    /// </summary>
    [Required]
    public string? CardId { get; set; }

    /// <summary>
    /// Ngày cấp cmt
    /// </summary>
    [Required]
    public string? IssueDate { get; set; }

    /// <summary>
    /// Nơi cấp cmt
    /// </summary>
    [Required]
    public string? IssuePlace { get; set; }

    /// <summary>
    /// Địa chỉ
    /// </summary>
    public string? Address { get; set; }

    /// <summary>
    /// sale ID
    /// </summary>
    public string? SaleID { get; set; }

    
    /// <summary>
    /// Tài khoản giao dịch ký quỹ
    /// </summary>
    public bool AccountMargin { get; set; }

    public decimal? CreditLimit { get; set; } // Hạn mức dư nợ
    public int? DebtTerm { get; set; } = null; // Thời hạn nợ hợp đồng
    public double? DepositRate { get; set; } // Tỷ lệ ký quỹ

    /// <summary>
    /// Nguồn mở tk
    /// </summary>
    public int? SourceAccount { get; set; }
}