using System.ComponentModel.DataAnnotations;

namespace TVSI.Crm.API.Lib.Models.Request.Account;

public class OpenForeignAccountRequest : BaseRequest, IValidatableObject
{
    ////// Thông tin cơ bản
 
    ///
    /// <summary>
    /// Loai hồ sơ
    /// </summary>
    public int ProfileType { get; set; }

    ///
    /// <summary>
    /// Mã khách hàng
    /// </summary>
    [Required]
    public string? CustomerCode { get; set; }

    ///
    /// <summary>
    /// Họ tên
    /// </summary>
    [Required]
    public string? FullName { get; set; }


    /// <summary>
    /// Ngày sinh
    /// </summary>
    [Required]
    public string? BirthDay { get; set; }

    /// <summary>
    /// giới tính
    /// </summary>
    [Required]
    public int Gender { get; set; }

    /// <summary>
    /// Số CMT/CCCD
    /// </summary>
    [Required]
    public string? CardId { get; set; }

    /// <summary>
    /// Ngày cấp cmt
    /// </summary>
    [Required]
    public string? IssueDate { get; set; }

    /// <summary>
    /// Nơi cấp cmt
    /// </summary>
    [Required]
    public string? IssuePlace { get; set; }

    /// <summary>
    /// Địa chỉ
    /// </summary>
    [Required]
    public string? Address { get; set; }

    /// <summary>
    /// Địa chỉ tại việt nam
    /// </summary>
    public string? AddressVn { get; set; }

    /// <summary>
    /// Quốc tịch
    /// </summary>
    [Required]
    public string? Nationality { get; set; }

    /// <summary>
    /// Người đại diện tvsi
    /// </summary>
    public string? ManTvsi { get; set; }
    /// <summary>
    /// Vị trí
    /// </summary>
    public string? ManPosition { get; set; }
    /// <summary>
    ///  giấy ủy quyền
    /// </summary>
    public string? ManAnPower { get; set; }
    /// <summary>
    /// Ngày cập giấy ủy quyền
    /// </summary>
    public string? ManDateAnPower { get; set; }
    /// <summary>
    /// Số CMT/CCCD của TVSI 
    /// </summary>
    public string? ManCardId { get; set; }
    /// <summary>
    /// Ngày cấp số CMT/CCCD
    /// </summary>
    public string? ManIssueDate { get; set; }
    /// <summary>
    /// Nơi cấp số CMT/CCCD
    /// </summary>
    public string? ManIssuePlace { get; set; }
    
    /// <summary>
    /// sale ID
    /// </summary>
    public string? SaleID { get; set; }

    //////// Đăng ký dịch vụ

    /// <summary>
    /// Phương thức nhận SMS
    /// </summary>
    public bool TradingResultSms { get; set; }

    /// <summary>
    /// Check phương thức nhận Email
    /// </summary>
    public bool TradingResultEmail { get; set; }

    /// <summary>
    /// Số điện thoại 1
    /// </summary>
    public string? Phone01 { get; set; }

    /// <summary>
    /// Số điện thoại
    /// </summary>
    public string? Phone02 { get; set; }


    /// <summary>
    /// Email
    /// </summary>
    public string? Email { get; set; }

    /// Ngân hàng chuyển tiền

    /// <summary>
    /// Số TK NH
    /// </summary>
    public string? BankAccountNo { get; set; }
    /// <summary>
    /// Tên chủ tài khoản NH 01
    /// </summary>
    public string? BankAccountName { get; set; }

    /// <summary>
    /// Mã ngân hàng
    /// </summary>
    public string? BankNo { get; set; }

    /// <summary>
    /// Mã chi nhánh
    /// </summary>
    public string? SubBranchNo { get; set; }



    public string TradingResultMethod
    {
        get
        {
            var result = "";
            if (TradingResultSms)
                result += "1";
            if (TradingResultEmail)
                result += "2";
            return result;
        }
    }


    public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
    {
       
        if (TradingResultEmail)
            if (string.IsNullOrEmpty(Email))
                yield return new ValidationResult($"Field is Required {Email}", new[] { nameof(Email) });
        if (TradingResultSms)
            if (string.IsNullOrEmpty(Phone01))
                yield return new ValidationResult($"Field is Required {Phone01}", new[] { nameof(Phone01) });



        if (!string.IsNullOrEmpty(BankNo) || !string.IsNullOrEmpty(SubBranchNo) || !string.IsNullOrEmpty(BankAccountNo))
        {
            if (string.IsNullOrEmpty(BankAccountNo))
                yield return new ValidationResult($"Field is Required BankAccountNo", new[] { nameof(BankAccountNo) });
            if (string.IsNullOrEmpty(BankNo))
                yield return new ValidationResult($"Field is Required BankNo", new[] { nameof(BankNo) });
            if (string.IsNullOrEmpty(SubBranchNo))
                yield return new ValidationResult($"Field is Required SubBranchNo", new[] { nameof(SubBranchNo) });
        }

    }
}