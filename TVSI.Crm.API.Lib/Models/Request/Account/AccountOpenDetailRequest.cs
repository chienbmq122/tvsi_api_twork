namespace TVSI.Crm.API.Lib.Models.Request.Account;
/// <summary>
/// Chi tiết hồ sơ tài khoản 
/// </summary>
public class AccountOpenDetailRequest : BaseRequest
{
    public int Id { get; set; }
    public string? Custcode { get; set; }
}