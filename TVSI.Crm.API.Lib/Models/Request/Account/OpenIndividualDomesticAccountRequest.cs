using System.ComponentModel.DataAnnotations;

namespace TVSI.Crm.API.Lib.Models.Request.Account;

public class OpenIndividualDomesticAccountRequest : BaseRequest, IValidatableObject
{
    ////// Thông tin cơ bản
 
    ///
    /// <summary>
    /// Loai hồ sơ
    /// </summary>
    public int ProfileType { get; set; }

    ///
    /// <summary>
    /// Mã khách hàng
    /// </summary>
    [Required]
    public string? CustomerCode { get; set; }

    ///
    /// <summary>
    /// Họ tên
    /// </summary>
    [Required]
    public string? FullName { get; set; }


    /// <summary>
    /// Ngày sinh
    /// </summary>
    [Required]
    public string? BirthDay { get; set; }

    /// <summary>
    /// giới tính
    /// </summary>
    [Required]
    public int Gender { get; set; }

    /// <summary>
    /// Số CMT/CCCD
    /// </summary>
    [Required]
    public string? CardId { get; set; }

    /// <summary>
    /// Ngày cấp cmt
    /// </summary>
    [Required]
    public string? IssueDate { get; set; }

    /// <summary>
    /// Nơi cấp cmt
    /// </summary>
    [Required]
    public string? IssuePlace { get; set; }

    /// <summary>
    /// Địa chỉ
    /// </summary>
    [Required]
    public string? Address { get; set; }

    /// <summary>
    /// Người đại diện tvsi
    /// </summary>
    public string? ManTvsi { get; set; }
    /// <summary>
    /// Vị trí
    /// </summary>
    public string? ManPosition { get; set; }
    /// <summary>
    ///  giấy ủy quyền
    /// </summary>
    public string? ManAnPower { get; set; }
    /// <summary>
    /// Ngày cập giấy ủy quyền
    /// </summary>
    public string? ManDateAnPower { get; set; }
    /// <summary>
    /// Số CMT/CCCD của TVSI 
    /// </summary>
    public string? ManCardId { get; set; }
    /// <summary>
    /// Ngày cấp số CMT/CCCD
    /// </summary>
    public string? ManIssueDate { get; set; }
    /// <summary>
    /// Nơi cấp số CMT/CCCD
    /// </summary>
    public string? ManIssuePlace { get; set; }

    /// <summary>
    /// trạng thái hồ sơ. 0: Nợ, 1: hoàn thiện
    /// </summary>
    public bool IsProfileCompletion { get; set; }

    /// <summary>
    /// CMT/CCCD mặt trước , base 64
    /// </summary>
    public string? ImageFront { get; set; }
    /// <summary>
    /// CMT/CCCD mặt sau, base 64
    /// </summary>
    public string? ImageBack { get; set; }


    /// <summary>
    /// sale ID
    /// </summary>
    public string? SaleID { get; set; }

    //////// Đăng ký dịch vụ

    /// <summary>
    /// Tài khoản giao dịch ký quỹ
    /// </summary>
    public bool AccountMargin { get; set; }
    public decimal? CreditLimit { get; set; } // Hạn mức dư nợ
    public int? DebtTerm { get; set; } = null; // Thời hạn nợ hợp đồng
    public double? DepositRate { get; set; } // Tỷ lệ ký quỹ

    /// <summary>
    /// Phương thức nhận SMS
    /// </summary>
    public bool TradingResultSms { get; set; }

    /// <summary>
    /// Check phương thức nhận Email
    /// </summary>
    public bool TradingResultEmail { get; set; }

    /// <summary>
    /// Số điện thoại 1
    /// </summary>
    public string? Phone01 { get; set; }

    /// <summary>
    /// Số điện thoại
    /// </summary>
    public string? Phone02 { get; set; }


    /// <summary>
    /// Email
    /// </summary>
    public string? Email { get; set; }

    /// <summary>
    /// Ngân hàng chuyển tiền
    /// </summary>
    public bool TransferCashService { get; set; }

    public List<BankInfoRes>? BankInfo { get; set; }

    public string TradingResultMethod
    {
        get
        {
            var result = "";
            if (TradingResultSms)
                result += "1";
            if (TradingResultEmail)
                result += "2";
            return result;
        }
    }

    ///// Thông tin mở rộng 

    /// <summary>
    /// Thông tin HOA KỲ
    /// </summary>
    public bool IsInfoUsa { get; set; }
    public UsaInfo? UsaInfo { get; set; }


    /// <summary>
    /// Thông tin cổ đông
    /// </summary>
    public bool IsShareHolder { get; set; }
    public List<ShareHolderInfo>? ShareHolderInfo { get; set; }

    public List<ExtendCust>? ExtendCust { get; set; }
    /// <summary>
    /// Nguồn mở tk
    /// </summary>
    public int? SourceAccount { get; set; }
    public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
    {
        if (AccountMargin)
        {
            if (CreditLimit == 0)
                yield return new ValidationResult($"Field is Required {CreditLimit}", new[] { nameof(CreditLimit) });
            if (DebtTerm == 0)
                yield return new ValidationResult($"Field is Required {DebtTerm}", new[] { nameof(DebtTerm) });
            if (DepositRate == 0)
                yield return new ValidationResult($"Field is Required {DepositRate}", new[] { nameof(DepositRate) });
        }
        if (TradingResultEmail)
            if (string.IsNullOrEmpty(Email))
                yield return new ValidationResult($"Field is Required {Email}", new[] { nameof(Email) });
        if (TradingResultSms)
            if (string.IsNullOrEmpty(Phone01))
                yield return new ValidationResult($"Field is Required {Phone01}", new[] { nameof(Phone01) });

      

        if (BankInfo?.Count > 0)
        {
            for (int i = 0; i < BankInfo?.Count; i++)
            {
                if (!string.IsNullOrEmpty(BankInfo?[i].BankNo) || !string.IsNullOrEmpty(BankInfo?[i].SubBranchNo) || !string.IsNullOrEmpty(BankInfo?[i].BankAccountNo))
                {
                    if (string.IsNullOrEmpty(BankInfo?[i].BankAccountNo))
                        yield return new ValidationResult($"Field is Required BankAccountNo", new[] { nameof(BankInfo) });
                    if (string.IsNullOrEmpty(BankInfo?[i].BankNo))
                        yield return new ValidationResult($"Field is Required BankNo", new[] { nameof(BankInfo) });
                    if (string.IsNullOrEmpty(BankInfo?[i].SubBranchNo))
                        yield return new ValidationResult($"Field is Required SubBranchNo", new[] { nameof(BankInfo) });
                }

            }
        }

    }
}

public class BankInfoRes
{
    /// <summary>
    /// Số TK NH
    /// </summary>
    public string? BankAccountNo { get; set; }
    /// <summary>
    /// Tên chủ tài khoản NH 01
    /// </summary>
    public string? BankAccountName { get; set; }

    /// <summary>
    /// Mã ngân hàng
    /// </summary>
    public string? BankNo { get; set; }

    /// <summary>
    /// Mã chi nhánh
    /// </summary>
    public string? SubBranchNo { get; set; }
}

public class UsaInfo
{
    public bool Info01 { get; set; }
    public bool Info02 { get; set; }
    public bool Info03 { get; set; }
    public bool Info04 { get; set; }
    public bool Info05 { get; set; }
    public bool Info06 { get; set; }
    public bool Info07 { get; set; }
}

public class ShareHolderInfo
{
    public bool ShareType_1 { get; set; }
    public bool ShareType_2 { get; set; }
    public bool ShareType_3 { get; set; }
    public bool ShareType_4 { get; set; }
    public string? StockCode { get; set; }
}

public class ExtendCust
{
    public int CategoryType { get; set; }
    public int Value { get; set; }
}