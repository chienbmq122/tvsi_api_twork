using System.ComponentModel.DataAnnotations;

namespace TVSI.Crm.API.Lib.Models.Request.Account;

/// <summary>
/// Thông tin Request danh sách tài khoản của KH
/// </summary>
public class AccountListRequest : BaseRequest
{
    /// <summary>
    /// Họ và tên
    /// </summary>
    public string? FullName { get; set; }  
    
    /// <summary>
    /// Mã Khách hàng
    /// </summary>
    public string? Custcode { get; set; }
    
    /// <summary>
    /// Trạng thái tk
    /// </summary>
    public string? AccountStatus { get; set; }
    
    /// <summary>
    /// trạng thái hồ sơ
    /// </summary>
    public string? ProfileStatus { get; set; } 
    
    /// <summary>
    /// Thứ tự trang (tính từ 1)
    /// </summary>
    public int PageIndex { get; set; }

    /// <summary>
    /// Số bản ghi cần lấy trên một trang
    /// </summary>
    public int PageSize { get; set; }
    
}