namespace TVSI.Crm.API.Lib.Models.Request.Account;
/// <summary>
/// Chi tiết hồ sơ tài khoản 
/// </summary>
public class OpenAccountAdditionalDetailRequest : BaseRequest
{
    public string? Custcode { get; set; }
}