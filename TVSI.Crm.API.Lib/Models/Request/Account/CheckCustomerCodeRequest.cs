namespace TVSI.Crm.API.Lib.Models.Request.Account;
/// <summary>
/// Chi tiết hồ sơ tài khoản 
/// </summary>
public class CheckCustomerCodeRequest : BaseRequest
{
    public int TypeAccount { get; set; }
    public string CustomerCode { get; set; }
}