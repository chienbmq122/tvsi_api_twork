namespace TVSI.Crm.API.Lib.Models.Request.Account;

public class CancelOpenAccountRequest : BaseRequest
{
    /// <summary>
    /// Id
    /// </summary>
    public int Id { get; set; }
}