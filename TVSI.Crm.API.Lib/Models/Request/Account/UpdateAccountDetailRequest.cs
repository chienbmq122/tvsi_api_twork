using System.ComponentModel.DataAnnotations;

namespace TVSI.Crm.API.Lib.Models.Request.Account;

public class UpdateAccountDetailRequest : BaseRequest,IValidatableObject
{
        private List<ExtendCust>? _extendCust;
        /// <summary>
        /// Id
        /// </summary>
        public int Id { get; set; }
        
        /// <summary>
        /// Họ tên
        /// </summary>
        [Required]
        public string? CustCode { get; set; }
        
        /// <summary>
        /// Họ tên
        /// </summary>
        [Required]
        public string? FullName { get; set; }
        
        /// <summary>
        /// Ngày sinh
        /// </summary>
        [Required]
        public string? BirthDay { get; set; }
        
        /// <summary>
        /// giới tính
        /// </summary>
        [Required]
        public int Gender { get; set; } 
        
        
        /// <summary>
        /// Số CMT/CCCD
        /// </summary>
        [Required]
        public string? CardId { get; set; }
        
        /// <summary>
        /// Ngày cấp cmt
        /// </summary>
        [Required]
        public string? IssueDate { get; set; }
        
        /// <summary>
        /// Nơi cấp cmt
        /// </summary>
        [Required]
        public string? IssuePlace { get; set; }

        /// <summary>
        /// Địa chỉ
        /// </summary>
        [Required]
        public string? Address { get; set; }


        /// <summary>
        /// Người đại diện tvsi
        /// </summary>
        public string? ManTvsi { get; set; }
        /// <summary>
        /// Vị trí
        /// </summary>
        public string? ManPosition { get; set; }
        /// <summary>
        ///  giấy ủy quyền
        /// </summary>
        public string? ManAnPower { get; set; }
        /// <summary>
        /// Ngày cập giấy ủy quyền
        /// </summary>
        public string? ManDateAnPower { get; set; }
        /// <summary>
        /// Số CMT/CCCD của TVSI
        /// </summary>
        public string? ManCardId { get; set; }
        /// <summary>
        /// Ngày cấp số CMT/CCCD
        /// </summary>
     
        public string? ManIssueDate { get; set; }
        /// <summary>
        /// Nơi cấp số CMT/CCCD
        /// </summary>
  
        public string? ManIssuePlace { get; set; }
        
        
        
        /// <summary>
        /// Trạng thái HĐ
        /// </summary>
        /// Trạng thái hồ sơ 1 là hoàn thiện, 2 là nợ hồ sơ
        [Required]
        public int ContractStatus { get; set; }

        //////// Đăng ký dịch vụ
        
        /// <summary>
        /// Tài khoản giao dịch ký quỹ
        /// </summary>
        public bool AccountMargin { get; set; }
        /// <summary>
        /// Phương thức nhận SMS
        /// </summary>
        public bool TradingResultSms { get; set; }
        /// <summary>
        /// Số điện thoại 1
        /// </summary>
        public string? Phone01 { get; set; }
        
        /// <summary>
        /// Số điện thoại 2
        /// </summary>
        public string? Phone02 { get; set; }
        
        /// <summary>
        /// Check phương thức nhận Email
        /// </summary>
        public bool TradingResultEmail { get; set; }
        
        /// <summary>
        /// Email
        /// </summary>
        public string? Email { get; set; }
        
        /// <summary>
        /// Ngân hàng chuyển tiền
        /// </summary>
        public bool TransferCashService { get; set; }
        
        
        public List<BankInfo>? BankInfo { get; set; }

        public string TradingResultMethod
        {
                get
                {
                        var result = "";
                        if (TradingResultSms)
                                result += "1";
                        if (TradingResultEmail)
                                result += "2";
                        return result;
                }
        }
        
        public List<ExtendCust>? ExtendCust
        {
                get => _extendCust;
                set => _extendCust = value;
        }

        /// <summary>
        /// Thông tin cổ đông
        /// </summary>
        public bool IsShareHolder { get; set; }
        
        public List<ShareHolderInfo>? ShareHolderInfo { get; set; }
        
        
        /*/// <summary>
        /// tên chủ Tài khoản đối ứng 01
        /// </summary>
        public string? AccountContraName01 { get; set; }
        /// <summary>
        /// Số tài khoản đối ứng 02
        /// </summary>
        public string? AccountContraNo01 { get; set; }
        
        /// <summary>
        /// tên chủ Tài khoản đối ứng 02
        /// </summary>
        public string? AccountContraName02 { get; set; }
        /// <summary>
        /// Số tài khoản đối ứng 02
        /// </summary>
        /// 
        public string? AccountContraNo02 { get; set; }*/
        
        
        
        /// <summary>
        /// Thông tin HOA KỲ
        /// </summary>
        public bool InfoUsa { get; set; }
        public bool Info01 { get; set; }
        public bool Info02 { get; set; }
        public bool Info03 { get; set; }
        public bool Info04 { get; set; }
        public bool Info05 { get; set; }
        public bool Info06 { get; set; }
        public bool Info07 { get; set; }
        
        public string? ImageFront { get; set; }
        public string? ImageBack { get; set; }
        public string? ImageFace { get; set; }
        
        public int? CreditLimit { get; set; } // Hạn mức dư nợ
        public int? DebtTerm { get; set; } = null; // Thời hạn nợ hợp đồng
        public int? DepositRate { get; set; } // Tỷ lệ ký quỹ
        
        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
                if (ContractStatus == 2 && AccountMargin)
                {
                        if (CreditLimit == 0) 
                                yield return new ValidationResult($"Field is Required {CreditLimit}", new[] { nameof(CreditLimit) });
                        if (DebtTerm == 0)
                                yield return new ValidationResult($"Field is Required {DebtTerm}", new[] { nameof(DebtTerm) });
                        if (DepositRate == 0)
                                yield return new ValidationResult($"Field is Required {DepositRate}", new[] { nameof(DepositRate) });
                }
                if (TradingResultEmail)
                        if(string.IsNullOrEmpty(Email))
                                yield return new ValidationResult($"Field is Required {Email}", new[] { nameof(Email) });
                if(TradingResultSms)
                        if (string.IsNullOrEmpty(Phone01)) 
                                yield return new ValidationResult($"Field is Required {Phone01}", new[] { nameof(Phone01) });
                if (BankInfo?.Count > 0)
                {
                        for (int i = 0; i < BankInfo?.Count; i++)
                        {
                                if (!string.IsNullOrEmpty(BankInfo?[i].BankNo) || !string.IsNullOrEmpty(BankInfo?[i].SubBranchNo) || !string.IsNullOrEmpty(BankInfo?[i].BankAccountNo))
                                {
                                        if (string.IsNullOrEmpty(BankInfo?[i].BankAccountNo))
                                                yield return new ValidationResult($"Field is Required BankAccountNo", new[] { nameof(BankInfo) });
                                        if (string.IsNullOrEmpty(BankInfo?[i].BankNo))
                                                yield return new ValidationResult($"Field is Required BankNo", new[] { nameof(BankInfo) }); 
                                        if (string.IsNullOrEmpty(BankInfo?[i].SubBranchNo))
                                                yield return new ValidationResult($"Field is Required SubBranchNo", new[] { nameof(BankInfo) });
                                }

                        }
                }

        }
}

public class BankInfo
{

        /// <summary>
        /// Số TK NH 01
        /// </summary>
        public string? BankAccountNo { get; set; }
        /// <summary>
        /// Tên chủ tài khoản NH 01
        /// </summary>
        public string? BankAccountName { get; set; }
        
        /// <summary>
        /// Mã ngân hàng
        /// </summary>
        public string? BankNo { get; set; }

        /// <summary>
        /// Mã chi nhánh
        /// </summary>
        public string? SubBranchNo { get; set; }
}

public class FsDebt
{
        public decimal CreditLimit { get; set; } // Hạn mức dư nợ
        public int? DebtTerm { get; set; } = null; // Thời hạn nợ hợp đồng
        public double? DepositRate { get; set; } // Tỷ lệ ký quỹ
}

