namespace TVSI.Crm.API.Lib.Models.Request.Account;
/// <summary>
/// Chi tiết hồ sơ tài khoản 
/// </summary>
public class OpenAccountDetailRequest : BaseRequest
{
    /// <summary>
    /// ID hồ sơ
    /// </summary>
    public long Id { get; set; }

    /// <summary>
    /// SearchType , 1: Tìm kiếm phê duyệt, 2: Tìm kiếm lịch sử
    /// </summary>
    public int SearchType { get; set; }


}