using System.ComponentModel.DataAnnotations;

namespace TVSI.Crm.API.Lib.Models.Request.Account;

/// <summary>
/// Thông tin Request danh sách tài khoản của KH
/// </summary>
public class OpenAccountDebtDocumentListRequest : BaseRequest
{
    /// <summary>
    /// Mã Khách hàng
    /// </summary>
    public string? Custcode { get; set; }

    /// <summary>
    /// Họ và tên
    /// </summary>
    public string? FullName { get; set; }

    /// <summary>
    /// trạng thái tài khoản
    /// </summary>
    public string? AccountStatus { get; set; }

    /// <summary>
    /// SaleID
    /// </summary>
    public string? SaleId { get; set; }


    /// <summary>
    /// SearchType , 1: Tìm kiếm phê duyệt, 2: Tìm kiếm lịch sử
    /// </summary>
    public int SearchType { get; set; }

    /// <summary>
    /// Thứ tự trang (tính từ 1)
    /// </summary>
    public int PageIndex { get; set; }

    /// <summary>
    /// Số bản ghi cần lấy trên một trang
    /// </summary>
    public int PageSize { get; set; }
    
}