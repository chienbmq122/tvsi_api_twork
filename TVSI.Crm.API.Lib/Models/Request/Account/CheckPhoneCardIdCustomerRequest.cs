using System.ComponentModel.DataAnnotations;

namespace TVSI.Crm.API.Lib.Models.Request.Account;
/// <summary>
/// Chi tiết hồ sơ tài khoản 
/// </summary>
public class CheckPhoneCardIdAccountRequest : BaseRequest
{
    /// <summary>
    /// Số điện thoại KH
    /// </summary>
    public string? Phone { get; set; }
    /// <summary>
    /// Số CMT
    /// </summary>
    public string? CardId { get; set; }
    /// <summary>
    /// Ngày cấp CMT/CCCD
    /// </summary>
    public string? CardIssueDate { get; set; }

    /// <summary>
    /// 1 là tạo mới, 2 là cập nhật
    /// </summary>
    public string? Type { get; set; } = "1";
}