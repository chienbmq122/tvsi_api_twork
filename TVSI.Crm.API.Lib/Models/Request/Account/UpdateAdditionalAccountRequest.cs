using System.ComponentModel.DataAnnotations;

namespace TVSI.Crm.API.Lib.Models.Request.Account;

public class UpdateAdditionalAccountRequest : BaseRequest
{
    /// <summary>
    /// Id
    /// </summary>
    public int Id { get; set; }
    
    /// <summary>
    /// Đăng ký margin (giao dịch ký quỹ)
    /// </summary>
    public bool MarginAccount { get; set; }
    
    /// <summary>
    /// Hạn mức dư nợ
    /// </summary>
    public decimal CreditLimit { get; set; } 
    
    /// <summary>
    /// Thời gian nợ HĐ
    /// </summary>
    public int? DebtTerm { get; set; } = null;
    
    /// <summary>
    /// Tỷ lệ ký quỹ
    /// </summary>
    public double? DepositRate { get; set; } // Tỷ lệ ký quỹ
    
}