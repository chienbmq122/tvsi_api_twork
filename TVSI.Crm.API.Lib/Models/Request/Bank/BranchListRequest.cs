namespace TVSI.Crm.API.Lib.Models.Request.Bank;

public class BranchListRequest : BaseRequest
{
    public string? BankNo { get; set; }
}