using System.ComponentModel.DataAnnotations;

namespace TVSI.Crm.API.Lib.Models.Request.ManTvsi;

public class ManTvsiRequest : BaseRequest
{
    /// <summary>
    /// Mã Man code
    /// </summary>
    [Required]
    public string? ManCode { get; set; }
}