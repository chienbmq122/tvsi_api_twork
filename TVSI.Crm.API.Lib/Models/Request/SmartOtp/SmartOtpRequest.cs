﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVSI.Crm.API.Lib.Models.Request.SmartOtp
{
    public class CheckSmartOtpDeviceRequest : BaseRequest
    {
        public string? deviceId { get; set; }
    }
    public class SmartOtpRequest : BaseRequest
    {
        public string? deviceId { get; set; }
        public string otp { get; set; }
        public string otpTime { get; set; }
    }

    public class RegisterDeviceRequest : BaseRequest
    {
        public string? deviceId { get; set; }
        public string? smartPin { get; set; }
    }

    public class ActiveOtpRequest : BaseRequest
    {
        public string deviceId { get; set; }
        public string otp { get; set; }
    }

    public class VerifyPinRequest : BaseRequest
    {
        public string? smartPin { get; set; }
        public string? deviceId { get; set; }
    }
}
