using System.ComponentModel.DataAnnotations;

namespace TVSI.Crm.API.Lib.Models.Request.Shareholder;

public class CancelShareholderRequest : BaseRequest
{
    /// <summary>
    /// id cổ đông
    /// </summary>
    public int Id { get; set; }
    
    /// <summary>
    /// Mã KH
    /// </summary>
    [Required]
    public string? Custcode { get; set; }
    
}