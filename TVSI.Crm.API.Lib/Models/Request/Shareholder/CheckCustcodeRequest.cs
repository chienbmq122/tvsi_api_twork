using System.ComponentModel.DataAnnotations;

namespace TVSI.Crm.API.Lib.Models.Request.Shareholder;

public class CheckCustcodeRequest : BaseRequest
{
    /// <summary>
    /// Mã KH
    /// </summary>
    [Required]
    public string? Custcode { get; set; }
}