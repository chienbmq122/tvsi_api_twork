using System.ComponentModel.DataAnnotations;

namespace TVSI.Crm.API.Lib.Models.Request.Shareholder;

public class ShareholderDetailRequest : BaseRequest
{
    /// <summary>
    /// Mã KH
    /// </summary>
    [Required]
    public string? Custcode { get; set; }
    
    /// <summary>
    /// Id hồ sơ cổ đông
    /// </summary>
    [Required]
    public int Id { get; set; }
}