using System.ComponentModel.DataAnnotations;

namespace TVSI.Crm.API.Lib.Models.Request.Shareholder;

public class CreateShareholderRequest : BaseRequest
{
    /// <summary>
    /// Tên KH
    /// </summary>
    public string? FullName { get; set; }
    
    /// <summary>
    /// Mã kh
    /// </summary>
    [Required,MinLength(6),MaxLength(6)]
    public string? CustCode { get; set; }
    
    /// <summary>
    /// địa chỉ KH
    /// </summary>
    public string? Address { get; set; }
    
    /// <summary>
    /// Mã CK
    /// </summary>
    [Required]
    public string? StockCode { get; set; }


    /// <summary>
    /// Cổ đông lớn
    /// </summary>
    public bool ShareholderType01 { get; set; }
    
    /// <summary>
    /// cổ đông nội bộ
    /// </summary>
    public bool ShareholderType02 { get; set; }
    
    /// <summary>
    /// Người liên quan đến cổ đông lớn
    /// </summary>
    public bool ShareholderType03 { get; set; }
    
    /// <summary>
    /// Sắp thành cổ đông lớn
    /// </summary>
    public bool ShareholderType04 { get; set; }
    
}