using System.ComponentModel.DataAnnotations;

namespace TVSI.Crm.API.Lib.Models.Request.Shareholder;

public class ShareholderListRequest : BaseRequest
{
    public string? Custcode { get; set; }
    public string? StockCode { get; set; }
    public string? FullName { get; set; }
    public int Status { get; set; }
    public int ShareholderType { get; set; }
    
    /// <summary>
    /// Thứ tự trang (tính từ 1)
    /// </summary>
    [Required]
    public int PageIndex { get; set; }

    /// <summary>
    /// Số bản ghi cần lấy trên một trang
    /// </summary>
    [Required]
    public int PageSize { get; set; }
}