namespace TVSI.Crm.API.Lib.Models.Request.Shareholder;

public class UpdateShareholderRequest : BaseRequest
{
    /// <summary>
    /// Id
    /// </summary>
    public int Id { get; set; }
    /// <summary>
    /// cổ đông lớn
    /// </summary>
    public bool ShareholderType01 { get; set; }
    /// <summary>
    /// Cổ Đông nội bộ
    /// </summary>
    public bool ShareholderType02 { get; set; }
    /// <summary>
    ///  Người l/quan Cổ Đông lớn
    /// </summary>
    public bool ShareholderType03 { get; set; }
    /// <summary>
    /// Sắp thành Cổ Đông lớn
    /// </summary>
    public bool ShareholderType04 { get; set; }
    /// <summary>
    /// Mã CK
    /// </summary>
    public string? StockCode { get; set; }
}