﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVSI.Crm.API.Lib.Models.Request.Dashboard
{
    public class DashboardChartRequest : BaseRequest
    {
        [Description("Loại thống kê: 1 tuần này, 2 tháng này")]
        public int Status { get; set; }
        [Description("Ngày gửi format dd/mm/yyyy")]
        public string? DateIndex { get; set; }
    }
}
