﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVSI.Crm.API.Lib.Models.Request.Dashboard
{
    public class DashboardTradeRequest : BaseRequest
    {
        [Description("Ngày gửi format dd/mm/yyyy")]
        public string DateIndex { get; set; }
    }
}
