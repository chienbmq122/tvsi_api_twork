﻿namespace TVSI.Crm.API.Lib.Models.Request.Activity
{
    /// <summary>
    /// Request lấy danh sách hoạt động
    /// </summary>
    public class ActivityRequestByEventType : BaseRequest
    {
        /// <summary>
        /// Loại event: 0 = get all, 1 = sự kiện, 2 = công việc
        /// </summary>
        public int EventType { get; set; }

        /// <summary>
        /// Ngày
        /// </summary>
        public string? DateIndex { get; set; }

        public int PageIndex { get; set; }

        public int PageSize { get; set; }
    }
}
