﻿namespace TVSI.Crm.API.Lib.Models.Request.Activity
{
    /// <summary>
    /// Thông tin Request hoạt động tháng
    /// </summary>
    public class ActivityDataMonthRequest : BaseRequest
    {
        /// <summary>
        /// Ngày cần lấy
        /// </summary>
        public string? DateIndex { get; set; }
    }
}
