﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVSI.Crm.API.Lib.Models.Request.Activity
{
    /// <summary>
    /// Request lấy danh sách hoạt động với filtter
    /// </summary>
    public class FilterActivityRequest : BaseRequest
    {
        /// <summary>
        /// FROM DATE
        /// </summary>
        
        public string? FromDate { get; set; }
        
        /// <summary>
        /// TO DATE
        /// </summary>
        public string? ToDate { get; set; }
        
        /// <summary>
        /// Loại event: 0 = get all, 1 = sự kiện, 2 = công việc
        /// </summary>
        public int EventType { get; set; }
        
        /// <summary>
        /// người phụ trách
        /// </summary>
        public string? Assigned { get; set; }
        
        /// <summary>
        /// Mức độ ưu tiên
        /// </summary>
        public int Priority { get; set; }
        
        /// <summary>
        /// Status
        /// </summary>
        public int Status { get; set; }
        
        /// <summary>
        /// Trang
        /// </summary>
        public int PageIndex { get; set; }
        
        /// <summary>
        /// Số bản ghi 1 trang
        /// </summary>
        public int PageSize { get; set; }
    }
}
