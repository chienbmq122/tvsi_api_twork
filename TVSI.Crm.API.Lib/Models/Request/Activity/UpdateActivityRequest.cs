﻿namespace TVSI.Crm.API.Lib.Models.Request.Activity
{
    /// <summary>
    /// Thông tin Request cập nhật nhiều activity
    /// </summary>
    public class UpdateListActivityRequest : BaseRequest
    {
        /// <summary>
        /// Danh sách activityid
        /// </summary>
        public List<int>? Ids { get; set; }

        /// <summary>
        /// Status nếu update nhiều thì truyền trạng thái, nếu xóa nhiều thì truyền 99
        /// </summary>
        public int Status { get; set; }
    }

    public class ConfirmOrRejectRequest : BaseRequest
    {
        /// <summary>
        /// activityid
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Confirm = 1, reject = 2
        /// </summary>
        public int Status { get; set; }

        /// <summary>
        /// nếu reject truyền thêm lý do        
        /// </summary>
        public string? RejectReason { get; set; }
    }

    /// <summary>
    /// Thông tin Request tạo mới hoạt động
    /// </summary>
    public class UpdateActivityRequest : BaseRequest
    {
        
        /// <summary>
        /// ID hoạt động
        /// </summary>
        public int ActivityId { get; set; }
        
        /// <summary>
        /// Đối tác
        /// </summary>
        public int LeadId { get; set; }
        
        /// <summary>
        /// Code khách hàng
        /// </summary>
        public string? Custcode { get; set; }
        
        /// <summary>
        /// Tiêu đề hoạt động
        /// </summary>
        public string? Title { get; set; }
        
        /// <summary>
        /// Phân loại hoạt động
        /// </summary>
        public int Type { get; set; }
        
        /// <summary>
        /// Phân loại event
        /// </summary>
        public int EventType { get; set; }
        
        /// <summary>
        /// Ngày bắt đầu
        /// </summary>
        public string? StartDate { get; set; }
        public string? RemindTime { get; set; }

        /// <summary>
        /// Ngày kết thúc
        /// </summary>
        public string? EndDate { get; set; }
        
        /// <summary>
        /// Địa điểm
        /// </summary>
        public string? Location { get; set; }
        
        /// <summary>
        /// Mức độ ưu tiên hoạt động
        /// </summary>
        public int Priority { get; set; }
        
        /// <summary>
        /// Tình trạng
        /// </summary>
        public int Status { get; set; }

        /// <summary>
        /// Người duyệt Nếu là nhân viên thì mặc định = UserID của nhân viên và ko cho sửa / Trưởng phòng và GĐCN mặc định = UserID đăng nhập và cho phép sửa
        /// </summary>
        public string? Approved { get; set; }

        /// <summary>
        /// Assigned Nếu là nhân viên thì mặc định = UserID của nhân viên và ko cho sửa / Trưởng phòng và GĐCN mặc định = UserID đăng nhập và cho phép sửa
        /// </summary>
        public string? Assigned { get; set; }

        /// <summary>
        /// Mô tả
        /// </summary>
        public string? Detail { get; set; }

        /// <summary>
        /// Lý do
        /// </summary>
        public int ReasonType { get; set; }
    }
}
