﻿namespace TVSI.Crm.API.Lib.Models.Request.Activity
{
    /// <summary>
    /// Thông tin Request fillter hoạt động
    /// </summary>
    public class FillterActivityRequest : BaseRequest
    {
        /// <summary>
        /// FROM DATE
        /// </summary>
        public string? FromDate { get; set; }
        
        /// <summary>
        /// TO DATE
        /// </summary>
        public string? ToDate { get; set; }
        
        /// <summary>
        /// Loại event: 0 = get all, 1 = sự kiện, 2 = công việc
        /// </summary>
        public int EventType { get; set; }
        
        /// <summary>
        /// người phụ trách
        /// </summary>
        public string? Assigned { get; set; }
        
        /// <summary>
        /// Mức độ ưu tiên
        /// </summary>
        public int Priority { get; set; }
        
        /// <summary>
        /// Status
        /// </summary>
        public int Status { get; set; }
        
        /// <summary>
        /// Trang
        /// </summary>
        public int PageIndex { get; set; }
        
        /// <summary>
        /// Số bản ghi 1 trang
        /// </summary>
        public int PageSize { get; set; }
    }
}
