﻿namespace TVSI.Crm.API.Lib.Models.Request.Activity
{
    /// <summary>
    /// Thông tin Request xóa activity
    /// </summary>
    public class DeleteActivityRequest : BaseRequest
    {
        public int ActivityId { get; set; }
    }
}
