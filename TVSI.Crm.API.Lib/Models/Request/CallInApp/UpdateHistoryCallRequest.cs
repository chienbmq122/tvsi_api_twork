﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVSI.Crm.API.Lib.Models.Request.CallInApp
{
    public class UpdateHistoryCallRequest : BaseRequest
    {
        public string? CallId { get; set; }
        public string? Note { get; set; }
        public int Status { get; set; }
        public string? Comment { get; set; }
        public int Rate { get; set; }
        public string? CallAppTypeList { get; set; }
        public string? TimeCall { get; set; }
    }
}
