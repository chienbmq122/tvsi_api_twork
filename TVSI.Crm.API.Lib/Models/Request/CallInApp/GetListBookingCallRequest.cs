﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVSI.Crm.API.Lib.Models.Request.CallInApp
{
    public class GetListBookingCallRequest : BaseRequest
    {
        /// <summary>
        /// Mã khách hàng
        /// </summary>
        public string? CustCode { get; set; }

        /// <summary>
        /// SaleID
        /// </summary>
        public string? SaleId { get; set; }

        /// <summary>
        /// Loại khách hàng (-1: Tất cả, 1: Khách hàng, 2: KH tiềm năng, 3: KH đăng ký mở TK)
        /// </summary>
        public int CustType { get; set; }

        /// <summary>
        /// Từ ngày
        /// </summary>
        public string? FromDate { get; set; }

        /// <summary>
        /// Đến ngày
        /// </summary>
        public string? ToDate { get; set; }

        public int PageIndex { get; set; }
        public int PageSize { get; set; }

        public int StateStatus { get; set; }
    }
}
