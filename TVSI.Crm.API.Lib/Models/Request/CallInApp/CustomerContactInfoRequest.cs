﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVSI.Crm.API.Lib.Models.Request.CallInApp
{
    public class CustomerContactInfoRequest : BaseRequest
    {
		public string? Custcode { get; set; }
		public string? Name { get; set; }
		public string? Phone { get; set; }
		public int PageIndex { get; set; }
		public int PageSize { get; set; }
    }
}
