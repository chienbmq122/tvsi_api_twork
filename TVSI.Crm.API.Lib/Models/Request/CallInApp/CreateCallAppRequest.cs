﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVSI.Crm.API.Lib.Models.Request.CallInApp
{
    public class CreateCallAppRequest : BaseRequest
    {
        public string? CallId { get; set; }
        public string? CustCode { get; set; }
        public int LeadId { get; set; }
        public int OpenAccId { get; set; }
        public string? CustName { get; set; }
        public int Direction { get; set; }
        public string? Mobile { get; set; }
    }
}
