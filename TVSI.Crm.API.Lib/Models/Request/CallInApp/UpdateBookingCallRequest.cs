﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVSI.Crm.API.Lib.Models.Request.CallInApp
{
    public class UpdateBookingCallRequest : BaseRequest
    {
		public int BookApptCallId { get; set; }
		public string? CallAppTypeList { get; set; }
		public string? StartTime { get; set; }
		public string? EndTime { get; set; }
		public string? RemindTime { get; set; }
		public string? Description { get; set; }
		public int BookCallType { get; set; }
		public string? AssignUser { get; set; }
		public int Status { get; set; }
	}
}
