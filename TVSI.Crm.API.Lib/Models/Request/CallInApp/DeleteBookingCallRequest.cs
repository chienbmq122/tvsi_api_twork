﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVSI.Crm.API.Lib.Models.Request.CallInApp
{
    public class DeleteBookingCallRequest : BaseRequest
    {
        public int BookApptCallId { get; set; }
    }
}
