namespace TVSI.Crm.API.Lib.Models.Request.Common;

public class GetWardListRequest : BaseRequest
{
    public  string? DistrictCode { get; set; }
}