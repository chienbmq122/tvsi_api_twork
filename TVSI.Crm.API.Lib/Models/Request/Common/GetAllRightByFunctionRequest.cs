namespace TVSI.Crm.API.Lib.Models.Request.Common;

public class GetAllRightByFunctionRequest : BaseRequest
{
    public string? Function { get; set; }
}