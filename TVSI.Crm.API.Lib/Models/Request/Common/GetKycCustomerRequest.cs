namespace TVSI.Crm.API.Lib.Models.Request.Common;

public class GetKycCustomerRequest : BaseRequest
{
    /// <summary>
    /// Mã kh
    /// </summary>
    public string? CustCode { get; set; }
}