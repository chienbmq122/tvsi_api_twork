namespace TVSI.Crm.API.Lib.Models.Request.Common;

public class GetDistrictListRequest : BaseRequest
{
    public string? ProvinceCode { get; set; }
}