﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVSI.Crm.API.Lib.Models.Request.Comment
{
    public class CreateReplyRequest : BaseRequest
    {

        [Description("id comment")]
        public int CommentID { get; set; }

        [Description("Nội dung")]
        public string Reply { get; set; }

        [Description("full name người rep giống bên comment")]
        public string FullName { get; set; }

    }
}
