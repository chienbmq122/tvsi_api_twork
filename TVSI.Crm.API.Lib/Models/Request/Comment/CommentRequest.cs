﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVSI.Crm.API.Lib.Models.Request.Comment
{
    public class CommentModelRequest : BaseRequest
    {
        [Description("customer code nếu là Lead thì truyền string ''(rỗng) ")]
        public string CustCode { get; set; }
        [Description("LeadId nếu KH là Customer thì truyền ID > 0 nếu là customer truyền -1")]
        public int LeadID { get; set; }
    }

    public class CreateCommentRequest : BaseRequest
    {
        [Description("customer code nếu là customer thì truyền string ''(rỗng) ")]
        public string CustCode { get; set; }
        [Description("LeadId nếu KH là lead thì truyền ID > 0 nếu là customer truyền -1")]
        public int LeadID { get; set; }

        [Description("Nội dung")]
        public string Content { get; set; }

        [Description("Full tên của user comment (chỗ này lấy full tên user đang comment ra truyền vào)")]
        public string FullName { get; set; }

        [Description("Số điểm đánh giá max 5 min 1")]
        public int Rate { get; set; }
    }

    public class GetRateRequest : BaseRequest
    {
        public string CustCode { get; set; }
        public int LeadID { get; set; }
    } 
}
