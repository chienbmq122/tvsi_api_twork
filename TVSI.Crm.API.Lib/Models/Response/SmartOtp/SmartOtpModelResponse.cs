﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVSI.Crm.API.Lib.Models.Response.SmartOtp
{
    public class SmartOtpModelResponse
    {
        public string? SecretKey { get; set; }
    }

    public class SmartOtpModel
    {
        public int UserDeviceId { get; set; }
        public string? UserDomain { get; set; }
        public string? UserLogin { get; set; }
        public string? DeviceId { get; set; }
        public string? SecretKey { get; set; }
        public int Status { get; set; }
    }
}
