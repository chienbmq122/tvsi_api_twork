namespace TVSI.Crm.API.Lib.Models.Response.Bank;

public class BranchListResponse
{
   public string? SubBranchNo { get; set; }
   public string? ShortBranchName { get; set; }
}