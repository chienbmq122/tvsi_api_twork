namespace TVSI.Crm.API.Lib.Models.Response.Bank;

public class BankListResponse
{
    
    /// <summary>
    /// Mã ngân hàng
    /// </summary>
    public string? BankNo { get; set; }
    
    /// <summary>
    /// Tên ngân hàng
    /// </summary>
    public string? ShortName { get; set; }
}