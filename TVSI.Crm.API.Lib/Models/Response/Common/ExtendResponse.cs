namespace TVSI.Crm.API.Lib.Models.Response.Common;

public class ExtendResponse
{
    
    public string? ExtendName { get; set; }
    private List<ExtendKeyValueResponse>? _extendValue { get; set; } = new List<ExtendKeyValueResponse>();
    
    public int CategoryType { get; set; }
    
    public bool CrmSale { get; set; }
    public bool CrmBb { get; set; }

    public List<ExtendKeyValueResponse> ExtendValue
    {
        get => _extendValue;
        set => _extendValue = value;
    }
    
}

public class ExtendKeyValueResponse
{
    public string? Name { get; set; }
    public int Value { get; set; }
}

public class ExtendCategoryResponse
{
    public int CategoryId { get; set; }
    public int CategoryType { get; set; }
    public string? CategoryName { get; set; }
    
    public bool CrmSale { get; set; }
    public bool CrmBb { get; set; }
}
