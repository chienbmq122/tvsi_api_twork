namespace TVSI.Crm.API.Lib.Models.Response.Common;

public class KycInfoResponse
{
    /// <summary>
    ///  Tên thuộc tính
    /// </summary>
    public string? KycName { get; set; }
    
    /// <summary>
    /// Kiểu thuộc tính
    /// </summary>
    public int? KycType { get; set; }
    
    
    /// <summary>
    /// Key và name
    /// </summary>
    public List<KycNameValue>? KycValue { get; set; } = new List<KycNameValue>();
    
    public int? TypeSeleted { get; set; }
}


public class KycNameValue
{
    public int Value { get; set; }
    public string? Name { get; set; }
}
public class CustomerKycResponse
{
    /// <summary>
    /// Id
    /// </summary>
    public int KycId { get; set; }
    /// <summary>
    /// Tên dữ liệu
    /// </summary>
    public string? KycName { get; set; }
    /// <summary>
    /// Kiểu dữ liệu
    /// </summary>
    public int? KycType { get; set; }
    public string? KycTypeName { get; set; }
    
    public int? TypeSeleted { get; set; }
    
}





