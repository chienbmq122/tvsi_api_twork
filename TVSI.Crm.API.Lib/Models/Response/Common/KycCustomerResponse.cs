namespace TVSI.Crm.API.Lib.Models.Response.Common;


public class KycCustomerInfomationResponse
{
    public List<KycSelected> KycSelecteds { get; set; } = new List<KycSelected>();
    public KycJoined KycJoined { get; set; } = new KycJoined();
}

public class KycSelected
{
    /// <summary>
    ///  Tên thuộc tính
    /// </summary>
    public string? KycName { get; set; }
    
    /// <summary>
    /// Kiểu thuộc tính
    /// </summary>
    public int? KycType { get; set; }
    
    
    /// <summary>
    /// Key và name
    /// </summary>
    public List<KycNameValue>? KycValue { get; set; } = new List<KycNameValue>();
    
    /// <summary>
    /// Kieu lua chon (1 la dropdowlist , 2 la kieu tich chon)
    /// </summary>
    public int TypeSeleted { get; set; }
    
}

public class KycJoined
{
    /// <summary>
    /// mục tiêu tài chính ngắn hạn
    /// </summary>
    public string? FinanceGoals { get; set; }
    
    /// <summary>
    /// Đặc điểm cần lưu ý
    /// </summary>
    /// <returns></returns>
    public string? OtherNote { get; set; }
    
    /// <summary>
    /// sản phẩm sẽ gới thiệu
    /// </summary>
    public string? ProductIntro { get; set; }
}




public class KycInfo
{
    /// <summary>
    /// Id
    /// </summary>
    public int Id { get; set; }
    /// <summary>
    /// Value
    /// </summary>
    public int Value { get; set; }
    
    /// <summary>
    /// tên
    /// </summary>
    public string? Name { get; set; }
    
    /// <summary>
    /// 
    /// </summary>
    public int KycType { get; set; }
}


public class KycTypeResponse
{
    /// <summary>
    /// 
    /// </summary>
    public int Id { get; set; }
    
    /// <summary>
    /// Kyc Name
    /// </summary>
    public string? KycTypeName { get; set; }
    
    
    /// <summary>
    /// Kyc Type
    /// </summary>
    public int? KycType { get; set; }
    
    
    /// <summary>
    /// Kieu lua chon (1 la dropdowlist , 2 la kieu tich chon)
    /// </summary>
    public int TypeSeleted { get; set; }
}




