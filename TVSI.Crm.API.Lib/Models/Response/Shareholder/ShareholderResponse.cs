namespace TVSI.Crm.API.Lib.Models.Response.Shareholder;

public class ShareholderResponse
{
    public int Id { get; set; }
    public string? CreatedDate { get; set; }
    public string? FullName { get; set; }
    public string? CustCode { get; set; }
    public string? ShareholderType { get; set; }
    public string? StatusColor { get; set; }
    public string? Status { get; set; }
}