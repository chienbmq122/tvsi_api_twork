﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVSI.Crm.API.Lib.Models.Response.SaleInfo
{
    public class SaleInfoReponse
    {
        public string? SaleID { get; set; }
        public string? BranchCode { get; set; }
        public string? UserName { get; set; }
        public string? FullName { get; set; }
        public string? Email { get; set; }
    }
}
