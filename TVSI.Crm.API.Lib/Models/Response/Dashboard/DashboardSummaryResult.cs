﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVSI.Crm.API.Lib.Models.Response.Dashboard
{
    public class DashboardSummaryResult
    {
        [Description("Tổng số cơ hội")]
        public int TotalOpportunity { get; set; }
        [Description("Tổng số KH tiềm năng")]
        public int TotalLead { get; set; }
        [Description("Tổng số Account")]
        public int TotalAccount { get; set; }
        [Description("Tổng số hoạt động")]
        public int TotalActivity { get; set; }
    }
}
