﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVSI.Crm.API.Lib.Models.Response.Dashboard
{
    public class TotalCustLeadResponse
    {
        public int TotalCust { get; set; }
        public int ActiviteCust { get; set; }
        public int CloseCust { get; set; }
        public int OpenCust { get; set; }
        public int TotalLead { get; set; }
        public int OpenLead { get; set; }
        public int OpenAccount { get; set; }
    }
}
