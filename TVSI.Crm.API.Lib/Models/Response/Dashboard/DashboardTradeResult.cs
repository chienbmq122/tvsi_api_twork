﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVSI.Crm.API.Lib.Models.Response.Dashboard
{
    public class DashboardTradeResult
    {
        [Description("% giao dịch trong ngày")]
        public double TodayPercent { get; set; }
        [Description("Giá trị giao dịch trong ngày")]
        public double TodayAmount { get; set; }
        [Description("Khối lượng giao dịch trong ngày")]
        public int TodayVolume { get; set; }
        [Description("Phí MG trong ngày")]
        public double TodayFee { get; set; }
        [Description("% giao dịch trong tháng")]
        public double MonthPercent { get; set; }
        [Description("Giá trị giao dịch trong tháng")]
        public double MonthAmount { get; set; }
        [Description("Khối lượng giao dịch trong tháng")]
        public int MonthVolume { get; set; }
        [Description("Phí MG trong tháng")]
        public double MonthFee { get; set; }
        [Description("Số lượng TK mở trong tháng")]
        public int OpenAccount { get; set; }
    }

    public class DashboardTradeResultSql
    {
        public double TotalAmountLastDay { get; set; }

        public double TotalAmountMonthBefore { get; set; }

        public string TotalLastMonth { get; set; }

        public int CountAccount { get; set; }
    }

    public class DashboardDailyTradeInfoData
    {
        [Description("Giá trị giao dịch trong tháng")]
        public double MonthAmount { get; set; }
        [Description("Khối lượng giao dịch trong tháng")]
        public int MonthVolume { get; set; }
    }
}
