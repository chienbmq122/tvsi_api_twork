﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVSI.Crm.API.Lib.Models.Response.Dashboard
{
    public class DashboardChartDetail
    {
        [Description("Ngày")]
        public DateTime Transaction_Date { get; set; }
        [Description("Ngày format")]
        public string Transaction_Date_Str
        {
            get
            {
                return Transaction_Date.ToString("dd/MM/yyyy");
            }
        }

        [Description("Giá trị giao dịch")]
        public double Transaction_Value { get; set; }

        [Description("Phí giao dịch")]
        public double Transaction_Fee { get; set; }
    }

    public class DashboardChartResult
    {
        [Description("Phí trong ngày")]
        public double TodayFee { get; set; }

        [Description("Thay đổi trong tuần")]
        public double PercentOfWeek { get; set; }

        [Description("Giá trị ngày T")]
        public IEnumerable<DashboardChartDetail>? Transaction_Data { get; set; }
    }

    public class DashboardSql
    {
        [Description("Phí trong ngày")]
        public double TodayFee { get; set; }

        [Description("Tổng fee tháng trước")]
        public double SumFeeLastWeek { get; set; }
        public string SumTradeInfo { get; set; }
    }

    public class DashboardMonthlyTradeInfoData
    {
        [Description("Giá trị giao dịch trong tháng")]
        public double MonthAmount { get; set; }
        [Description("Khối lượng giao dịch trong tháng")]
        public int MonthVolume { get; set; }
    }
}
