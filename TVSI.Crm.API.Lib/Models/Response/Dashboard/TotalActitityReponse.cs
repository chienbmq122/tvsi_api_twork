﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVSI.Crm.API.Lib.Models.Response.Dashboard
{
    public class TotalActitityReponse
    {
        public int TotalActivity { get; set; }

        public int TotalDone { get; set; }

        public int TotalPending { get; set; }

        public int TotalOutOfDate { get; set; }
    }
}
