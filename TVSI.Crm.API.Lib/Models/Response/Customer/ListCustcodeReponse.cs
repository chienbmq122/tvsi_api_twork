﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVSI.Crm.API.Lib.Models.Response.Customer
{
    public class ListCustcodeReponse
    {
        public int TotalItem { get; set; }
        public List<ListCustcode>? Items { get; set; }
    }

    public class ListCustcode
    {
        public string? CustCode { get; set; }
    }

    public class ListCustcodeSql
    {
        public int TotalItem { get; set; }
        public string Items { get; set; }
    }
}
