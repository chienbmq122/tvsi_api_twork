using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Routing.Constraints;
using TVSI.Crm.API.Common;

namespace TVSI.Crm.API.Lib.Models.Response.Customer;

/*public class CustomerDetailInfoResponse
{
    public CustomerInfoResponse? CustomerInfoResponse { get; set; }
    public RegisterServiceResponse? RegisterServiceResponse { get; set; }
    public ExtendInfoResponse? ExtendInfoResponse { get; set; }
}*/

public class CustomerInfoResponse
{
    [Description("Họ và tên")] public string? FullName { get; set; }

    [Description("Ngày sinh")] public string? BirthDay { get; set; }

    [Description("giới tính")] public string? Gender { get; set; }

    [Description("Số CMT/CCCD")] public string? CardId { get; set; }

    [Description("Ngày cấp CMT/CCCD")] public string? IssueDate { get; set; }

    [Description("Nơi cấp CMT/CCCD")] public string? IssuePlace { get; set; }

    [Description("Địa chỉ")] public string? Address { get; set; }

    [Description("Quốc tịch")] public string? Nationality { get; set; }

    [Description("yêu cầu")] public string? Request { get; set; }

    [Description("Sale ID")] public string? SaleID { get; set; }

    [Description("Họ tên sale")] public string? SaleName { get; set; }

    [Description("Tên Nhánh")] public string? BranchName { get; set; }

    [Description("Branch Id")] public string? BranchId { get; set; }

    [Description("ngày tạo")] public string? CreatedDate { get; set; }

    [Description("Kênh mở tài khoản")] public int OpenSource { get; set; }

    [Description("Tên Kênh mở tài khoản")] public string? OpenSourceName { get; set; }

    [Description("Tên trạng thái eKYC")] public string? EkycName { get; set; }

    [Description("Màu trạng thái eKYC")] public string? EkycColor { get; set; }

    [Description("trạng thái hồ sơ")] public string? ProfileStatus { get; set; }

    [Description("Màu trạng thái hồ sơ")] public string? ProfileStatusColor { get; set; }

    [Description("trạng thái tương tác")] public string? ActionStatus { get; set; }

    [Description("Màu trạng thái tương tác")]
    public string? ActionStatusColor { get; set; }

    [Description("Sale xử lý")] public string? HandleBy { get; set; }

    [Description("Nội dung xử lý")] public string? Note { get; set; }

    [Description("Người cập nhật cuối cùng")]
    public string? UpdateLast { get; set; }

    [Description("Ngày cập nhật cuối cùng")]
    public string? DateLast { get; set; }

    [Description("Chu ky xac thuc")] public string? AuthSignature { get; set; }

    [Description("Màu Chu ky xac thuc")] public string? AuthSignatureColor { get; set; }

    [Description("ẢNH CMT/CCCD MẶT TRƯỚC")]
    public string? ImageFront { get; set; }

    [Description("ẢNH CMT/CCCD MẶT SAU")] public string? ImageBack { get; set; }

    [Description("ẢNH xác thực")] public string? ImageFace { get; set; }

    [Description("Đại diện TVSI")] public string? ManTvsi { get; set; }

    [Description("Chức vụ")] public string? ManPosition { get; set; }

    [Description("giấy ủy quyền")] public string? ManAnPower { get; set; }

    [Description("Ngày cấp giấy ủy quyền")]
    public string? ManDateAnPower { get; set; }

    [Description("Số CMND ủy quyền")] public string? ManCardId { get; set; }

    [Description("Ngày cấp CMT/CCCD")] public string? ManIssueDate { get; set; }

    [Description("Nơi cấp CMT/CCCD ủy quyền")]
    public string? ManIssuePlace { get; set; }
}

/*public class RegisterServiceResponse
{
    [Description("Đăng ký margin")] public bool AccountMargin { get; set; }

    [Description("Phương thức nhận KQGD")] public int MethodReceive { get; set; }

    [Description("Số điện thoại 01 ")] public string? MethodReceivePhone01 { get; set; }

    [Description("Số điện thoại 02 ")] public string? MethodReceivePhone02 { get; set; }

    [Description("Email")] public string? Email { get; set; }

    public List<BankInfo>? BankInfos { get; set; }
}*/
/*
public class BankInfo
{
    [Description("Tên ngân hàng")] public string? BankName_01 { get; set; }
    [Description("Tên nhánh ngân hàng")] public string? BankBranchName_01 { get; set; }

    [Description("Số tài khoản ngân hàng")]
    public string? BankNo_01 { get; set; }

    [Description("Tên chủ tk ngân hàng")] public string? BankAccountName_01 { get; set; }

    [Description("Tên ngân hàng")] public string? BankName_02 { get; set; }
    [Description("Tên nhánh ngân hàng")] public string? BankBranchName_02 { get; set; }

    [Description("Số tài khoản ngân hàng")]
    public string? BankNo_02 { get; set; }

    [Description("Tên chủ tk ngân hàng")] public string? BankAccountName_02 { get; set; }

    [Description("Tên ngân hàng")] public string? BankName_03 { get; set; }
    [Description("Tên nhánh ngân hàng")] public string? BankBranchName_03 { get; set; }

    [Description("Số tài khoản ngân hàng")]
    public string? BankNo_03 { get; set; }

    [Description("Tên chủ tk ngân hàng")] public string? BankAccountName_03 { get; set; }

    [Description("Tên ngân hàng")] public string? BankName_04 { get; set; }
    [Description("Tên nhánh ngân hàng")] public string? BankBranchName_04 { get; set; }

    [Description("Số tài khoản ngân hàng")]
    public string? BankNo_04 { get; set; }

    [Description("Tên chủ tk ngân hàng")] public string? BankAccountName_04 { get; set; }
}
*/

public class ExtendInfoResponse
{
    public bool InfoUsa { get; set; }
    public bool Info01 { get; set; }
    public bool Info02 { get; set; }
    public bool Info03 { get; set; }
    public bool Info04 { get; set; }
    public bool Info05 { get; set; }
    public bool Info06 { get; set; }
    public bool Info07 { get; set; }
}

public class CustomerInfoAllResponse
{
    [Description("Mã khác hàng")] public string? Custcode { get; set; }

    [Description("Họ và tên")] public string? FullName { get; set; }

    [Description("Ngày sinh")] public string? BirthDay { get; set; }

    [Description("giới tính")] public string? Gender { get; set; }

    [Description("Số CMT/CCCD")] public string? CardId { get; set; }

    [Description("Ngày cấp CMT/CCCD")] public string? IssueDate { get; set; }

    [Description("Nơi cấp CMT/CCCD")] public string? IssuePlace { get; set; }

    [Description("Địa chỉ")] public string? Address { get; set; }

    [Description("Quốc tịch")] public string? Nationality { get; set; }

    [Description("yêu cầu")] public string? Request { get; set; }

    [Description("Sale ID")] public string? SaleID { get; set; }

    [Description("Họ tên sale")] public string? SaleName { get; set; }

    [Description("Tên Nhánh")] public string? BranchName { get; set; }

    [Description("Branch Id")] public string? BranchId { get; set; }

    [Description("ngày tạo")] public string? CreatedDate { get; set; }

    [Description("Kênh mở tài khoản")] public int OpenSource { get; set; }

    [Description("Tên Kênh mở tài khoản")] public string? OpenSourceName { get; set; }

    [Description("Tên trạng thái eKYC")] public string? EkycName { get; set; }

    [Description("Màu trạng thái eKYC")] public string? EkycColor { get; set; }

    [Description("trạng thái eKYC")] public int EkycStatus { get; set; }

    [Description("ConfirmCode")] public string? ConfirmCode { get; set; }


    [Description("Trạng thái hợp đồng")] public string? ContractStatus { get; set; }

    [Description("Màu trạng thái hợp đồng")]
    public string? ContractStatusColor { get; set; }

    [Description("trạng thái hồ sơ")] public string? ProfileStatus { get; set; }

    [Description("Màu trạng thái hồ sơ")] public string? ProfileStatusColor { get; set; }

    [Description("trạng thái tương tác Name")]
    public string? ActionStatusName { get; set; }

    [Description("trạng thái tương tác")] public string? ActionStatus { get; set; }

    [Description("Màu trạng thái tương tác")]
    public string? ActionStatusColor { get; set; }

    [Description("trạng thái tài khoản ")] public string? AccountStatus { get; set; }

    [Description("Màu trạng thái tài khoản")]
    public string? AccountStatusColor { get; set; }

    [Description("Sale xử lý")] public string? HandleBy { get; set; }

    [Description("Nội dung xử lý")] public string? Note { get; set; }

    [Description("Người cập nhật cuối cùng")]
    public string? UpdateLast { get; set; }

    [Description("Ngày cập nhật cuối cùng")]
    public string? DateLast { get; set; }

    [Description("Chu ky xac thuc")] public string? AuthSignature { get; set; }

    [Description("Màu Chu ky xac thuc")] public string? AuthSignatureColor { get; set; }

    [Description("ẢNH CMT/CCCD MẶT TRƯỚC")] public string? ImageFront { get; set; }

    [Description("ẢNH CMT/CCCD MẶT SAU")] public string? ImageBack { get; set; }

    [Description("ẢNH xác thực")] public string? ImageFace { get; set; }

    [Description("Đại diện TVSI")] public string? ManTvsi { get; set; }

    [Description("Chức vụ")] public string? ManPosition { get; set; }

    [Description("giấy ủy quyền")] public string? ManAnPower { get; set; }

    [Description("Ngày cấp giấy ủy quyền")]
    public string? ManDateAnPower { get; set; }

    [Description("Số CMND ủy quyền")] public string? ManCardId { get; set; }

    [Description("Ngày cấp CMT/CCCD")] public string? ManIssueDate { get; set; }

    [Description("Nơi cấp CMT/CCCD ủy quyền")]
    public string? ManIssuePlace { get; set; }

    [Description("Đăng ký margin")] public bool AccountMargin { get; set; }

    [Description("Phương thức nhận KQGD")] public string? TradingResultMethod { get; set; } 

    [Description("Số điện thoại 01 ")] public string? Phone01 { get; set; }

    [Description("Số điện thoại 02 ")] public string? Phone02 { get; set; }

    [Description("Email")] public string? Email { get; set; }


    [Description("Thông tin ngân hàng")] public bool BankInfo { get; set; }


    [Description("trạng thái NH có đăng ký hay ko")]
    public bool StatusBank01 { get; set; }
    [Description("Tên trạng thái NH")] public string? StatusNameBank01 { get; set; }
    [Description("Màu trạng thái NH")] public string? StatusColorBank01 { get; set; }
    [Description("Tên ngân hàng")] public string? BankName01 { get; set; }
    [Description("Tên nhánh ngân hàng")] public string? BankBranchName01 { get; set; }
    [Description("Số tài khoản ngân hàng")] public string? BankNo01 { get; set; }
    [Description("Tên chủ tk ngân hàng")] public string? BankAccountName01 { get; set; }
    [Description("Chi nhánh NH Code")] public string? BranchCode01 { get; set; }
    [Description("Ngân hàng Code")] public string? BankCode01 { get; set; }
    
    [Description("trạng thái NH có đăng ký hay ko")] public bool StatusBank02 { get; set; }
    [Description("Tên trạng thái NH")] public string? StatusNameBank02 { get; set; }
    [Description("Màu trạng thái NH")] public string? StatusColorBank02 { get; set; }
    [Description("Tên ngân hàng")] public string? BankName02 { get; set; }
    [Description("Tên nhánh ngân hàng")] public string? BankBranchName02 { get; set; }
    [Description("Số tài khoản ngân hàng")] public string? BankNo02 { get; set; }
    [Description("Tên chủ tk ngân hàng")] public string? BankAccountName02 { get; set; }
    [Description("Chi nhánh NH Code")] public string? BranchCode02 { get; set; }
    [Description("Ngân hàng Code")] public string? BankCode02 { get; set; }
    


    [Description("trạng thái NH có đăng ký hay ko")]
    public bool StatusBank03 { get; set; }

    [Description("Tên trạng thái NH")] public string? StatusNameBank03 { get; set; }
    [Description("Màu trạng thái NH")] public string? StatusColorBank03 { get; set; }
    [Description("Tên ngân hàng")] public string? BankName03 { get; set; }
    [Description("Tên nhánh ngân hàng")] public string? BankBranchName03 { get; set; }

    [Description("Số tài khoản ngân hàng")]
    public string? BankNo03 { get; set; }

    [Description("Tên chủ tk ngân hàng")] public string? BankAccountName03 { get; set; }
    [Description("Chi nhánh NH Code")] public string? BranchCode03 { get; set; }
    [Description("Ngân hàng Code")] public string? BankCode03 { get; set; }


    [Description("trạng thái NH có đăng ký hay ko")]
    public bool StatusBank04 { get; set; }

    [Description("Tên trạng thái NH")] public string? StatusNameBank04 { get; set; }
    [Description("Màu trạng thái NH")] public string? StatusColorBank04 { get; set; }
    [Description("Tên ngân hàng")] public string? BankName04 { get; set; }
    [Description("Tên nhánh ngân hàng")] public string? BankBranchName04 { get; set; }

    [Description("Số tài khoản ngân hàng")]
    public string? BankNo04 { get; set; }

    [Description("Tên chủ tk ngân hàng")] public string? BankAccountName04 { get; set; }
    [Description("Chi nhánh NH Code")] public string? BranchCode04 { get; set; }
    [Description("Ngân hàng Code")] public string? BankCode04 { get; set; }

    public bool InfoUsa { get; set; }
    public bool Info01 { get; set; }
    public bool Info02 { get; set; }
    public bool Info03 { get; set; }
    public bool Info04 { get; set; }
    public bool Info05 { get; set; }
    public bool Info06 { get; set; }
    public bool Info07 { get; set; }

    [Description("1: có nút Kích hoạt tài khoản và cập nhật tt, 2: mở tk và tương tác, 3 tương tác")]
    public int CustomerInterfaceType { get; set; }
    
}

public class ImageDataByte
{
    public int Id { get; set; }
    public string? NameFront { get; set; }
    public string? NameBack { get; set; }
    public byte[] FileDataFront { get; set; }
    public byte[] FileDataBack { get; set; }
    
}

public class ImageFileData
{
    public long FileID { get; set; }
    public string CustCode { get; set; }
    public int Type { get; set; }
    public string FileName { get; set; }
    public byte[] FileData { get; set; }
}

public class GetImageCardId
{
    public string RetCode { get; set; }
    public DataImage RetData { get; set; } = new DataImage();
}
public class DataImage
{
    public string Front_Image { get; set; }
    public string Back_Image { get; set; }
    public string Face_Image { get; set; }
    public string Signature_Image_0 { get; set; }
}