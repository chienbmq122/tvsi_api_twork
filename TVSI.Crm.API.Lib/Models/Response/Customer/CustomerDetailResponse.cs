using TVSI.Crm.API.Lib.Models.Response.Account;

namespace TVSI.Crm.API.Lib.Models.Response.Customer;

public class CustomerDetailResponse
{
    
    public string? CustCode { get; set; }
    public string? FullName { get; set; }
    public string?  Birthday { get; set; }
    public string? Gender { get; set; }
    public string? CardId { get; set; }
    public string? IssueDate { get; set; }
    public string? IssuePlace { get; set; }
    public string? Phone01 { get; set; }
    public string? Phone01Text { get; set; }
    public string? Phone02 { get; set; }
    public string? Email { get; set; }
    public string? Address { get; set; }
    public string? OpenAccDate { get; set; }
    public string? SaleID { get; set; }
    public string? SaleName { get; set; }
    public string? BranchName { get; set; }
    public bool AccountMargin { get; set; }
    public string? CustomerType { get; set; }
    public string? CustomerTypeGroup { get; set; }
    public bool TradingResultSmsMethod { get; set; }
    public bool TradingResultEmailMethod { get; set; }
    public string? ImageFront { get; set; }
    public string? ImageBack { get; set; }
    public string? ImageFace { get; set; }
    
    public IEnumerable<CustomerBankInfo>? CustomerBankInfo { get; set; } = new List<CustomerBankInfo>();
    public IEnumerable<InternalAccountInfo>? CustomerInternalInfo { get; set; } = new List<InternalAccountInfo>();
    public List<CustomerExtend> CustomerExtend { get; set; } = new List<CustomerExtend>(); // thông tin mở rộng
    
    /// <summary>
    /// Thông tin cổ đông
    /// </summary>
    public bool IsShareHolder { get; set; }

    public List<ShareHolderInfoResponsive>? ShareHolderInfo { get; set; } = new List<ShareHolderInfoResponsive>();
    
    
    public bool InfoUsa { get; set; }
    public bool Info01 { get; set; }
    public bool Info02 { get; set; }
    public bool Info03 { get; set; }
    public bool Info04 { get; set; }
    public bool Info05 { get; set; }
    public bool Info06 { get; set; }
    public bool Info07 { get; set; }
}

public class CustomerBankInfo
{
    public string? BankName { get; set; }
    public string? BankAccountName { get; set; }
    public string? BankAccount { get; set; }
    public string? BankBranchName { get; set; }
    public string? StatusName { get; set; }
    public string? StatusColor { get; set; }
}

public class InternalAccountInfo
{
    public string? InternalAccountNo { get; set; }
    public string? InternalAccountName { get; set; }
    public string? StatusName { get; set; }
    public string? StatusColor { get; set; }
    public string? StatusCode { get; set; }
}

public class CustomerExtend
{
    
    public int CategoryType { get; set; }
    public int Value { get; set; }
      /*/// <summary>
        /// Nghề nghiệp
        /// </summary>
        public int JobId { get; set; }
        
        /// <summary>
        /// Tên nghề nghiệp
        /// </summary>
        public string? JobName { get; set; }
        
        /// <summary>
        /// Nơi làm việc
        /// </summary>
        public int WorkPlaceId { get; set; }
        
        /// <summary>
        /// Tên Nơi làm việc
        /// </summary>
        public string? WorkPlaceName { get; set; }
        
        
        /// <summary>
        /// Khả năng tài chính
        /// </summary>
        public int FinancialCapabilityId { get; set; }  
        
        /// <summary>
        /// tên khả năng tài chính
        /// </summary>
        public string? FinancialCapabilityName { get; set; }
        
        /// <summary>
        /// Kinh nghiệm đầu tư
        /// </summary>
        public int InvestmentExperienceId  { get; set; }   
        
        /// <summary>
        /// Tên Kinh nghiệm đầu tư
        /// </summary>
        public string? InvestmentExperienceName  { get; set; } 
        
        
        /// <summary>
        /// đã tham gia trái phiếu
        /// </summary>
        public int JoinedBondId { get; set; }
        
        /// <summary>
        ///tên đã tham gia trái phiếu
        /// </summary>
        public string? JoinedBondName { get; set; } 
        
        
        /// <summary>
        /// Khả năng chấp nhận rủi ro
        /// </summary>
        public int RiskAcceptId { get; set; }
        
        /// <summary>
        /// Tên Khả năng chấp nhận rủi ro
        /// </summary>
        public string? RiskAcceptName { get; set; }
        
        /// <summary>
        /// Sở thích đầu tư
        /// </summary>
        public int HobbyInvestmentId  { get; set; }
        
        /// <summary>
        /// Tên Sở thích đầu tư
        /// </summary>
        public string? HobbyInvestmentName  { get; set; }  
        
        
        /// <summary>
        /// Có quan hệ với sale
        /// </summary>
        public int RelationshipSaleId { get; set; } 
        
        /// <summary>
        /// Tên Có quan hệ với sale
        /// </summary>
        public string? RelationshipSaleName { get; set; }
        
        /// <summary>
        /// Lưu ý khi nói chuyện
        /// </summary>
        public int NoteTalkingId  { get; set; }  
         
        /// <summary>
        /// Tên Lưu ý khi nói chuyện
        /// </summary>
        public string? NoteTalkingName  { get; set; }  
        
        /// <summary>
        /// Kiến thức, Kinh nghiệm
        /// </summary>
        public int KnowledExpId { get; set; }  
        
        /// <summary>
        ///Tên Kiến thức, Kinh nghiệm
        /// </summary>
        public string? KnowledExpName { get; set; }
        
        /// <summary>
        /// Quan điểm đầu tư
        /// </summary>
        public int InvestmentPersId { get; set; }  
        
        /// <summary>
        ///Tên Quan điểm đầu tư
        /// </summary>
        public string? InvestmentPersName { get; set; }
        
        /// <summary>
        /// Nguồn khai thác
        /// </summary>
        public int SourceId { get; set; } 
        
        /// <summary>
        ///Tên Nguồn khai thác
        /// </summary>
        public string? SourceName { get; set; }    */
}

public class CustomerExtendInfo
{
    public int CategoryType { get; set; }
    public string? CategoryName { get; set; }
    public int CategoryId { get; set; }
}

public class ShareholderInfo
{
    public string? StockCode { get; set; }
    public string? ShareholderType { get; set; }
}