﻿using TVSI.Crm.API.Common;

namespace TVSI.Crm.API.Lib.Models.Response.Customer;

public class CustomerOnlineListResponse
{
    /// <summary>
    /// ID
    /// </summary>
    /// <example>123</example>
    public int Id { get; set; }

    /// <summary>
    /// Họ tên
    /// </summary>
    public string? FullName { get; set; }

    /// <summary>
    /// Mã khách hàng
    /// </summary>
    public string? CustCode { get; set; }

    /// <summary>
    /// Sale ID
    /// </summary>
    public string? SaleID { get; set; }

    /// <summary>
    /// Số điện thoại 01
    /// </summary>
    public string? PhoneNumber01 { get; set; }

    /// <summary>
    /// Số điện thoại 02
    /// </summary>
    public string? PhoneNumber02 { get; set; }

    /// <summary>
    /// Email
    /// </summary>
    public string? Email { get; set; }

    /// <summary>
    /// Ngày tạo
    /// </summary>
    public string? CreatedDate { get; set; }


    /// <summary>
    /// Kênh mở tài khoản
    /// </summary>
    public int OpenSource { get; set; }

    /// <summary>
    ///  Tên kênh mở tài khoản
    /// </summary>
    public string? OpenSourceName { get; set; }

    /// <summary>
    /// Trạng thái tk
    /// </summary>
    public string? AccountStatus { get; set; }

    /// <summary>
    /// Màu Trạng thái tk
    /// </summary>
    public string? AccountStatusColor { get; set; }

    /// <summary>
    /// trạng thái hồ sơ
    /// </summary>
    public string? ProfileStatus { get; set; }

    /// <summary>
    /// Màu trạng thái hồ sơ
    /// </summary>
    public string? ProfileStatusColor { get; set; }

    /// <summary>
    /// trạng thái Tương tác
    /// </summary>
    public string? ActionStatus { get; set; }

    /// <summary>
    /// mau trạng thái Tương tác
    /// </summary>
    public string? ActionStatusColor { get; set; }
    
    
    
    /// <summary>
    /// Trạng thái được phép hủy hồ sơ
    /// </summary>
    public bool CancelStatus { get; set; } 
    
    /// <summary>
    /// Trạng thái Chuyển đổi hồ sơ
    /// </summary>
    public bool ConvertStatus { get; set; }
     
    /// <summary>
    /// Trạng thái kích hoạt
    /// </summary>
    public bool ConfirmStatus { get; set; }
    
    
    /// <summary>
    /// Mã code kích hoạt Ekyc
    /// </summary>
    public string? ConfirmCode { get; set; }
    
    
    
}