namespace TVSI.Crm.API.Lib.Models.Response.Customer;

public class ConfirmEkycResponse
{
    public int? ReturnCode { get; set; }
}