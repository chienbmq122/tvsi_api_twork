using System.Text.Json.Serialization;

namespace TVSI.Crm.API.Lib.Models.Response.Customer;



public class CustomerReviewRateStarReponse
{
    public RateStar? RateStar { get; set; } = new RateStar();
    
    public List<CustomerReviewsResponse>? CustomerReviews { get; set; } = new List<CustomerReviewsResponse>();

}
public class CustomerReviewsResponse
{
    
    /// <summary>
    /// Id
    /// </summary>
    public int Id{ get; set;}

    [JsonIgnore]
    public int Star { get; set; }
    
    
    /// <summary>
    /// họ tên
    /// </summary>
    public string? FullName { get; set; }
    
    /// <summary>
    /// Nội dung
    /// </summary>
    public string? Content { get; set; }
    
    /// <summary>
    /// Ngày tạo
    /// </summary>
    public string? CreatedDate { get; set; }
    

    /// <summary>
    /// Tin nhắn reply
    /// </summary>
    public List<Reply>? Replies { get; set; } = new List<Reply>();
    
    

}

public class Reply {
    
    /// <summary>
    /// Id
    /// </summary>
    [JsonIgnore]
    public int Id { get; set; }
    
    /// <summary>
    /// Tên người reply
    /// </summary>
    public string? FullName { get; set; }
    
    /// <summary>
    /// nội dung reply
    /// </summary>
    public string? Content { get; set; }
}


public class RateStar
{
    /// <summary>
    /// Tính trung bình sao
    /// </summary>
    public double AvgStar { get; set; }
    
    /// <summary>
    /// sao 5
    /// </summary>
    public int StarFive { get; set; }
    
    /// <summary>
    /// sao 4
    /// </summary>
    public int StarFour { get; set; }
    
    
    /// <summary>
    /// sao 3
    /// </summary>
    public int StarThree { get; set; }
    
    /// <summary>
    /// sao 2
    /// </summary>
    public int StarTwo { get; set; }
    
    /// <summary>
    /// sao 1
    /// </summary>
    public int StarOne { get; set; }
}