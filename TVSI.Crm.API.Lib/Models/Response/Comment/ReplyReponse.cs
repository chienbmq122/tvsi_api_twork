﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVSI.Crm.API.Lib.Models.Response.Comment
{
    public class ReplyModel
    {
        [Description("id reply")]
        public int ReplyID { get; set; }

        [Description("id comment")]
        public int CommentID { get; set; }

        [Description("Nội dung")]
        public string Reply { get; set; }

        [Description("người rep")]
        public string UserName { get; set; }

        [Description("full name người rep")]
        public string FullName { get; set; }
        [Description("thời gian")]
        public string CreatedDate { get; set; }

    }
}
