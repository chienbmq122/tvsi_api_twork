﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVSI.Crm.API.Lib.Models.Response.Comment
{
    public class CommentDBModel
    {
        [Description("id comment")]
        public int CommentID { get; set; }

        [Description("id lead")]
        public int LeadID { get; set; }

        [Description("customer code")]
        public string CustCode { get; set; }

        [Description("id comment")]
        public string Content { get; set; }

        [Description("tài khoản người comment")]
        public string UserName { get; set; }

        [Description("Tên người commnet")]
        public string FullName { get; set; }
        [Description("thời gian")]
        public string CreatedDate { get; set; }

        [Description("Điểm đánh giá (số sao)")]
        public int Rate1 { get; set; }

        public List<ReplyModel> ListReply { get; set; }
    }

    public class TotalRateResponse
    {
        public double Total { get; set; }
        public int Rate1 { get; set; }
        public int Rate2 { get; set; }
        public int Rate3 { get; set; }
        public int Rate4 { get; set; }
        public int Rate5 { get; set; }
        public int TotalRate { get; set; }
    }
}
