using System.ComponentModel.DataAnnotations;

namespace TVSI.Crm.API.Lib.Models.Response.ManTvsi;

public class ManTvsiListResponse
{
    public string? ManCode { get; set; }
    public string? ManName { get; set; }
}

public class ManTvsiDetailResponse
{
    /// <summary>
    /// Người đại diện tvsi
    /// </summary>
    [Required]
    public string? ManTvsi { get; set; }

    /// <summary>
    /// Vị trí
    /// </summary>
    [Required]
    public string? ManPosition { get; set; }

    /// <summary>
    ///  giấy ủy quyền
    /// </summary>
    [Required]
    public string? ManAnPower { get; set; }

    /// <summary>
    /// Ngày cập giấy ủy quyền
    /// </summary>
    [Required]
    public string? ManDateAnPower { get; set; }  
    /// <summary>
    /// Ngày cập giấy ủy quyền
    /// </summary>

    /// <summary>
    /// Số CMT/CCCD của TVSI
    /// </summary>
    [Required]
    public string? ManCardId { get; set; }

    /// <summary>
    /// Ngày cấp số CMT/CCCD
    /// </summary>
    [Required]
    public string? ManIssueDate { get; set; }

    /// <summary>
    /// Nơi cấp số CMT/CCCD
    /// </summary>
    [Required]
    public string? ManIssuePlace { get; set; }
}