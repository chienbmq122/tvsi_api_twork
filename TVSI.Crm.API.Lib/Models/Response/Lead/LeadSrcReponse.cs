﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVSI.Crm.API.Lib.Models.Response.Lead
{
    public class LeadSrcReponse
    {
        public int LeadSourceID { get; set; }
        public string SourceName { get; set; }
    }
}
