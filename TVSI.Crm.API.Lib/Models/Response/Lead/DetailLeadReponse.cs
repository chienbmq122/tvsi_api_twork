﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVSI.Crm.API.Lib.Models.Response.Lead
{
    public class DetailLeadReponse
    {
        public int LeadID { get; set; }
        public string LeadName { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public int ProfileType { get; set; }
        public int Status { get; set; }
        public string? StatusColor { get; set; }
        public int BranchID { get; set; }
        public string AssignUser { get; set; }
        public string Description { get; set; }
        public int LeadSourceID { get; set; }
        public string CreatedDate { get; set; }
        public string UpdatedDate { get; set; }
        public string FullName { get; set; }
        public string SaleID { get; set; }
        public string SourceName { get; set; }

        public List<LeadAtribute> LeadAttribute { get; set; }
    }

    public class DetailLeadSql
    {
        public int LeadID { get; set; }
        public string LeadName { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public int ProfileType { get; set; }
        public int Status { get; set; }
        public string? StatusColor { get; set; }
        public int BranchID { get; set; }
        public string AssignUser { get; set; }
        public string Description { get; set; }
        public int LeadSourceID { get; set; }
        public string CreatedDate { get; set; }
        public string UpdatedDate { get; set; }
        public string FullName { get; set; }
        public string SaleID { get; set; }
        public string SourceName { get; set; }

        public string LeadAttribute { get; set; }
    }

    public class LeadAtribute
    {
        public int Category { get; set; }
        public string CategoryName { get; set; }
        public int AttributeId { get; set; }
        public string AttributeName { get; set; }
    }
}
