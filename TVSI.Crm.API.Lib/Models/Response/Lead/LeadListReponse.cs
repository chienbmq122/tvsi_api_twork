﻿namespace TVSI.Crm.API.Lib.Models.Response.Lead;

/// <summary>
/// Thông tin danh sách KH tiềm năng
/// </summary>
public class LeadData
{
    /// <summary>
    /// Ngày tạo
    /// </summary>
    public string? CreatedDate { get; set; }

    /// <summary>
    ///  Lead ID
    /// </summary>
    public long LeadID { get; set; }

    /// <summary>
    /// Tên KH tiềm năng
    /// </summary>
    public string? LeadName { get; set; }

    /// <summary>
    ///  Số điện thoại
    /// </summary>
    public string? Mobile { get; set; }

    /// <summary>
    /// Email
    /// </summary>
    public string? Email { get; set; }

    /// <summary>
    /// UserName tạo
    /// </summary>
    public string? AssignUser { get; set; }

    /// <summary>
    /// Loại khách hàng
    /// </summary>
    public int? ProfileType { get; set; }

    /// <summary>
    /// Tên loại khách hàng
    /// </summary>
    public string? ProfileName 
    { 
        get {
            switch (ProfileType)
            {
                case 1:
                    return "Cá nhân";
                case 2:
                    return "Tổ chức";
                default:
                    return "Khác";
            }
        }
    }

    /// <summary>
    /// Trạng thái
    /// </summary>
    public int Status { get; set; }

    /// <summary>
    /// Tên trạng thái
    /// </summary>
    public string? StatusName 
    {
        get
        {
            switch (Status)
            {
                case 1:
                    return "Đã được assign";
                case 2:
                    return "Đã chuyển thành Account";
                default:
                    return "Đã từ chối";
            }
        }
    }

    /// <summary>
    /// Tên trạng thái
    /// </summary>
    public string? StatusColor { get; set; }

    /// <summary>
    /// Check trạng thái cancel
    /// </summary>
    public bool? CancelStatus
    {
        get
        {
            switch (Status)
            {
                case 1:
                    return true;
                case 2:
                default:
                    return false;
            }
        }
    }

    /// <summary>
    /// Check trạng thái covert
    /// </summary>
    public bool? CovertStatus
    {
        get
        {
            switch (Status)
            {
                case 1:
                    return true;
                case 2:
                default:
                    return false;
            }
        }
    }

    /// <summary>
    /// ID nguồn dữ liệu KH tiềm năng
    /// </summary>
    public int LeadSourceId { get; set; }
}

public class LeadListSQL
{
    public int TotalItem { get; set; }
    public string? Items { get; set; }
}

public class LeadDataReponse
{
    public int TotalItem { get; set; }
    public List<LeadData> Items { get; set; }
}