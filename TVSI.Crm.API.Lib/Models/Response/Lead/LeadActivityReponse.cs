﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TVSI.Crm.API.Lib.Models.Response.Activity;

namespace TVSI.Crm.API.Lib.Models.Response.Lead
{
    public class LeadActivitySQL
    {
        public int TotalItem { get; set; }
        public string? Items { get; set; }
    }

    public class LeadActivityReponse
    {
        public int TotalItem { get; set; }
        public List<ActivityResult> Items { get; set; }
    }
}
