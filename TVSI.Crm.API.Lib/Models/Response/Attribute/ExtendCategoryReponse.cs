﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVSI.Crm.API.Lib.Models.Response.Attribute
{
    public class ExtendCategoryReponse
    {
        [Description("Id category")]
        public int ItemID { get; set; }
        [Description("tên hiển thị category")]
        public string? ItemName { get; set; }
        [Description("Id type category")]
        public int ItemTypeID { get; set; }
    }
}
