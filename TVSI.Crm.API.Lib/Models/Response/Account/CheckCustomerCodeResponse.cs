namespace TVSI.Crm.API.Lib.Models.Request.Account;
/// <summary>
/// Chi tiết hồ sơ tài khoản 
/// </summary>
public class CheckCustomerCodeResponse
{
    public string CustomerCode { get; set; }
    public int? CustomerType { get; set; }
    public int? DocType { get; set; }
    public string? AccountStatus { get; set; }
    public string? AddAccount { get; set; }
}