namespace TVSI.Crm.API.Lib.Models.Response.Account;

/// <summary>
/// 
/// </summary>
public class OpenAccountDetailResponse
{

    /// <summary>
    /// SearchType , 1: Tìm kiếm phê duyệt, 2: Tìm kiếm lịch sử
    /// </summary>
    public int SearchType { get; set; }

    /// <summary>
    /// Id
    /// </summary>
    public long Id { get; set; }

    /// <summary>
    /// mã khách hàng
    /// </summary>
    public string? Custcode { get; set; }
    /// <summary>
    /// Họ tên
    /// </summary>
    public string? FullName { get; set; }

    /// <summary>
    /// Ngày sinh
    /// </summary>
    public string? BirthDay { get; set; }

    /// <summary>
    /// Giới tính
    /// </summary>
    public int Gender { get; set; }

    /// <summary>
    /// Tên giới tính
    /// </summary>
    public string? GenderName { get; set; }

    /// <summary>
    /// Số CMT/CCCD
    /// </summary>
    public string? CardId { get; set; }
    /// <summary>
    /// Ngày cấp cmt
    /// </summary>
    public string? IssueDate { get; set; }

    /// <summary>
    /// Nơi cấp cmt
    /// </summary>
    public string? IssuePlace { get; set; }

    /// <summary>
    /// Địa chỉ
    /// </summary>
    public string? Address { get; set; }
    /// <summary>
    /// Quốc tịch
    /// </summary>
    public string? Nationality { get; set; }

    /// <summary>
    /// SaleID
    /// </summary>
    public string? SaleId { get; set; }

    /// <summary>
    /// Sale name
    /// </summary>
    public string? SaleName { get; set; }

    /// <summary>
    /// Branch Name
    /// </summary>
    public string? BranchName { get; set; }

    /// <summary>
    /// Hạn mức chi nhánh
    /// </summary>
    public string? BranchMargin { get; set; }

    /// <summary>
    /// Branch Name
    /// </summary>
    public string? RemainBranchMargin { get; set; }

    /// <summary>
    /// Ngày tạo
    /// </summary>
    public string? CreateDate { get; set; }
    /// <summary>
    /// Ngày update cuối
    /// </summary>
    public string? UpdateDate { get; set; }
    /// <summary>
    /// Hồ sơ mở bao gồm
    /// </summary>
    public string? ProfileContain { get; set; }
    /// <summary>
    /// "1": HĐ Margin, "2": HĐ Phái sinh, "12": HĐ Margin + HĐ Phái sinh
    /// </summary>
    public string? AddContract { get; set; }
    
    /// <summary>
    /// Loại hồ sơ, 1: Thêm mới, 2: bổ sung
    /// </summary>
    public int ProfileType { get; set; }
    /// <summary>
    /// Tên loại hồ sơ
    /// </summary>
    public string? ProfileTypeName { get; set; }

    /// <summary>
    /// Trạng thái hồ sơ
    /// </summary>
    public string? ProfileStatus { get; set; }

    /// <summary>
    /// Tên trạng thái hồ sơ
    /// </summary>
    public string? ProfileStatusName { get; set; }
    /// <summary>
    /// Màu Trạng thái hồ sơ
    /// </summary>
    public string? ProfileStatusColor { get; set; }
    /// <summary>
    /// Trạng thái tài khoản
    /// </summary>
    public string? AccountStatus { get; set; }

    /// <summary>
    /// Tên trạng thái tài khoản
    /// </summary>
    public string? AccountStatusName { get; set; }

    /// <summary>
    /// Màu trạng thái tài khoản
    /// </summary>
    public string? AccountStatusColor { get; set; }
    /// <summary>
    /// Tên file CMND
    /// </summary>
    public string? CardIdFileName { get; set; }
    /// <summary>
    /// dữ liệu file CMND
    /// </summary>
    public byte[]? CardIdFileData { get; set; }
    public string? CardIdFileDataBase64 { get; set; }
    /// <summary>
    /// Tên file CMND mặt sau
    /// </summary>
    public string? CardIdBackFileName { get; set; }
    /// <summary>
    /// dữ liệu file CMND mặt sau
    /// </summary>
    public byte[]? CardIdBackFileData { get; set; }
    public string? CardIdBackFileDataBase64 { get; set; }
    /// <summary>
    /// Người đại diện tvsi
    /// </summary>
    public string? ManTvsi { get; set; }
    /// <summary>
    /// Vị trí
    /// </summary>
    public string? ManPosition { get; set; }
    /// <summary>
    ///  giấy ủy quyền
    /// </summary>
    public string? ManAnPower { get; set; }
    /// <summary>
    /// Ngày cập giấy ủy quyền
    /// </summary>
    public string? ManDateAnPower { get; set; }
    /// <summary>
    /// Số CMT/CCCD của TVSI
    /// </summary>
    public string? ManCardId { get; set; }
    /// <summary>
    /// Ngày cấp số CMT/CCCD
    /// </summary>
    public string? ManIssueDate { get; set; }
    /// <summary>
    /// Nơi cấp số CMT/CCCD
    /// </summary>
    public string? ManIssuePlace { get; set; }
    /// <summary>
    /// Tài khoản giao dịch ký quỹ
    /// </summary>
    public bool MarginAccount { get; set; }
    public decimal CreditLimit { get; set; } // Hạn mức dư nợ
    public int? DebtTerm { get; set; } // Thời hạn nợ hợp đồng
    public float? DepositRate { get; set; } // Tỷ lệ ký quỹ



    /// <summary>
    /// Check Phương thức nhận SMS
    /// </summary>
    public bool TradingResultPhone { get; set; }
    /// <summary>
    /// Số điện thoại 1
    /// </summary>
    public string? Phone01 { get; set; }

    /// <summary>
    /// Số điện thoại 2
    /// </summary>
    public string? Phone02 { get; set; }

    /// <summary>
    /// Check phương thức nhận Email
    /// </summary>
    public bool TradingResultEmail { get; set; }

    /// <summary>
    /// Email
    /// </summary>
    public string? Email { get; set; }



    /// <summary>
    /// Đăng ký ngân hàng chuyển tiền
    /// </summary>
    public bool TransferCashBankService { get; set; }

    /// <summary>
    /// Trạng thái NH có đăng ký hay k
    /// </summary>
    public bool StatusBank01 { get; set; }
    /// <summary>
    /// Tên NH
    /// </summary>
    public string? BankName01 { get; set; }
    /// <summary>
    /// Số TK NH
    /// </summary>
    public string? BankNo01 { get; set; }
    /// <summary>
    /// Chủ tài khoản NH
    /// </summary>
    public string? BankAccountName01 { get; set; }
    public string? BankBranchName01 { get; set; }
    

    public bool StatusBank02 { get; set; }
    public string? BankName02 { get; set; }
    public string? BankNo02 { get; set; }
    public string? BankAccountName02 { get; set; }
    public string? BankBranchName02 { get; set; }

    public bool StatusBank03 { get; set; }
    public string? BankName03 { get; set; }
    public string? BankNo03 { get; set; }
    public string? BankAccountName03 { get; set; }
    public string? BankBranchName03 { get; set; }


    public bool StatusBank04 { get; set; }
    public string? BankName04 { get; set; }
    public string? BankNo04 { get; set; }
    public string? BankAccountName04 { get; set; }
    public string? BankBranchName04 { get; set; }

    /// <summary>
    /// Đăng ký tk đối ứng
    /// </summary>
    public bool TransferReciprocalAccountService { get; set; }
    public bool StatusReciprocalAccount01 { get; set; }
    public string? ReciprocalAccountName01 { get; set; }
    public string? ReciprocalAccountNo01 { get; set; }


    public bool StatusReciprocalAccount02 { get; set; }
    public string? ReciprocalAccountName02 { get; set; }
    public string? ReciprocalAccountNo02 { get; set; }
    /// <summary>
    /// Thông tin HOA KỲ
    /// </summary>
    public bool InfoUsa { get; set; }
    public bool Info01 { get; set; }
    public bool Info02 { get; set; }
    public bool Info03 { get; set; }
    public bool Info04 { get; set; }
    public bool Info05 { get; set; }
    public bool Info06 { get; set; }
    public bool Info07 { get; set; }



    /// <summary>
    /// Thông tin cổ đông
    /// </summary>
    public bool IsShareHolder { get; set; }
    public List<ShareHolder>? ShareHolderInfo { get; set; }

    public List<ExtendInfo>? ExtendCust { get; set; }

    /// <summary>
    /// Nguồn mở tk
    /// </summary>
    public int? SourceAccount { get; set; }
}
public class ShareHolder
{
    public bool ShareType_1 { get; set; }
    public bool ShareType_2 { get; set; }
    public bool ShareType_3 { get; set; }
    public bool ShareType_4 { get; set; }
    public string? StockCode { get; set; }
}
public class ExtendInfo
{
    public int CategoryType { get; set; }
    public int Value { get; set; }
}
