namespace TVSI.Crm.API.Lib.Models.Request.Account;
/// <summary>
/// Chi tiết hồ sơ tài khoản 
/// </summary>
public class CheckAccountPermissionResponse
{
    public string CustCode { get; set; }
    public string AssignUser { get; set; }
    public string BranchID { get; set; }
    public string? SaleID_BB { get; set; }

}