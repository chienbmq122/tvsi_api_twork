namespace TVSI.Crm.API.Lib.Models.Response.Account;

/// <summary>
/// 
/// </summary>
public class OpenAccountAdditionalDetailResponse
{
    /// <summary>
    /// mã khách hàng
    /// </summary>
    public string? Custcode { get; set; }
    /// <summary>
    /// Họ tên
    /// </summary>
    public string? FullName { get; set; }

    /// <summary>
    /// Ngày sinh
    /// </summary>
    public string? BirthDay { get; set; }

    /// <summary>
    /// Giới tính
    /// </summary>
    public int Gender { get; set; }

    /// <summary>
    /// Tên giới tính
    /// </summary>
    public string? GenderName { get; set; }

    /// <summary>
    /// Số CMT/CCCD
    /// </summary>
    public string? CardId { get; set; }
    /// <summary>
    /// Ngày cấp cmt
    /// </summary>
    public string? IssueDate { get; set; }

    /// <summary>
    /// Nơi cấp cmt
    /// </summary>
    public string? IssuePlace { get; set; }

    /// <summary>
    /// Địa chỉ
    /// </summary>
    public string? Address { get; set; }

    public string? SaleId { get; set; }

    public int? Status { get; set; }

}

