namespace TVSI.Crm.API.Lib.Models.Request.Account;
/// <summary>
/// Chi tiết hồ sơ tài khoản 
/// </summary>
public class CheckCustCodeResponse
{
    public int Code { get; set; }
    public int CountDebt { get; set; }
}