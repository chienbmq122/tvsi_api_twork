﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVSI.Crm.API.Lib.Models.Response.Account
{
    public class ListCustcodeReponseAccount
    {
        public int TotalItem { get; set; }
        public List<ListCustcodeAccount> Items { get; set; }
    }

    public class ListCustcodeAccount
    {
        public string? CustCode { get; set; }
    }

    public class ListCustcodeSqlAccount
    {
        public int TotalItem { get; set; }
        public string Items { get; set; }
    }
}
