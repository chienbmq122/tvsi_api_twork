namespace TVSI.Crm.API.Lib.Models.Response.Account;

public class BranchInfoResponse
{
    /// <summary>
    /// Mã chi nhánh
    /// </summary>
    public string? BranchNo { get; set; }
    
    /// <summary>
    /// Tên bộ phận (tên chi nhánh)
    /// </summary>
    public string? BranchName { get; set; }
    /// <summary>
    /// Tên nhân viên
    /// </summary>
    public string? SaleName { get; set; }
    
}