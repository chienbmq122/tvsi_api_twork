namespace TVSI.Crm.API.Lib.Models.Response.Account;

/// <summary>
/// 
/// </summary>
public class AccountOpenDetailResponse
{

        /// <summary>
        /// Id
        /// </summary>
        public int Id { get; set; }  
        
        /// <summary>
        /// mã khách hàng
        /// </summary>
        public string? Custcode { get; set; }
        
        /// <summary>
        /// Tên loại hồ sơ
        /// </summary>
        public string? ProfileTypeName { get; set; }
        
        
        /// <summary>
        /// Họ tên
        /// </summary>
        public string? FullName { get; set; }
        
        /// <summary>
        /// Ngày sinh
        /// </summary>
        public string? BirthDay { get; set; }
        
        /// <summary>
        /// giới tính
        /// </summary>
        public int? Gender { get; set; }
        
        /// <summary>
        ///Tên giới tính
        /// </summary>
        public string? GenderName { get; set; }
        
        
        
        /// <summary>
        /// Số CMT/CCCD
        /// </summary>
        public string? CardId { get; set; }
        /// <summary>
        /// Ngày cấp cmt
        /// </summary>
        public string? IssueDate { get; set; }
        
        /// <summary>
        /// Nơi cấp cmt
        /// </summary>
        public string? IssuePlace { get; set; }

        /// <summary>
        /// Địa chỉ
        /// </summary>
        public string? Address { get; set; }
        /// <summary>
        /// Quốc tịch
        /// </summary>
        public string? Nationality { get; set; }
        /// <summary>
        /// SaleID
        /// </summary>
        public string? SaleID { get; set; }  
        /// <summary>
        /// SaleID
        /// </summary>
        public string? SaleName { get; set; }
        /// <summary>
        /// Tên nhánh
        /// </summary>
        public string? BranchName { get; set; }
        /// <summary>
        /// Ngày tạo
        /// </summary>
        public string? CreatedDate { get; set; }
        /// <summary>
        /// Người cập nhật cuối cùng
        /// </summary>
        public string? UpdateLast { get; set; }
        /// <summary>
        /// Ngày cập nhật cuối cùng
        /// </summary>
        public string? DateLast { get; set; }
        
        /// <summary>
        /// Loại hợp đồng
        /// </summary>
        public string? ContractType { get; set; }
                
        
        /// <summary>
        /// TRẠNG THÁI HĐ
        /// 1 là đã hoàn thiện, 2 là nợ hồ sơ
        /// </summary>
        public string? ContractStatusName { get; set; }
        
        /// <summary>
        /// TRẠNG THÁI HĐ
        /// 1 là đã hoàn thiện, 2 là nợ hồ sơ
        /// </summary>
        public int? ContractStatus { get; set; }
        
        /// <summary>
        /// Loại hồ sơ
        /// </summary>
        public int ProfileType { get; set; }
        /// <summary>
        /// trạng thái hồ sơ
        /// </summary>
        public string? ProfileStatus { get; set; }
        /// <summary>
        /// màu hồ sơ
        /// </summary>
        public string? ProfileStatusColor { get; set; }
        /// <summary>
        /// trạng thái tài khoản
        /// </summary>
        public string? AccountStatus { get; set; }
        /// <summary>
        /// Màu trạng thái tài khoản
        /// </summary>
        public string? AccountStatusColor { get; set; }
        /// <summary>
        /// Lý do
        /// </summary>
        public string? Reason { get; set; }
        /// <summary>
        /// Người đại diện tvsi
        /// </summary>
        public string? ManTvsi { get; set; }
        /// <summary>
        /// Vị trí
        /// </summary>
        public string? ManPosition { get; set; }
        /// <summary>
        ///  giấy ủy quyền
        /// </summary>
        public string? ManAnPower { get; set; }
        /// <summary>
        /// Ngày cập giấy ủy quyền
        /// </summary>
        public string? ManDateAnPower { get; set; }
        /// <summary>
        /// Số CMT/CCCD của TVSI
        /// </summary>
        public string? ManCardId { get; set; }
        /// <summary>
        /// Ngày cấp số CMT/CCCD
        /// </summary>
        public string? ManIssueDate { get; set; }
        /// <summary>
        /// Nơi cấp số CMT/CCCD
        /// </summary>
        public string? ManIssuePlace { get; set; }
        /// <summary>
        /// Tài khoản giao dịch ký quỹ
        /// </summary>
        public bool AccountMargin { get; set; }
        /// <summary>
        /// Check Phương thức nhận SMS
        /// </summary>
        public bool TradingResultSms { get; set; }
        /// <summary>
        /// Số điện thoại 1
        /// </summary>
        public string? Phone01 { get; set; }
        
        /// <summary>
        /// Số điện thoại 2
        /// </summary>
        public string? Phone02 { get; set; }
        
        /// <summary>
        /// Check phương thức nhận Email
        /// </summary>
        public bool TradingResultEmail { get; set; }
        
        /// <summary>
        /// Email
        /// </summary>
        public string? Email { get; set; }
        
        /// <summary>
        /// Ngân hàng chuyển tiền
        /// </summary>
        public bool TransferCashService { get; set; }
        
        
        /// <summary>
        /// Trạng thái NH có đăng ký hay k
        /// </summary>
        public bool StatusBank01 { get; set; }
        /// <summary>
        /// tên trạng thái NH
        /// </summary>
        public string? StatusNameBank01 { get; set; }
        
        /// <summary>
        /// Màu trạng thái ngân hàng
        /// </summary>
        public string? StatusColorBank01 { get; set; }
        /// <summary>
        /// Tên NH
        /// </summary>
        public string? BankName01 { get; set; }
        
        /// <summary>
        /// Số TK NH
        /// </summary>
        public string? BankNo01 { get; set; }
        
        /// <summary>
        /// Mã ngân hàng 
        /// </summary>
        public string? BankCode01 { get; set; }  
        
        /// <summary>
        /// Mã chi nhánh
        /// </summary>
        public string? SubBranchCode01 { get; set; }
        
        /// <summary>
        /// Chủ tài khoản NH
        /// </summary>
        public string? BankAccountName01 { get; set; }
        
        /// <summary>
        /// Tên chi nhánh
        /// </summary>
        public string? SubBranchName01 { get; set; }
        
        
        
        public bool StatusBank02 { get; set; }
        public string? StatusNameBank02 { get; set; }
        public string? StatusColorBank02 { get; set; }
        public string? BankName02 { get; set; }
        public string? BankNo02 { get; set; }
        /// <summary>
        /// Mã ngân hàng 
        /// </summary>
        public string? BankCode02 { get; set; }  
        
        /// <summary>
        /// Mã chi nhánh
        /// </summary>
        public string? SubBranchCode02 { get; set; }
        public string? BankAccountName02 { get; set; }
        public string? SubBranchName02 { get; set; }

        public bool StatusBank03 { get; set; }
        public string? StatusNameBank03 { get; set; }
        public string? StatusColorBank03 { get; set; }
        public string? BankName03 { get; set; }
        public string? BankNo03 { get; set; }
        /// <summary>
        /// Mã ngân hàng 
        /// </summary>
        public string? BankCode03 { get; set; }  
        
        /// <summary>
        /// Mã chi nhánh
        /// </summary>
        public string? SubBranchCode03 { get; set; }
        public string? BankAccountName03 { get; set; }
        public string? SubBranchName03 { get; set; }
        
        public bool StatusBank04 { get; set; }
        public string? StatusNameBank04 { get; set; }
        public string? StatusColorBank04 { get; set; }
        public string? BankName04 { get; set; }
        public string? BankNo04 { get; set; }
        /// <summary>
        /// Mã ngân hàng 
        /// </summary>
        public string? BankCode04 { get; set; }  
        
        /// <summary>
        /// Mã chi nhánh
        /// </summary>
        public string? SubBranchCode04 { get; set; }
        public string? BankAccountName04 { get; set; }
        public string? SubBranchName04 { get; set; }
        
        public bool StatusInternalAccount { get; set; }

        
        /// <summary>
        /// tài khoản đối ứng có được đăng ký hay ko?
        /// </summary>
        public bool StatusInternalAccount01 { get; set; }
        /// <summary>
        /// Tên trạng thái tk đối ứng
        /// </summary>
        public string? StatusNameInternalAccount01 { get; set; }
        
        /// <summary>
        /// Màu trạng thái tk đối ứng
        /// </summary>
        public string? StatusColorInternalAccount01 { get; set; }
        
        /// <summary>
        /// Tên chủ tk đối ứung
        /// </summary>
        public string? InternalAccountName01 { get; set; } 
        
        /// <summary>
        /// Số tk đối ứng
        /// </summary>
        public string? InternalAccountNo01 { get; set; } 
        
        
        /// <summary>
        /// tài khoản đối ứng có được đăng ký hay ko?
        /// </summary>
        public bool StatusInternalAccount02 { get; set; }
        /// <summary>
        /// Tên trạng thái tk đối ứng
        /// </summary>
        public string? StatusNameInternalAccount02 { get; set; }
        
        /// <summary>
        /// Màu trạng thái tk đối ứng
        /// </summary>
        public string? StatusColorInternalAccount02 { get; set; }
        public string? InternalAccountName02 { get; set; }
        public string? InternalAccountAccountNo02 { get; set; }
        /// <summary>
        /// Thông tin HOA KỲ
        /// </summary>
        public bool InfoUsa { get; set; }
        public bool Info01 { get; set; }
        public bool Info02 { get; set; }
        public bool Info03 { get; set; }
        public bool Info04 { get; set; }
        public bool Info05 { get; set; }
        public bool Info06 { get; set; }
        public bool Info07 { get; set; }
        public string? ImageFront { get; set; }
        public string? ImageBack { get; set; }
        public string? ImageFace { get; set; }
        public decimal CreditLimit { get; set; } // Hạn mức dư nợ
        public int? DebtTerm { get; set; } // Thời hạn nợ hợp đồng
        public float? DepositRate { get; set; } // Tỷ lệ ký quỹ
        
        /// <summary>
        /// Nguồn data
        /// </summary>
        public int? OpenSource { get; set; }
        

        public List<ExtendCategory> ExtendCust { get; set; } = new List<ExtendCategory>();
        
        
        /// <summary>
        /// Thông tin cổ đông
        /// </summary>
        public bool IsShareHolder { get; set; }
        public List<ShareHolderInfoResponsive>? ShareHolderInfo { get; set; }

}

public class ImageAccountDetailResponse
{
        public byte[]? ImageFront { get; set; }
        public byte[]? ImageBack { get; set; }
}

public class ExtendInfoAccountResponse
{ 
        /// <summary>
        /// Nghề nghiệp
        /// </summary>
        public int JobId { get; set; }
        
        /// <summary>
        /// Tên nghề nghiệp
        /// </summary>
        public string? JobName { get; set; }
        
        /// <summary>
        /// Nơi làm việc
        /// </summary>
        public int WorkPlaceId { get; set; }
        
        /// <summary>
        /// Tên Nơi làm việc
        /// </summary>
        public string WorkPlaceName { get; set; }
        
        
        /// <summary>
        /// Khả năng tài chính
        /// </summary>
        public int FinancialCapabilityId { get; set; }  
        
        /// <summary>
        /// tên khả năng tài chính
        /// </summary>
        public int FinancialCapabilityName { get; set; }
        
        /// <summary>
        /// Kinh nghiệm đầu tư
        /// </summary>
        public int InvestmentExperienceId  { get; set; }   
        
        /// <summary>
        /// Tên Kinh nghiệm đầu tư
        /// </summary>
        public string? InvestmentExperienceName  { get; set; } 
        
        
        /// <summary>
        /// đã tham gia trái phiếu
        /// </summary>
        public int JoinedBondId { get; set; }
        
        /// <summary>
        ///tên đã tham gia trái phiếu
        /// </summary>
        public int JoinedBondName { get; set; } 
        
        
        /// <summary>
        /// Khả năng chấp nhận rủi ro
        /// </summary>
        public int RiskAcceptId { get; set; }
        
        /// <summary>
        /// Tên Khả năng chấp nhận rủi ro
        /// </summary>
        public int RiskAcceptName { get; set; }
        
        /// <summary>
        /// Sở thích đầu tư
        /// </summary>
        public int HobbyInvestmentId  { get; set; }
        
        /// <summary>
        /// Tên Sở thích đầu tư
        /// </summary>
        public int HobbyInvestmentName  { get; set; }  
        
        
        /// <summary>
        /// Có quan hệ với sale
        /// </summary>
        public int RelationshipSaleId { get; set; } 
        
        /// <summary>
        /// Tên Có quan hệ với sale
        /// </summary>
        public int RelationshipSaleName { get; set; }
        
        /// <summary>
        /// Lưu ý khi nói chuyện
        /// </summary>
        public int NoteTalkingId  { get; set; }  
         
        /// <summary>
        /// Tên Lưu ý khi nói chuyện
        /// </summary>
        public int NoteTalkingName  { get; set; }  
        
        /// <summary>
        /// Kiến thức, Kinh nghiệm
        /// </summary>
        public int KnowledExpId { get; set; }  
        
        /// <summary>
        ///Tên Kiến thức, Kinh nghiệm
        /// </summary>
        public int KnowledExpName { get; set; }
        
        /// <summary>
        /// Quan điểm đầu tư
        /// </summary>
        public int InvestmentPersId { get; set; }  
        
        /// <summary>
        ///Tên Quan điểm đầu tư
        /// </summary>
        public int InvestmentPersName { get; set; }
        
        /// <summary>
        /// Nguồn khai thác
        /// </summary>
        public int SourceId { get; set; } 
        
        /// <summary>
        ///Tên Nguồn khai thác
        /// </summary>
        public int SourceName { get; set; }    

        
}

public class ExtendInfoResponse
{
        public int CategoryType { get; set; }
        public string? CategoryName { get; set; }
        public int CategoryId { get; set; }
}

public class ExtendCategory
{
        public int CategoryType { get; set; }
        public int Value { get; set; }
}

public class ShareHolderInfoResponsive
{
    public bool ShareType_1 { get; set; }
    public bool ShareType_2 { get; set; }
    public bool ShareType_3 { get; set; }
    public bool ShareType_4 { get; set; }
    public string? StockCode { get; set; }
}