using TVSI.Crm.API.Common;

namespace TVSI.Crm.API.Lib.Models.Response.Account;

public class AccountListResponse
{
    /// <summary>
    /// Id
    /// </summary>
    public int Id { get; set; }
    
    
    /// <summary>
    /// Tên Loại hồ sơ
    /// </summary>
    public string? ProfileTypeName { get; set; } 
    
    public string? SaleID { get; set; } 
    
    /// <summary>
    /// Loại hồ sơ
    /// </summary>
    public int ProfileType { get; set; }
    
    
    
    /// <summary>
    /// Họ và tên
    /// </summary>
    public string? FullName { get; set; }
    
    /// <summary>
    /// Mã khách hàng
    /// </summary>
    public string? Custcode { get; set; }
    /// <summary>
    /// ngày tạo
    /// </summary>
    public string? CreatedDate { get; set; }
    
    /// <summary>
    /// Trạng thái hồ sơ
    /// </summary>
    public string? ProfileStatus { get; set; }

    /// <summary>
    /// Màu trạng thái hồ sơ
    /// </summary>
    public string? ProfileStatusColor { get; set; }
    /// <summary>
    /// Trạng thái tk
    /// </summary>
    public string? AccountStatus { get; set; }
    
    /// <summary>
    /// Màu Trạng thái tk
    /// </summary>
    public string? AccountStatusColor { get; set; }
    
    
    /// <summary>
    /// Trạng thái được phép hủy hồ sơ
    /// </summary>
    public bool CancelStatus { get; set; } 
    
    /// <summary>
    /// trạng thái đc phép edit
    /// </summary>
    public bool EditStatus { get; set; }
}