﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace TVSI.Crm.API.Lib.Models.Response.CallInApp
{
    public class BookingCallApp
    {
        /// <summary>
        /// ID
        /// </summary>
        public int BookApptCallId { get; set; }

        /// <summary>
        /// Phân loại
        /// </summary>
        public string? CallAppTypeList { get; set; }

        public string CallAppTypeName { get; set; }
        
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public string? CallId { get; set; }
        public string StatusState
        {
            get
            {
                var timeStart = DateTime.Now;
                var timeEnd = DateTime.Now;

                if (!string.IsNullOrEmpty(StartTime.ToString()))
                {
                    timeStart = DateTime.ParseExact(StartTime, "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture);
                }

                if (!string.IsNullOrEmpty(EndTime.ToString()))
                {
                    timeEnd = DateTime.ParseExact(EndTime, "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture);
                }

                var compareStart = DateTime.Compare(timeStart, DateTime.Now);
                var compareEnd = DateTime.Compare(timeEnd, DateTime.Now);

                if ((compareStart > -1 || compareEnd > -1) && string.IsNullOrEmpty(CallId))
                {
                    return "Chưa đến hạn";
                }

                if (string.IsNullOrEmpty(CallId))
                {
                    return "Quá hạn";
                }
                else
                {
                    return "Trong hạn";
                }
            }
        }

        public string? RemindTime { get; set; }

        /// <summary>
        /// Mô tả
        /// </summary>
        public string? Description { get; set; }

        /// <summary>
        /// Phương thức (1: Hẹn lịch, 2: Giao việc)
        /// </summary>
        [JsonIgnore]
        public int BookCallType { get; set; }

        /// <summary>
        /// Tên phương thức
        /// </summary>
        public string? BookCallTypeName { get; set; }

        /// <summary>
        /// ID người phải thực hiện
        /// </summary>
        public string? AssignUser { get; set; }

        /// <summary>
        /// Tên người phải thực hiện
        /// </summary>
        public string? AssignUserName { get; set; }

        /// <summary>
        /// Chi nhánh
        /// </summary>
        public string? BranchName { get; set; }

        public int Status { get; set; }

        public int LeadId { get; set; }

        public int OpenAccId { get; set; }

        /// <summary>
        /// Tên trạng thái
        /// </summary>
        public string? StatusName { get; set; }

        /// <summary>
        ///  Mã khách hàng
        /// </summary>
        public string? CustCode { get; set; }
        
        /// <summary>
        /// Họ và tên
        /// </summary>
        public string? CustName { get; set; }

        /// <summary>
        /// Loại khách hàng 
        /// </summary>
        public int CustType { get; set; }

        /// <summary>
        /// Tên Loại Khách hàng
        /// </summary>
        public string? CustTypeName { get; set; }

        /// <summary>
        /// Nhân viên thực hiện
        /// </summary>
        public string? CreatedBy { get; set; }

        /// <summary>
        ///  Họ và tên nhân viên thực hiện
        /// </summary>
        public string? CreatedName { get; set; }

        /// <summary>
        /// Thời gian bắt đầu
        /// </summary>
        public string? CreatedDate { get; set; }

        /// <summary>
        /// Người cập nhật cuối
        /// </summary>
        public string? UpdatedBy { get; set; }

        /// <summary>
        /// Ngày cập nhật cuối
        /// </summary>
        public string UpdatedDate { get; set; }

        /// <summary>
        ///  Họ và tên người cập nhật
        /// </summary>
        public string? UpdatedName { get; set; }

        /// <summary>
        ///  Số điện thoại
        /// </summary>
        public string? Mobile { get; set; }

        /// <summary>
        ///  saleID
        /// </summary>
        public string? SaleID { get; set; }

        /// <summary>
        ///  BranchId
        /// </summary>
        public string? BranchID { get; set; }

        public string Birthday { get; set; }

        public string? Gender { get; set; }
    }

    public class BookingCallAppSql
    {
        public int TotalItem { get; set; }
        public string? Items { get; set; }
    }

    public class BookingCallAppResponse
    {
        public int TotalItem { get; set; }
        public List<BookingCallApp>? Items { get; set; }
    }
}
