﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVSI.Crm.API.Lib.Models.Response.CallInApp
{
    public class CustomerContactInfo
    {
		public string? CustName { get; set; }
		public string? TypeCustomer { get; set; }
		public string? TypeCustomerName { get; set; }
		public string? CustCode { get; set; }
		public int LeadId { get; set; }
		public int OpenAccId { get; set; }
		public string? Gender { get; set; }
		public string? BirthDay { get; set; }
		public string? SaleId { get; set; }
		public string? SaleName { get; set; }
		public string? PhoneNumber_01 { get; set; }
		public string? PhoneNumber_02 { get; set; }
	}

	public class CustomerContactInfoSql
    {
		public int TotalItem { get; set; }
		public string? Items { get; set; }
	}

	public class CustomerContactInfoResponse
	{
		public int TotalItem { get; set; }
		public List<CustomerContactInfo>? Items { get; set; }
	}
}
