﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using TVSI.Crm.API.Common.Enums;

namespace TVSI.Crm.API.Lib.Models.Response.CallInApp
{
    public class HistoryCallAppInfo
    {
        /// <summary>
        /// ID của bảng
        /// </summary>
        public int CallAppHistId { get; set; }

        /// <summary>
        /// ID cuộc gọi
        /// </summary>S
        public string? CallId { get; set; }

        /// <summary>
        ///  Mã Khách hàng
        /// </summary>
        public string? CustCode { get; set; }

        /// <summary>
        ///  Tên khách hàng
        /// </summary>
        public string? CustName { get; set; }

        public int? CustType { get; set; }
        public string CustTypeName { get; set; }

        public string? CallAppTypeList { get; set; }

        /// <summary>
        /// Phân loại
        /// </summary>
        public string? CallAppTypeName { get; set; }
        public string? Mobile { get; set; }

        public string? BranchID { get; set; }

        /// <summary>
        /// Đánh giá
        /// </summary>
        public int Rate { get; set; }

        public int LeadId { get; set; }

        public int OpenAccId { get; set; }

        public string? Birthday { get; set; }

        public string? Gender { get; set; }

        /// <summary>
        /// Hình thức
        /// </summary>
        [JsonIgnore]
        public int Direction { get; set; }

        /// <summary>
        /// Tên hình thức
        /// </summary>
        public string? DirectionName { get; set; }

        /// <summary>
        /// Ghi chú
        /// </summary>
        public string? Note { get; set; }

        public string? Comment { get; set; }
        
        /// <summary>
        /// Sale ID
        /// </summary>
        public string? SaleID { get; set; }

        public string? SaleName { get; set; }
        
        /// <summary>
        /// Thời gian gọi
        /// </summary>
        public string? TimeCall { get; set; }

        /// <summary>
        /// Nhân viên thực hiện
        /// </summary>
        public string? CreatedBy { get; set; }

        /// <summary>
        ///  Họ và tên nhân viên thực hiện
        /// </summary>
        public string? CreatedName { get; set; }
        
        /// <summary>
        /// Thời gian bắt đầu
        /// </summary>
        public string? CreatedDate { get; set; }

        /// <summary>
        /// Người cập nhật cuối
        /// </summary>
        public string? UpdatedBy { get; set; }

        /// <summary>
        /// Ngày cập nhật cuối
        /// </summary>
        public string? UpdatedDate { get; set; }

        /// <summary>
        ///  Họ và tên người cập nhật
        /// </summary>
        public string? UpdatedName { get; set; }
    }

    public class HistoryCallAppInfoSql
    {
        public int TotalItem { get; set; }
        public string? Items { get; set; }
    }

    public class HistoryCallAppInfoResponse
    {
        public int TotalItem { get; set; }
        public List<HistoryCallAppInfo>? Items { get; set; }
    }
}
