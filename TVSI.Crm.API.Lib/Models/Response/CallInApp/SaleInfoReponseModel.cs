﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVSI.Crm.API.Lib.Models.Response.CallInApp
{
    public class SaleInfo
    {
        public string UserName { get; set; }
        public string SaleID { get; set; }
        public string FullName { get; set; }
        public string BranchCode { get; set; }
        public string BranchName { get; set; }
        public int Position { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
    }

    public class SaleInfoReponseModel
    {
        public List<SaleInfo> Items { get; set; }
        public int TotalItem { get; set; }
    }

    public class SaleInfoSqlModel
    {
        public string Items { get; set; }
        public int TotalItem { get; set; }
    }
}
