﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVSI.Crm.API.Lib.Models.Response.CallInApp
{
    public class StatusBookingReponse
    {
        public int Value { get; set; }
        public string Text { get; set; }
    }
}
