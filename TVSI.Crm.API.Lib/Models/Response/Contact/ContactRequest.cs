﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVSI.Crm.API.Lib.Models.Response.Contact
{
    public class ContactRequest
    {
        public class ContactInfoStaff
        {
            public string? SaleID { get; set; }
            public string? SaleName { get; set; }
            public int Level { get; set; }
            public string LevelStr
            {
                get
                {
                    switch (Level)
                    {
                        case 1:
                            return "GĐ chi nhánh";
                        case 2:
                            return "Trưởng nhóm";
                        default:
                            return "Sale";
                    }
                }
            }
            public string? Department { get; set; }
        }

        public class ContactInfoLead
        {
            public int LeadId { get; set; }
            public string? LeadName { get; set; }
            public string? SaleID { get; set; }
            public string? SaleName { get; set; }
            public int LeadSourceID { get; set; }
            public string SourceName
            {
                get
                {
                    switch (LeadSourceID)
                    {
                        case 1:
                            return "Hội thảo";
                        case 2:
                            return "Quảng cáo";
                        case 3:
                            return "Người giới thiệu";
                        case 4:
                            return "Sự kiện công ty tổ chức";
                        case 5:
                            return "Qua Website";
                        case 6:
                            return "Khác";
                        default:
                            return "Không xác định";
                    }
                }
            }
        }

        public class ContactInfoCustomer
        {
            public string? CustomerID { get; set; }
            public string? CustomerName { get; set; }
            public string? SaleID { get; set; }
            public string? SaleName { get; set; }
            public int ProfileType { get; set; }
            public string ProfileTypeName
            {
                get
                {
                    switch (ProfileType)
                    {
                        case 1:
                            return "Cá nhân";
                        case 2:
                            return "Tổ chức";
                        default:
                            return "Khác";
                    }
                }
            }
        }

        public class ContactStaff
        {
            public string? SaleID { get; set; }
            public string? FullName { get; set; }
            public string? UserName { get; set; }
            public int UserLevel { get; set; }
            public string? UserRole { get; set; }
        }

        public class ContactStaffResponse
        {
            public List<ContactStaff>? ListContactStaffs { get; set; }
            public int TotalContact { get; set; }
        }

        public class ContactCustomer
        {
            public string? CustName { get; set; }
            public string? CustCode { get; set; }
            public int LeadID { get; set; }

            public string? FirstCharacter { get; set; }
            public int BitIsCust { get; set; }

            public string? Mobile { get; set; }
        }

        public class ContactRole
        {
            public string? ten_dang_nhap { get; set; }
            public string? ma_chuc_nang { get; set; }
            public string? ten_chuc_nang { get; set; }
            public int IsRead { get; set; }
            public int IsEdit { get; set; }
            public int IsCreate { get; set; }
            public int IsDelete { get; set; }
            public int IsPrint { get; set; }
            public int IsFind { get; set; }
            public int IsAllBranch { get; set; }
            public int IsApproval { get; set; }
            public int IsRekey { get; set; }

        }

        public class ContactCustomerResponse
        {
            public List<ContactCustomer>? ListContactCustomers { get; set; }
            public int TotalContact { get; set; }
        }
    }
}
