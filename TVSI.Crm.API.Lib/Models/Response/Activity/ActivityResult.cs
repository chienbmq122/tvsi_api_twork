﻿using System.Globalization;

namespace TVSI.Crm.API.Lib.Models.Response.Activity
{
    /// <summary>
    /// Thông tin hoạt động
    /// </summary>
    public class ActivityResult
    {
        
        /// <summary>
        /// ID activity
        /// </summary>
        public long ActivityId { get; set; }
        
        /// <summary>
        /// Đối tác
        /// </summary>
        public int LeadId { get; set; }
        
        /// <summary>
        /// Tên đối tác
        /// </summary>
        public string? LeadName { get; set; }
        
        /// <summary>
        /// Mã khách hàng
        /// </summary>
        public string? CustCode { get; set; }
        
        /// <summary>
        /// Tên khách hàng
        /// </summary>
        public string? CustName { get; set; }
        
        /// <summary>
        /// Tiêu đề hoạt động
        /// </summary>
        public string? Title { get; set; }
        
        /// <summary>
        /// Phân loại hoạt động
        /// </summary>
        public int Type { get; set; }
        
        /// <summary>
        /// Phân loại hoạt động name
        /// </summary>
        public string? TypeName
        {
            get
            {
                switch (Type)
                {
                    case 1:
                        return "Hẹn trực tiếp";
                    case 2:
                        return "Gọi điện thoại";
                    case 3:
                        return "Gửi Email";
                    case 5:
                        return "Nghỉ phép";
                    case 4:
                    default:
                        return "Khác";
                }
            }
        }
        
        /// <summary>
        /// Phân loại event
        /// </summary>
        public int EventType { get; set; }

        public string EventName
        {
            get
            {
                switch (EventType)
                {
                    case 1:
                        return "Sự kiện";
                    case 2:
                        return "Công việc";
                    default:
                        return "";
                }
            }
        }

        /// <summary>
        /// Ngày bắt đầu
        /// </summary>
        public string StartDate { get; set; }
        
        /// <summary>
        /// Ngày kết thúc
        /// </summary>
        public string EndDate { get; set; }
        
        /// <summary>
        /// Địa điểm
        /// </summary>
        public string? Location { get; set; }
        
        /// <summary>
        /// Mức độ ưu tiên hoạt động
        /// </summary>
        public int Priority { get; set; }

        public string PriorityName {
            get
            {
                switch (Priority)
                {
                    case 1:
                        return "Cao";
                    case 2:
                        return "Trung bình";
                    default:
                        return "Thấp";
                }
            }
        }

        /// <summary>
        /// Tình trạng
        /// </summary>
        public int Status { get; set; }

        ///// <summary>
        ///// Tên tình trạng
        ///// </summary>
        public string? StatusName
        {
            get {
                switch (Status)
                {
                    case 1:
                        return "Chưa lập kế hoạch";
                    case 2:
                        return "Đang xử lý";
                    case 3:
                        return "Hoàn thành";
                    case 4:
                        return "Đang tạm hoãn";
                    case 11:
                        return "Đã lập kết hoạch";
                    case 12:
                        return "Đã tổ chức";
                    case 13:
                        return "Đang tổ chức";
                    case 14:
                        return "Hoàn thành";
                    case 99:
                    default:
                        return "Đã hủy";
                }
            }
        }

        public string? StatusColor
        {
            get
            {
                switch (Status)
                {
                    case 1:
                        return "#879dad";
                    case 2:
                        return "#50bfe0";
                    case 3:
                        return "#5cca33";
                    case 4:
                        return "#879dad";
                    case 11:
                        return "#50bfe0";
                    case 12:
                        return "#5cca33";
                    case 13:
                        return "#50bfe0";
                    case 14:
                        return "#5cca33";
                    case 99:
                    default:
                        return "#e43b3b";
                }
            }
        }

        /// <summary>
        /// Người duyệt Nếu là nhân viên thì mặc định = UserID của nhân viên và ko cho sửa / Trưởng phòng và GĐCN mặc định = UserID đăng nhập và cho phép sửa
        /// </summary>
        public string? Approved { get; set; }
        
        /// <summary>
        /// Assigned Nếu là nhân viên thì mặc định = UserID của nhân viên và ko cho sửa / Trưởng phòng và GĐCN mặc định = UserID đăng nhập và cho phép sửa
        /// </summary>
        public string? Assigned { get; set; }
        
        /// <summary>
        /// Mô tả
        /// </summary>
        public string? Detail { get; set; }
        public int ConfirmStatus { get; set; }
        public string? ConfirmBy { get; set; }
        public string? ConfirmDate { get; set; }
        public string? ConfirmCode { get; set; }
        public string? RejectCode { get; set; }
        public string? RejectReason { get; set; }
        public int ReasonType { get; set; }
        public string? CreatedBy { get; set; }
        public string? CreatedDate { get; set; }
        public string? statusDeadLine
        { 
            get {
                var time = DateTime.Now;
                if (!string.IsNullOrEmpty(EndDate.ToString()))
                {
                    time = DateTime.ParseExact(EndDate, "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture);
                }
                var compare = DateTime.Compare(time, DateTime.Now);
                switch (compare)
                {
                    case -1:
                        return "Quá hạn";
                    case 1:
                        return "Đúng hạn";
                    case 0:
                    default:
                        return "Hạn cuối";
                }
            }
        }

        public string? statusDeadLineColor
        {
            get
            {
                var time = DateTime.Now;
                if (!string.IsNullOrEmpty(EndDate.ToString()))
                {
                    time = DateTime.ParseExact(EndDate, "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture);
                }
                var compare = DateTime.Compare(time, DateTime.Now);
                switch (compare)
                {
                    case -1:
                        return "#e43b3b";
                    case 1:
                        return "#5cca33";
                    case 0:
                    default:
                        return "#cba321";
                }
            }
        }

        public string? Birthday { get; set; }
        public string? Gender { get; set; }
        public string? SaleID { get; set; }
    }

    public class ActivitySql
    {
        public int TotalItem { get; set; }
    
        public string Items { get; set; }
    }

    public class ActivityResponseModel
    {
        public int TotalItem { get; set; }
    
        public List<ActivityResult>? Items { get; set; }
    }
}
