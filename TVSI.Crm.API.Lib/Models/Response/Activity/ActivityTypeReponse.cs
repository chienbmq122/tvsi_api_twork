﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVSI.Crm.API.Lib.Models.Response.Activity
{
    public class ActivityTypeReponse
    {
        public int ActivityTypeID { get; set; }
        public string? ActivityTypeName { get; set; }
    }
}
