﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVSI.Crm.API.Lib.Models.Response.Activity
{
    public class BOActivityDatabase
    {
        public int BOActivityID { get; set; }
        public int EventType { get; set; }
        public string? ActivityName { get; set; }
        public int BranchID { get; set; }
        public string? AssignUser { get; set; }
        public int ActivityType { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int Status { get; set; }
        public int Priority { get; set; }
        public string? Location { get; set; }
        public string? Description { get; set; }
        public string? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string? UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public int ConfirmStatus { get; set; }
        public string? ConfirmBy { get; set; }
        public DateTime? ConfirmDate { get; set; }
        public string? ConfirmCode { get; set; }
        public string? RejectCode { get; set; }
        public string? RejectReason { get; set; }
    }
}
