﻿namespace TVSI.Crm.API.Lib.Models.Response.Activity
{
    /// <summary>
    /// Thông tin hoạt động
    /// </summary>
    public class ActivityResultData
    {
        /// <summary>
        /// Tổng số Item
        /// </summary>
        public int TotalItem { get; set; }

        /// <summary>
        /// Danh sách hoạt động
        /// </summary>
        public List<ActivityResult>? Items { get; set; }
    }
}
