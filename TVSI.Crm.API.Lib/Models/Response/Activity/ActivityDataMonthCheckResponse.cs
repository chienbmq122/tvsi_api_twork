﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVSI.Crm.API.Lib.Models.Response.Activity
{
    /// <summary>
    /// Các ngày có data work trong tháng
    /// </summary>
    public class ActivityDataMonthCheckResponse
    {

        /// <summary>
        /// Ngày làm việc
        /// </summary>
        public string? DateWork { get; set; }

        /// <summary>
        /// Số lượng sự kiến
        /// </summary>
        public int EventCount { get; set; }

        /// <summary>
        /// Số lượng công việc
        /// </summary>
        public int WorkCount { get; set; }
    }

    public class ActivityDataFromQuery
    {
        public DateTime StartDate { get; set; }
        public string DateWork
        {
            get
            {
                return StartDate.ToString("dd/MM/yyyy");
            }
        }
        public int CountActivity { get; set; }
    }
}
