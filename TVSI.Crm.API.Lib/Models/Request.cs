﻿using System.ComponentModel.DataAnnotations;

namespace TVSI.Crm.API.Lib.Models;

public class BaseRequest
{
    /// <summary>
    /// Tên đăng nhập
    /// </summary>
    [Required]
    public string UserName { get; set; }

}