﻿using TVSI.Crm.API.Lib.Models;
using TVSI.Crm.API.Lib.Models.Request.Lead;
using TVSI.Crm.API.Lib.Models.Response.Lead;

namespace TVSI.Crm.API.Lib.Services
{
    public interface ILeadService
    {
        Task<ResponseSingle<LeadDataReponse>> GetLeadList(LeadListRequest model, string lang);

        Task<ResponseSingle<bool>> CreateLead(CreateLeadRequest model, string lang);

        Task<ResponseSingle<LeadActivityReponse>> GetLeadActivity(ListActivityRequest model, string lang);

        Task<ResponseSingle<DetailLeadReponse>> GetDetailLead(DetailLeadRequest model, string lang);

        Task<Response<LeadSrcReponse>> GetSrcLead(BaseRequest model);

        Task<ResponseSingle<bool>> DeleteLead(DeleteLead model);

        Task<ResponseSingle<bool>> UpdateLead(UpdateLeadRequest model);

        Task<ResponseSingle<bool>> CovertLead(ConverLeadToCust model);
    }
}
