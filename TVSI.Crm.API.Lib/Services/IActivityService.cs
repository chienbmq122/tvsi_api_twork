﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TVSI.Crm.API.Lib.Models;
using TVSI.Crm.API.Lib.Models.Request.Activity;
using TVSI.Crm.API.Lib.Models.Response.Activity;

namespace TVSI.Crm.API.Lib.Services
{
    public interface IActivityService
    {
        Task<ResponseSingle<ActivityResponseModel>> GetActivity(ActivityRequestByEventType model);
        Task<ResponseSingle<ActivityResultData>> GetActivityByFiltter(FilterActivityRequest model);
        Task<ResponseSingle<ActivityResult>> GetDetailActivity(DeleteActivityRequest model);
        Task<ResponseSingle<bool>> CreateActivity(CreateActivityRequest model);
        Task<ResponseSingle<bool>> UpdateActivity(UpdateActivityRequest model);
        Task<ResponseSingle<bool>> DeleteActivity(DeleteActivityRequest model);
        Task<ResponseSingle<bool>> UpdateOrDeleteListActivity(UpdateListActivityRequest model);
        Task<ResponseSingle<bool>> ConfirmOrRejectActivity(ConfirmOrRejectRequest model);
        Task<Response<ActivityDataMonthCheckResponse>> GetHisTimeWorkStaffByMonth(ActivityDataMonthRequest model);

        Task<Response<ActivityTypeReponse>> GetActivityType(BaseRequest model);
    }
}
