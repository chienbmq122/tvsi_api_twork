﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TVSI.Crm.API.Lib.Models;
using TVSI.Crm.API.Lib.Models.Request.Comment;
using TVSI.Crm.API.Lib.Models.Response.Comment;

namespace TVSI.Crm.API.Lib.Services
{
    public interface ICommentService
    {
        Task<Response<CommentDBModel>> GetListComment(CommentModelRequest model);
        Task<ResponseSingle<bool>> CreateComent(CreateCommentRequest model);
        Task<ResponseSingle<bool>> CreateReply(CreateReplyRequest model);
        Task<ResponseSingle<TotalRateResponse>> GetTotalReply(GetRateRequest model);

    }
}
