﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TVSI.Crm.API.Lib.Models;
using static TVSI.Crm.API.Lib.Models.Request.Contact.ContactRequest;
using static TVSI.Crm.API.Lib.Models.Response.Contact.ContactRequest;

namespace TVSI.Crm.API.Lib.Services
{
    public interface IContactService
    {
        Task<ResponseSingle<ContactStaffResponse>> GetListContactStaff(ContactStaffRequest model);

        Task<ResponseSingle<ContactCustomerResponse>> GetListContactCust(ContactStaffRequest model);

        Task<ResponseSingle<ContactInfoStaff>> GetInfoContactStaff(ContactinfoStaffRequest model);

        Task<ResponseSingle<ContactInfoLead>> GetInfoContactLead(ContactinfoLeadRequest model);

        Task<ResponseSingle<ContactInfoCustomer>> GetInfoContactCust(ContactinfoCustRequest model);
    }
}
