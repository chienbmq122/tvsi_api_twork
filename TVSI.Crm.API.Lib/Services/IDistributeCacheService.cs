﻿namespace TVSI.Crm.API.Lib.Services;
public interface IDistributeCacheService
{
    Task LoadDataToCache();
    string? GetItemName(string itemCode, string lang = "VI");
}