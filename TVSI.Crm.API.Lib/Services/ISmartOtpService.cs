﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TVSI.Crm.API.Lib.Models;
using TVSI.Crm.API.Lib.Models.Request.SmartOtp;
using TVSI.Crm.API.Lib.Models.Response.SmartOtp;

namespace TVSI.Crm.API.Lib.Services
{
    public interface ISmartOtpService
    {
        Task<ResponseSingle<bool>> CheckDeviceSmartOtp(CheckSmartOtpDeviceRequest model);
        Task<ResponseSingle<SmartOtpModelResponse>> RegisterSmartOtp(RegisterDeviceRequest model);
        Task<ResponseSingle<bool>> ActiveSmartOtp(ActiveOtpRequest model);
        Task<ResponseSingle<bool>> UnRegisterSmartOtp(ActiveOtpRequest model);
        Task<ResponseSingle<bool>> VerifySmartOtp(SmartOtpRequest model);
    }
}
