﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TVSI.Crm.API.Lib.Models;
using TVSI.Crm.API.Lib.Models.Request.Dashboard;
using TVSI.Crm.API.Lib.Models.Response.Dashboard;

namespace TVSI.Crm.API.Lib.Services
{
    public interface IDashboardService
    {
        Task<ResponseSingle<DashboardChartResult>> TradeInfo(DashboardChartRequest model);

        Task<ResponseSingle<DashboardSummaryResult>> TradeTotalHistory(BaseRequest model);

        Task<ResponseSingle<DashboardTradeResult>> TradeTotalInfoHistory(DashboardTradeRequest model);

        Task<ResponseSingle<TotalCustLeadResponse>> TotalCustLead(BaseRequest model);

        Task<ResponseSingle<TotalActitityReponse>> TotalAcitvity(BaseRequest model);
    }
}
