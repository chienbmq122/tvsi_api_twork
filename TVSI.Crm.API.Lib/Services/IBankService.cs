using TVSI.Crm.API.Lib.Models;
using TVSI.Crm.API.Lib.Models.Request.Bank;
using TVSI.Crm.API.Lib.Models.Response.Bank;

namespace TVSI.Crm.API.Lib.Services;

public interface IBankService
{
    Task<Response<BankListResponse>> GetBankList(BankListRequest model);
    Task<Response<BranchListResponse>> GetBranchList(BranchListRequest model);

    Task<ResponseSingle<string>> GetBankName(string? bankNo);
    Task<ResponseSingle<string>> GetSubBranchName(string? bankNo, string? branchNo);
}