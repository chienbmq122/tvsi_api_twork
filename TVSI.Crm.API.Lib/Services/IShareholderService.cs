using TVSI.Crm.API.Lib.Models;
using TVSI.Crm.API.Lib.Models.Request.Shareholder;

namespace TVSI.Crm.API.Lib.Services;

public interface IShareholderService
{
    Task<Response<dynamic>> GetShareholderList(ShareholderListRequest model);
    Task<ResponseSingle<bool>> CreateShareholder(CreateShareholderRequest model);
    Task<ResponseSingle<dynamic>> GetShareholderDetail(ShareholderDetailRequest model);
    Task<ResponseSingle<dynamic>> CheckCustcodeShareholder(string? custcode,string? username);
    Task<Response<dynamic>> GetStockInfoList(BaseRequest model);
    Task<ResponseSingle<bool>> CancelShareholderInfo(CancelShareholderRequest model);
    Task<ResponseSingle<bool>> UpdateShareholder(UpdateShareholderRequest model);


}