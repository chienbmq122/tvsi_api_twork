using TVSI.Crm.API.Lib.Models;
using TVSI.Crm.API.Lib.Models.Request.ManTvsi;
using TVSI.Crm.API.Lib.Models.Response.ManTvsi;

namespace TVSI.Crm.API.Lib.Services;

public interface IManTvsiService
{
    Task<Response<ManTvsiListResponse>> GetManInfoList(BaseRequest model);
    Task<ResponseSingle<ManTvsiDetailResponse>> GetManInfoDetail(ManTvsiRequest model);
}