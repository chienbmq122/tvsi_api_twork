using TVSI.Crm.API.Lib.Models;
using TVSI.Crm.API.Lib.Models.Request.Account;
using TVSI.Crm.API.Lib.Models.Response.Account;
using TVSI.Crm.API.Lib.Models.Response.Stringee;

namespace TVSI.Crm.API.Lib.Services;

public interface IAccountService
{
    Task<Response<dynamic>> GetAccountOpenList(AccountListRequest model);
    Task<Response<dynamic>> GetCustCodeOpenList(BaseRequest model);
    Task<ResponseSingle<dynamic>> GetAccountOpenDetailInfo(AccountOpenDetailRequest model);
    Task<ResponseSingle<bool>> UpdateAccountDetail(UpdateAccountDetailRequest model);
    Task<ResponseSingle<bool>> CancelOpenAccount(CancelOpenAccountRequest model);
    Task<ResponseSingle<bool>> CheckPhoneCardIdAccount(CheckPhoneCardIdAccountRequest model);
    Task<ResponseSingle<CheckCustCodeResponse>> CheckCustomerCode(CheckCustomerCodeRequest model);
    Task<ResponseSingle<bool>> OpenIndividualDomesticAccount(OpenIndividualDomesticAccountRequest model);
    Task<ResponseSingle<dynamic>> GetOpenAccountAdditionalDetail(OpenAccountAdditionalDetailRequest model);
    Task<ResponseSingle<bool>> OpenIndividualDomesticAdditionalAccount(OpenIndividualDomesticAdditionalAccountRequest model);
    Task<Response<dynamic>> GetOpenAccountDebtDocumentList(OpenAccountDebtDocumentListRequest model);
    Task<ResponseSingle<dynamic>> GetOpenAccountDetail(OpenAccountDetailRequest model);
    Task<ResponseSingle<bool>> UpdateAccountStatus(UpdateAccountStatusRequest model);
    Task<ResponseSingle<bool>> UpdateAdditionalAccount(UpdateAdditionalAccountRequest model);
    Task<ResponseSingle<ListCustcodeReponseAccount>> ListCustcode(GetCustcodeRequestAccount model);
    Task<ResponseSingle<StringeeResponse>> GetStringeeToken(BaseRequest model);
}