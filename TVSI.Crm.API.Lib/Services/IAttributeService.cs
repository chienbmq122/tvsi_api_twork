﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TVSI.Crm.API.Lib.Models;
using TVSI.Crm.API.Lib.Models.Response.Attribute;

namespace TVSI.Crm.API.Lib.Services
{
    public interface IAttributeService
    {
        Task<Response<AttributeReponse>> GetAttribute(BaseRequest model);

        Task<Response<ExtendCategoryReponse>> GetExtendCategory(BaseRequest model);
    }
}
