﻿using TVSI.Crm.API.Lib.Models;
using TVSI.Crm.API.Lib.Models.Request.Common;
using TVSI.Crm.API.Lib.Models.Request.Customer;
using TVSI.Crm.API.Lib.Models.Response.Customer;
using TVSI.Crm.API.Lib.Models.Response.Lead;

namespace TVSI.Crm.API.Lib.Services;

public interface ICustomerService
{ 
    Task<Response<CustomerOnlineListResponse>> GetCustomerOnlineList(CustomerOnlineListRequest model, string lang);
    Task<ResponseSingle<CustomerInfoAllResponse>> GetCustomerOnlineDetailInfo(CustomerOnlineDetailInfoRequest model, string lang);
    Task<Response<dynamic>> GetCustomerList(CustomerListRequest model);
    Task<ResponseSingle<dynamic>> GetActionStatusDetail(GetActionStatusDetailRequest model);
    Task<ResponseSingle<dynamic>> GetCustomerDetailInfo(CustomerDetailRequest model);
    Task<ResponseSingle<bool>> CancelCustomerInfo(CancelCustomerInfoRequest model);
    Task<ResponseSingle<bool>> UpdateExtendInfo(UpdateCustomerExtendInfoRequest model);
    Task<ResponseSingle<bool>> UpdateCustomerKycInfo(UpdateKycCustomerInfoRequest model);
    Task<ResponseSingle<bool>> UpdateActionStatus(UpdateCustomerActionStatusRequest model);
    Task<ResponseSingle<bool>> ConvertCustomerToAccount(ConvertCustomerToAccountRequest model);
    Task<ResponseSingle<bool>> CheckValidateCustcode(CheckValidateCustCodeRequest model);
    Task<ResponseSingle<ListCustcodeReponse>> ListCustcode(GetCustcodeRequest model);
    Task<ResponseSingle<LeadActivityReponse>> GetCustomerActivity(ListCustomerActivityRequest model);
    Task<ResponseSingle<CustomerReviewRateStarReponse>> GetCustomerReviews(CustomerReviewsRequest model);
    Task<ResponseSingle<dynamic>> ConfirmEkycRequest(ConfirmEkycRequest model);
}