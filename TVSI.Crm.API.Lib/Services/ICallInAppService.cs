﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TVSI.Crm.API.Lib.Models;
using TVSI.Crm.API.Lib.Models.Request.CallInApp;
using TVSI.Crm.API.Lib.Models.Response.CallInApp;

namespace TVSI.Crm.API.Lib.Services
{
    public interface ICallInAppService
    {
        Task<ResponseSingle<CustomerContactInfoResponse>> GetListCustomerContact(CustomerContactInfoRequest model);

        Task<ResponseSingle<bool>> CreateCallApp(CreateCallAppRequest model);

        Task<ResponseSingle<bool>> UpdateCallApp(UpdateHistoryCallRequest model);

        Task<ResponseSingle<HistoryCallAppInfoResponse>> GetListHistoryCall(GetListHistoryCallRequest model);

        Task<ResponseSingle<HistoryCallAppInfo>> GetDetailCall(GetDetailHistoryCall model);

        Task<ResponseSingle<BookingCallAppResponse>> GetListBookingCall(GetListBookingCallRequest model);

        Task<ResponseSingle<bool>> CreateBookingCall(CreateBookingCallRequest model);

        Task<ResponseSingle<bool>> UpdateBookingCall(UpdateBookingCallRequest model);

        Task<ResponseSingle<bool>> DeleteBookingCall(DeleteBookingCallRequest model);

        Task<ResponseSingle<List<StatusBookingReponse>>> GetListStatus(BaseRequest model);

        Task<ResponseSingle<SaleInfoReponseModel>> GetListSaleInfo(SaleInfoRequestModel model);
    }
}
