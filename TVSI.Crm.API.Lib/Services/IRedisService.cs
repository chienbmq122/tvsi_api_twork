﻿using StackExchange.Redis;

namespace TVSI.Crm.API.Lib.Services;

public interface IRedisService
{
    /// <summary>
    /// Get value from redis and convert to model T
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="key"></param>
    /// <returns></returns>
    T GetKey<T>(string key);

    /// <summary>
    /// Set value to key redis
    /// </summary>
    /// <param name="key"></param>
    /// <param name="value"></param>
    /// <param name="expireMinutes"></param>
    /// <returns></returns>
    bool SetKey(string key, object value, int expireMinutes = 0);

    /// <summary>
    /// Remove key from redis
    /// </summary>
    /// <param name="key"></param>
    /// <returns></returns>
    bool RemoveKey(string key);

    /// <summary>
    /// Get all key from redis database
    /// </summary>
    /// <param name="database"></param>
    /// <param name="host"></param>
    /// <param name="port"></param>
    /// <returns></returns>
    List<string> GetAllKeys(int database);

    /// <summary>
    /// Get all key from redis database that satisfy the condition pattern
    /// </summary>
    /// <param name="database"></param>
    /// <param name="pattern"></param>
    /// <returns></returns>
    List<string> GetAllKeysByPattern(int database, string pattern);

    /// <summary>
    /// Remove all keys that satisfy the condition pattern
    /// </summary>
    /// <param name="pattern"></param>
    /// <returns></returns>
    bool RemoveKeyByPattern(string pattern);

    /// <summary>
    /// Hash model T to redis with hashField is properties name of model
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="key"></param>
    /// <param name="value"></param>
    /// <returns></returns>
    bool HashSet<T>(string key, T value);

    /// <summary>
    /// Hash value to redis. If key does not exist, a new key holding a hash is created. If field already exists in the hash, it is overwritten
    /// </summary>
    /// <param name="key"></param>
    /// <param name="hashKey"></param>
    /// <param name="value"></param>
    /// <returns></returns>
    bool HashSet(string key, string hashKey, string value);

    /// <summary>
    /// Check exist hashKey
    /// </summary>
    /// <param name="key"></param>
    /// <param name="hashKey"></param>
    /// <returns></returns>
    bool HashExists(string key, string hashKey);

    /// <summary>
    /// Returns all fields and values of the hash stored at key.
    /// </summary>
    /// <param name="key"></param>
    /// <returns></returns>
    HashEntry[]? HashGetAll(string key);

    /// <summary>
    /// Return model with properties name is hashFiled and properties value is redis value
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="key"></param>
    /// <returns></returns>
    List<T>? HashGetAll<T>(string key);

    /// <summary>
    /// Returns all values in the hash stored at key.
    /// </summary>
    /// <param name="key"></param>
    /// <returns></returns>
    RedisValue[]? HashValues(string key);

    /// <summary>
    /// Returns the value associated with field in the hash stored at key.
    /// </summary>
    /// <param name="key"></param>
    /// <param name="hashKey"></param>
    /// <returns></returns>
    RedisValue HashGet(string key, string hashKey);

    /// <summary>
    /// Removes the specified fields from the hash stored at key.
    /// </summary>
    /// <param name="key"></param>
    /// <param name="hashKey"></param>
    /// <returns></returns>
    bool HashRemove(string key, string hashKey);
}