using System.Data;
using System.Reflection;
using Dapper;
using Helper.DataAccess.Dapper;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using TVSI.Crm.API.Common;
using TVSI.Crm.API.Common.Enums;
using TVSI.Crm.API.Lib.Models;
using TVSI.Crm.API.Lib.Models.Request.ManTvsi;
using TVSI.Crm.API.Lib.Models.Response.Account;
using TVSI.Crm.API.Lib.Models.Response.ManTvsi;
using TVSI.TWork.API.Common.ExtensionMethods;

namespace TVSI.Crm.API.Lib.Services.Impls;

public class ManTvsiService : BaseService<ManTvsiService>, IManTvsiService
{
    private readonly IDapperHelper _dapper;
    private readonly string _crmDbConn;
    private readonly string _commonDbConn;
    private readonly string _emsDbConn;
    private readonly string _innoDbConn;
    private readonly int _sqlTimeout;

    public ManTvsiService(IConfiguration config, ILogger<ManTvsiService> logger, IDapperHelper dapper): base(config,
        logger)
    {
        var connStr = _config.GetConnectionString("CRMDB_CONNECTION");
        var commonStr = _config.GetConnectionString("COMMONDB_CONNECTION");
        var emsStr = _config.GetConnectionString("EMSDB_CONNECTION");
        var innoStr = _config.GetConnectionString("INNODB_CONNECTION");
        _crmDbConn = connStr;
        _commonDbConn = commonStr;
        _emsDbConn = emsStr;
        _innoDbConn = innoStr;
        _dapper = dapper;
        _sqlTimeout = _config["Timeout:Database"] == null
            ? CommonConstants.SqlServerTimeout
            : int.Parse(_config["Timeout:Database"]);
    }
    public async Task<Response<ManTvsiListResponse>> GetManInfoList(BaseRequest model)
    {
        try
        {
            var result = await _dapper.SelectAsync<ManTvsiListResponse>(_commonDbConn,
                "TVSI_sGET_MANTVSI_LIST", null!, _sqlTimeout);
            var data = new List<ManTvsiListResponse>();
            if (result != null)
            {
                foreach (var item in result)
                {
                    var value = item.ManName?.Split('|');
                    data.Add(new ManTvsiListResponse()
                    { 
                        ManCode = item.ManCode,
                        ManName = value?[0]

                    });
                }
            }
            return new Response<ManTvsiListResponse>
            {
                Code = ((int)ErrorCodeDetail.Success).ErrorCodeFormat(),
                Message = ErrorCodeDetail.Success.ToEnumDescription(),
                Data = data
            };
           
        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
            return new Response<ManTvsiListResponse>
            {
                Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                Message = ErrorCodeDetail.Exception.ToEnumDescription()
            };
        }
    }

    public async Task<ResponseSingle<ManTvsiDetailResponse>> GetManInfoDetail(ManTvsiRequest model)
    {
        try
        {
            var data = new ManTvsiDetailResponse();
            var param = new DynamicParameters();
            param.Add("@ManCode",model.ManCode,DbType.String,ParameterDirection.Input);
            var result = await _dapper.GetAsync<string>(_commonDbConn,
                "TVSI_sGET_MANTVSI_DETAIL", param, _sqlTimeout);
            if (result != null)
            {
                var items = result.Split("|");
                if (items.Length > 0)
                {
                    for (int i = 0; i < items.Length; i++)
                    {
                        if (i == 0)
                            data.ManTvsi = items[i]; 
                        if (i == 1)
                            data.ManPosition = items[i];
                        if (i == 2)
                            data.ManAnPower = items[i];  
                        if (i == 3)
                            data.ManDateAnPower = items[i]; 
                        if (i == 4)
                            data.ManCardId = items[i];
                        if (i == 5)
                            data.ManIssueDate = items[i];  
                        if (i == 6)
                            data.ManIssuePlace = items[i];
                    }
                }   
            }

            return new ResponseSingle<ManTvsiDetailResponse>
            {
                Code = ((int)ErrorCodeDetail.Success).ErrorCodeFormat(),
                Message = ErrorCodeDetail.Success.ToEnumDescription(),
                Data  = data
            };
        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
            return new ResponseSingle<ManTvsiDetailResponse>
            {
                Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                Message = ErrorCodeDetail.Exception.ToEnumDescription()
            };
        }
    }
}