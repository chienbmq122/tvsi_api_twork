using System.Data;
using System.Reflection;
using Dapper;
using Helper.DataAccess.Dapper;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using TVSI.Crm.API.Common;
using TVSI.Crm.API.Lib.Models;
using TVSI.Crm.API.Lib.Models.Request.Bank;
using TVSI.Crm.API.Lib.Models.Response.Account;
using TVSI.Crm.API.Lib.Models.Response.Bank;
using TVSI.TWork.API.Common.Enums;
using TVSI.TWork.API.Common.ExtensionMethods;

namespace TVSI.Crm.API.Lib.Services.Impls;

public class BankService : BaseService<BankService>,IBankService
{
    private readonly IDapperHelper _dapper;
    private readonly string _crmDbConn;
    private readonly string _emsDbConn;
    private readonly string _innoDbConn;
    private readonly int _sqlTimeout;
    
    public BankService(IConfiguration config, ILogger<BankService> logger,IDapperHelper dapper) : base(config, logger)
    {
        var connStr = _config.GetConnectionString("CRMDB_CONNECTION");
        var commonStr = _config.GetConnectionString("COMMONDB_CONNECTION");
        var emsStr = _config.GetConnectionString("EMSDB_CONNECTION");
        var innoStr = _config.GetConnectionString("INNODB_CONNECTION");
        _crmDbConn = connStr;
        _emsDbConn = emsStr;
        _innoDbConn = innoStr;
        
        _dapper = dapper;
        _sqlTimeout = _config["Timeout:Database"] == null
            ? CommonConstants.SqlServerTimeout
            : int.Parse(_config["Timeout:Database"]);
    }
    public async Task<Response<BankListResponse>> GetBankList(BankListRequest model)
    {
        try
        {
            var data = await _dapper.SelectAsync<BankListResponse>(_innoDbConn,
                "BB_TW_GETBANKLIST", null!, _sqlTimeout);
            return new Response<BankListResponse>
            {
                Code = ((int)ErrorCodeDetail.Success).ErrorCodeFormat(),
                Message = ErrorCodeDetail.Success.ToEnumDescription(),
                Data = await _dapper.SelectAsync<BankListResponse>(_innoDbConn,
                    "BB_TW_GETBANKLIST", null!, _sqlTimeout)
            };
        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
            return new Response<BankListResponse>
            {
                Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                Message = ErrorCodeDetail.Exception.ToEnumDescription()
            };
        }
    }

    public async Task<Response<BranchListResponse>> GetBranchList(BranchListRequest model)
    {
        try
        {
            var param = new DynamicParameters();
            param.Add("@BankNo",model.BankNo,DbType.String, ParameterDirection.Input);
            return new Response<BranchListResponse>
            {
                Code = ((int)ErrorCodeDetail.Success).ErrorCodeFormat(),
                Message = ErrorCodeDetail.Success.ToEnumDescription(),
                Data = await _dapper.SelectAsync<BranchListResponse>(_innoDbConn,
                    "BB_TW_GETBRANCHLIST", param, _sqlTimeout)
            };
        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
            return new Response<BranchListResponse>
            {
                Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                Message = ErrorCodeDetail.Exception.ToEnumDescription()
            };
        }
    }

   public async Task<ResponseSingle<string>> GetBankName(string? bankNo)
    {
        try
        {
            var param = new DynamicParameters();
            param.Add("@BankNo", bankNo, DbType.String, ParameterDirection.Input);
            return new ResponseSingle<string>
            {
                Code = ((int)ErrorCodeDetail.Success).ErrorCodeFormat(),
                Message = ErrorCodeDetail.Success.ToEnumDescription(),
                Data = await _dapper.GetAsync<string>(_innoDbConn, "TW_MO_BANKNAME", param, _sqlTimeout)
            };
        }
        catch (Exception ex)
        {
            LogException("BankService",
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
            return new ResponseSingle<string>
            {
                Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                Message = ErrorCodeDetail.Exception.ToEnumDescription()
            };
        }
    }

    public async Task<ResponseSingle<string>> GetSubBranchName(string? bankNo, string? branchNo)
    {
        try
        {
            var param = new DynamicParameters();
            param.Add("@BankNo", bankNo, DbType.String, ParameterDirection.Input);
            param.Add("@BranchNo", branchNo, DbType.String, ParameterDirection.Input);
            return new ResponseSingle<string>
            {
                Code = ((int)ErrorCodeDetail.Success).ErrorCodeFormat(),
                Message = ErrorCodeDetail.Success.ToEnumDescription(),
                Data = await _dapper.GetAsync<string>(_innoDbConn, "TW_MO_SUBBRANCHNAME", param, _sqlTimeout)
            };
        }
        catch (Exception ex)
        {
            LogException("BankService",
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
            return new ResponseSingle<string>
            {
                Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                Message = ErrorCodeDetail.Exception.ToEnumDescription()
            };
        }
    }
}