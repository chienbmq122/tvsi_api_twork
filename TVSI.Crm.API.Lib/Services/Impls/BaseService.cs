﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Serilog;

namespace TVSI.Crm.API.Lib.Services.Impls;

public abstract class BaseService<T> where T : class
{
    protected readonly IConfiguration _config;
    protected readonly ILogger<T> _logger;


    protected BaseService(IConfiguration config,ILogger<T> logger)
    {
        _logger = logger;
        _config = config;
    }

    #region "Logging"
    protected void LogInformation(string userName, string message)
    {
        if (string.IsNullOrEmpty(userName))
            userName = "Other";

        Log.ForContext("Name", DateTime.Now.ToString("yyyyMMdd") + "\\" + userName).Information(message);
    }

    protected void LogWarning(string userName, string message)
    {
        if (string.IsNullOrEmpty(userName))
            userName = "Other";

        Log.ForContext("Name", DateTime.Now.ToString("yyyyMMdd") + "\\" + userName).Warning(message);
    }

    protected void LogException(string userName, string message)
    {
        Log.ForContext("Name", "exception").Error(message);
        if (!string.IsNullOrEmpty(userName))
        {
            Log.ForContext("Name", DateTime.Now.ToString("yyyyMMdd") + "\\" + userName).Error(message);
        }
    }
    #endregion
}