using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Globalization;
using System.Reflection;
using Dapper;
using Helper.DataAccess.Dapper;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using TVSI.Crm.API.Common;
using TVSI.Crm.API.Common.ConstParams;
using TVSI.Crm.API.Common.Enums;
using TVSI.Crm.API.Lib.Models;
using TVSI.Crm.API.Lib.Models.Request.Account;
using TVSI.Crm.API.Lib.Models.Response.Account;
using TVSI.Crm.API.Lib.Models.Response.Customer;
using TVSI.Crm.API.Lib.Models.Response.Stringee;
using TVSI.Crm.API.Lib.Utilities;
using TVSI.TWork.API.Common.ExtensionMethods;
using ExtendCust = TVSI.Crm.API.Lib.Models.Request.Account.ExtendCust;
using ExtendInfoResponse = TVSI.Crm.API.Lib.Models.Response.Account.ExtendInfoResponse;

namespace TVSI.Crm.API.Lib.Services.Impls;

public class AccountService : BaseService<AccountService>, IAccountService
{
    private readonly IDapperHelper _dapper;
    private readonly string _crmDbConn;
    private readonly string _commonDbConn;
    private readonly string _emsDbConn;
    private readonly string _innoDbConn;
    private readonly string _bmsDbConn;
    private readonly string _signatureDbConn;
    private readonly string _centralizeDBConn;

    private readonly string _apiBond;
    private readonly string _urlHome;
    private readonly int _sqlTimeout;
    private readonly IBankService _bankService;

    public AccountService(IConfiguration config, ILogger<AccountService> logger, IDapperHelper dapper,
        IBankService bankService) : base(config,
        logger)
    {
        var connStr = _config.GetConnectionString("CRMDB_CONNECTION");
        var commonStr = _config.GetConnectionString("COMMONDB_CONNECTION");
        var emsStr = _config.GetConnectionString("EMSDB_CONNECTION");
        var innoStr = _config.GetConnectionString("INNODB_CONNECTION");
        var bmsStr = _config.GetConnectionString("BMS_CONNECTION");
        var signatureStr = _config.GetConnectionString("SIGNATUREDB_CONNECTION");
        var centralizeStr = _config.GetConnectionString("CENTRALIZEDB_CONNECTION");

        var apiBond = _config["Tvsi:BOND_API"];
        var urlHome = _config["Tvsi:Url_Home"];


        _crmDbConn = connStr;
        _commonDbConn = commonStr;
        _emsDbConn = emsStr;
        _innoDbConn = innoStr;
        _bmsDbConn = bmsStr;
        _signatureDbConn = signatureStr;
        _centralizeDBConn = centralizeStr;

        _urlHome = urlHome;
        _apiBond = apiBond;
        _bankService = bankService;
        _dapper = dapper;
        _sqlTimeout = _config["Timeout:Database"] == null
            ? CommonConstants.SqlServerTimeout
            : int.Parse(_config["Timeout:Database"]);
    }

    public async Task<Response<dynamic>> GetAccountOpenList(AccountListRequest model)
    {
        try
        {
            var param = new DynamicParameters();
            var start = (model.PageIndex - 1) * model.PageSize;
            param.Add("@UserName", model.UserName, DbType.String, ParameterDirection.Input);
            param.Add("@Custcode", model.Custcode, DbType.String, ParameterDirection.Input);
            param.Add("@FullName", model.FullName, DbType.String, ParameterDirection.Input);
            param.Add("@AccountStatus", model.AccountStatus, DbType.String, ParameterDirection.Input);
            param.Add("@ProfileStatus", model.ProfileStatus, DbType.String, ParameterDirection.Input);
            param.Add("@Start", start, DbType.Int32, ParameterDirection.Input);
            param.Add("@PageSize", model.PageSize, DbType.Int32, ParameterDirection.Input);
            return new Response<dynamic>
            {
                Code = ((int)ErrorCodeDetail.Success).ErrorCodeFormat(),
                Message = ErrorCodeDetail.Success.ToEnumDescription(),
                Data = await _dapper.SelectAsync<dynamic>(_commonDbConn,
                    "TVSI_sGetAccountOpenList", param, _sqlTimeout)
            };
        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
            return new Response<dynamic>
            {
                Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                Message = ErrorCodeDetail.Exception.ToEnumDescription()
            };
        }
    }


    public async Task<Response<dynamic>> GetCustCodeOpenList(BaseRequest model)
    {
        var param = new DynamicParameters();
        param.Add("@UserName", model.UserName, DbType.String, ParameterDirection.Input);

        return new Response<dynamic>
        {
            Code = ((int)ErrorCodeDetail.Success).ErrorCodeFormat(),
            Message = ErrorCodeDetail.Success.ToEnumDescription(),
            Data = await _dapper.SelectAsync<dynamic>(_commonDbConn,
                "TVSI_sGET_CUSTCODE_AND_ID_OPENACCOUNT", param, _sqlTimeout)
        };
    }

    public async Task<ResponseSingle<dynamic>> GetAccountOpenDetailInfo(AccountOpenDetailRequest model)
    {
        try
        {
            var param = new DynamicParameters();
            param.Add("@UserName", model.UserName, DbType.String, ParameterDirection.Input);
            param.Add("@Id", model.Id, DbType.Int32, ParameterDirection.Input);
            var data = await _dapper.GetAsync<AccountOpenDetailResponse>(_commonDbConn,
                "TVSI_sGetAccountOpenDetail", param, _sqlTimeout);

            if (data != null)
            {
                if (!string.IsNullOrEmpty(data.SaleID))
                {
                    var paramName = new DynamicParameters();
                    paramName.Add("@SaleID", data.SaleID, DbType.String, ParameterDirection.Input);
                    data.SaleName = await _dapper.GetAsync<string>(_emsDbConn, "TVSI_sGetSaleName", paramName, _sqlTimeout);
                }

                if (data.OpenSource != null && data.OpenSource > 0)
                {
                    //if ((data.OpenSource == 2 || data.OpenSource == 3 || data.OpenSource == 7) && !string.IsNullOrEmpty(data.Custcode))
                    //{
                    //    var paramiImg = new DynamicParameters();
                    //    paramiImg.Add("@CustCode", data.Custcode, DbType.String, ParameterDirection.Input);
                    //    paramiImg.Add("@UserName", model.UserName, DbType.String, ParameterDirection.Input);
                    //    var dataImage = await _dapper.GetAsync<ImageDataByte>(_commonDbConn,
                    //        "GET_IMAGE_CARDID_CUSTOMER", paramiImg, _sqlTimeout); // lấy data ảnh từ commondb
                    //    if (dataImage != null && dataImage.FileDataBack.Length > 0 && dataImage.FileDataFront.Length > 0)
                    //    {
                    //        data.ImageFront = Convert.ToBase64String(dataImage.FileDataFront);
                    //        data.ImageBack = Convert.ToBase64String(dataImage.FileDataBack);
                    //    }
                    //}
                    //if (data.OpenSource == 4)
                    //{

                    //    try
                    //    {
                    //        var urlFront =  _urlHome + "EkycCustomer/" + model.Id + "/" + model.Id + "_Front.jpg";
                    //        var urlBack =  _urlHome + "EkycCustomer/" + model.Id + "/" +  model.Id + "_Back.jpg";
                    //        var urlFace =  _urlHome + "EkycCustomer/" + model.Id + "/" +  model.Id + "_Face.jpg";

                    //        var frontByte = GetImageByte.GetImageByteUrl(urlFront);
                    //        var backByte = GetImageByte.GetImageByteUrl(urlBack);
                    //        var faceByte = GetImageByte.GetImageByteUrl(urlFace);
                    //        if (frontByte != null && frontByte.Length > 0 && backByte != null && backByte.Length > 0 && faceByte != null && faceByte.Length > 0)
                    //        {
                    //            data.ImageFront = Convert.ToBase64String(frontByte);
                    //            data.ImageBack = Convert.ToBase64String(backByte);
                    //            data.ImageFace = Convert.ToBase64String(faceByte);
                    //        }
                    //    }
                    //    catch (Exception ex)
                    //    {
                    //        LogException(model.UserName,
                    //            $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
                    //    }

                    //} 

                    //// to be continued
                    //if (data.OpenSource == 6 && !string.IsNullOrEmpty(data.CardId))
                    //{
                    //    try
                    //    {
                    //        var paramImage = new
                    //        {
                    //            CardId = data.CardId
                    //        };
                    //        var responseData = CallApiHelper.GetImageCardId(data.CardId,paramImage,_apiBond);
                    //        if (responseData != null 
                    //            && !string.IsNullOrEmpty(responseData.RetData.Front_Image) 
                    //            && !string.IsNullOrEmpty(responseData.RetData.Back_Image) 
                    //            && !string.IsNullOrEmpty(responseData.RetData.Face_Image))
                    //        {
                    //            data.ImageFront = responseData.RetData.Front_Image;
                    //            data.ImageBack = responseData.RetData.Back_Image;
                    //            data.ImageFace = responseData.RetData.Face_Image;
                    //        }
                    //    }
                    //    catch (Exception ex)
                    //    {
                    //        LogException(model.UserName,
                    //            $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
                    //    }
                    //}

                    // 20220929 CuongLM Add Xu ly du lieu anh tap trung
                    // BEGIN
                    try
                    {
                        var imageDataList = await _dapper.SelectAsync<ImageFileData>(_signatureDbConn, "TVSI_sGET_IMAGE_DATA", 
                            new DynamicParameters(new { CustCode = data.Custcode }), _sqlTimeout);
                        if (imageDataList != null)
                        {
                            foreach (var item in imageDataList)
                            {
                                if (item.Type == 1)
                                    data.ImageFront = Convert.ToBase64String(item.FileData);

                                if (item.Type == 2)
                                    data.ImageBack = Convert.ToBase64String(item.FileData);

                                if (item.Type == 3)
                                    data.ImageFace = Convert.ToBase64String(item.FileData);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        LogException(model.UserName,
                                    $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
                    }
                    // END
                }
                
                

                var paramExtend = new DynamicParameters();
                paramExtend.Add("@Custcode", model.Custcode, DbType.String, ParameterDirection.Input);
                var dataExtend = await _dapper.SelectAsync<ExtendInfoResponse>(_crmDbConn,
                    "TVSI_sGetExtendAccountInfo", paramExtend, _sqlTimeout);
                if (dataExtend != null && dataExtend.Any())
                {
                    foreach (var item in dataExtend)
                    {
                        data.ExtendCust?.Add(new ExtendCategory()
                        {
                            Value = item.CategoryId,
                            CategoryType = item.CategoryType
                        });

                    }
                }

                var paramShareHolder = new DynamicParameters();
                paramShareHolder.Add("@Custcode", model.Custcode, DbType.String, ParameterDirection.Input);
                var dataShareHolder = await _dapper.SelectAsync<ShareHolderInfoResponsive>(_crmDbConn,
                    "TVSI_sGET_SHAREHOLDER_INFO_CUSTOMER", paramShareHolder, _sqlTimeout);
                if (dataShareHolder?.Count() > 0)
                {
                    data.IsShareHolder = true;
                    List<ShareHolderInfoResponsive> lstShareHolder = new List<ShareHolderInfoResponsive>();
                    foreach (var item in dataShareHolder)
                    {
                        lstShareHolder.Add(item);
                    }
                    data.ShareHolderInfo = lstShareHolder;
                }
                else
                {
                    data.IsShareHolder = false;
                }
            }

            return new ResponseSingle<dynamic>
            {
                Code = ((int)ErrorCodeDetail.Success).ErrorCodeFormat(),
                Message = ErrorCodeDetail.Success.ToEnumDescription(),
                Data = data
            };
        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
            return new ResponseSingle<dynamic>
            {
                Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                Message = ErrorCodeDetail.Exception.ToEnumDescription()
            };
        }
    }

    public async Task<ResponseSingle<bool>> UpdateAccountDetail(UpdateAccountDetailRequest model)
    {
        try
        {
            var paramAccount = new DynamicParameters();
            paramAccount.Add("@UserName", model.UserName, DbType.String, ParameterDirection.Input);
            paramAccount.Add("@Id", model.Id, DbType.Int32, ParameterDirection.Input);
            var data = await _dapper.GetAsync<AccountOpenDetailResponse>(_commonDbConn,
                "TVSI_sGetAccountOpenDetail", paramAccount, _sqlTimeout);

            var description = "HĐ thường";
            if (data != null)
            {
                var addMargin = model.AccountMargin ? "1" : "0";
                if (addMargin.Equals("1"))
                    description += ", Margin";

                var branchNo = CommonConstants.Branchiddefault;
                var branchName = CommonConstants.Branchnamedefault;

                var paramBranch = new DynamicParameters();
                paramBranch.Add("@SaleID", data.SaleID, DbType.String, ParameterDirection.Input);
                var branchInfo =
                    await _dapper.GetAsync<BranchInfoResponse>(_emsDbConn, "TVSI_sGetBranchInfo", paramBranch,
                        _sqlTimeout);
                if (branchInfo != null)
                {
                    if (!string.IsNullOrEmpty(branchInfo.BranchName))
                        branchName = branchInfo.BranchName;
                    if (!string.IsNullOrEmpty(branchInfo.BranchNo))
                        branchNo = branchInfo.BranchNo;
                }

                var serviceRegister = CommonConstants.BankNotRegister;
                var transferType = "";

                var bank01 = CommonConstants.BankNotRegister;
                var bank02 = CommonConstants.BankNotRegister;
                var bank03 = CommonConstants.BankNotRegister;
                var bank04 = CommonConstants.BankNotRegister;

                for (int i = 0; i < model.BankInfo?.Count; i++)
                {
                    if (i <= 4)
                    {
                        if (i == 0)
                            if (!string.IsNullOrEmpty(model.BankInfo?[i].BankNo))
                                bank01 = 0;
                        if (i == 1)
                            if (!string.IsNullOrEmpty(model.BankInfo?[i].BankNo))
                                bank02 = 0;
                        if (i == 2)
                            if (!string.IsNullOrEmpty(model.BankInfo?[i].BankNo))
                                bank03 = 0;
                        if (i == 3)
                            if (!string.IsNullOrEmpty(model.BankInfo?[i].BankNo))
                                bank04 = 0;
                    }
                }

                if (bank01 == 0 || bank02 == 0 || bank03 == 0 || bank04 == 0)
                {
                    serviceRegister = 0;
                    transferType += 1;
                }


                // trạng thái dv kênh thông tin
                var ChannelStatusInfo = CommonConstants.BankNotRegister;
                
                
                DateTime? birthDay = null;
                if (!string.IsNullOrEmpty(model.BirthDay))
                    birthDay =  DateTime.ParseExact(model.BirthDay!, CommonConstants.DateFormat, CultureInfo.CurrentCulture);
                DateTime? issueDate = null;
                if (!string.IsNullOrEmpty(model.IssueDate))
                     issueDate = DateTime.ParseExact(model.IssueDate! , CommonConstants.DateFormat, CultureInfo.CurrentCulture);
                DateTime? manIssueDate = null;
                if (!string.IsNullOrEmpty(model.ManIssueDate))
                    manIssueDate = DateTime.ParseExact(model.ManIssueDate!, CommonConstants.DateFormat, CultureInfo.CurrentCulture);
                DateTime? manDateAnPower = null;
                if (!string.IsNullOrEmpty(model.ManDateAnPower))
                    manDateAnPower = DateTime.ParseExact(model.ManDateAnPower!, CommonConstants.DateFormat, CultureInfo.CurrentCulture);
                
                var param = new DynamicParameters();
                param.Add("@Id", model.Id, DbType.Int32, ParameterDirection.Input); // id
                param.Add("@UserName", model.UserName, DbType.String, ParameterDirection.Input); // UserName
                param.Add("@FullName", model.FullName?.ToTitleCase(), DbType.String,
                    ParameterDirection.Input); // họ tên
                param.Add("@BirthDay", birthDay, DbType.DateTime, ParameterDirection.Input); // ngày sinh
                param.Add("@Gender", model.Gender, DbType.Int32, ParameterDirection.Input); // giói tính
                param.Add("@CardId", model.CardId, DbType.String, ParameterDirection.Input); // số cmt/cccd
                param.Add("@IssueDate", issueDate, DbType.DateTime, ParameterDirection.Input); // ngày cấp cmt/cccd
                param.Add("@IssuePlace", model.IssuePlace, DbType.String, ParameterDirection.Input); // nơi cấp cmt/cccd
                param.Add("@Address", model.Address, DbType.String, ParameterDirection.Input); // địa chỉ liên hệ
                param.Add("@NationalityName", Nationality.NationalityName, DbType.String,
                    ParameterDirection.Input); // quốc tịch
                param.Add("@NationalityCode", Nationality.NationalityCode, DbType.String,
                    ParameterDirection.Input); // quốc tịch
                param.Add("@ManTvsi", model.ManTvsi ?? null, DbType.String, ParameterDirection.Input); // đại diện tvsi
                param.Add("@ManPosition", model.ManPosition ?? null, DbType.String, ParameterDirection.Input); // vị trí
                param.Add("@ManAnPower", model.ManAnPower ?? null, DbType.String, ParameterDirection.Input); // giấy ủy quền
                param.Add("@ManDateAnPower", manDateAnPower, DbType.DateTime,
                    ParameterDirection.Input); // ngày cấp giấy ủy quyền
                param.Add("@ManCardId", model.ManCardId ?? null, DbType.String, ParameterDirection.Input); // số cmt/cccd
                param.Add("@ManIssueDate", manIssueDate, DbType.DateTime,
                    ParameterDirection.Input); // ngày cấp cmt/cccd
                param.Add("@ManIssuePlace", model.ManIssuePlace ?? null, DbType.String,
                    ParameterDirection.Input); // nơi cấp cmt /cccd
                param.Add("@ContractAdd", addMargin, DbType.String, ParameterDirection.Input); // HĐ bổ sung
                param.Add("@TradingResultMethod", model.TradingResultMethod, DbType.String,
                    ParameterDirection.Input); // Phương thức nhận KQ giao dịch
                param.Add("@TransferType", transferType, DbType.String,
                    ParameterDirection.Input); // Dịch vụ chuyển tiền
                param.Add("@Phone_01", model.Phone01, DbType.String, ParameterDirection.Input); // SĐT 01 
                param.Add("@Phone_02", model.Phone02, DbType.String, ParameterDirection.Input); // SĐT 02 
                param.Add("@Email", model.Email, DbType.String, ParameterDirection.Input); // Email
                for (int i = 0; i < model.BankInfo?.Count; i++)
                {
                    if (i < 4)
                    {
                        param.Add("@BankAccountName_0" + (i + 1), model.BankInfo?[i].BankAccountName, DbType.String,
                            ParameterDirection.Input); // tên chủ tk ngân hàng
                        param.Add("@BankAccountNo_0" + (i + 1), model.BankInfo?[i].BankAccountNo, DbType.String,
                            ParameterDirection.Input); // số tk ngân hàng

                        if (!string.IsNullOrEmpty(model.BankInfo?[i].BankNo))
                            param.Add("@BankName_0" + (i + 1), // tên NH
                                _bankService.GetBankName(model.BankInfo?[i].BankNo).Result.Data ?? "", DbType.String,
                                ParameterDirection.Input); // tên ngân hàng
                        if (!string.IsNullOrEmpty(model.BankInfo?[i].BankNo) &&
                            !string.IsNullOrEmpty(model.BankInfo?[i].SubBranchNo))
                            param.Add("@SubBranchName_0" + (i + 1), // tên nhánh NH
                                _bankService.GetSubBranchName(model.BankInfo?[i].BankNo, model.BankInfo?[i].SubBranchNo)
                                    .Result.Data ?? "", DbType.String, ParameterDirection.Input); // tên chi nhánh

                        param.Add("@SubBranchNo_0" + (i + 1), model.BankInfo?[i].SubBranchNo, DbType.String,
                            ParameterDirection.Input); // mã chi nhánh
                        param.Add("@BankNo_0" + (i + 1), model.BankInfo?[i].BankNo, DbType.String,
                            ParameterDirection.Input); // mã NH
                    }
                }

                param.Add("@InfoUS01", model.Info01 ? 1 : 0, DbType.Int32, ParameterDirection.Input); // thông tin us
                param.Add("@InfoUS02", model.Info02 ? 1 : 0, DbType.Int32, ParameterDirection.Input); // thông tin us
                param.Add("@InfoUS03", model.Info03 ? 1 : 0, DbType.Int32, ParameterDirection.Input); // thông tin us
                param.Add("@InfoUS04", model.Info04 ? 1 : 0, DbType.Int32, ParameterDirection.Input); // thông tin us
                param.Add("@InfoUS05", model.Info05 ? 1 : 0, DbType.Int32, ParameterDirection.Input); // thông tin us
                param.Add("@InfoUS06", model.Info06 ? 1 : 0, DbType.Int32, ParameterDirection.Input); // thông tin us
                param.Add("@InfoUS07", model.Info07 ? 1 : 0, DbType.Int32, ParameterDirection.Input); // thông tin us

                if ((data.AccountStatus!.Equals(AccountStatusConst.WaitingBuMen) && model.ContractStatus == 2)
                    || (data.AccountStatus!.Equals(AccountStatusConst.Reject) && model.ContractStatus == 2)
                    || model.ContractStatus == 2)
                {
                    param.Add("@AccountStatus", AccountStatusConst.WaitingBuMen, DbType.String,
                        ParameterDirection.Input);
                    param.Add("@ProfileStatus", ProfileStatusConst.ProfileDebt, DbType.String,
                        ParameterDirection.Input);
                }
                else
                {
                    param.Add("@AccountStatus", AccountStatusConst.Process, DbType.String, ParameterDirection.Input);
                    param.Add("@ProfileStatus", ProfileStatusConst.Process, DbType.String, ParameterDirection.Input);
                }

                param.Add("@ServiceStatus", serviceRegister, DbType.Int32,
                    ParameterDirection.Input); // trạng thái dịch vụ
                param.Add("@Bank01Status", bank01, DbType.Int32, ParameterDirection.Input); // trạng thái ngân hàng 1
                param.Add("@Bank02Status", bank02, DbType.Int32, ParameterDirection.Input); // trạng thái ngân hàng 2
                param.Add("@Bank03Status", bank03, DbType.Int32, ParameterDirection.Input); // trạng thái ngân hàng 3
                param.Add("@Bank04Status", bank04, DbType.Int32, ParameterDirection.Input); // trạng thái ngân hàng 4
                param.Add("@ChannelStatusInfo", ChannelStatusInfo, DbType.Int32,
                    ParameterDirection.Input); // trạng thái kênh thông tin
                param.Add("@Description", description, DbType.String, ParameterDirection.Input); // Mô tả
                param.Add("@BranchID", branchNo, DbType.String, ParameterDirection.Input); // mã chi nhánh
                param.Add("@BranchName", branchName, DbType.String, ParameterDirection.Input); // tên chi nhánh
                param.Add("@Result", null, DbType.Int32, ParameterDirection.Output); // OUTPUT

                await _dapper.ExecuteAsync(_commonDbConn, "TVSI_sUpdateAccountDetailInfo", param,
                    _sqlTimeout); // Cập nhật thông tin người dùng
                var result = param.Get<int?>("@Result");
                var paramDebtGe = new DynamicParameters();
                paramDebtGe.Add("@Id", model.Id, DbType.Int32, ParameterDirection.Input); // id
                var dataDebtFs = await _dapper.GetAsync<FsDebt>(_commonDbConn, "TVSI_sGET_CONTRACT_FS_INFO",
                    paramDebtGe,
                    _sqlTimeout); // cập nhật dữ liệu ghi nợ margin 
                if (dataDebtFs != null)
                {
                    if (model.ContractStatus == 2 && model.AccountMargin)
                    {
                        var paramDebtUd = new DynamicParameters();
                        paramDebtUd.Add("@Id", model.Id, DbType.Int32, ParameterDirection.Input); // Id
                        paramDebtUd.Add("@CreditLimit", model.CreditLimit, DbType.Int32,
                            ParameterDirection.Input); // Hạn mức dư nợ
                        paramDebtUd.Add("@DebtTerm", model.DebtTerm, DbType.Int64,
                            ParameterDirection.Input); // Thời gian ghi nợ
                        paramDebtUd.Add("@DepositRate", model.DepositRate, DbType.Int32,
                            ParameterDirection.Input); // Ty lệ ký quỹ
                        paramDebtUd.Add("@Result", dbType: DbType.Int32,
                            direction: ParameterDirection.Output); // OUTPUT
                        await _dapper.ExecuteAsync(_commonDbConn, "TVSI_sUPDATE_CONTRACT_FS_INFO", paramDebtUd,
                            _sqlTimeout); // cập nhật dữ liệu ghi nợ margin 
                        var debtStatus = paramDebtUd.Get<int>("@Result");
                        if (debtStatus == -1)
                            LogInformation(model.UserName,
                                $"Cập nhật thông tin dữ liệu ghi nợ lỗi id {model.Id},CreditLimit{model.CreditLimit},DebtTerm{model.DebtTerm},DepositRate{model.DepositRate}");
                        if (model.ImageFront != null && model.ImageBack != null)
                        {
                            byte[] dataCardIdImage = Convert.FromBase64String(model.ImageFront!);
                            byte[] dataCardIdBackImage = Convert.FromBase64String(model.ImageBack!);
                            var cardIdFileName = model.Id + ".jpg";
                            var cardIdBackFileName = model.Id + "_ms.jpg";

                            var paramImageInsert = new DynamicParameters();
                            paramImageInsert.Add("@OpenId", model.Id, DbType.Int64, ParameterDirection.Input);
                            paramImageInsert.Add("@CustomerCode", model, DbType.String, ParameterDirection.Input);
                            paramImageInsert.Add("@FileName", cardIdFileName, DbType.String, ParameterDirection.Input);
                            paramImageInsert.Add("@FileData", dataCardIdImage, DbType.Binary, ParameterDirection.Input);
                            paramImageInsert.Add("@FileBackName", cardIdBackFileName, DbType.String, ParameterDirection.Input);
                            paramImageInsert.Add("@FileBackData", dataCardIdBackImage, DbType.Binary, ParameterDirection.Input);
                            paramImageInsert.Add("@CreateBy", model.UserName, DbType.String, ParameterDirection.Input);
                            paramImageInsert.Add("@Result", dbType: DbType.Int32, direction: ParameterDirection.Output);

                            await _dapper.ExecuteAsync(_commonDbConn, "TVSI_sOpen_Account_CardId_Image", paramImageInsert,
                                _sqlTimeout);
                            var iInsertImage = paramImageInsert.Get<int>("@Result");
                            if (iInsertImage == -1)
                                LogInformation(model.UserName,
                                    $"Insert thông tin ảnh cmnd lỗi id { model.Id}, CustomerCode {model.CustCode}");
                            else
                            {
                                AddCentralizeImageDB(model.Id, 7, model.CustCode, model.UserName, cardIdFileName,
                                    model.ImageFront, cardIdBackFileName, model.ImageBack);
                            }
                        }
                    }
                    else
                    {
                        var paramDebtDel = new DynamicParameters();
                        paramDebtDel.Add("@Id", model.Id, DbType.Int32, ParameterDirection.Input); // Id
                        paramDebtDel.Add("@Result", dbType: DbType.Int32,
                            direction: ParameterDirection.Output); // OUTPUT
                        await _dapper.ExecuteAsync(_commonDbConn, "TVSI_sDELETE_CONTRACT_FS_INFO", paramDebtDel,
                            _sqlTimeout); // xoa dữ liệu ghi nợ margin 
                        var debtStatus = paramDebtDel.Get<int>("@Result");
                        if (debtStatus == -1)
                            LogInformation(model.UserName,
                                $"Xóa thông tin dữ liệu ghi nợ lỗi id {model.Id},CreditLimit{model.CreditLimit},DebtTerm{model.DebtTerm},DepositRate{model.DepositRate}");
                    }
                }
                else
                {
                    if (model.ContractStatus == 2 && model.AccountMargin)
                    {
                        var paramDebtInsert = new DynamicParameters();
                        paramDebtInsert.Add("@Id", model.Id, DbType.Int32, ParameterDirection.Input); // Id
                        paramDebtInsert.Add("@CreditLimit", model.CreditLimit, DbType.Decimal,
                            ParameterDirection.Input); // Hạn mức dư nợ
                        paramDebtInsert.Add("@DebtTerm", model.DebtTerm, DbType.Int32,
                            ParameterDirection.Input); // Thời gian ghi nợ
                        paramDebtInsert.Add("@DepositRate", model.DepositRate, DbType.Double,
                            ParameterDirection.Input); // Ty lệ ký quỹ
                        paramDebtInsert.Add("@Result", dbType: DbType.Int32,
                            direction: ParameterDirection.Output); // OUTPUT
                        await _dapper.ExecuteAsync(_commonDbConn, "TVSI_sINSERT_CONTRACT_FS_INFO", paramDebtInsert,
                            _sqlTimeout); // Insert du liệu ghi nợ margin 
                        var debtStatus = paramDebtInsert.Get<int>("@Result");
                        if (debtStatus == -1)
                            LogInformation(model.UserName,
                                $"Insert thông tin dữ liệu ghi nợ lỗi id {model.Id},CreditLimit{model.CreditLimit},DebtTerm{model.DebtTerm},DepositRate{model.DepositRate}");
                        if (model.ImageFront != null && model.ImageBack != null)
                        {
                            byte[] dataCardIdImage = Convert.FromBase64String(model.ImageFront!);
                            byte[] dataCardIdBackImage = Convert.FromBase64String(model.ImageBack!);
                            var cardIdFileName = model.Id + ".jpg";
                            var cardIdBackFileName = model.Id + "_ms.jpg";

                            var paramImageInsert = new DynamicParameters();
                            paramImageInsert.Add("@OpenId", model.Id, DbType.Int64, ParameterDirection.Input);
                            paramImageInsert.Add("@CustomerCode", model, DbType.String, ParameterDirection.Input);
                            paramImageInsert.Add("@FileName", cardIdFileName, DbType.String, ParameterDirection.Input);
                            paramImageInsert.Add("@FileData", dataCardIdImage, DbType.Binary, ParameterDirection.Input);
                            paramImageInsert.Add("@FileBackName", cardIdBackFileName, DbType.String, ParameterDirection.Input);
                            paramImageInsert.Add("@FileBackData", dataCardIdBackImage, DbType.Binary, ParameterDirection.Input);
                            paramImageInsert.Add("@CreateBy", model.UserName, DbType.String, ParameterDirection.Input);
                            paramImageInsert.Add("@Result", dbType: DbType.Int32, direction: ParameterDirection.Output);

                            await _dapper.ExecuteAsync(_commonDbConn, "TVSI_sOpen_Account_CardId_Image", paramImageInsert,
                                _sqlTimeout);
                            var iInsertImage = paramImageInsert.Get<int>("@Result");
                            if (iInsertImage == -1)
                                LogInformation(model.UserName,
                                    $"Insert thông tin ảnh cmnd lỗi id { model.Id}, CustomerCode {model.CustCode}");
                            else
                            {
                                AddCentralizeImageDB(model.Id, 7, model.CustCode, model.UserName, cardIdFileName,
                                    model.ImageFront, cardIdBackFileName, model.ImageBack);
                            }
                        }

                    }
                }

                var paramGetExtend = new DynamicParameters();
                paramGetExtend.Add("@Custcode", data.Custcode, DbType.String, ParameterDirection.Input);
                var extedData = await _dapper.SelectAsync<string>(_crmDbConn, "TVSI_sGET_INFO_EXTEND", paramGetExtend,
                    _sqlTimeout);
                if (extedData != null && extedData.Any())
                {
                    // DELETE thông tin mở rộng.
                    var paramDelExtend = new DynamicParameters();
                    paramDelExtend.Add("@Custcode", data.Custcode, DbType.String, ParameterDirection.Input);
                    paramDelExtend.Add("@Result", null, DbType.Int32, ParameterDirection.Output);
                    await _dapper.ExecuteAsync(_crmDbConn, "TVSI_sDELETE_INFO_EXTEND", paramDelExtend, _sqlTimeout);
                    var extendResult = paramDelExtend.Get<int?>("@Result");

                    if (extendResult == 0)
                    {
                        //INSERT THÔNG TIN MỞ RỘNG MỚI
                        if (model.ExtendCust?.Count() > 0)
                        {
                            foreach (var item in model.ExtendCust)
                            {
                                var paramInsetExtend = new DynamicParameters();
                                paramInsetExtend.Add("@Custcode", data.Custcode, DbType.String, ParameterDirection.Input);
                                paramInsetExtend.Add("@CategoryId", item.Value, DbType.String, ParameterDirection.Input);
                                paramInsetExtend.Add("@CreateBy", model.UserName, DbType.String,
                                    ParameterDirection.Input);
                                paramInsetExtend.Add("@Result", null, DbType.Int32, ParameterDirection.Output);
                                await _dapper.ExecuteAsync(_crmDbConn, "TVSI_sINSERT_INFO_EXTEND", paramInsetExtend,
                                    _sqlTimeout);
                                var insertExtend = paramInsetExtend.Get<int>("@Result");
                                if (insertExtend == -1)
                                    LogInformation(model.UserName,
                                        $"INSERT thông tin mở rộng thất bại custcode {data.Custcode},CategoryId {item.Value} , ");
                            }
                        }
                    }
                    else
                        LogInformation(model.UserName, $"Xóa thông tin mở rộng thất bại custcode {data.Custcode}");
                }
                if (model.IsShareHolder)
                {
                    foreach (var item in model.ShareHolderInfo!)
                    {
                        var paramShareHolderInsert = new DynamicParameters();
                        paramShareHolderInsert.Add("@CustCode", data.Custcode, DbType.String, ParameterDirection.Input);
                        paramShareHolderInsert.Add("@CustName", model.FullName, DbType.String, ParameterDirection.Input);
                        paramShareHolderInsert.Add("@Address", model.Address, DbType.String, ParameterDirection.Input);
                        paramShareHolderInsert.Add("@ShareType1", item.ShareType_1, DbType.Boolean, ParameterDirection.Input);
                        paramShareHolderInsert.Add("@ShareType2", item.ShareType_2, DbType.Boolean, ParameterDirection.Input);
                        paramShareHolderInsert.Add("@ShareType3", item.ShareType_3, DbType.Boolean, ParameterDirection.Input);
                        paramShareHolderInsert.Add("@ShareType4", item.ShareType_4, DbType.Boolean, ParameterDirection.Input);
                        paramShareHolderInsert.Add("@StockCode", item.StockCode, DbType.String, ParameterDirection.Input);
                        paramShareHolderInsert.Add("@CreatedBy", model.UserName, DbType.String, ParameterDirection.Input);
                        paramShareHolderInsert.Add("@Result", dbType: DbType.Int32, direction: ParameterDirection.Output);
                        await _dapper.ExecuteAsync(_crmDbConn, "TVSI_sInsert_shareholder_customer", paramShareHolderInsert,
                            _sqlTimeout);
                        var sharaHolderStatus = paramShareHolderInsert.Get<int>("@Result");
                        if (sharaHolderStatus == -1)
                            LogInformation(model.UserName,
                                $"Insert thông tin dữ liệu thông tin cổ đông {data.Custcode}, StockCode {item.StockCode}");
                    }
                }
                
                return new ResponseSingle<bool>
                {
                    Code = (result is (int)ErrorCodeDetail.Success
                        ? (int)ErrorCodeDetail.Success
                        : (int)ErrorCodeDetail.Failed).ErrorCodeFormat(),
                    Message = result is (int)ErrorCodeDetail.Success
                        ? ErrorCodeDetail.Success.ToEnumDescription()
                        : ErrorCodeDetail.Failed.ToEnumDescription(),
                    Data = result is (int)ErrorCodeDetail.Success
                };
            }

            return new ResponseSingle<bool>
            {
                Code = ((int)ErrorCodeDetail.Failed).ErrorCodeFormat(),
                Message = ErrorCodeDetail.Failed.ToEnumDescription(),
                Data = false
            };
        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
            return new ResponseSingle<bool>
            {
                Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                Message = ErrorCodeDetail.Exception.ToEnumDescription()
            };
        }
    }


    public async Task<ResponseSingle<bool>> CancelOpenAccount(CancelOpenAccountRequest model)
    {
        try
        {
            var param = new DynamicParameters();
            param.Add("@Id", model.Id, DbType.Int32, ParameterDirection.Input);
            param.Add("@UserName", model.UserName, DbType.String, ParameterDirection.Input);
            param.Add("@Result", null, DbType.Int32, ParameterDirection.Output);
            await _dapper.ExecuteAsync(_commonDbConn, "TVSI_sCANCEL_OPEN_ACCOUNT", param,
                _sqlTimeout);
            var result = param.Get<int>("@Result");
            return new ResponseSingle<bool>
            {
                Code = (result is (int)ErrorCodeDetail.Success
                    ? (int)ErrorCodeDetail.Success
                    : (int)ErrorCodeDetail.Failed).ErrorCodeFormat(),
                Message = result is (int)ErrorCodeDetail.Success
                    ? ErrorCodeDetail.Success.ToEnumDescription()
                    : ErrorCodeDetail.Failed.ToEnumDescription(),
                Data = result is (int)ErrorCodeDetail.Success
            };
        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
            return new ResponseSingle<bool>
            {
                Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                Message = ErrorCodeDetail.Exception.ToEnumDescription(),
            };
        }
    }
    public async Task<ResponseSingle<bool>> CheckPhoneCardIdAccount(CheckPhoneCardIdAccountRequest model)
    {
        try
        {
            var param = new DynamicParameters();
            param.Add("@Type", model.Type, DbType.String, ParameterDirection.Input); // 1 là tạo mới, 2 là cập nhật
            param.Add("@Phone", model.Phone, DbType.String, ParameterDirection.Input);
            param.Add("@CardId", model.CardId, DbType.String, ParameterDirection.Input);
            param.Add("@CardIssueDate",model.CardIssueDate, DbType.String, ParameterDirection.Input);
            param.Add("@Result", null, DbType.Int32, ParameterDirection.Output);
            var data = await _dapper.GetAsync<string>(_commonDbConn, "TVSI_sCheckPhoneCardIdCustomer", param, _sqlTimeout);
            var result = param.Get<int?>("@Result");
            return new ResponseSingle<bool>
            {
                Code = (result is (int)ErrorCodeDetail.Success
                    ? (int)ErrorCodeDetail.Success
                    : (int)ErrorCodeDetail.Failed).ErrorCodeFormat(),
                Message = result is (int)ErrorCodeDetail.Success
                    ? ErrorCodeDetail.Success.ToEnumDescription()
                    : data,
                Data = result is (int)ErrorCodeDetail.Success 
            };

        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
            return new ResponseSingle<bool>
            {
                Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                Message = ErrorCodeDetail.Exception.ToEnumDescription(),
            };
        }
    }

    public async Task<ResponseSingle<CheckCustCodeResponse>> CheckCustomerCode(CheckCustomerCodeRequest model)
    {
        try
        {
            var result = -1;
            var saleId = model.UserName.Length > 4 ? model.UserName.Substring(0, 4) : model.UserName;
            var iCountDebtDoc = await _dapper.GetAsync<int>(_commonDbConn,
                "TVSI_sGetCountDebtDocument", new DynamicParameters(new
                {
                    SaleId = saleId
                }), _sqlTimeout);

            var dataCustCommon = await _dapper.GetAsync<string>(_commonDbConn,
                "TVSI_sGET_CUSTOMER_CODE_COMMONDB", new DynamicParameters(new
                {
                    CustomerCode = model.CustomerCode
                }), _sqlTimeout);

            var dataCustCommonOpenAcc = await _dapper.SelectAsync<CheckCustomerCodeResponse>(_commonDbConn,
                "TVSI_sGET_CUSTOMER_CODE_AND_TYPE_AND_STATUS", new DynamicParameters(new
                {
                    CustomerCode = model.CustomerCode
                }), _sqlTimeout);

            var dataCustBmsReserved = await _dapper.GetAsync<string>(_bmsDbConn,
                "TVSI_sGET_CUSTOMER_CODE_RESERVED", new DynamicParameters(new
                {
                    CustomerCode = model.CustomerCode
                }), _sqlTimeout);


            if (dataCustCommonOpenAcc?.Count() > 0 && dataCustCommonOpenAcc != null)
            {
                foreach (var item in dataCustCommonOpenAcc)
                {
                    if (item.AccountStatus != "199" && item.AccountStatus != "299")
                    {
                        result = (int)ValidateAccountConst.ACCOUNT_EXIST;
                    }
                }

            }
            else if (dataCustCommon != null || dataCustBmsReserved != null)
            {
                var dataAccCommon = await _dapper.SelectAsync<string>(_commonDbConn,
                    "TVSI_sGET_ACC_NUMBER_COMMON", new DynamicParameters(new
                    {
                        CustomerCode = model.CustomerCode

                    }), _sqlTimeout);
                if (dataAccCommon!.Count() > 0)
                {
                    if (dataAccCommon.Contains(model.CustomerCode + "1") && !dataAccCommon.Contains(model.CustomerCode + "6"))
                        result = (int)ValidateAccountConst.ACCOUNT_EXIST_NOT_MARGIN;
                    else
                        result = (int)ValidateAccountConst.ACCOUNT_EXIST;
                }
                else
                {
                    result = (int)ValidateAccountConst.ACCOUNT_EXIST;
                }
            }
            else
            {

                if (iCountDebtDoc >= 5)
                    result = (int)ValidateAccountConst.ACCOUNT_MAX_DEBT;
                else
                    result = (int)ValidateAccountConst.ACCOUNT_NOT_EXIST;
            }

            if (result == (int)ValidateAccountConst.ACCOUNT_EXIST_NOT_MARGIN)
            {
                var dataCheckPermission = await _dapper.SelectAsync<string>(_centralizeDBConn,
                    "TVSI_sGetOpenAccountToAddition", new DynamicParameters(new
                    {
                        Custcode = model.CustomerCode,
                        UserName = model.UserName,
                        SaleID = saleId
                    }), _sqlTimeout);
                if (dataCheckPermission.Count() <= 0 || dataCheckPermission == null)
                {
                    result = (int)ValidateAccountConst.ACCOUNT_EXIST;
                }
            }

            var dataCheckClosedAcc  = await _dapper.SelectAsync<OpenAccountAdditionalDetailResponse>(_centralizeDBConn,
                "TVSI_sGetOpenAccountAdditional", new DynamicParameters(new
                {
                    CustomerCode = model.CustomerCode
                }), _sqlTimeout);
            if (dataCheckClosedAcc.Count() > 0 && dataCheckClosedAcc != null)
            {
                foreach (var item in dataCheckClosedAcc)
                {
                    if (item.Status == 0)
                    {
                        result = (int)ValidateAccountConst.ACCOUNT_CLOSED;
                        break;
                    }
                }
                
            }

            var retData = new CheckCustCodeResponse
            {
                Code = result,
                CountDebt = iCountDebtDoc
            };
            
            return new ResponseSingle<CheckCustCodeResponse>
            {
                Code = (result is not -1
                    ? (int)ErrorCodeDetail.Success
                    : (int)ErrorCodeDetail.Failed).ErrorCodeFormat(),
                Message = result is not -1
                    ? ErrorCodeDetail.Success.ToEnumDescription()
                    : ErrorCodeDetail.Failed.ToEnumDescription(),
                Data = retData
            };

        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
            return new ResponseSingle<CheckCustCodeResponse>
            {
                Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                Message = ErrorCodeDetail.Exception.ToEnumDescription(),
            };
        }
    }


    public async Task<ResponseSingle<bool>> OpenIndividualDomesticAccount(OpenIndividualDomesticAccountRequest model)
    {
        try
        {
            var modelCheck = new CheckCustomerCodeRequest()
            {
                UserName = model.UserName,
                CustomerCode = model.CustomerCode,
                TypeAccount = 1
            };
            var checkCustCode = await CheckCustomerCode(modelCheck);
            var retCheck = checkCustCode.Data.Code;
            if (retCheck == 0 || retCheck == 33)
            {
                var description = "HĐ thường";
                var addMargin = model.AccountMargin ? "1" : "0";
                if (addMargin.Equals("1"))
                    description += ", Margin";

                var branchNo = CommonConstants.Branchiddefault;
                var branchName = CommonConstants.Branchnamedefault;

                var paramBranch = new DynamicParameters();
                paramBranch.Add("@SaleID", model.SaleID, DbType.String, ParameterDirection.Input);
                var branchInfo = await _dapper.GetAsync<BranchInfoResponse>(_emsDbConn, "TVSI_sGetBranchInfo", paramBranch, _sqlTimeout);
                if (branchInfo != null)
                {
                    if (!string.IsNullOrEmpty(branchInfo.BranchName))
                        branchName = branchInfo.BranchName;
                    if (!string.IsNullOrEmpty(branchInfo.BranchNo))
                        branchNo = branchInfo.BranchNo;
                }
                var saleId = model.SaleID.Length > 4 ? model.SaleID.Substring(0, 4) : model.SaleID;

                var serviceRegister = CommonConstants.BankNotRegister;
                var transferType = "";

                var bank01 = CommonConstants.BankNotRegister;
                var bank02 = CommonConstants.BankNotRegister;
                var bank03 = CommonConstants.BankNotRegister;
                var bank04 = CommonConstants.BankNotRegister;

                for (int i = 0; i < model.BankInfo?.Count; i++)
                {
                    if (i <= 4)
                    {
                        if (i == 0)
                            if (!string.IsNullOrEmpty(model.BankInfo?[i].BankNo))
                                bank01 = 0;
                        if (i == 1)
                            if (!string.IsNullOrEmpty(model.BankInfo?[i].BankNo))
                                bank02 = 0;
                        if (i == 2)
                            if (!string.IsNullOrEmpty(model.BankInfo?[i].BankNo))
                                bank03 = 0;
                        if (i == 3)
                            if (!string.IsNullOrEmpty(model.BankInfo?[i].BankNo))
                                bank04 = 0;
                    }
                }

                if (bank01 == 0 || bank02 == 0 || bank03 == 0 || bank04 == 0)
                {
                    serviceRegister = 0;
                    transferType += 1;
                }

                // trạng thái dv kênh thông tin
                var ChannelStatusInfo = CommonConstants.BankNotRegister;

                var birthDay = (!string.IsNullOrEmpty(model.BirthDay)) ?
                    DateTime.ParseExact(model.BirthDay!, CommonConstants.DateFormat, CultureInfo.CurrentCulture) : (DateTime?)null;
                var issueDate = (!string.IsNullOrEmpty(model.IssueDate)) ?
                    DateTime.ParseExact(model.IssueDate!, CommonConstants.DateFormat, CultureInfo.CurrentCulture) : (DateTime?)null;
                var manIssueDate = (!string.IsNullOrEmpty(model.ManIssueDate)) ?
                    DateTime.ParseExact(model.ManIssueDate!, CommonConstants.DateFormat, CultureInfo.CurrentCulture) : (DateTime?)null;
                var manDateAnPower = (!string.IsNullOrEmpty(model.ManDateAnPower)) ?
                    DateTime.ParseExact(model.ManDateAnPower!, CommonConstants.DateFormat, CultureInfo.CurrentCulture) : (DateTime?)null;
                var ProfileStatus = model.IsProfileCompletion ? ProfileStatusConst.Process : ProfileStatusConst.ProfileDebt;
                var AccountStatus = model.IsProfileCompletion ? AccountStatusConst.Process : AccountStatusConst.WaitingBuMen;

                var param = new DynamicParameters();
                param.Add("@ProfileType", DocumentTypeConst.OPEN_DOCUMENT, DbType.Int32, ParameterDirection.Input);
                param.Add("@CustomerCode", model.CustomerCode, DbType.String, ParameterDirection.Input);
                param.Add("@AccountNo", "044C" + model.CustomerCode, DbType.String, ParameterDirection.Input);
                param.Add("@TypeAccount", TypeAccountConst.INDIVIDUAL_DOMESTIC_INVEST, DbType.Int32, ParameterDirection.Input);
                param.Add("@FullName", model.FullName?.ToTitleCase(), DbType.String, ParameterDirection.Input); // họ tên
                param.Add("@BirthDay", birthDay, DbType.DateTime, ParameterDirection.Input); // ngày sinh
                param.Add("@Gender", model.Gender, DbType.Int32, ParameterDirection.Input); // giói tính
                param.Add("@CardId", model.CardId, DbType.String, ParameterDirection.Input); // số cmt/cccd
                param.Add("@IssueDate", issueDate, DbType.DateTime, ParameterDirection.Input); // ngày cấp cmt/cccd
                param.Add("@IssuePlace", model.IssuePlace, DbType.String, ParameterDirection.Input); // nơi cấp cmt/cccd
                param.Add("@Address", model.Address, DbType.String, ParameterDirection.Input); // địa chỉ liên hệ
                param.Add("@NationalityName", Nationality.NationalityName, DbType.String, ParameterDirection.Input); // quốc tịch
                param.Add("@NationalityCode", Nationality.NationalityCode, DbType.String, ParameterDirection.Input); // quốc tịch
                param.Add("@ManTvsi", model.ManTvsi, DbType.String, ParameterDirection.Input); // đại diện tvsi
                param.Add("@ManPosition", model.ManPosition, DbType.String, ParameterDirection.Input); // vị trí
                param.Add("@ManAnPower", model.ManAnPower, DbType.String, ParameterDirection.Input); // giấy ủy quền
                param.Add("@ManDateAnPower", manDateAnPower, DbType.DateTime, ParameterDirection.Input); // ngày cấp giấy ủy quyền
                param.Add("@ManCardId", model.ManCardId, DbType.String, ParameterDirection.Input); // số cmt/cccd
                param.Add("@ManIssueDate", manIssueDate, DbType.DateTime, ParameterDirection.Input); // ngày cấp cmt/cccd
                param.Add("@ManIssuePlace", model.ManIssuePlace, DbType.String, ParameterDirection.Input); // nơi cấp cmt /cccd
                param.Add("@ContractAdd", addMargin, DbType.String, ParameterDirection.Input); // HĐ bổ sung
                param.Add("@TradingResultMethod", model.TradingResultMethod, DbType.String, ParameterDirection.Input); // Phương thức nhận KQ giao dịch
                param.Add("@TransferType", transferType, DbType.String, ParameterDirection.Input); // Dịch vụ chuyển tiền
                param.Add("@Phone_01", model.Phone01, DbType.String, ParameterDirection.Input); // SĐT 01 
                param.Add("@Phone_02", model.Phone02, DbType.String, ParameterDirection.Input); // SĐT 02 
                param.Add("@Email", model.Email, DbType.String, ParameterDirection.Input); // Email
                for (int i = 0; i < model.BankInfo?.Count; i++)
                {
                    if (i < 4)
                    {
                        param.Add("@BankAccountName_0" + (i + 1), model.BankInfo?[i].BankAccountName, DbType.String, ParameterDirection.Input); // tên chủ tk ngân hàng
                        param.Add("@BankAccountNo_0" + (i + 1), model.BankInfo?[i].BankAccountNo, DbType.String, ParameterDirection.Input); // số tk ngân hàng

                        if (!string.IsNullOrEmpty(model.BankInfo?[i].BankNo))
                            param.Add("@BankName_0" + (i + 1), _bankService.GetBankName(model.BankInfo?[i].BankNo).Result.Data ?? "", DbType.String, ParameterDirection.Input); // tên ngân hàng
                        if (!string.IsNullOrEmpty(model.BankInfo?[i].BankNo) && !string.IsNullOrEmpty(model.BankInfo?[i].SubBranchNo))
                            param.Add("@SubBranchName_0" + (i + 1), _bankService.GetSubBranchName(model.BankInfo?[i].BankNo, model.BankInfo?[i].SubBranchNo).Result.Data ?? "", DbType.String, ParameterDirection.Input); // tên chi nhánh

                        param.Add("@SubBranchNo_0" + (i + 1), model.BankInfo?[i].SubBranchNo, DbType.String, ParameterDirection.Input); // mã chi nhánh
                        param.Add("@BankNo_0" + (i + 1), model.BankInfo?[i].BankNo, DbType.String, ParameterDirection.Input); // mã NH
                    }
                }
                if (!model.IsInfoUsa)
                {
                    param.Add("@InfoUS01", 0, DbType.Int32, ParameterDirection.Input); // thông tin us
                    param.Add("@InfoUS02", 0, DbType.Int32, ParameterDirection.Input); // thông tin us
                    param.Add("@InfoUS03", 0, DbType.Int32, ParameterDirection.Input); // thông tin us
                    param.Add("@InfoUS04", 0, DbType.Int32, ParameterDirection.Input); // thông tin us
                    param.Add("@InfoUS05", 0, DbType.Int32, ParameterDirection.Input); // thông tin us
                    param.Add("@InfoUS06", 0, DbType.Int32, ParameterDirection.Input); // thông tin us
                    param.Add("@InfoUS07", 0, DbType.Int32, ParameterDirection.Input); // thông tin us
                }
                else
                {
                    param.Add("@InfoUS01", model.UsaInfo.Info01 ? 1 : 0, DbType.Int32, ParameterDirection.Input); // thông tin us
                    param.Add("@InfoUS02", model.UsaInfo.Info02 ? 1 : 0, DbType.Int32, ParameterDirection.Input); // thông tin us
                    param.Add("@InfoUS03", model.UsaInfo.Info03 ? 1 : 0, DbType.Int32, ParameterDirection.Input); // thông tin us
                    param.Add("@InfoUS04", model.UsaInfo.Info04 ? 1 : 0, DbType.Int32, ParameterDirection.Input); // thông tin us
                    param.Add("@InfoUS05", model.UsaInfo.Info05 ? 1 : 0, DbType.Int32, ParameterDirection.Input); // thông tin us
                    param.Add("@InfoUS06", model.UsaInfo.Info06 ? 1 : 0, DbType.Int32, ParameterDirection.Input); // thông tin us
                    param.Add("@InfoUS07", model.UsaInfo.Info07 ? 1 : 0, DbType.Int32, ParameterDirection.Input); // thông tin us
                }

                param.Add("@ServiceStatus", serviceRegister, DbType.Int32, ParameterDirection.Input); // trạng thái dịch vụ
                param.Add("@Bank01Status", bank01, DbType.Int32, ParameterDirection.Input); // trạng thái ngân hàng 1
                param.Add("@Bank02Status", bank02, DbType.Int32, ParameterDirection.Input); // trạng thái ngân hàng 2
                param.Add("@Bank03Status", bank03, DbType.Int32, ParameterDirection.Input); // trạng thái ngân hàng 3
                param.Add("@Bank04Status", bank04, DbType.Int32, ParameterDirection.Input); // trạng thái ngân hàng 4
                param.Add("@ChannelStatusInfo", ChannelStatusInfo, DbType.Int32, ParameterDirection.Input); // trạng thái kênh thông tin
                param.Add("@Description", description, DbType.String, ParameterDirection.Input); // Mô tả
                param.Add("@BranchID", branchNo, DbType.String, ParameterDirection.Input); // mã chi nhánh
                param.Add("@BranchName", branchName, DbType.String, ParameterDirection.Input); // tên chi nhánh
                param.Add("@SaleId", saleId, DbType.String, ParameterDirection.Input); // SaleId
                param.Add("@SourceType", model.SourceAccount, DbType.String, ParameterDirection.Input); // Mở từ nguồn
                param.Add("@ProfileStatus", ProfileStatus, DbType.String, ParameterDirection.Input); // Trạng thái hồ sơ
                param.Add("@AccountStatus", AccountStatus, DbType.String, ParameterDirection.Input); // Trạng thái tk
                param.Add("@UserName", model.UserName, DbType.String, ParameterDirection.Input); // người tạo
                param.Add("@Result", null, DbType.String, ParameterDirection.Output, 100); // OUTPUT
                await _dapper.ExecuteAsync(_commonDbConn, "TVSI_sOpen_Account", param, _sqlTimeout);
                var result = -1;
                var resultIdInsert = Convert.ToInt64(param.Get<string?>("@Result"));
                if (resultIdInsert > 0)
                {
                    result = 0;
                    //Update ảnh CMND
                    if (!model.IsProfileCompletion)
                    {
                        if (model.ImageFront != null && model.ImageBack != null)
                        {
                            byte[] dataCardIdImage = Convert.FromBase64String(model.ImageFront!);
                            byte[] dataCardIdBackImage = Convert.FromBase64String(model.ImageBack!);
                            var cardIdFileName = resultIdInsert + ".jpg";
                            var cardIdBackFileName = resultIdInsert + "_ms.jpg";

                            var paramImageInsert = new DynamicParameters();
                            paramImageInsert.Add("@OpenId", resultIdInsert, DbType.Int64, ParameterDirection.Input);
                            paramImageInsert.Add("@CustomerCode", model.CustomerCode, DbType.String, ParameterDirection.Input);
                            paramImageInsert.Add("@FileName", cardIdFileName, DbType.String, ParameterDirection.Input);
                            paramImageInsert.Add("@FileData", dataCardIdImage, DbType.Binary, ParameterDirection.Input);
                            paramImageInsert.Add("@FileBackName", cardIdBackFileName, DbType.String, ParameterDirection.Input);
                            paramImageInsert.Add("@FileBackData", dataCardIdBackImage, DbType.Binary, ParameterDirection.Input);
                            paramImageInsert.Add("@CreateBy", model.UserName, DbType.String, ParameterDirection.Input);
                            paramImageInsert.Add("@Result", dbType: DbType.Int32, direction: ParameterDirection.Output);

                            await _dapper.ExecuteAsync(_commonDbConn, "TVSI_sOpen_Account_CardId_Image", paramImageInsert, _sqlTimeout);
                            var iInsertImage = paramImageInsert.Get<int>("@Result");
                            if (iInsertImage == -1)
                                LogInformation(model.UserName, $"Insert thông tin ảnh cmnd lỗi id {resultIdInsert}, CustomerCode {model.CustomerCode}");
                            else
                            {
                                AddCentralizeImageDB(resultIdInsert, model.SourceAccount, model.CustomerCode, model.UserName, cardIdFileName, 
                                    model.ImageFront, cardIdBackFileName, model.ImageBack);
                            }
                            if (model.AccountMargin)
                            {
                                var paramDebtInsert = new DynamicParameters();
                                paramDebtInsert.Add("@Id", resultIdInsert, DbType.Int32, ParameterDirection.Input); // Id
                                paramDebtInsert.Add("@CreditLimit", model.CreditLimit, DbType.Decimal, ParameterDirection.Input); // Hạn mức dư nợ
                                paramDebtInsert.Add("@DebtTerm", model.DebtTerm, DbType.Int32, ParameterDirection.Input); // Thời gian ghi nợ
                                paramDebtInsert.Add("@DepositRate", model.DepositRate, DbType.Double, ParameterDirection.Input); // Ty lệ ký quỹ
                                paramDebtInsert.Add("@Result", dbType: DbType.Int32, direction: ParameterDirection.Output); // OUTPUT
                                await _dapper.ExecuteAsync(_commonDbConn, "TVSI_sINSERT_CONTRACT_FS_INFO", paramDebtInsert, _sqlTimeout); // Insert du liệu ghi nợ margin 
                                var debtStatus = paramDebtInsert.Get<int>("@Result");
                                if (debtStatus == -1)
                                    LogInformation(model.UserName, $"Insert thông tin dữ liệu ghi nợ lỗi id {resultIdInsert},CreditLimit{model.CreditLimit},DebtTerm{model.DebtTerm},DepositRate{model.DepositRate}");
                            }

                        }
                    }


                    //INSERT THÔNG TIN MỞ RỘNG 
                    if (model.ExtendCust?.Count() > 0)
                    {
                        foreach (var item in model.ExtendCust)
                        {
                            var paramInsetExtend = new DynamicParameters();
                            paramInsetExtend.Add("@Custcode", model.CustomerCode, DbType.String, ParameterDirection.Input);
                            paramInsetExtend.Add("@CategoryId", item.Value, DbType.Int32, ParameterDirection.Input);
                            paramInsetExtend.Add("@CreateBy", model.UserName, DbType.String, ParameterDirection.Input);
                            paramInsetExtend.Add("@Result", null, DbType.Int32, ParameterDirection.Output);
                            await _dapper.ExecuteAsync(_crmDbConn, "TVSI_sINSERT_INFO_EXTEND", paramInsetExtend, _sqlTimeout);
                            var insertExtend = paramInsetExtend.Get<int>("@Result");
                            if (insertExtend == -1)
                                LogInformation(model.UserName, $"INSERT thông tin mở rộng thất bại custcode {model.CustomerCode},CategoryId {item.Value} , ");
                        }
                    }

                    //THông tin cổ đông 
                    if (model.IsShareHolder)
                    {
                        foreach (var item in model.ShareHolderInfo)
                        {
                            var paramShareHolderInsert = new DynamicParameters();
                            paramShareHolderInsert.Add("@CustCode", model.CustomerCode, DbType.String, ParameterDirection.Input);
                            paramShareHolderInsert.Add("@CustName", model.FullName, DbType.String, ParameterDirection.Input);
                            paramShareHolderInsert.Add("@Address", model.Address, DbType.String, ParameterDirection.Input);
                            paramShareHolderInsert.Add("@ShareType1", item.ShareType_1, DbType.Boolean, ParameterDirection.Input);
                            paramShareHolderInsert.Add("@ShareType2", item.ShareType_2, DbType.Boolean, ParameterDirection.Input);
                            paramShareHolderInsert.Add("@ShareType3", item.ShareType_3, DbType.Boolean, ParameterDirection.Input);
                            paramShareHolderInsert.Add("@ShareType4", item.ShareType_4, DbType.Boolean, ParameterDirection.Input);
                            paramShareHolderInsert.Add("@StockCode", item.StockCode, DbType.String, ParameterDirection.Input);
                            paramShareHolderInsert.Add("@CreatedBy", model.UserName, DbType.String, ParameterDirection.Input);

                            paramShareHolderInsert.Add("@Result", dbType: DbType.Int32, direction: ParameterDirection.Output);

                            await _dapper.ExecuteAsync(_crmDbConn, "TVSI_sInsert_shareholder_customer", paramShareHolderInsert, _sqlTimeout);
                            var sharaHolderStatus = paramShareHolderInsert.Get<int>("@Result");
                            if (sharaHolderStatus == -1)
                                LogInformation(model.UserName,
                                    $"Insert thông tin dữ liệu thông tin cổ đông thất bại : CustomerCode {model.CustomerCode}, StockCode {item.StockCode}");
                        }
                    }
                }

                return new ResponseSingle<bool>
                {
                    Code = (result is (int)ErrorCodeDetail.Success
                        ? (int)ErrorCodeDetail.Success
                        : (int)ErrorCodeDetail.Failed).ErrorCodeFormat(),
                    Message = result is (int)ErrorCodeDetail.Success
                        ? ErrorCodeDetail.Success.ToEnumDescription()
                        : ErrorCodeDetail.Failed.ToEnumDescription(),
                    Data = result is (int)ErrorCodeDetail.Success
                };
            }
            else
            {
                return new ResponseSingle<bool>
                {
                    Code = ((int)ErrorCodeDetail.Registered).ErrorCodeFormat(),
                    Message = ErrorCodeDetail.Registered.ToEnumDescription(),
                };
            }
        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
            return new ResponseSingle<bool>
            {
                Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                Message = ErrorCodeDetail.Exception.ToEnumDescription()
            };
        }
    }

    public async Task<ResponseSingle<dynamic>> GetOpenAccountAdditionalDetail(OpenAccountAdditionalDetailRequest model)
    {
        try
        {
            var param = new DynamicParameters();
            param.Add("@CustomerCode", model.Custcode, DbType.String, ParameterDirection.Input);
            var data = await _dapper.GetAsync<OpenAccountAdditionalDetailResponse>(_centralizeDBConn, "TVSI_sGetOpenAccountAdditional", param, _sqlTimeout);

            return new ResponseSingle<dynamic>
            {
                Code = ((int)ErrorCodeDetail.Success).ErrorCodeFormat(),
                Message = ErrorCodeDetail.Success.ToEnumDescription(),
                Data = data
            };
        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
            return new ResponseSingle<dynamic>
            {
                Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                Message = ErrorCodeDetail.Exception.ToEnumDescription()
            };
        }
    }

    public async Task<ResponseSingle<bool>> OpenIndividualDomesticAdditionalAccount(OpenIndividualDomesticAdditionalAccountRequest model)
    {
        try
        {
            var modelCheck = new CheckCustomerCodeRequest()
            {
                UserName = model.UserName,
                CustomerCode = model.CustomerCode,
                TypeAccount = 1
            };
            var checkCustCode = await CheckCustomerCode(modelCheck);
            var retCheck = checkCustCode.Data.Code;
            if (retCheck == 1 || retCheck == 33)
            {
                var additionService = (int)AdditionContractConst.Margin;
                var description = "HĐ Margin ";

                var branchNo = CommonConstants.Branchiddefault;
                var branchName = CommonConstants.Branchnamedefault;

                var paramBranch = new DynamicParameters();
                paramBranch.Add("@SaleID", model.SaleID, DbType.String, ParameterDirection.Input);
                var branchInfo =
                    await _dapper.GetAsync<BranchInfoResponse>(_emsDbConn, "TVSI_sGetBranchInfo", paramBranch,
                        _sqlTimeout);
                if (branchInfo != null)
                {
                    if (!string.IsNullOrEmpty(branchInfo.BranchName))
                        branchName = branchInfo.BranchName;
                    if (!string.IsNullOrEmpty(branchInfo.BranchNo))
                        branchNo = branchInfo.BranchNo;
                }

                var birthDay = (!string.IsNullOrEmpty(model.BirthDay))
                    ? DateTime.ParseExact(model.BirthDay!, CommonConstants.DateFormat, CultureInfo.CurrentCulture)
                    : (DateTime?)null;
                var cardIssueDate = (!string.IsNullOrEmpty(model.IssueDate))
                    ? DateTime.ParseExact(model.IssueDate!, CommonConstants.DateFormat, CultureInfo.CurrentCulture)
                    : (DateTime?)null;
                ;

                var saleId = model.SaleID.Length > 4 ? model.SaleID.Substring(0, 4) : model.SaleID;

                var param = new DynamicParameters();
                param.Add("@CustomerCode", model.CustomerCode, DbType.String, ParameterDirection.Input);
                param.Add("@AccountNo", "044C" + model.CustomerCode, DbType.String, ParameterDirection.Input);
                param.Add("@TypeAccount", CustTypeConst.INDIVIDUAL_DOMESTIC_INVEST, DbType.Int32, ParameterDirection.Input);
                param.Add("@TypeProfile", DocumentTypeConst.ADDITION_DOCUMENT, DbType.Int32, ParameterDirection.Input);
                param.Add("@FullName", model.FullName?.ToTitleCase(), DbType.String, ParameterDirection.Input);
                param.Add("@BirthDay", birthDay, DbType.DateTime, ParameterDirection.Input);
                param.Add("@Gender", model.Gender, DbType.Int32, ParameterDirection.Input);
                param.Add("@CardId", model.CardId, DbType.String, ParameterDirection.Input);
                param.Add("@IssueDate", cardIssueDate, DbType.DateTime, ParameterDirection.Input);
                param.Add("@IssuePlace", model.IssuePlace, DbType.String, ParameterDirection.Input);
                param.Add("@Address", model.Address, DbType.String, ParameterDirection.Input);
                param.Add("@NationalityName", Nationality.NationalityName, DbType.String, ParameterDirection.Input);
                param.Add("@NationalityCode", Nationality.NationalityCode, DbType.String, ParameterDirection.Input);
                param.Add("@MainContract", false, DbType.Boolean, ParameterDirection.Input);
                param.Add("@ContractAdd", additionService, DbType.String, ParameterDirection.Input);
                param.Add("@ServiceStatus", ServiceStatusConst.KO_DANG_KY, DbType.Int32, ParameterDirection.Input);
                param.Add("@Bank01Status", ServiceStatusConst.KO_DANG_KY, DbType.Int32, ParameterDirection.Input);
                param.Add("@Bank02Status", ServiceStatusConst.KO_DANG_KY, DbType.Int32, ParameterDirection.Input);
                param.Add("@Bank03Status", ServiceStatusConst.KO_DANG_KY, DbType.Int32, ParameterDirection.Input);
                param.Add("@Bank04Status", ServiceStatusConst.KO_DANG_KY, DbType.Int32, ParameterDirection.Input);
                param.Add("@ProfileStatus", ProfileStatusConst.Process, DbType.String, ParameterDirection.Input);
                param.Add("@AccountStatus", AccountStatusConst.Process, DbType.String, ParameterDirection.Input);
                param.Add("@ChannelStatusInfo", ServiceStatusConst.KO_DANG_KY, DbType.Int32, ParameterDirection.Input);
                param.Add("@Description", description, DbType.String, ParameterDirection.Input);
                param.Add("@BranchID", branchNo, DbType.String, ParameterDirection.Input);
                param.Add("@BranchName", branchName, DbType.String, ParameterDirection.Input);
                param.Add("@SaleId", saleId, DbType.String, ParameterDirection.Input);
                param.Add("@SourceType", model.SourceAccount, DbType.String, ParameterDirection.Input); // Mở từ nguồn
                param.Add("@UserName", model.UserName, DbType.String, ParameterDirection.Input); // Người tạo
                param.Add("@Result", null, DbType.String, ParameterDirection.Output, 100);

                await _dapper.ExecuteAsync(_commonDbConn, "TVSI_sOpen_Additional_Account", param, _sqlTimeout);
                var result = -1;
                var resultIdInsert = Convert.ToInt64(param.Get<string?>("@Result"));
                if (resultIdInsert > 0)
                    result = 0;
                return new ResponseSingle<bool>
                {
                    Code = (result is (int)ErrorCodeDetail.Success
                        ? (int)ErrorCodeDetail.Success
                        : (int)ErrorCodeDetail.Failed).ErrorCodeFormat(),
                    Message = result is (int)ErrorCodeDetail.Success
                        ? ErrorCodeDetail.Success.ToEnumDescription()
                        : ErrorCodeDetail.Failed.ToEnumDescription(),
                    Data = result is (int)ErrorCodeDetail.Success
                };
            }
            else
            {
                return new ResponseSingle<bool>
                {
                    Code = ((int)ErrorCodeDetail.MarginRegistered).ErrorCodeFormat(),
                    Message = ErrorCodeDetail.MarginRegistered.ToEnumDescription(),
                };
            }

        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
            return new ResponseSingle<bool>
            {
                Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                Message = ErrorCodeDetail.Exception.ToEnumDescription()
            };
        }
    }

    public async Task<Response<dynamic>> GetOpenAccountDebtDocumentList(OpenAccountDebtDocumentListRequest model)
    {
        try
        {
            var start = (model.PageIndex - 1) * model.PageSize;
            var data = await _dapper.SelectAsync<dynamic>(_commonDbConn, "TVSI_sGetOpenAccountList", new DynamicParameters(new
            {
                UserName = model.UserName,
                CustCode = model.Custcode,
                FullName = model.FullName,
                SaleID = model.SaleId,
                AccountStatus = model.AccountStatus,
                ProfileStatus = ProfileStatusConst.ProfileDebt,
                SearchType = model.SearchType,
                start = start,
                PageSize = model.PageSize

            }), _sqlTimeout);

            return new Response<dynamic>
            {
                Code = ((int)ErrorCodeDetail.Success).ErrorCodeFormat(),
                Message = ErrorCodeDetail.Success.ToEnumDescription(),
                Data = data

            };
        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                 $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
            return new Response<dynamic>
            {
                Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                Message = ErrorCodeDetail.Exception.ToEnumDescription()
            };
        }
    }

    public async Task<ResponseSingle<dynamic>> GetOpenAccountDetail(OpenAccountDetailRequest model)
    {
        try
        {
            var param = new DynamicParameters();
            param.Add("@Id", model.Id, DbType.String, ParameterDirection.Input);
            var data = await _dapper.GetAsync<OpenAccountDetailResponse>(_commonDbConn, "TVSI_sGetOpenAccountDetail", param, _sqlTimeout);

            if (data != null)
            {
                data.SearchType = model.SearchType;
                var branchNo = CommonConstants.Branchiddefault;
                var branchName = CommonConstants.Branchnamedefault;
                var saleName = "";

                var paramBranch = new DynamicParameters();
                paramBranch.Add("@SaleID", data.SaleId, DbType.String, ParameterDirection.Input);
                var branchInfo =
                    await _dapper.GetAsync<BranchInfoResponse>(_emsDbConn, "TVSI_sGetBranchInfo", paramBranch, _sqlTimeout);
                if (branchInfo != null)
                {
                    if (!string.IsNullOrEmpty(branchInfo.BranchName))
                        branchName = branchInfo.BranchName;
                    if (!string.IsNullOrEmpty(branchInfo.BranchNo))
                        branchNo = branchInfo.BranchNo;
                    if (!string.IsNullOrEmpty(branchInfo.SaleName))
                        saleName = branchInfo.SaleName;
                }
                data.SaleName = saleName;
                data.BranchName = branchName;

                var paramTotalMargin = new DynamicParameters();
                paramTotalMargin.Add("@branchid", branchNo, DbType.String, ParameterDirection.Input);
                var totalMarginBranch = await _dapper.GetAsync<double?>(_commonDbConn, "TVSI_sLAY_TONG_HAN_MUC_DU_NO", paramTotalMargin, _sqlTimeout);
                totalMarginBranch = totalMarginBranch == null ? 0 : totalMarginBranch;


                var paramMarginBranch = new DynamicParameters();
                paramMarginBranch.Add("@BranchId", branchNo, DbType.String, ParameterDirection.Input);
                var marginBranch = await _dapper.GetAsync<double?>(_emsDbConn, "select han_muc_du_no from TVSI_FS_THAM_SO_NO_HOP_DONG where ma_chi_nhanh = @BranchId", paramMarginBranch, _sqlTimeout, CommandType.Text);
                marginBranch = marginBranch == null ? 0 : marginBranch;

                data.BranchMargin = marginBranch?.ToString("#,##0");// string.Format("{#,##0}", marginBranch);
                data.RemainBranchMargin = (marginBranch - totalMarginBranch)?.ToString("#,##0");//string.Format("{#,##0}", (marginBranch - totalMarginBranch));
                if (data.SourceAccount == (int)CommonDetail.SourceAccount.OpenAccountCRM 
                    || data.SourceAccount == (int)CommonDetail.SourceAccount.OpenWebsite 
                    || data.SourceAccount == (int)CommonDetail.SourceAccount.TWork)
                {
                    if (data.CardIdFileData != null && data.CardIdFileData.Length > 0)
                        data.CardIdFileDataBase64 = Convert.ToBase64String(data.CardIdFileData);
                    if (data.CardIdBackFileData != null && data.CardIdBackFileData.Length > 0)
                        data.CardIdBackFileDataBase64 = Convert.ToBase64String(data.CardIdBackFileData);
                }
                
                else if (data.SourceAccount == (int)CommonDetail.SourceAccount.OpeneKYCWeb)
                {
                    //var urlFront = _urlHome + "EkycCustomer/" + model.Id + "/" + model.Id + "_Front.jpg";
                    //var urlBack = _urlHome + "EkycCustomer/" + model.Id + "/" + model.Id + "_Back.jpg";
                    //var urlFace = _urlHome + "EkycCustomer/" + model.Id + "/" + model.Id + "_Face.jpg";

                    //var frontByte = GetImageByte.GetImageByteUrl(urlFront);
                    //var backByte = GetImageByte.GetImageByteUrl(urlBack);
                    //var faceByte = GetImageByte.GetImageByteUrl(urlFace);
                    //if (frontByte != null && frontByte.Length > 0 && backByte != null && backByte.Length > 0 && faceByte != null && faceByte.Length > 0)
                    //{
                    //    data.CardIdFileDataBase64 = Convert.ToBase64String(frontByte);
                    //    data.CardIdBackFileDataBase64 = Convert.ToBase64String(backByte);
                    //}
                }

                else if (data.SourceAccount == (int)CommonDetail.SourceAccount.OpeneKYCMobile && !string.IsNullOrEmpty(data.CardId))
                {
                    var paramImage = new
                    {
                        CardId = data.CardId
                    };
                    var responseData = CallApiHelper.GetImageCardId(data.CardId, paramImage, _apiBond);
                    if (responseData != null
                        && !string.IsNullOrEmpty(responseData.RetData.Front_Image)
                        && !string.IsNullOrEmpty(responseData.RetData.Back_Image))
                    {

                        data.CardIdFileDataBase64 = responseData.RetData.Front_Image;
                        data.CardIdBackFileDataBase64 = responseData.RetData.Back_Image;
                    }
                }
                else
                {
                    data.CardIdFileDataBase64 = "";
                    data.CardIdBackFileDataBase64 = "";
                }

                var paramExtend = new DynamicParameters();
                paramExtend.Add("@Custcode", data.Custcode, DbType.String, ParameterDirection.Input);
                var dataExtend = await _dapper.SelectAsync<ExtendInfo>(_crmDbConn,
                    "TVSI_sGetExtendAccountInfo", paramExtend, _sqlTimeout);
                if (dataExtend != null)
                {
                    var lstExtentInfo = new List<ExtendInfo>();
                    foreach (var item in dataExtend)
                    {
                        lstExtentInfo.Add(item);
                    }
                    data.ExtendCust = lstExtentInfo;
                }
                var paramShareHolder = new DynamicParameters();
                paramShareHolder.Add("@Custcode", data.Custcode, DbType.String, ParameterDirection.Input);
                var dataShareHolder = await _dapper.SelectAsync<ShareHolder>(_crmDbConn,
                    "TVSI_sGET_SHAREHOLDER_INFO_CUSTOMER", paramShareHolder, _sqlTimeout);
                if (dataShareHolder?.Count() > 0)
                {
                    data.IsShareHolder = true;
                    List<ShareHolder> lstShareHolder = new List<ShareHolder>();
                    foreach (var item in dataShareHolder)
                    {
                        lstShareHolder.Add(item);
                    }
                    data.ShareHolderInfo = lstShareHolder;
                }
                else
                {
                    data.IsShareHolder = false;
                }
            }

            return new ResponseSingle<dynamic>
            {
                Code = ((int)ErrorCodeDetail.Success).ErrorCodeFormat(),
                Message = ErrorCodeDetail.Success.ToEnumDescription(),
                Data = data
            };
        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
            return new ResponseSingle<dynamic>
            {
                Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                Message = ErrorCodeDetail.Exception.ToEnumDescription()
            };
        }
    }

    public async Task<ResponseSingle<bool>> UpdateAccountStatus(UpdateAccountStatusRequest model)
    {
        try
        {

            var saleId = model.SaleId.Length > 4 ? model.SaleId.Substring(0, 4) : model.SaleId;

            var isMaxDebtProfile = await _dapper.GetAsync<int>(_commonDbConn,
                "TVSI_sCheckMaxCountDebtDocument", new DynamicParameters(new
                {
                    SaleId = saleId
                }), _sqlTimeout);


            var accStatus = AccountStatusConst.Process;
            if (!model.IsConfirm)
            {
                if (model.AccountStatus == AccountStatusConst.WaitingBuMen)
                    accStatus = AccountStatusConst.BuMenReject;
                if (model.AccountStatus == AccountStatusConst.BDSaleApprove)
                    accStatus = AccountStatusConst.BDSaleReject;
            }
            else
            {
                if (model.AccountStatus == AccountStatusConst.WaitingBuMen)
                {
                    if (isMaxDebtProfile != 0)
                        accStatus = AccountStatusConst.BDSaleApprove;
                    else if (model.AddContract.Contains(((int)AdditionContractConst.Margin).ToString()))
                        accStatus = AccountStatusConst.WaitingFs;
                    else
                        accStatus = AccountStatusConst.Process;
                }
                if (model.AccountStatus == AccountStatusConst.BDSaleApprove)
                {
                    if (model.AddContract.Contains(((int)AdditionContractConst.Margin).ToString()))
                        accStatus = AccountStatusConst.WaitingFs;
                    else
                        accStatus = AccountStatusConst.Process;
                }
            }
            var param = new DynamicParameters();
            param.Add("@Id", model.Id, DbType.Int32, ParameterDirection.Input);
            param.Add("@AccountStatus", accStatus, DbType.String, ParameterDirection.Input);
            param.Add("@UserName", model.UserName, DbType.String, ParameterDirection.Input);
            param.Add("@Description", model.Description, DbType.String, ParameterDirection.Input);
            param.Add("@Result", null, DbType.Int32, ParameterDirection.Output);
            await _dapper.ExecuteAsync(_commonDbConn, "TVSI_sUpdateAccountStatus", param, _sqlTimeout);
            var result = param.Get<int>("@Result");
            return new ResponseSingle<bool>
            {
                Code = (result is (int)ErrorCodeDetail.Success
                    ? (int)ErrorCodeDetail.Success
                    : (int)ErrorCodeDetail.Failed).ErrorCodeFormat(),
                Message = result is (int)ErrorCodeDetail.Success
                    ? ErrorCodeDetail.Success.ToEnumDescription()
                    : ErrorCodeDetail.Failed.ToEnumDescription(),
                Data = result is (int)ErrorCodeDetail.Success
            };
        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
            return new ResponseSingle<bool>
            {
                Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                Message = ErrorCodeDetail.Exception.ToEnumDescription()
            };
        }
    }

    public async Task<ResponseSingle<bool>> UpdateAdditionalAccount(UpdateAdditionalAccountRequest model)
    {
        try
        {
            var param = new DynamicParameters();
            param.Add("@Id", model.Id, DbType.Int32, ParameterDirection.Input); // Id
            param.Add("@CreditLimit", model.CreditLimit, DbType.Decimal, ParameterDirection.Input); // Hạn mức dư nợ
            param.Add("@DebtTerm", model.DebtTerm, DbType.Int32, ParameterDirection.Input); // Thời gian ghi nợ
            param.Add("@DepositRate", model.DepositRate, DbType.Double, ParameterDirection.Input); // Ty lệ ký quỹ
            param.Add("@Result", dbType: DbType.Int32, direction: ParameterDirection.Output); // OUTPUT
            await _dapper.ExecuteAsync(_commonDbConn, "TVSI_sUPDATE_ADDITIONAL_ACCOUNT_CONTRACT_FS_INFO", param, _sqlTimeout);
            var result = param.Get<int>("@Result");
            return new ResponseSingle<bool>
            {
                Code = (result is (int)ErrorCodeDetail.Success
                    ? (int)ErrorCodeDetail.Success
                    : (int)ErrorCodeDetail.Failed).ErrorCodeFormat(),
                Message = result is (int)ErrorCodeDetail.Success
                    ? ErrorCodeDetail.Success.ToEnumDescription()
                    : ErrorCodeDetail.Failed.ToEnumDescription(),
                Data = result is (int)ErrorCodeDetail.Success
            };
        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
            return new ResponseSingle<bool>
            {
                Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                Message = ErrorCodeDetail.Exception.ToEnumDescription()
            };
        }
    }

    public async Task<ResponseSingle<ListCustcodeReponseAccount>> ListCustcode(GetCustcodeRequestAccount model)
    {
        try
        {
            var start = (model.PageIndex - 1) * model.PageSize;

            var param = new DynamicParameters();
            param.Add("@userName", model.UserName, DbType.String, ParameterDirection.Input);
            param.Add("@Start", start, DbType.Int32, ParameterDirection.Input);
            param.Add("@PageSize", model.PageSize, DbType.Int32, ParameterDirection.Input);
            param.Add("@CustCode", model.CustCode, DbType.String, ParameterDirection.Input);
            var data = await _dapper.GetAsync<ListCustcodeSqlAccount>(_crmDbConn, "GET_CUSTOMER_CODE_LIST", param, _sqlTimeout);

            if (data != null)
            {
                var dataResponse = new ListCustcodeReponseAccount();

                dataResponse.TotalItem = data.TotalItem;

                if (!string.IsNullOrEmpty(data.Items))
                    dataResponse.Items = JsonConvert.DeserializeObject<List<ListCustcodeAccount>>(data.Items);

                return new ResponseSingle<ListCustcodeReponseAccount>
                {
                    Code = ((int)ErrorCodeDetail.Success).ErrorCodeFormat(),
                    Message = ErrorCodeDetail.Success.ToEnumDescription(),
                    Data = dataResponse
                };
            }

            return new ResponseSingle<ListCustcodeReponseAccount>
            {
                Code = ((int)ErrorCodeDetail.Success).ErrorCodeFormat(),
                Message = ErrorCodeDetail.Success.ToEnumDescription(),
                Data = null
            };
        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
            return new ResponseSingle<ListCustcodeReponseAccount>
            {
                Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                Message = ErrorCodeDetail.Exception.ToEnumDescription()
            };
        }
    }

    public async Task<ResponseSingle<StringeeResponse>> GetStringeeToken(BaseRequest model)
    {
        try
        {
            StringeeResponse response = new StringeeResponse();

            response.AcceptTokenStringee = StringeeToken.GenerateToken(model.UserName);

            return new ResponseSingle<StringeeResponse>
            {
                Code = ((int)ErrorCodeDetail.Success).ErrorCodeFormat(),
                Message = ErrorCodeDetail.Success.ToEnumDescription(),
                Data = response
            };
        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
            return new ResponseSingle<StringeeResponse>
            {
                Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                Message = ErrorCodeDetail.Exception.ToEnumDescription()
            };
        }
    }


    public async Task<ResponseSingle<bool>> OpenForeignAccount(OpenForeignAccountRequest model)
    {
        try
        {
            var modelCheck = new CheckCustomerCodeRequest()
            {
                UserName = model.UserName,
                CustomerCode = model.CustomerCode,
                TypeAccount = 3
            };
            var checkCustCode = await CheckCustomerCode(modelCheck);
            var retCheck = checkCustCode.Data.Code;
            if (retCheck == 0 || retCheck == 33)
            {
                var description = "HĐ thường";

                var branchNo = CommonConstants.Branchiddefault;
                var branchName = CommonConstants.Branchnamedefault;

                var paramBranch = new DynamicParameters();
                paramBranch.Add("@SaleID", model.SaleID, DbType.String, ParameterDirection.Input);
                var branchInfo = await _dapper.GetAsync<BranchInfoResponse>(_emsDbConn, "TVSI_sGetBranchInfo", paramBranch, _sqlTimeout);
                if (branchInfo != null)
                {
                    if (!string.IsNullOrEmpty(branchInfo.BranchName))
                        branchName = branchInfo.BranchName;
                    if (!string.IsNullOrEmpty(branchInfo.BranchNo))
                        branchNo = branchInfo.BranchNo;
                }

                var serviceRegister = CommonConstants.BankNotRegister;
                var transferType = "";

                var bank01 = CommonConstants.BankNotRegister;
                var bank02 = CommonConstants.BankNotRegister;
                var bank03 = CommonConstants.BankNotRegister;
                var bank04 = CommonConstants.BankNotRegister;

                if (!string.IsNullOrEmpty(model.BankNo))
                    bank01 = 0;

                if (bank01 == 0 )
                {
                    serviceRegister = 0;
                    transferType += 1;
                }

                // trạng thái dv kênh thông tin
                var ChannelStatusInfo = CommonConstants.BankNotRegister;

                var birthDay = (!string.IsNullOrEmpty(model.BirthDay)) ?
                    DateTime.ParseExact(model.BirthDay!, CommonConstants.DateFormat, CultureInfo.CurrentCulture) : (DateTime?)null;
                var issueDate = (!string.IsNullOrEmpty(model.IssueDate)) ?
                    DateTime.ParseExact(model.IssueDate!, CommonConstants.DateFormat, CultureInfo.CurrentCulture) : (DateTime?)null;
                var manIssueDate = (!string.IsNullOrEmpty(model.ManIssueDate)) ?
                    DateTime.ParseExact(model.ManIssueDate!, CommonConstants.DateFormat, CultureInfo.CurrentCulture) : (DateTime?)null;
                var manDateAnPower = (!string.IsNullOrEmpty(model.ManDateAnPower)) ?
                    DateTime.ParseExact(model.ManDateAnPower!, CommonConstants.DateFormat, CultureInfo.CurrentCulture) : (DateTime?)null;

                var param = new DynamicParameters();
                param.Add("@ProfileType", DocumentTypeConst.OPEN_DOCUMENT, DbType.Int32, ParameterDirection.Input);
                param.Add("@CustomerCode", model.CustomerCode, DbType.String, ParameterDirection.Input);
                param.Add("@AccountNo", "044C" + model.CustomerCode, DbType.String, ParameterDirection.Input);
                param.Add("@TypeAccount", TypeAccountConst.INDIVIDUAL_DOMESTIC_INVEST, DbType.Int32, ParameterDirection.Input);
                param.Add("@FullName", model.FullName?.ToTitleCase(), DbType.String, ParameterDirection.Input); // họ tên
                param.Add("@BirthDay", birthDay, DbType.DateTime, ParameterDirection.Input); // ngày sinh
                param.Add("@Gender", model.Gender, DbType.Int32, ParameterDirection.Input); // giói tính
                param.Add("@CardId", model.CardId, DbType.String, ParameterDirection.Input); // số cmt/cccd
                param.Add("@IssueDate", issueDate, DbType.DateTime, ParameterDirection.Input); // ngày cấp cmt/cccd
                param.Add("@IssuePlace", model.IssuePlace, DbType.String, ParameterDirection.Input); // nơi cấp cmt/cccd
                param.Add("@Address", model.Address, DbType.String, ParameterDirection.Input); // địa chỉ liên hệ
                param.Add("@NationalityName", Nationality.NationalityName, DbType.String, ParameterDirection.Input); // quốc tịch
                param.Add("@NationalityCode", Nationality.NationalityCode, DbType.String, ParameterDirection.Input); // quốc tịch
                param.Add("@ManTvsi", model.ManTvsi, DbType.String, ParameterDirection.Input); // đại diện tvsi
                param.Add("@ManPosition", model.ManPosition, DbType.String, ParameterDirection.Input); // vị trí
                param.Add("@ManAnPower", model.ManAnPower, DbType.String, ParameterDirection.Input); // giấy ủy quền
                param.Add("@ManDateAnPower", manDateAnPower, DbType.DateTime, ParameterDirection.Input); // ngày cấp giấy ủy quyền
                param.Add("@ManCardId", model.ManCardId, DbType.String, ParameterDirection.Input); // số cmt/cccd
                param.Add("@ManIssueDate", manIssueDate, DbType.DateTime, ParameterDirection.Input); // ngày cấp cmt/cccd
                param.Add("@ManIssuePlace", model.ManIssuePlace, DbType.String, ParameterDirection.Input); // nơi cấp cmt /cccd
                param.Add("@TradingResultMethod", model.TradingResultMethod, DbType.String, ParameterDirection.Input); // Phương thức nhận KQ giao dịch
                param.Add("@TransferType", transferType, DbType.String, ParameterDirection.Input); // Dịch vụ chuyển tiền
                param.Add("@Phone_01", model.Phone01, DbType.String, ParameterDirection.Input); // SĐT 01 
                param.Add("@Phone_02", model.Phone02, DbType.String, ParameterDirection.Input); // SĐT 02 
                param.Add("@Email", model.Email, DbType.String, ParameterDirection.Input); // Email
                if (bank01 == 0)
                {
                    param.Add("@BankAccountName_01", model.BankAccountName, DbType.String, ParameterDirection.Input); // tên chủ tk ngân hàng
                    param.Add("@BankAccountNo_01" , model.BankAccountNo, DbType.String, ParameterDirection.Input); // số tk ngân hàng

                    if (!string.IsNullOrEmpty(model.BankNo))
                        param.Add("@BankName_01", _bankService.GetBankName(model.BankNo).Result.Data ?? "", DbType.String, ParameterDirection.Input); // tên ngân hàng
                    if (!string.IsNullOrEmpty(model.BankNo) && !string.IsNullOrEmpty(model.SubBranchNo))
                        param.Add("@SubBranchName_01", _bankService.GetSubBranchName(model.BankNo, model.SubBranchNo).Result.Data ?? "", DbType.String, ParameterDirection.Input); // tên chi nhánh

                    param.Add("@SubBranchNo_01", model.SubBranchNo, DbType.String, ParameterDirection.Input); // mã chi nhánh
                    param.Add("@BankNo_01", model.BankNo, DbType.String, ParameterDirection.Input); // mã NH
                }


                param.Add("@ServiceStatus", serviceRegister, DbType.Int32, ParameterDirection.Input); // trạng thái dịch vụ
                param.Add("@Bank01Status", bank01, DbType.Int32, ParameterDirection.Input); // trạng thái ngân hàng 1
                param.Add("@Bank02Status", bank02, DbType.Int32, ParameterDirection.Input); // trạng thái ngân hàng 2
                param.Add("@Bank03Status", bank03, DbType.Int32, ParameterDirection.Input); // trạng thái ngân hàng 3
                param.Add("@Bank04Status", bank04, DbType.Int32, ParameterDirection.Input); // trạng thái ngân hàng 4
                param.Add("@ChannelStatusInfo", ChannelStatusInfo, DbType.Int32, ParameterDirection.Input); // trạng thái kênh thông tin
                param.Add("@Description", description, DbType.String, ParameterDirection.Input); // Mô tả
                param.Add("@BranchID", branchNo, DbType.String, ParameterDirection.Input); // mã chi nhánh
                param.Add("@BranchName", branchName, DbType.String, ParameterDirection.Input); // tên chi nhánh
                param.Add("@SaleId", model.SaleID, DbType.String, ParameterDirection.Input); // SaleId
                param.Add("@SourceType", 3, DbType.String, ParameterDirection.Input); // Mở từ nguồn
                param.Add("@ProfileStatus", ProfileStatusConst.Process, DbType.String, ParameterDirection.Input); // Trạng thái hồ sơ
                param.Add("@AccountStatus", AccountStatusConst.Process, DbType.String, ParameterDirection.Input); // Trạng thái tk
                param.Add("@Result", null, DbType.String, ParameterDirection.Output, 100); // OUTPUT
                await _dapper.ExecuteAsync(_commonDbConn, "TVSI_sOpen_Account", param, _sqlTimeout);
                var result = -1;
                var resultIdInsert = Convert.ToInt64(param.Get<string?>("@Result"));
               

                return new ResponseSingle<bool>
                {
                    Code = (result is (int)ErrorCodeDetail.Success
                        ? (int)ErrorCodeDetail.Success
                        : (int)ErrorCodeDetail.Failed).ErrorCodeFormat(),
                    Message = result is (int)ErrorCodeDetail.Success
                        ? ErrorCodeDetail.Success.ToEnumDescription()
                        : ErrorCodeDetail.Failed.ToEnumDescription(),
                    Data = result is (int)ErrorCodeDetail.Success
                };
            }
            else
            {
                return new ResponseSingle<bool>
                {
                    Code = ((int)ErrorCodeDetail.Registered).ErrorCodeFormat(),
                    Message = ErrorCodeDetail.Registered.ToEnumDescription(),
                };
            }
        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
            return new ResponseSingle<bool>
            {
                Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                Message = ErrorCodeDetail.Exception.ToEnumDescription()
            };
        }
    }


    private void AddCentralizeImageDB(long openAccID, int? sourceType, string CustCode, string userName, 
                                        string frontName, string frontData, string backName, string backData)
    {
        try
        {
            using (var imageConn = new SqlConnection(_signatureDbConn))
            {
                var sql = @"Insert into TVSI_OPENACCOUNT_FILES(CustCode, OpenAccID, SourceType, Type, FileName, FileData, Status, CreatedBy, CreatedDate)
                            values(@CustCode, @OpenAccID, @SourceType, @Type, @FileName, @FileData, @Status, @CreatedBy, @CreatedDate)"; ;


                imageConn.Execute(sql, new
                {
                    CustCode = CustCode,
                    OpenAccID = openAccID,
                    SourceType = sourceType,
                    Type = 1,
                    FileName = frontName,
                    FileData = frontData,
                    Status = 1,
                    CreatedBy = userName,
                    CreatedDate = DateTime.Now
                });

                imageConn.Execute(sql, new
                {
                    CustCode = CustCode,
                    OpenAccID = openAccID,
                    SourceType = sourceType,
                    Type = 2,
                    FileName = backName,
                    FileData = backData,
                    Status = 1,
                    CreatedBy = userName,
                    CreatedDate = DateTime.Now
                });
            }

            //using (var connCommon = new SqlConnection(_commonDbConn))
            //{
            //    var sql = @"select ma_khach_hang CustCode, file_data FrontSide, file_data_backside BackSide 
            //                    from TVSI_DANG_KY_MO_TAI_KHOAN_FILES where ho_so_mo_tk_id = @ID";
            //    var openAccInfo = connCommon.QueryFirstOrDefault<dynamic>(sql, new { ID = openAccID });
            //    if (openAccInfo != null)
            //    {
            //        using (var imageConn = new SqlConnection(_signatureDbConn))
            //        {
            //            sql = @"Insert into TVSI_OPENACCOUNT_FILES(CustCode, OpenAccID, SourceType, Type, FileName, FileData, Status, CreatedBy, CreatedDate)
            //                values(@CustCode, @OpenAccID, @SourceType, @Type, @FileName, @FileData, @Status, @CreatedBy, @CreatedDate)"; ;


            //            if (openAccInfo.FrontSide != null && openAccInfo.FrontSide.Length > 0)
            //            {
            //                var dataImage = Convert.ToBase64String(openAccInfo.FrontSide);
            //                imageConn.Execute(sql, new
            //                {
            //                    CustCode = openAccInfo.CustCode,
            //                    OpenAccID = openAccID,
            //                    SourceType = sourceType,
            //                    Type = 1,
            //                    FileName = openAccID + ".jpg",
            //                    FileData = dataImage,
            //                    Status = 1,
            //                    CreatedBy = userName,
            //                    CreatedDate = DateTime.Now
            //                });
            //            }
            //            if (openAccInfo.BackSide != null && openAccInfo.BackSide.Length > 0)
            //            {
            //                var dataImage = Convert.ToBase64String(openAccInfo.BackSide);
            //                imageConn.Execute(sql, new
            //                {
            //                    CustCode = openAccInfo.CustCode,
            //                    OpenAccID = openAccID,
            //                    SourceType = sourceType,
            //                    Type = 2,
            //                    FileName = openAccID + "_ms.jpg",
            //                    FileData = dataImage,
            //                    Status = 1,
            //                    CreatedBy = userName,
            //                    CreatedDate = DateTime.Now
            //                });
            //            }
            //        }
            //    }
            //}

        }
        catch (Exception ex)
        {
            LogException(userName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
        }
    }
}