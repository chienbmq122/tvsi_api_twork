﻿using System.Reflection;
using Helper.DataAccess.Dapper;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using TVSI.Crm.API.Common.ConstParams;
using TVSI.Crm.API.Lib.Models;

namespace TVSI.Crm.API.Lib.Services.Impls;

public class DistributeCacheService : BaseService<DistributeCacheService>, IDistributeCacheService
{
    private readonly IRedisService _redisService;
    private readonly int _timeout;
    private readonly IDapperHelper _dapper;
    private readonly string _crmDbConn;

    public DistributeCacheService(IConfiguration config,
        IRedisService redisService, ILogger<DistributeCacheService> logger, 
        IDapperHelper dapper) : base(config, logger)
    {
        _redisService = redisService;
        _timeout = 0; //seconds
        var connStr = _config.GetConnectionString("CRMDB_CONNECTION");
        _crmDbConn = connStr;
        _dapper = dapper;
    }

    public async Task LoadDataToCache()
    {
        await LoadLanguageToCache();
    }

    #region Language
    public string? GetItemName(string itemCode, string lang = "VI")
    {
        var key = CacheKeyConst.Language;
        var languageList = _redisService.GetKey<List<LanguageItem>>(key);

        var langInfo = languageList.FirstOrDefault(x => x.ItemCode == itemCode);

        if (lang == "EN")
            return langInfo?.English;

        return langInfo?.Vietnamese;
    }

    private async Task LoadLanguageToCache()
    {
        try
        {
            var key = CacheKeyConst.Language;

            var data = await _dapper.SelectAsync<LanguageItem>(_crmDbConn,
                "SYS001_GET_ALL_LANGUAGES", null!, 60);
            
            _redisService.SetKey(key, data, _timeout);
        }
        catch (Exception ex)
        {
            LogException("System", $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
        }
    }

       
}
#endregion