using System.Data;
using System.Reflection;
using System.Text.Json.Serialization;
using Dapper;
using Helper.DataAccess.Dapper;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using TVSI.Crm.API.Common;
using TVSI.Crm.API.Common.Enums;
using TVSI.Crm.API.Lib.Models;
using TVSI.Crm.API.Lib.Models.Request.Shareholder;
using TVSI.TWork.API.Common.ExtensionMethods;

namespace TVSI.Crm.API.Lib.Services.Impls;

public class ShareholderService : BaseService<ShareholderService>, IShareholderService
{
    private readonly IDapperHelper _dapper;
    private readonly string _crmDbConn;
    private readonly string _commonDbConn;
    private readonly string _emsDbConn;
    private readonly string _innoDbConn;
    private readonly string _priceinfoDbConn;
    private readonly int _sqlTimeout;

    public ShareholderService(IConfiguration config, ILogger<ShareholderService> logger, IDapperHelper dapper): base(config,
        logger)
    {
        var connStr = _config.GetConnectionString("CRMDB_CONNECTION");
        var commonStr = _config.GetConnectionString("COMMONDB_CONNECTION");
        var emsStr = _config.GetConnectionString("EMSDB_CONNECTION");
        var innoStr = _config.GetConnectionString("INNODB_CONNECTION");
        var priceStr = _config.GetConnectionString("PRICEINOFO_CONNECTION");
        _crmDbConn = connStr;
        _commonDbConn = commonStr;
        _emsDbConn = emsStr;
        _innoDbConn = innoStr;
        _priceinfoDbConn = priceStr;
        _dapper = dapper;
        _sqlTimeout = _config["Timeout:Database"] == null
            ? CommonConstants.SqlServerTimeout
            : int.Parse(_config["Timeout:Database"]);
    }

    public async Task<Response<dynamic>> GetShareholderList(ShareholderListRequest model)
    {
        try
        {
            var start = (model.PageIndex - 1) * model.PageSize;
            var param = new DynamicParameters();
            param.Add("@UserName", model.UserName ,DbType.String,ParameterDirection.Input);
            param.Add("@Custcode", model.Custcode ,DbType.String,ParameterDirection.Input);
            param.Add("@StockCode", model.StockCode ,DbType.String,ParameterDirection.Input);
            param.Add("@FullName", model.FullName ,DbType.String,ParameterDirection.Input);
            param.Add("@ShareholderType", model.ShareholderType ,DbType.Int32,ParameterDirection.Input);
            param.Add("@Status", model.Status ,DbType.Int32,ParameterDirection.Input);
            param.Add("@start", start, DbType.Int32, ParameterDirection.Input);
            param.Add("@PageSize", model.PageSize, DbType.Int32, ParameterDirection.Input);
            return new Response<dynamic>
            {
                Code = ((int)ErrorCodeDetail.Success).ErrorCodeFormat(),
                Message = ErrorCodeDetail.Success.ToEnumDescription(),
                Data  = await _dapper.SelectAsync<dynamic>(_crmDbConn,
                    "TVSI_sGET_SHAREHOLDER_LIST", param, _sqlTimeout)
            };
        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
            return new Response<dynamic>
            {
                Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                Message = ErrorCodeDetail.Exception.ToEnumDescription()
            };
        }
    }

    public async Task<ResponseSingle<bool>> CreateShareholder(CreateShareholderRequest model)
    {
        try
        {
            var param = new DynamicParameters();
            param.Add("@UserName", model.UserName ,DbType.String,ParameterDirection.Input);
            param.Add("@Custcode", model.CustCode ,DbType.String,ParameterDirection.Input);
            param.Add("@FullName", model.FullName ,DbType.String,ParameterDirection.Input);
            param.Add("@StockCode", model.StockCode ,DbType.String,ParameterDirection.Input);
            param.Add("@Address", model.Address ,DbType.String,ParameterDirection.Input);
            param.Add("@ShareholderType01", model.ShareholderType01 ? 1 : 0 ,DbType.Int32,ParameterDirection.Input);
            param.Add("@ShareholderType02", model.ShareholderType02 ? 1 : 0 ,DbType.Int32,ParameterDirection.Input);
            param.Add("@ShareholderType03", model.ShareholderType03 ? 1 : 0 ,DbType.Int32,ParameterDirection.Input);
            param.Add("@ShareholderType04", model.ShareholderType04 ? 1 : 0 ,DbType.Int32,ParameterDirection.Input);
            param.Add("@Result", null ,DbType.Int32,ParameterDirection.Output);
            await _dapper.ExecuteAsync(_crmDbConn,
                "TVSI_sCREATE_SHAREHOLDER", param, _sqlTimeout);
            var result = param.Get<int>("@Result");
            return new ResponseSingle<bool>
            {
                Code = (result is (int)ErrorCodeDetail.Success
                    ? (int)ErrorCodeDetail.Success
                    : (int)ErrorCodeDetail.Failed).ErrorCodeFormat(),
                Message = result is (int)ErrorCodeDetail.Success
                    ? ErrorCodeDetail.Success.ToEnumDescription()
                    : ErrorCodeDetail.Failed.ToEnumDescription(),
                Data = result is (int)ErrorCodeDetail.Success
            };
        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
            return new ResponseSingle<bool>
            {
                Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                Message = ErrorCodeDetail.Exception.ToEnumDescription()
            };
        }
    }

    public async Task<ResponseSingle<dynamic>> GetShareholderDetail(ShareholderDetailRequest model)
    {
        try
        {
            var param = new DynamicParameters();
            param.Add("@Custcode", model.Custcode ,DbType.String,ParameterDirection.Input);
            param.Add("@UserName", model.UserName ,DbType.String,ParameterDirection.Input);
            param.Add("@Id", model.Id,DbType.String,ParameterDirection.Input);
            return new ResponseSingle<dynamic>
            {
                Code = ((int)ErrorCodeDetail.Success).ErrorCodeFormat(),
                Message = ErrorCodeDetail.Success.ToEnumDescription(),
                Data  = await _dapper.SelectAsync<dynamic>(_crmDbConn,
                    "TVSI_sGET_SHAREHOLDER_DETAIL_INFO", param, _sqlTimeout)
            };
        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
            return new ResponseSingle<dynamic>
            {
                Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                Message = ErrorCodeDetail.Exception.ToEnumDescription()
            };
        }
    }

    public async Task<ResponseSingle<dynamic>> CheckCustcodeShareholder(string? custcode,string? username)
    {
        try
        {
            var param = new DynamicParameters();
            param.Add("@Custcode", custcode ,DbType.String,ParameterDirection.Input);
            param.Add("@UserName", username ,DbType.String,ParameterDirection.Input); 
            param.Add("@Result", null ,DbType.Int32,ParameterDirection.Output);
            await _dapper.GetAsync<dynamic>(_crmDbConn,
                "TVSI_sCHECK_CUSTCODE_SHAREHOLDER_INFO", param, _sqlTimeout);
            var result = param.Get<int>("@Result");
            return new ResponseSingle<dynamic>
            {
                Code = (result is (int)ErrorCodeDetail.Success
                    ? (int)ErrorCodeDetail.Success
                    : (int)ErrorCodeDetail.Failed).ErrorCodeFormat(),
                Message = result is (int)ErrorCodeDetail.Success
                    ? ErrorCodeDetail.Success.ToEnumDescription()
                    : "Mã Khách Hàng không tồn tại.",
                Data = await _dapper.GetAsync<dynamic>(_crmDbConn,
                    "TVSI_sCHECK_CUSTCODE_SHAREHOLDER_INFO", param, _sqlTimeout)
            };
            
        }
        catch (Exception ex)
        {
            LogException(custcode ?? "Shareholder",
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
            return new ResponseSingle<dynamic>
            {
                Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                Message = ErrorCodeDetail.Exception.ToEnumDescription()
            };
        }
    }

    public async Task<Response<dynamic>> GetStockInfoList(BaseRequest model)
    {
        try
        {
            return new Response<dynamic>
            {
                Code = ((int)ErrorCodeDetail.Success).ErrorCodeFormat(),
                Message = ErrorCodeDetail.Success.ToEnumDescription(),
                Data  = await _dapper.SelectAsync<dynamic>(_priceinfoDbConn,
                    "TVSI_sGET_STOCKINFO_LIST", null, _sqlTimeout)
            };
        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
            return new Response<dynamic>
            {
                Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                Message = ErrorCodeDetail.Exception.ToEnumDescription()
            };
        }
    }

    public async Task<ResponseSingle<bool>> CancelShareholderInfo(CancelShareholderRequest model)
    {
        try
        {
            var param = new DynamicParameters();
            param.Add("@Custcode", model.Custcode ,DbType.String,ParameterDirection.Input);
            param.Add("@UserName", model.UserName ,DbType.String,ParameterDirection.Input); 
            param.Add("@Id", model.Id ,DbType.Int32,ParameterDirection.Input); 
            param.Add("@Result", null ,DbType.Int32,ParameterDirection.Output);
            await _dapper.ExecuteAsync(_crmDbConn,
                "TVSI_sCANCEL_SHAREHOLDER_INFO", param, _sqlTimeout);
            var result = param.Get<int>("@Result");
            return new ResponseSingle<bool>
            {
                Code = (result is (int)ErrorCodeDetail.Success
                    ? (int)ErrorCodeDetail.Success
                    : (int)ErrorCodeDetail.Failed).ErrorCodeFormat(),
                Message = result is (int)ErrorCodeDetail.Success
                    ? ErrorCodeDetail.Success.ToEnumDescription()
                    : ErrorCodeDetail.Failed.ToEnumDescription(),
                Data = result is (int)ErrorCodeDetail.Success
            };
        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
            return new ResponseSingle<bool>
            {
                Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                Message = ErrorCodeDetail.Exception.ToEnumDescription()
            };
        }
    }

    public async Task<ResponseSingle<bool>> UpdateShareholder(UpdateShareholderRequest model)
    {
        try
        {
            var param = new DynamicParameters();
            param.Add("@Id", model.Id ,DbType.Int32,ParameterDirection.Input); 
            param.Add("@UserName", model.UserName ,DbType.String,ParameterDirection.Input); 
            param.Add("@StockCode", model.StockCode ,DbType.String,ParameterDirection.Input); 
            param.Add("@ShareholderType01", model.ShareholderType01 ? 1 : 0 ,DbType.String,ParameterDirection.Input); 
            param.Add("@ShareholderType02", model.ShareholderType02 ? 1 : 0 ,DbType.String,ParameterDirection.Input); 
            param.Add("@ShareholderType03", model.ShareholderType03 ? 1 : 0 ,DbType.String,ParameterDirection.Input); 
            param.Add("@ShareholderType04", model.ShareholderType04 ? 1 : 0 ,DbType.String,ParameterDirection.Input); 
            param.Add("@Result", null ,DbType.Int32,ParameterDirection.Output);
            await _dapper.ExecuteAsync(_crmDbConn,
                "TVSI_sUPDATE_SHAREHOLDER", param, _sqlTimeout);
            var result = param.Get<int>("@Result");
            return new ResponseSingle<bool>
            {
                Code = (result is (int)ErrorCodeDetail.Success
                    ? (int)ErrorCodeDetail.Success
                    : (int)ErrorCodeDetail.Failed).ErrorCodeFormat(),
                Message = result is (int)ErrorCodeDetail.Success
                    ? ErrorCodeDetail.Success.ToEnumDescription()
                    : ErrorCodeDetail.Failed.ToEnumDescription(),
                Data = result is (int)ErrorCodeDetail.Success
            };
        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
            return new ResponseSingle<bool>
            {
                Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                Message = ErrorCodeDetail.Exception.ToEnumDescription()
            };
        }
    }
}