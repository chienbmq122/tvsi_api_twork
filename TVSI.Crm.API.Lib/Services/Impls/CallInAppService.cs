﻿using Dapper;
using Helper.DataAccess.Dapper;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlTypes;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using TVSI.Crm.API.Common;
using TVSI.Crm.API.Lib.Models;
using TVSI.Crm.API.Lib.Models.Request.CallInApp;
using TVSI.Crm.API.Lib.Models.Response.CallInApp;
using TVSI.Crm.API.Lib.Models.Response.SaleInfo;
using TVSI.TWork.API.Common.Enums;
using TVSI.TWork.API.Common.ExtensionMethods;

namespace TVSI.Crm.API.Lib.Services.Impls
{
    public class CallInAppService : BaseService<CallInAppService>, ICallInAppService
    {
        private readonly IDapperHelper _dapper;
        private readonly string _crmDbConn;
        private readonly string _notificationDbConn;
        private readonly int _sqlTimeout;
        private readonly IDistributeCacheService _distributeCacheService;

        public CallInAppService(IConfiguration config, ILogger<CallInAppService> logger, 
            IDapperHelper dapper, IDistributeCacheService distributeCacheService) : base(config, logger)
        {
            var connStr = _config.GetConnectionString("CRMDB_CONNECTION");
            var notificationStr = _config.GetConnectionString("NOTIFICATIONDB_CONNECTION");
            _crmDbConn = connStr;
            _notificationDbConn = notificationStr;
            _dapper = dapper;
            _distributeCacheService = distributeCacheService;
            _sqlTimeout = _config["Timeout:Database"] == null
                ? CommonConstants.SqlServerTimeout
                : int.Parse(_config["Timeout:Database"]);
        }

        public async Task<ResponseSingle<CustomerContactInfoResponse>> GetListCustomerContact(CustomerContactInfoRequest model)
        {
            try
            {
                var start = (model.PageIndex - 1) * model.PageSize;

                var param = new DynamicParameters();

                param.Add("@custcode", model.Custcode, DbType.String, ParameterDirection.Input);
                param.Add("@name", model.Name, DbType.String, ParameterDirection.Input);
                param.Add("@phone", model.Phone, DbType.String, ParameterDirection.Input);
                param.Add("@assignUser", model.UserName, DbType.String, ParameterDirection.Input);
                param.Add("@start", start, DbType.Int32, ParameterDirection.Input);
                param.Add("@pageSize", model.PageSize, DbType.Int32, ParameterDirection.Input);
                param.Add("@UserName", model.UserName, DbType.String, ParameterDirection.Input);
                param.Add("@SaleID", model.UserName.Substring(0,4), DbType.String, ParameterDirection.Input);
                var data = await _dapper.GetAsync<CustomerContactInfoSql>(_crmDbConn, "BBB14_GET_CUSTOMER_CONTACT_INFO", param, _sqlTimeout);

                var dataResponse = new CustomerContactInfoResponse();

                if (String.IsNullOrEmpty(data.Items))
                {
                    return new ResponseSingle<CustomerContactInfoResponse>
                    {
                        Code = ((int)ErrorCodeDetail.Success).ErrorCodeFormat(),
                        Message = ErrorCodeDetail.Success.ToEnumDescription(),
                        Data = dataResponse
                    };
                }

                dataResponse.TotalItem = data.TotalItem;
                dataResponse.Items = JsonConvert.DeserializeObject<List<CustomerContactInfo>>(data.Items);

                return new ResponseSingle<CustomerContactInfoResponse>
                {
                    Code = ((int)ErrorCodeDetail.Success).ErrorCodeFormat(),
                    Message = ErrorCodeDetail.Success.ToEnumDescription(),
                    Data = dataResponse
                };

            }
            catch (Exception ex)
            {
                LogException(model.UserName,
                    $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
                return new ResponseSingle<CustomerContactInfoResponse> {
                    Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                    Message = ex.Message
                };
            }
        }

        public async Task<ResponseSingle<bool>> CreateCallApp(CreateCallAppRequest model)
        {
            try
            {
                var paramSaleInfo = new DynamicParameters();
                paramSaleInfo.Add("@UserName", model.UserName, DbType.String, ParameterDirection.Input);
                var saleInfo = await _dapper.GetAsync<SaleInfoReponse>(_crmDbConn, "BBB35_GET_INFO_LEAD", paramSaleInfo, _sqlTimeout);

                if (saleInfo == null || String.IsNullOrEmpty(saleInfo.SaleID))
                {
                    return new ResponseSingle<bool>
                    {
                        Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                        Message = "Sale không tồn tại",
                        Data = false
                    };
                }

                var param = new DynamicParameters();
                param.Add("@CallId", model.CallId, DbType.String, ParameterDirection.Input);
                param.Add("@CustCode", model.CustCode, DbType.String, ParameterDirection.Input);
                param.Add("@LeadId", model.LeadId, DbType.Int32, ParameterDirection.Input);
                param.Add("@OpenAccId", model.OpenAccId, DbType.Int32, ParameterDirection.Input);
                param.Add("@CustName", model.CustName, DbType.String, ParameterDirection.Input);
                param.Add("@Direction", model.Direction, DbType.Int32, ParameterDirection.Input);
                param.Add("@UserName", model.UserName, DbType.String, ParameterDirection.Input);
                param.Add("@Mobile", model.Mobile, DbType.String, ParameterDirection.Input);
                param.Add("@SaleID", saleInfo.SaleID, DbType.String, ParameterDirection.Input);
                param.Add("@BranchID", saleInfo.BranchCode, DbType.String, ParameterDirection.Input);

                if (!string.IsNullOrEmpty(model.CustCode))
                {
                    param.Add("@CustType", 1, DbType.String, ParameterDirection.Input);
                }

                if(model.LeadId != 0)
                {
                    param.Add("@CustType", 2, DbType.String, ParameterDirection.Input);
                }

                if (model.OpenAccId != 0)
                {
                    param.Add("@CustType", 3, DbType.String, ParameterDirection.Input);
                }

                var data = await _dapper.ExecuteAsync(_crmDbConn, "BBB15_CREATE_CALL_APP_HISTORY", param, _sqlTimeout);
                return new ResponseSingle<bool>
                {
                    Code = ((int)ErrorCodeDetail.Success).ErrorCodeFormat(),
                    Message = ErrorCodeDetail.Success.ToEnumDescription(),
                    Data = true
                };
            }
            catch (Exception ex)
            {
                LogException(model.UserName,
                    $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
                return new ResponseSingle<bool>
                {
                    Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                    Message = ex.Message
                };
            }
        }

        public async Task<ResponseSingle<bool>> UpdateCallApp(UpdateHistoryCallRequest model)
        {
            try
            {
                var param = new DynamicParameters();
                param.Add("@CallId", model.CallId, DbType.String, ParameterDirection.Input);
                param.Add("@Note", model.Note, DbType.String, ParameterDirection.Input);
                param.Add("@Status", model.Status, DbType.Int32, ParameterDirection.Input);
                param.Add("@Rate", model.Rate, DbType.Int32, ParameterDirection.Input);
                param.Add("@Comment", model.Comment, DbType.String, ParameterDirection.Input);
                param.Add("@CallAppTypeList", model.CallAppTypeList, DbType.String, ParameterDirection.Input);
                param.Add("@UserName", model.UserName, DbType.String, ParameterDirection.Input);
                param.Add("@TimeCall", model.TimeCall, DbType.String, ParameterDirection.Input);
                var data = await _dapper.ExecuteAsync(_crmDbConn, "BBB16_UPDATE_CALL_APP_HISTORY", param, _sqlTimeout);
                return new ResponseSingle<bool>
                {
                    Code = ((int)ErrorCodeDetail.Success).ErrorCodeFormat(),
                    Message = ErrorCodeDetail.Success.ToEnumDescription(),
                    Data = true
                };
            }
            catch (Exception ex)
            {
                LogException(model.UserName,
                    $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
                return new ResponseSingle<bool>
                {
                    Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                    Message = ex.Message
                };
            }
        }

        public async Task<ResponseSingle<HistoryCallAppInfoResponse>> GetListHistoryCall(GetListHistoryCallRequest model)
        {
            try
            {
                var start = (model.PageIndex - 1) * model.PageSize;

                var dtxFromDate = SqlDateTime.MinValue;
                var dtxToDate = SqlDateTime.MaxValue;

                if (!string.IsNullOrEmpty(model.FromDate))
                    dtxFromDate = DateTime.ParseExact(model.FromDate + " 00:00:00", "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);

                if (!string.IsNullOrEmpty(model.ToDate))
                    dtxToDate = DateTime.ParseExact(model.ToDate + " 23:59:59", "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);

                var param = new DynamicParameters();
                param.Add("@CustCode", model.CustCode ?? "", DbType.String, ParameterDirection.Input);
                param.Add("@LeadId", model.LeadId, DbType.Int32, ParameterDirection.Input);
                
                param.Add("@SaleID", model.SaleId ?? "", DbType.String, ParameterDirection.Input);
                param.Add("@CustType", model.CustType, DbType.Int32, ParameterDirection.Input);
                param.Add("@FromDate", dtxFromDate, DbType.DateTime, ParameterDirection.Input);
                param.Add("@ToDate", dtxToDate, DbType.DateTime, ParameterDirection.Input);
                
                param.Add("@Start", start, DbType.Int32, ParameterDirection.Input);
                param.Add("@PageSize", model.PageSize, DbType.Int32, ParameterDirection.Input);
                param.Add("@ViewType", 0, DbType.Int32, ParameterDirection.Input);
                param.Add("@UserName", model.UserName, DbType.String, ParameterDirection.Input);

                var data = await _dapper.GetAsync<HistoryCallAppInfoSql>(_crmDbConn, "BBB17_GET_LIST_CALL_APP_HISTORY", param, _sqlTimeout);

                if (data != null)
                {
                    var dataResponse = new HistoryCallAppInfoResponse();

                    dataResponse.TotalItem = data.TotalItem;
                    
                    if (!string.IsNullOrEmpty(data.Items))
                        dataResponse.Items = JsonConvert.DeserializeObject<List<HistoryCallAppInfo>>(data.Items);

                    if (dataResponse.Items != null)
                    {
                        foreach (var item in dataResponse.Items)
                        {
                            // item.CustTypeName = _distributeCacheService.GetItemName("CustType_" + item.CustType);
                            item.DirectionName = "Gọi đi";

                            if (!string.IsNullOrEmpty(item.CallAppTypeList))
                            {
                                var callAppTypeName = "";
                                var callAppTypeArr = item.CallAppTypeList.Split(',');
                                foreach (var callAppTypeItem in callAppTypeArr)
                                {
                                    callAppTypeName +=
                                        _distributeCacheService.GetItemName("CallAppType_" + callAppTypeItem) + ",";
                                }

                                if (callAppTypeName.LastIndexOf(",") > 0)
                                    callAppTypeName = callAppTypeName.Substring(0, callAppTypeName.Length - 1)
                                        .Replace(",", ", ");
                                item.CallAppTypeName = callAppTypeName;

                            }
                        }
                    }

                    return new ResponseSingle<HistoryCallAppInfoResponse>
                    {
                        Code = ((int)ErrorCodeDetail.Success).ErrorCodeFormat(),
                        Message = ErrorCodeDetail.Success.ToEnumDescription(),
                        Data = dataResponse
                    };
                }

                return new ResponseSingle<HistoryCallAppInfoResponse>
                {
                    Code = ((int)ErrorCodeDetail.Success).ErrorCodeFormat(),
                    Message = ErrorCodeDetail.Success.ToEnumDescription(),
                    Data = new HistoryCallAppInfoResponse()
                };

            }
            catch (Exception ex)
            {
                LogException(model.UserName,
                    $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
                return new ResponseSingle<HistoryCallAppInfoResponse> 
                {
                    Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                    Message = ErrorCodeDetail.Success.ToEnumDescription()
                };
            }
        }
    
        public async Task<ResponseSingle<HistoryCallAppInfo>> GetDetailCall(GetDetailHistoryCall model)
        {
            try
            {
                var param = new DynamicParameters();
                param.Add("@CallId", model.CallId, DbType.String, ParameterDirection.Input);
                param.Add("@UserName", model.UserName, DbType.String, ParameterDirection.Input);

                return new ResponseSingle<HistoryCallAppInfo>
                {
                    Code = ((int)ErrorCodeDetail.Success).ErrorCodeFormat(),
                    Message = ErrorCodeDetail.Success.ToEnumDescription(),
                    Data = await _dapper.GetAsync<HistoryCallAppInfo>(_crmDbConn, "BBB48_GET_DETAIL_CALL_APP_HISTORY", param, _sqlTimeout)
                };
            }
            catch (Exception ex)
            {
                LogException(model.UserName,
                    $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
                return new ResponseSingle<HistoryCallAppInfo>
                {
                    Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                    Message = ErrorCodeDetail.Success.ToEnumDescription()
                };
            }
        }

        public async Task<ResponseSingle<BookingCallAppResponse>> GetListBookingCall(GetListBookingCallRequest model)
        {
            try
            {
                var start = (model.PageIndex - 1) * model.PageSize;

                var dtxFromDate = SqlDateTime.MinValue;
                var dtxToDate = SqlDateTime.MaxValue;

                if (!string.IsNullOrEmpty(model.FromDate))
                    dtxFromDate = DateTime.ParseExact(model.FromDate + " 00:00:00", "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);

                if (!string.IsNullOrEmpty(model.ToDate))
                    dtxToDate = DateTime.ParseExact(model.ToDate + " 23:59:59", "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);

                var param = new DynamicParameters();
                param.Add("@CustCode", model.CustCode ?? "", DbType.String, ParameterDirection.Input);
                param.Add("@SaleID", model.SaleId ?? "", DbType.String, ParameterDirection.Input);
                param.Add("@CustType", model.CustType, DbType.Int32, ParameterDirection.Input);
                param.Add("@FromDate", dtxFromDate, DbType.DateTime, ParameterDirection.Input);
                param.Add("@ToDate", dtxToDate, DbType.DateTime, ParameterDirection.Input);
                param.Add("@Start", start, DbType.Int32, ParameterDirection.Input);
                param.Add("@PageSize", model.PageSize, DbType.Int32, ParameterDirection.Input);
                param.Add("@StateStatus", model.StateStatus, DbType.Int32, ParameterDirection.Input);
                param.Add("@UserName", model.UserName, DbType.String, ParameterDirection.Input);

                var data = await _dapper.GetAsync<BookingCallAppSql>(_crmDbConn, "BBB18_GET_LIST_BOOK_CALL", param, _sqlTimeout);

                if (data != null)
                {
                    var dataResponse = new BookingCallAppResponse();

                    dataResponse.TotalItem = data.TotalItem;
                    if (!string.IsNullOrEmpty(data.Items))
                        dataResponse.Items = JsonConvert.DeserializeObject<List<BookingCallApp>>(data.Items);

                    if (dataResponse.Items != null)
                    {
                        foreach (var item in dataResponse.Items)
                        {
                            item.CustTypeName = _distributeCacheService.GetItemName("CustType_" + item.CustType);
                            item.BookCallTypeName = _distributeCacheService.GetItemName("BookCallType_" + item.BookCallType);
                            item.StatusName = _distributeCacheService.GetItemName("BookCallStatus_" + item.Status);

                            if (string.IsNullOrEmpty(item.CallId))
                            {
                                var paramQuerySelect = new DynamicParameters();
                                paramQuerySelect.Add("@StartDate", item.StartTime.ToString(), DbType.String, ParameterDirection.Input);
                                paramQuerySelect.Add("@EndTime", item.EndTime.ToString(), DbType.String, ParameterDirection.Input);
                                paramQuerySelect.Add("@CustCode", item.CustCode ?? "", DbType.String, ParameterDirection.Input);
                                paramQuerySelect.Add("@LeadId", item.LeadId, DbType.Int32, ParameterDirection.Input);
                                paramQuerySelect.Add("@OpenAccId", item.OpenAccId, DbType.Int32, ParameterDirection.Input);
                                paramQuerySelect.Add("@UserName", item.AssignUser ?? "", DbType.String, ParameterDirection.Input);

                                var dataCallId = await _dapper.GetAsync<string>(_crmDbConn, "BBB43_GET_CALL_APP_HIST_DETAIL", paramQuerySelect, _sqlTimeout);

                                if (!string.IsNullOrEmpty(dataCallId))
                                {
                                    var paramQueryUpdate = new DynamicParameters();
                                    paramQueryUpdate.Add("@CallId", dataCallId, DbType.String, ParameterDirection.Input);
                                    paramQueryUpdate.Add("@BookApptCallId", item.BookApptCallId, DbType.Int32, ParameterDirection.Input);
                                    paramQueryUpdate.Add("@UserName", item.AssignUser ?? "", DbType.String, ParameterDirection.Input);

                                    await _dapper.ExecuteAsync(_crmDbConn, "BBB44_UPDATE_BOOKING_CALL_TO_CALL", paramQueryUpdate, _sqlTimeout);
                                    item.CallId = dataCallId;
                                }
                            }

                            if (!string.IsNullOrEmpty(item.CallAppTypeList))
                            {
                                var callAppTypeName = "";
                                var callAppTypeArr = item.CallAppTypeList.Split(',');
                                foreach (var callAppTypeItem in callAppTypeArr)
                                {
                                    callAppTypeName +=
                                        _distributeCacheService.GetItemName("CallAppType_" + callAppTypeItem) + ",";
                                }

                                if (callAppTypeName.LastIndexOf(",") > 0)
                                    callAppTypeName = callAppTypeName.Substring(0, callAppTypeName.Length - 1)
                                        .Replace(",", ", ");
                                item.CallAppTypeName = callAppTypeName;
                            }
                        }
                    }

                    return new ResponseSingle<BookingCallAppResponse>
                    {
                        Code = ((int)ErrorCodeDetail.Success).ErrorCodeFormat(),
                        Message = ErrorCodeDetail.Success.ToEnumDescription(),
                        Data = dataResponse
                    };
                }

                return new ResponseSingle<BookingCallAppResponse>
                {
                    Code = ((int)ErrorCodeDetail.Success).ErrorCodeFormat(),
                    Message = ErrorCodeDetail.Success.ToEnumDescription()
                };

            }
            catch (Exception ex)
            {
                LogException(model.UserName,
                    $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
                return new ResponseSingle<BookingCallAppResponse>
                {
                    Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                    Message = ErrorCodeDetail.Success.ToEnumDescription()
                };
            }
        }
    
        public async Task<ResponseSingle<bool>> CreateBookingCall(CreateBookingCallRequest model)
        {
            try
            {
                var paramSaleInfo = new DynamicParameters();
                paramSaleInfo.Add("@UserName", model.UserName, DbType.String, ParameterDirection.Input);
                var saleInfo = await _dapper.GetAsync<SaleInfoReponse>(_crmDbConn, "BBB35_GET_INFO_LEAD", paramSaleInfo, _sqlTimeout);

                if(saleInfo == null || String.IsNullOrEmpty(saleInfo.SaleID))
                {
                    return new ResponseSingle<bool>
                    {
                        Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                        Message = "Sale không tồn tại",
                        Data = false
                    };
                }

                var paramSaleCust = new DynamicParameters();
                paramSaleCust.Add("@UserName", model.AssignUser, DbType.String, ParameterDirection.Input);
                paramSaleCust.Add("@CustCode", model.CustCode, DbType.String, ParameterDirection.Input);
                paramSaleCust.Add("@LeadId", model.LeadId, DbType.Int32, ParameterDirection.Input);
                paramSaleCust.Add("@OpenAccId", model.OpenAccId, DbType.Int32, ParameterDirection.Input);

                if (!String.IsNullOrEmpty(model.CustCode))
                {
                    paramSaleCust.Add("@ViewType", 1, DbType.Int32, ParameterDirection.Input);
                }
                else if (model.LeadId > 0)
                {
                    paramSaleCust.Add("@ViewType", 2, DbType.Int32, ParameterDirection.Input);
                }
                else if (model.OpenAccId > 0)
                {
                    paramSaleCust.Add("@ViewType", 3, DbType.Int32, ParameterDirection.Input);
                }

                var dataCust = await _dapper.SelectAsync<CustomerContactInfo>(_crmDbConn, "BBB47_GET_CUSTOMER_CONTACT_INFO_BY_SALE", paramSaleCust, _sqlTimeout);

                if(dataCust == null || (dataCust.ToList().Count == 0))
                {
                    return new ResponseSingle<bool>
                    {
                        Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                        Message = "Khách hàng không được chỉ định cho sale",
                        Data = false
                    };
                }

                var param = new DynamicParameters();
                param.Add("@CallAppTypeList", model.CallAppTypeList, DbType.String, ParameterDirection.Input);
                param.Add("@StartTime", model.StartTime, DbType.String, ParameterDirection.Input);
                param.Add("@EndTime", model.EndTime, DbType.String, ParameterDirection.Input);
                param.Add("@RemindTime", model.RemindTime, DbType.String, ParameterDirection.Input);
                param.Add("@Description", model.Description, DbType.String, ParameterDirection.Input);
                param.Add("@BookCallType", model.BookCallType, DbType.Int32, ParameterDirection.Input);
                param.Add("@AssignUser", model.AssignUser, DbType.String, ParameterDirection.Input);
                param.Add("@CustCode", model.CustCode, DbType.String, ParameterDirection.Input);
                param.Add("@LeadId", model.LeadId, DbType.Int32, ParameterDirection.Input);
                param.Add("@CustName", model.CustName, DbType.String, ParameterDirection.Input);
                param.Add("@OpenAccId", model.OpenAccId, DbType.Int32, ParameterDirection.Input);
                param.Add("@UserName", model.UserName, DbType.String, ParameterDirection.Input);
                param.Add("@Mobile", model.Mobile, DbType.String, ParameterDirection.Input);
                param.Add("@SaleID", saleInfo.SaleID, DbType.String, ParameterDirection.Input);
                param.Add("@BranchID", saleInfo.BranchCode, DbType.String, ParameterDirection.Input);
                if (!string.IsNullOrEmpty(model.CustCode))
                {
                    param.Add("@CustType", 1, DbType.String, ParameterDirection.Input);
                }

                if (model.LeadId != 0)
                {
                    param.Add("@CustType", 2, DbType.String, ParameterDirection.Input);
                }

                if (model.OpenAccId != 0)
                {
                    param.Add("@CustType", 3, DbType.String, ParameterDirection.Input);
                }
                var data = await _dapper.ExecuteAsync(_crmDbConn, "BBB19_CREATE_BOOK_CALL", param, _sqlTimeout);

                if(!string.IsNullOrEmpty(model.RemindTime))
                {
                    var content = @"Dear [SaleID],
                                \nBạn có cuộc hẹn đang chờ xử lý tới
                                \nKH STK: [CustCode] -[CustName] trong thời gian từ' [StartTime] ' đến '[EndTime]' với nội dung'[Description]'.
                                \nTrân trong.";

                    content = content.Replace("[SaleID]", model.AssignUser);
                    content = content.Replace("[CustCode]", model.CustCode);
                    content = content.Replace("[CustName]", model.CustName);
                    content = content.Replace("[StartTime]", model.StartTime);
                    content = content.Replace("[EndTime]", model.EndTime);
                    content = content.Replace("[Description]", model.Description);

                    var paramNoti = new DynamicParameters();

                    paramNoti.Add("@titleNotification", "Thông báo nhắc lịch hẹn cuộc gọi", DbType.String, ParameterDirection.Input);
                    paramNoti.Add("@contentNotification", content, DbType.String, ParameterDirection.Input);
                    paramNoti.Add("@staffCode", model.AssignUser, DbType.String, ParameterDirection.Input);
                    paramNoti.Add("@timeSend", model.RemindTime, DbType.String, ParameterDirection.Input);
                    paramNoti.Add("@userName", model.UserName, DbType.String, ParameterDirection.Input);

                    var dataNoti = await _dapper.ExecuteAsync(_notificationDbConn, "TVSI_sHANG_DOI_THONG_BAO_NHAN_VIEN", paramNoti, _sqlTimeout);

                    var contentMail = "";

                    contentMail = System.IO.File.ReadAllText(@"C:\Template\Mail_RequestBookingCall.html");

                    contentMail = contentMail.Replace("{FullName}", saleInfo.FullName ?? "");
                    contentMail = contentMail.Replace("{CustCode}", model.CustCode ?? "");
                    contentMail = contentMail.Replace("{CustName}", model.CustName ?? "");
                    contentMail = contentMail.Replace("{StartTime}", model.StartTime ?? "");
                    contentMail = contentMail.Replace("{EndTime}", model.EndTime ?? "");
                    contentMail = contentMail.Replace("{Description}", model.Description ?? "");

                    var paramMail = new DynamicParameters();

                    paramMail.Add("@tieu_de", "CRM: Thông báo nhắc lịch hẹn cuộc gọi", DbType.String, ParameterDirection.Input);
                    paramMail.Add("@noi_dung", contentMail, DbType.String, ParameterDirection.Input);
                    paramMail.Add("@dia_chi_email", saleInfo.Email, DbType.String, ParameterDirection.Input);
                    paramMail.Add("@trang_thai", 0, DbType.Int32, ParameterDirection.Input);

                    var dataMaill = await _dapper.ExecuteAsync(_notificationDbConn, "TVSI_sHANG_DOI_EMAIL_INSERT", paramMail, _sqlTimeout);
                }

                return new ResponseSingle<bool>
                {
                    Code = ((int)ErrorCodeDetail.Success).ErrorCodeFormat(),
                    Message = ErrorCodeDetail.Success.ToEnumDescription(),
                    Data = true
                };
            }
            catch (Exception ex)
            {
                LogException(model.UserName,
                    $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
                return new ResponseSingle<bool>
                {
                    Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                    Message = ex.Message
                };
            }
        }
    
        public async Task<ResponseSingle<bool>> UpdateBookingCall(UpdateBookingCallRequest model)
        {
            try
            {
                var param = new DynamicParameters();
                param.Add("@BookApptCallId", model.BookApptCallId, DbType.Int32, ParameterDirection.Input);
                param.Add("@CallAppTypeList", model.CallAppTypeList, DbType.String, ParameterDirection.Input);
                param.Add("@StartTime", model.StartTime, DbType.String, ParameterDirection.Input);
                param.Add("@EndTime", model.EndTime, DbType.String, ParameterDirection.Input);
                param.Add("@RemindTime", model.RemindTime, DbType.String, ParameterDirection.Input);
                param.Add("@Description", model.Description, DbType.String, ParameterDirection.Input);
                param.Add("@BookCallType", model.BookCallType, DbType.Int32, ParameterDirection.Input);
                param.Add("@AssignUser", model.AssignUser, DbType.String, ParameterDirection.Input);
                param.Add("@Status", model.Status, DbType.Int32, ParameterDirection.Input);
                param.Add("@UserName", model.UserName, DbType.String, ParameterDirection.Input);
                var data = await _dapper.ExecuteAsync(_crmDbConn, "BBB20_UPDATE_BOOK_CALL", param, _sqlTimeout);
                return new ResponseSingle<bool>
                {
                    Code = ((int)ErrorCodeDetail.Success).ErrorCodeFormat(),
                    Message = ErrorCodeDetail.Success.ToEnumDescription(),
                    Data = true
                };
            }
            catch (Exception ex)
            {
                LogException(model.UserName,
                    $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
                return new ResponseSingle<bool>
                {
                    Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                    Message = ex.Message
                };
            }
        }

        public async Task<ResponseSingle<bool>> DeleteBookingCall(DeleteBookingCallRequest model)
        {
            try
            {
                var param = new DynamicParameters();
                param.Add("@BookApptCallId", model.BookApptCallId, DbType.Int32, ParameterDirection.Input);
                param.Add("@UserName", model.UserName, DbType.String, ParameterDirection.Input);
                var data = await _dapper.ExecuteAsync(_crmDbConn, "BBB42_DELETE_BOOK_CALL", param, _sqlTimeout);
                return new ResponseSingle<bool>
                {
                    Code = ((int)ErrorCodeDetail.Success).ErrorCodeFormat(),
                    Message = ErrorCodeDetail.Success.ToEnumDescription(),
                    Data = true
                };
            }
            catch (Exception ex)
            {
                LogException(model.UserName,
                    $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
                return new ResponseSingle<bool>
                {
                    Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                    Message = ex.Message
                };
            }
        }

        public async Task<ResponseSingle<List<StatusBookingReponse>>> GetListStatus(BaseRequest model)
        {
            try
            {
                var param = new DynamicParameters();
                param.Add("@ConfigKey", "BookCallStatus", DbType.String, ParameterDirection.Input);
                var data = await _dapper.SelectAsync<StatusBookingReponse>(_crmDbConn, "BBB40_GET_LIST_STATUS", param, _sqlTimeout);

                return new ResponseSingle<List<StatusBookingReponse>>
                {
                    Code = ((int)ErrorCodeDetail.Success).ErrorCodeFormat(),
                    Message = ErrorCodeDetail.Success.ToEnumDescription(),
                    Data = data.ToList()
                };
            }
            catch (Exception ex)
            {
                LogException(model.UserName,
                    $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
                return new ResponseSingle<List<StatusBookingReponse>>
                {
                    Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                    Message = ex.Message,
                };
            }
        }
    
        public async Task<ResponseSingle<SaleInfoReponseModel>> GetListSaleInfo(SaleInfoRequestModel model)
        {
            try
            {
                var start = (model.PageIndex - 1) * model.PageSize;
                var param = new DynamicParameters();
                param.Add("@Name", model.NameSearch, DbType.String, ParameterDirection.Input);
                param.Add("@SaleID", model.UserName.Substring(0,4), DbType.String, ParameterDirection.Input);
                param.Add("@Start", start, DbType.Int32, ParameterDirection.Input);
                param.Add("@PageSize", model.PageSize, DbType.Int32, ParameterDirection.Input);
                param.Add("@UserName", model.UserName, DbType.String, ParameterDirection.Input);

                var data = await _dapper.GetAsync<SaleInfoSqlModel>(_crmDbConn, "BBB46_GET_LIST_SALE_INFO", param, _sqlTimeout);

                var dataResponse = new SaleInfoReponseModel();

                if(String.IsNullOrEmpty(data.Items))
                {
                    return new ResponseSingle<SaleInfoReponseModel>
                    {
                        Code = ((int)ErrorCodeDetail.Success).ErrorCodeFormat(),
                        Message = ErrorCodeDetail.Success.ToEnumDescription(),
                        Data = dataResponse
                    };
                }

                dataResponse.TotalItem = data.TotalItem;
                dataResponse.Items = JsonConvert.DeserializeObject<List<SaleInfo>>(data.Items);

                return new ResponseSingle<SaleInfoReponseModel>
                {
                    Code = ((int)ErrorCodeDetail.Success).ErrorCodeFormat(),
                    Message = ErrorCodeDetail.Success.ToEnumDescription(),
                    Data = dataResponse
                };
            }
            catch (Exception ex)
            {
                _logger.LogError(
                    $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
                return new ResponseSingle<SaleInfoReponseModel>
                {
                    Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                    Message = ErrorCodeDetail.Success.ToEnumDescription()
                };
            }
        }
    }
}
