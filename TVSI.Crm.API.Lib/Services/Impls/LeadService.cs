﻿using Dapper;
using Helper.DataAccess.Dapper;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System.Data;
using System.Reflection;
using TVSI.Crm.API.Common;
using TVSI.Crm.API.Lib.Models;
using TVSI.Crm.API.Lib.Models.Request.Lead;
using TVSI.Crm.API.Lib.Models.Response.Activity;
using TVSI.Crm.API.Lib.Models.Response.Lead;
using TVSI.TWork.API.Common.Enums;
using TVSI.TWork.API.Common.ExtensionMethods;

namespace TVSI.Crm.API.Lib.Services.Impls;

public class LeadService : BaseService<LeadService>, ILeadService
{
    private readonly IDapperHelper _dapper;
    private readonly string _crmDbConn;
    private readonly int _sqlTimeout;

    public LeadService(IConfiguration config, ILogger<LeadService> logger,IDapperHelper dapper) : base(config, logger)
    {
        var connStr = _config.GetConnectionString("CRMDB_CONNECTION");
        _crmDbConn = connStr;
        _dapper = dapper;
        _sqlTimeout = _config["Timeout:Database"] == null
            ? CommonConstants.SqlServerTimeout
            : int.Parse(_config["Timeout:Database"]);
    }

    public async Task<ResponseSingle<LeadDataReponse>> GetLeadList(LeadListRequest model, string lang)
    {
        try
        {
            var start = (model.PageIndex - 1) * model.PageSize;

            var param = new DynamicParameters();
            param.Add("@Name", model.Name, DbType.String, ParameterDirection.Input);
            param.Add("@Phone", model.Phone, DbType.String, ParameterDirection.Input);
            param.Add("@Email", model.Email, DbType.String, ParameterDirection.Input);
            param.Add("@LeadSourceID", model.LeadSourceID, DbType.Int32, ParameterDirection.Input);
            param.Add("@Status", model.Status, DbType.Int32, ParameterDirection.Input);
            param.Add("@ProfileType", model.ProfileType, DbType.Int32, ParameterDirection.Input);
            param.Add("@Start", start, DbType.Int32, ParameterDirection.Input);
            param.Add("@PageSize", model.PageSize, DbType.Int32, ParameterDirection.Input);
            param.Add("@@UserName", model.UserName, DbType.String, ParameterDirection.Input);
            var data = await _dapper.GetAsync<LeadListSQL>(_crmDbConn, "BBB21_GET_LIST_LEAD", param, _sqlTimeout);
            
            var dataResponse = new LeadDataReponse();

            if (String.IsNullOrEmpty(data.Items))
            {
                return new ResponseSingle<LeadDataReponse>
                {
                    Code = ((int)ErrorCodeDetail.Success).ErrorCodeFormat(),
                    Message = ErrorCodeDetail.Success.ToEnumDescription(),
                    Data = dataResponse
                };
            }

            dataResponse.TotalItem = data.TotalItem;
            dataResponse.Items = JsonConvert.DeserializeObject<List<LeadData>>(data.Items);

            return new ResponseSingle<LeadDataReponse>
            {
                Code = ((int)ErrorCodeDetail.Success).ErrorCodeFormat(),
                Message = ErrorCodeDetail.Success.ToEnumDescription(),
                Data = dataResponse
            };

        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
            return new ResponseSingle<LeadDataReponse> {
                Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                Message = ex.Message
            };
        }
    }

    public async Task<ResponseSingle<bool>> CreateLead(CreateLeadRequest model, string lang)
    {
        try
        {
            var param = new DynamicParameters();
            param.Add("@Name", model.Name, DbType.String, ParameterDirection.Input);
            param.Add("@Phone", model.Phone, DbType.String, ParameterDirection.Input);
            param.Add("@Email", model.Email, DbType.String, ParameterDirection.Input);
            param.Add("@Address", model.Address, DbType.String, ParameterDirection.Input);
            param.Add("@ProfileType", model.ProfileType, DbType.Int32, ParameterDirection.Input);
            param.Add("@BranchID", model.BranchID, DbType.Int32, ParameterDirection.Input);
            param.Add("@AssignUser", model.AssignUser, DbType.String, ParameterDirection.Input);
            param.Add("@Description", model.Description, DbType.String, ParameterDirection.Input);
            param.Add("@LeadSourceID", model.LeadSourceID, DbType.Int32, ParameterDirection.Input);
            param.Add("@UserName", model.UserName, DbType.String, ParameterDirection.Input);
            var data = await _dapper.GetAsync<int>(_crmDbConn, "BBB22_CREATE_LEAD_INFO", param, _sqlTimeout);

            foreach(var item in model.Attributes)
            {
                var paramAttribute = new DynamicParameters();
                paramAttribute.Add("@Category", item.Category, DbType.Int32, ParameterDirection.Input);
                paramAttribute.Add("@LeadID", data, DbType.Int32, ParameterDirection.Input);
                paramAttribute.Add("@UserName", model.UserName, DbType.String, ParameterDirection.Input);

                var dataAtribute = await _dapper.ExecuteAsync(_crmDbConn, "BBB23_CREATE_LEAD_ATTRIBUTE", paramAttribute, _sqlTimeout);
            }

            return new ResponseSingle<bool>
            {
                Code = ((int)ErrorCodeDetail.Success).ErrorCodeFormat(),
                Message = ErrorCodeDetail.Success.ToEnumDescription(),
                Data = true
            };
        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
            return new ResponseSingle<bool> {
                Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                Message = ex.Message
            };
        }
    }

    public async Task<ResponseSingle<LeadActivityReponse>> GetLeadActivity(ListActivityRequest model, string lang)
    {
        try
        {
            var start = (model.PageIndex - 1) * model.PageSize;

            var param = new DynamicParameters();
            param.Add("@LeadID", model.LeadID, DbType.Int32, ParameterDirection.Input);
            param.Add("@Start", start, DbType.Int32, ParameterDirection.Input);
            param.Add("@PageSize", model.PageSize, DbType.Int32, ParameterDirection.Input);
            var data = await _dapper.GetAsync<LeadActivitySQL>(_crmDbConn, "BBB27_GET_LIST_LEAD_ACTIVITY", param, _sqlTimeout);

            var dataResponse = new LeadActivityReponse();

            if (String.IsNullOrEmpty(data.Items))
            {
                return new ResponseSingle<LeadActivityReponse>
                {
                    Code = ((int)ErrorCodeDetail.Success).ErrorCodeFormat(),
                    Message = ErrorCodeDetail.Success.ToEnumDescription(),
                    Data = dataResponse
                };
            }

            dataResponse.TotalItem = data.TotalItem;
            dataResponse.Items = JsonConvert.DeserializeObject<List<ActivityResult>>(data.Items);

            return new ResponseSingle<LeadActivityReponse>
            {
                Code = ((int)ErrorCodeDetail.Success).ErrorCodeFormat(),
                Message = ErrorCodeDetail.Success.ToEnumDescription(),
                Data = dataResponse
            };

        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
            return new ResponseSingle<LeadActivityReponse> {
                Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                Message = ex.Message
            };
        }
    }

    public async Task<ResponseSingle<DetailLeadReponse>> GetDetailLead(DetailLeadRequest model, string lang)
    {
        try
        {

            var param = new DynamicParameters();
            param.Add("@LeadID", model.LeadID, DbType.Int32, ParameterDirection.Input);
            var data = await _dapper.GetAsync<DetailLeadSql>(_crmDbConn, "BBB24_GET_DETAIL_LEAD", param, _sqlTimeout);

            var dataResponse = new DetailLeadReponse();

            dataResponse.LeadID = data.LeadID;
            dataResponse.LeadName = data.LeadName;
            dataResponse.Mobile = data.Mobile;
            dataResponse.Email = data.Email;
            dataResponse.Address = data.Address;
            dataResponse.ProfileType = data.ProfileType;
            dataResponse.Status = data.Status;
            dataResponse.StatusColor = data.StatusColor;
            dataResponse.BranchID = data.BranchID;
            dataResponse.AssignUser = data.AssignUser;
            dataResponse.Description = data.Description;
            dataResponse.LeadSourceID = data.LeadSourceID;
            dataResponse.CreatedDate = data.CreatedDate;
            dataResponse.UpdatedDate = data.UpdatedDate;
            dataResponse.FullName = data.FullName;
            dataResponse.SaleID = data.SaleID;
            dataResponse.SourceName = data.SourceName;

            if (!string.IsNullOrEmpty(data.LeadAttribute))
            {
                dataResponse.LeadAttribute = JsonConvert.DeserializeObject<List<LeadAtribute>>(data.LeadAttribute);
            } else
            {
                dataResponse.LeadAttribute = new List<LeadAtribute>();
            }

            return new ResponseSingle<DetailLeadReponse>
            {
                Code = ((int)ErrorCodeDetail.Success).ErrorCodeFormat(),
                Message = ErrorCodeDetail.Success.ToEnumDescription(),
                Data = dataResponse
            };

        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
            return new ResponseSingle<DetailLeadReponse> {
                Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                Message = ex.Message
            };
        }
    }

    public async Task<Response<LeadSrcReponse>> GetSrcLead(BaseRequest model)
    {
        try
        {
            var param = new DynamicParameters();
            return new Response<LeadSrcReponse>
            {
                Code = ((int)ErrorCodeDetail.Success).ErrorCodeFormat(),
                Message = ErrorCodeDetail.Success.ToEnumDescription(),
                Data = await _dapper.SelectAsync<LeadSrcReponse>(_crmDbConn, "BBB32_GET_LIST_SRC_LEAD", param, _sqlTimeout)
            };
        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
            return new Response<LeadSrcReponse> {
                Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                Message = ex.Message
            };
        }
    }

    public async Task<ResponseSingle<bool>> DeleteLead(DeleteLead model)
    {
        try
        {
            var param = new DynamicParameters();
            param.Add("@LeadID", model.LeadID, DbType.Int32, ParameterDirection.Input);
            param.Add("@UserName", model.UserName, DbType.String, ParameterDirection.Input);
            param.Add("@Reason", model.Reason, DbType.String, ParameterDirection.Input);
            await _dapper.ExecuteAsync(_crmDbConn, "BBB33_DELETE_LEAD_INFO", param, _sqlTimeout);

            return new ResponseSingle<bool>
            {
                Code = ((int)ErrorCodeDetail.Success).ErrorCodeFormat(),
                Message = ErrorCodeDetail.Success.ToEnumDescription(),
                Data = true
            };
        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
            return new ResponseSingle<bool> {
                Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                Message = ex.Message
            };
        }
    }

    public async Task<ResponseSingle<bool>> UpdateLead(UpdateLeadRequest model)
    {
        try
        {
            var param = new DynamicParameters();
            param.Add("@LeadName", model.LeadName, DbType.String, ParameterDirection.Input);
            param.Add("@Mobile", model.Mobile, DbType.String, ParameterDirection.Input);
            param.Add("@Email", model.Email, DbType.String, ParameterDirection.Input);
            param.Add("@Address", model.Address, DbType.String, ParameterDirection.Input);
            param.Add("@ProfileType", model.ProfileType, DbType.Int32, ParameterDirection.Input);
            param.Add("@Status", model.Status, DbType.Int32, ParameterDirection.Input);
            param.Add("@BranchID", model.BranchID, DbType.Int32, ParameterDirection.Input);
            param.Add("@AssignUser", model.AssignUser, DbType.String, ParameterDirection.Input);
            param.Add("@Description", model.Description, DbType.String, ParameterDirection.Input);
            param.Add("@LeadSourceID", model.LeadSourceID, DbType.Int32, ParameterDirection.Input);
            param.Add("@LeadID", model.LeadID, DbType.Int32, ParameterDirection.Input);
            param.Add("@UserName", model.UserName, DbType.String, ParameterDirection.Input);
            await _dapper.ExecuteAsync(_crmDbConn, "BBB34_UPDATE_LEAD_INFO", param, _sqlTimeout);

            foreach (var item in model.Attributes)
            {
                var paramAttribute = new DynamicParameters();
                paramAttribute.Add("@Category", item.Category, DbType.Int32, ParameterDirection.Input);
                paramAttribute.Add("@LeadID", model.LeadID, DbType.Int32, ParameterDirection.Input);
                paramAttribute.Add("@UserName", model.UserName, DbType.String, ParameterDirection.Input);

                await _dapper.ExecuteAsync(_crmDbConn, "BBB23_CREATE_LEAD_ATTRIBUTE", paramAttribute, _sqlTimeout);
            }

            return new ResponseSingle<bool>
            {
                Code = ((int)ErrorCodeDetail.Success).ErrorCodeFormat(),
                Message = ErrorCodeDetail.Success.ToEnumDescription(),
                Data = true
            };
        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
            return new ResponseSingle<bool> {
                Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                Message = ex.Message
            };
        }
    }

    public async Task<ResponseSingle<bool>> CovertLead(ConverLeadToCust model)
    {
        try
        {
            var param = new DynamicParameters();
            param.Add("@LeadID", model.LeadID, DbType.Int32, ParameterDirection.Input);
            param.Add("@CustCode", model.CustCode, DbType.String, ParameterDirection.Input);
            param.Add("@UserName", model.UserName, DbType.String, ParameterDirection.Input);
            await _dapper.ExecuteAsync(_crmDbConn, "BBB45_COVERT_LEAD_TO_CUSTOMER", param, _sqlTimeout);

            return new ResponseSingle<bool>
            {
                Code = ((int)ErrorCodeDetail.Success).ErrorCodeFormat(),
                Message = ErrorCodeDetail.Success.ToEnumDescription(),
                Data = true
            };
        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
            return new ResponseSingle<bool> {
                Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                Message = ex.Message
            };
        }
    }
}