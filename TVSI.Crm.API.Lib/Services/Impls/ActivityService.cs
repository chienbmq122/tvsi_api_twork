﻿using Dapper;
using Helper.DataAccess.Dapper;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using TVSI.Crm.API.Common;
using TVSI.Crm.API.Lib.Models;
using TVSI.Crm.API.Lib.Models.Request.Activity;
using TVSI.Crm.API.Lib.Models.Response.Activity;
using TVSI.Crm.API.Lib.Utilities;
using TVSI.TWork.API.Common.Enums;
using TVSI.TWork.API.Common.ExtensionMethods;

namespace TVSI.Crm.API.Lib.Services.Impls
{
    public class ActivityService : BaseService<ActivityService>, IActivityService
    {
        private readonly IDapperHelper _dapper;
        private readonly string _crmDbConn;
        private readonly string _notificationDbConn;
        private readonly int _sqlTimeout;

        public ActivityService(IConfiguration config, ILogger<ActivityService> logger, IDapperHelper dapper) : base(config, logger)
        {
            var connStr = _config.GetConnectionString("CRMDB_CONNECTION");
            var notificationStr = _config.GetConnectionString("NOTIFICATIONDB_CONNECTION");
            _crmDbConn = connStr;
            _notificationDbConn = notificationStr;
            _dapper = dapper;
            _sqlTimeout = _config["Timeout:Database"] == null
                ? CommonConstants.SqlServerTimeout
                : int.Parse(_config["Timeout:Database"]);
        }

        public async Task<ResponseSingle<ActivityResponseModel>> GetActivity(ActivityRequestByEventType model)
        {
            try
            {
                var start = (model.PageIndex - 1) * model.PageSize;
                string dateTime = DateTime.Now.ToString("yyyy-MM-dd");
                if (!string.IsNullOrEmpty(model.DateIndex))
                {
                   dateTime = DateTime.ParseExact(model.DateIndex, "dd/MM/yyyy", CultureInfo.InvariantCulture).ToString("yyyy-MM-dd");
                }
                var param = new DynamicParameters();
                param.Add("@DateStart", dateTime + " 23:59:59", DbType.String, ParameterDirection.Input);
                param.Add("@DateEnd",dateTime + " 00:00:00", DbType.String, ParameterDirection.Input);
                param.Add("@start", start, DbType.Int32, ParameterDirection.Input);
                param.Add("@pageSize", model.PageSize, DbType.Int32, ParameterDirection.Input);
                param.Add("@UserAssign", model.UserName, DbType.String, ParameterDirection.Input);
                param.Add("@EventType", model.EventType, DbType.Int32, ParameterDirection.Input);

                var data = await _dapper.GetAsync<ActivitySql>(_crmDbConn, "BBB01_GET_LIST_ACTIVITY", param, _sqlTimeout);

                ActivityResponseModel dataReponse = new ActivityResponseModel();

                if (data == null || String.IsNullOrEmpty(data.Items))
                {
                    return new ResponseSingle<ActivityResponseModel>
                    {
                        Code = ((int)ErrorCodeDetail.Success).ErrorCodeFormat(),
                        Message = ErrorCodeDetail.Success.ToEnumDescription(),
                        Data = dataReponse
                    };
                }

                dataReponse.TotalItem = data.TotalItem;
                dataReponse.Items = JsonConvert.DeserializeObject<List<ActivityResult>>(data.Items);

                return new ResponseSingle<ActivityResponseModel>
                {
                    Code = ((int)ErrorCodeDetail.Success).ErrorCodeFormat(),
                    Message = ErrorCodeDetail.Success.ToEnumDescription(),
                    Data = dataReponse
                };

            }
            catch (Exception ex)
            {
                LogException(model.UserName,
                     $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
                return new ResponseSingle<ActivityResponseModel> {
                    Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                    Message = ex.Message
                };
            }
        }
        public async Task<ResponseSingle<ActivityResultData>> GetActivityByFiltter(FilterActivityRequest model) {
            try
            {
                var dataRespon = new ActivityResultData();
                var start = (model.PageIndex - 1) * model.PageSize;
                string DateStart = DateTime.Now.ToString("yyyy-MM-dd");
                string DateEnd = DateTime.Now.ToString("yyyy-MM-dd");
                if (!string.IsNullOrEmpty(model.FromDate))
                {
                    DateStart = DateTime.ParseExact(model.FromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture).ToString("yyyy-MM-dd");
                }
                if (!string.IsNullOrEmpty(model.ToDate))
                {
                    DateEnd = DateTime.ParseExact(model.ToDate, "dd/MM/yyyy", CultureInfo.InvariantCulture).ToString("yyyy-MM-dd");
                }
                var param = new DynamicParameters();
                param.Add("@DateStart", DateStart + " 00:00:00", DbType.String, ParameterDirection.Input);
                param.Add("@DateEnd", DateEnd + " 23:59:59", DbType.String, ParameterDirection.Input);
                param.Add("@UserAssign", model.UserName, DbType.String, ParameterDirection.Input);
                param.Add("@EventType", model.EventType, DbType.Int32, ParameterDirection.Input);
                param.Add("@Priority", model.Priority, DbType.Int32, ParameterDirection.Input);
                param.Add("@Status", model.Status, DbType.Int32, ParameterDirection.Input);
                param.Add("@Start", start, DbType.Int32, ParameterDirection.Input);
                param.Add("@PageSize", model.PageSize, DbType.Int32, ParameterDirection.Input);
                var data = await _dapper.SelectAsync<ActivityResult>(_crmDbConn, "BBB07_FILTER_ACTIVITY", param, _sqlTimeout);
                dataRespon.Items = data.ToList();
                
                
                var param1 = new DynamicParameters();

                param1.Add("@DateStart", DateStart + " 23:59:59", DbType.String, ParameterDirection.Input);
                param1.Add("@DateEnd", DateEnd + " 00:00:00", DbType.String, ParameterDirection.Input);
                param1.Add("@UserAssign", model.UserName, DbType.String, ParameterDirection.Input);
                param1.Add("@EventType", model.EventType, DbType.Int32, ParameterDirection.Input);
                param1.Add("@Priority", model.Priority, DbType.Int32, ParameterDirection.Input);
                param1.Add("@Status", model.Status, DbType.Int32, ParameterDirection.Input);
                var dataCount = await _dapper.GetAsync<int>(_crmDbConn, "BBB08_COUNT_FILTER_ACTIVITY", param1, _sqlTimeout);

                dataRespon.TotalItem = dataCount;
                return new ResponseSingle<ActivityResultData>
                {
                    Code = ((int)ErrorCodeDetail.Success).ErrorCodeFormat(),
                    Message = ErrorCodeDetail.Success.ToEnumDescription(),
                    Data = dataRespon
                };
            }
            catch (Exception ex)
            {
                LogException(model.UserName,
                    $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
                return new ResponseSingle<ActivityResultData> {
                    Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                    Message = ex.Message
                };
            }
        }
        public async Task<ResponseSingle<ActivityResult>> GetDetailActivity(DeleteActivityRequest model) {
            try
            {
                var param = new DynamicParameters();
                param.Add("@ID", model.ActivityId, DbType.Int32, ParameterDirection.Input);
                return new ResponseSingle<ActivityResult>
                {
                    Code = ((int)ErrorCodeDetail.Success).ErrorCodeFormat(),
                    Message = ErrorCodeDetail.Success.ToEnumDescription(),
                    Data = await _dapper.GetAsync<ActivityResult>(_crmDbConn, "BBB02_GET_ACTIVITY_DETAIL", param, _sqlTimeout)
                };

            }
            catch (Exception ex)
            {
                LogException(model.UserName,
                    $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
                return new ResponseSingle<ActivityResult> {
                    Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                    Message = ex.Message
                };
            }
        }
        public async Task<ResponseSingle<bool>> CreateActivity(CreateActivityRequest model) {
            try
            {
                string startTime = DateTime.ParseExact(model.StartDate, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture).ToString("yyyy-MM-dd HH:mm:ss");
                string endTime = DateTime.ParseExact(model.EndDate, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture).ToString("yyyy-MM-dd HH:mm:ss");
                var param = new DynamicParameters();
                param.Add("@AssignUser", model.Assigned, DbType.String, ParameterDirection.Input);
                param.Add("@Approved", model.Approved, DbType.String, ParameterDirection.Input);
                param.Add("@EventType", model.EventType, DbType.Int32, ParameterDirection.Input);
                param.Add("@ActivityName", model.Title, DbType.String, ParameterDirection.Input);
                param.Add("@ActivityType", model.Type, DbType.Int32, ParameterDirection.Input);
                param.Add("@StartDate", startTime, DbType.String, ParameterDirection.Input);
                param.Add("@EndDate", endTime, DbType.String, ParameterDirection.Input);
                param.Add("@Status", model.Status, DbType.Int32, ParameterDirection.Input);
                param.Add("@Priority", model.Priority, DbType.Int32, ParameterDirection.Input);
                param.Add("@Location", model.Location, DbType.String, ParameterDirection.Input);
                param.Add("@Recurrence", 0, DbType.Int32, ParameterDirection.Input);
                param.Add("@Notification", 1, DbType.Int32, ParameterDirection.Input);
                param.Add("@Description", model.Detail, DbType.String, ParameterDirection.Input);
                param.Add("@LeadID", model.LeadId, DbType.Int32, ParameterDirection.Input);
                param.Add("@CustCode", model.Custcode, DbType.String, ParameterDirection.Input);
                param.Add("@CreatedBy", model.UserName, DbType.String, ParameterDirection.Input);
                param.Add("@CreatedDate", DateTime.Now, DbType.DateTime, ParameterDirection.Input);
                param.Add("@ReasonType", model.ReasonType, DbType.Int32, ParameterDirection.Input);
                var data = await _dapper.ExecuteAsync(_crmDbConn, "BBB04_CREATE_ACTIVITY", param, _sqlTimeout);

                if (!String.IsNullOrEmpty(model.RemindTime))
                {
                    string RemindTimeData = DateTime.ParseExact(model.RemindTime, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture).ToString("yyyy-MM-dd HH:mm:ss");
                    var paramNoti = new DynamicParameters();

                    var content = "Bạn có {EventType} {ActivityType} vào {StartTime}, {EndTime} chờ xử lý.";
                    var title = "Thông báo nhắc {EventType}";

                    if (model.EventType == 1)
                    {
                        content = content.Replace("{EventType}", "Sự kiện");
                        title = title.Replace("{EventType}", "Sự kiện");
                    }
                    else if (model.EventType == 2)
                    {
                        content = content.Replace("{EventType}", "Công việc");
                        title = title.Replace("{EventType}", "Công việc");
                    }

                    if (model.Type == 1)
                    {
                        content = content.Replace("{ActivityType}", "Hẹn trực tiếp");
                    }
                    else if (model.Type == 2)
                    {
                        content = content.Replace("{ActivityType}", "Gọi điện thoại");
                    }
                    else if (model.Type == 3)
                    {
                        content = content.Replace("{ActivityType}", "Gửi Email");
                    }
                    else if (model.Type == 5)
                    {
                        content = content.Replace("{ActivityType}", "Nghỉ phép");
                    }
                    else
                    {
                        content = content.Replace("{ActivityType}", "Khác");
                    }

                    content = content.Replace("{StartTime}", startTime);
                    content = content.Replace("{EndTime}", endTime);

                    paramNoti.Add("@titleNotification", title, DbType.String, ParameterDirection.Input);
                    paramNoti.Add("@contentNotification", content, DbType.String, ParameterDirection.Input);
                    paramNoti.Add("@staffCode", model.Assigned, DbType.String, ParameterDirection.Input);
                    paramNoti.Add("@timeSend", RemindTimeData, DbType.String, ParameterDirection.Input);
                    paramNoti.Add("@userName", model.UserName, DbType.String, ParameterDirection.Input);

                    var dataNoti = await _dapper.ExecuteAsync(_notificationDbConn, "TVSI_sHANG_DOI_THONG_BAO_NHAN_VIEN", paramNoti, _sqlTimeout);
                }

                return new ResponseSingle<bool>
                {
                    Code = ((int)ErrorCodeDetail.Success).ErrorCodeFormat(),
                    Message = ErrorCodeDetail.Success.ToEnumDescription(),
                    Data = true
                };
            }
            catch (Exception ex)
            {
                LogException(model.UserName,
                     $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
                return new ResponseSingle<bool> {
                    Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                    Message = ex.Message
                };
            }
        }
        public async Task<ResponseSingle<bool>> UpdateActivity(UpdateActivityRequest model) {
            try
            {
                DateTime startTime = DateTime.ParseExact(model.StartDate, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                DateTime endTime = DateTime.ParseExact(model.EndDate, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                var param = new DynamicParameters();
                param.Add("@AssignUser", model.Assigned, DbType.String, ParameterDirection.Input);
                param.Add("@Approved", model.Approved, DbType.String, ParameterDirection.Input);
                param.Add("@EventType", model.EventType, DbType.Int32, ParameterDirection.Input);
                param.Add("@ActivityName", model.Title, DbType.String, ParameterDirection.Input);
                param.Add("@ActivityType", model.Type, DbType.Int32, ParameterDirection.Input);
                param.Add("@StartDate", startTime, DbType.DateTime, ParameterDirection.Input);
                param.Add("@EndDate", endTime, DbType.DateTime, ParameterDirection.Input);
                param.Add("@Status", model.Status, DbType.Int32, ParameterDirection.Input);
                param.Add("@Priority", model.Priority, DbType.Int32, ParameterDirection.Input);
                param.Add("@Location", model.Location, DbType.String, ParameterDirection.Input);
                param.Add("@Recurrence", 0, DbType.Int32, ParameterDirection.Input);
                param.Add("@Notification", 1, DbType.Int32, ParameterDirection.Input);
                param.Add("@Description", model.Detail, DbType.String, ParameterDirection.Input);
                param.Add("@LeadID", model.LeadId, DbType.Int32, ParameterDirection.Input);
                param.Add("@CustCode", model.Custcode, DbType.String, ParameterDirection.Input);
                param.Add("@updatedBy", model.UserName, DbType.String, ParameterDirection.Input);
                param.Add("@updatedDate", DateTime.Now, DbType.DateTime, ParameterDirection.Input);
                param.Add("@ReasonType", model.ReasonType, DbType.Int32, ParameterDirection.Input);
                param.Add("@activityID", model.ActivityId, DbType.Int32, ParameterDirection.Input);
                param.Add("@branchID", 0, DbType.Int32, ParameterDirection.Input);
                var data = await _dapper.ExecuteAsync(_crmDbConn, "BBB05_UPDATE_ACTIVITY", param, _sqlTimeout);

                if (!String.IsNullOrEmpty(model.RemindTime))
                {
                    string RemindTimeData = DateTime.ParseExact(model.RemindTime, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture).ToString("yyyy-MM-dd HH:mm:ss");
                    var paramNoti = new DynamicParameters();

                    var content = @"Bạn có {EventType} {ActivityType} vào {StartTime}, {EndTime} chờ xử lý.";
                    var title = "Thông báo nhắc {EventType}";

                    if (model.EventType == 1)
                    {
                        content = content.Replace("{EventType}", "Sự kiện");
                        title = title.Replace("{EventType}", "Sự kiện");
                    }
                    else if (model.EventType == 2)
                    {
                        content = content.Replace("{EventType}", "Công việc");
                        title = title.Replace("{EventType}", "Công việc");
                    }

                    if (model.Type == 1)
                    {
                        content = content.Replace("{ActivityType}", "Hẹn trực tiếp");
                    }
                    else if (model.Type == 2)
                    {
                        content = content.Replace("{ActivityType}", "Gọi điện thoại");
                    }
                    else if (model.Type == 3)
                    {
                        content = content.Replace("{ActivityType}", "Gửi Email");
                    }
                    else if (model.Type == 5)
                    {
                        content = content.Replace("{ActivityType}", "Nghỉ phép");
                    }
                    else
                    {
                        content = content.Replace("{ActivityType}", "Khác");
                    }

                    content = content.Replace("{StartTime}", startTime.ToString("yyyy-MM-dd HH:mm:ss"));
                    content = content.Replace("{EndTime}", endTime.ToString("yyyy-MM-dd HH:mm:ss"));

                    paramNoti.Add("@titleNotification", title, DbType.String, ParameterDirection.Input);
                    paramNoti.Add("@contentNotification", content, DbType.String, ParameterDirection.Input);
                    paramNoti.Add("@staffCode", model.Assigned, DbType.String, ParameterDirection.Input);
                    paramNoti.Add("@timeSend", RemindTimeData, DbType.String, ParameterDirection.Input);
                    paramNoti.Add("@userName", model.UserName, DbType.String, ParameterDirection.Input);

                    var dataNoti = await _dapper.ExecuteAsync(_notificationDbConn, "TVSI_sHANG_DOI_THONG_BAO_NHAN_VIEN", paramNoti, _sqlTimeout);
                }

                return new ResponseSingle<bool>
                {
                    Code = ((int)ErrorCodeDetail.Success).ErrorCodeFormat(),
                    Message = ErrorCodeDetail.Success.ToEnumDescription(),
                    Data = true
                };
            }
            catch (Exception ex)
            {
                LogException(model.UserName,
                    $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
                return new ResponseSingle<bool>
                {
                    Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                    Message = ex.Message
                };
            }
        }
        public async Task<ResponseSingle<bool>> DeleteActivity(DeleteActivityRequest model) {
            try
            {
                var param = new DynamicParameters();
                param.Add("@activityID", model.ActivityId, DbType.Int32, ParameterDirection.Input);
                var data = await _dapper.ExecuteAsync(_crmDbConn, "BBB06_DELETE_ACTIVITY", param, _sqlTimeout);
                return new ResponseSingle<bool>
                {
                    Code = ((int)ErrorCodeDetail.Success).ErrorCodeFormat(),
                    Message = ErrorCodeDetail.Success.ToEnumDescription(),
                    Data = true
                };
            }
            catch (Exception ex)
            {
                LogException(model.UserName,
                    $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
                return new ResponseSingle<bool>
                {
                    Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                    Message = ex.Message
                };
            }
        }
        public async Task<ResponseSingle<bool>> UpdateOrDeleteListActivity(UpdateListActivityRequest model) {
            try
            {
                var ids = "";
                if(model.Ids != null && model.Ids.Count > 0)
                {
                    foreach(var id in model.Ids)
                    {
                        ids += id.ToString() + ",";
                    }
                }

                var param = new DynamicParameters();

                param.Add("@Status", model.Status, DbType.Int32, ParameterDirection.Input);
                param.Add("@ActivityIDs", ids.Substring(0, ids.Length - 1), DbType.String, ParameterDirection.Input);

                var data = await _dapper.ExecuteAsync(_crmDbConn, "BBB09_UPDATE_LIST_ACTIVITY", param, _sqlTimeout);
                return new ResponseSingle<bool>
                {
                    Code = ((int)ErrorCodeDetail.Success).ErrorCodeFormat(),
                    Message = ErrorCodeDetail.Success.ToEnumDescription(),
                    Data = true
                };
            }
            catch (Exception ex)
            {
                LogException(model.UserName,
                    $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
                return new ResponseSingle<bool>
                {
                    Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                    Message = ex.Message
                };
            }
        }
        public async Task<ResponseSingle<bool>> ConfirmOrRejectActivity(ConfirmOrRejectRequest model)
        {
            try
            {
                var code = ActivityConst.EncriptMd5(model.UserName + ActivityConst.RandomNumber(6));
                if (code.Length > 50)
                {
                    code = code.Substring(0, 50);
                }

                var param = new DynamicParameters();
                
                param.Add("@ActivityID", model.Id, DbType.Int32, ParameterDirection.Input);

                if (model.Status == 1)
                {
                    param.Add("@ConfirmCode", code, DbType.String, ParameterDirection.Input);
                    await _dapper.ExecuteAsync(_crmDbConn, "BBB10_CONFIRM_ACTIVITY", param, _sqlTimeout);
                } else if(model.Status == 2)
                {
                    param.Add("@RejectCode", code, DbType.String, ParameterDirection.Input);
                    param.Add("@RejectReason", model.RejectReason, DbType.String, ParameterDirection.Input);
                    await _dapper.ExecuteAsync(_crmDbConn, "BBB11_REJECT_ACTIVITY", param, _sqlTimeout);
                }
                
                return new ResponseSingle<bool>
                {
                    Code = ((int)ErrorCodeDetail.Success).ErrorCodeFormat(),
                    Message = ErrorCodeDetail.Success.ToEnumDescription(),
                    Data = (model.Status == 1 || model.Status == 2) ? true : false
                };
            }
            catch (Exception ex)
            {
                LogException(model.UserName,
                    $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
                return new ResponseSingle<bool>
                {
                    Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                    Message = ex.Message
                };
            }
        }
        public async Task<Response<ActivityDataMonthCheckResponse>> GetHisTimeWorkStaffByMonth(ActivityDataMonthRequest model) {
            try
            {
                List<ActivityDataMonthCheckResponse> data = new List<ActivityDataMonthCheckResponse>();

                DateTime today = DateTime.ParseExact(model.DateIndex, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                int days = DateTime.DaysInMonth(today.Year, today.Month);
                var param = new DynamicParameters();

                List<ActivityDataFromQuery> dataWork = new List<ActivityDataFromQuery>();

                List<ActivityDataFromQuery> dataEvent = new List<ActivityDataFromQuery>();

                param.Add("@UserAssign", model.UserName, DbType.String, ParameterDirection.Input);

                if(dataWork.Count == 0)
                {
                    param.Add("@EventType", 2, DbType.Int32, ParameterDirection.Input);
                    var dataQuery = await _dapper.SelectAsync<ActivityDataFromQuery>(_crmDbConn, "BBB03_COUNT_ACTIVITY_DAILY_BY_MONTH", param, _sqlTimeout);
                    dataWork = dataQuery != null ? dataQuery.ToList() : new List<ActivityDataFromQuery>();
                }

                if (dataEvent.Count == 0)
                {
                    param.Add("@EventType", 1, DbType.Int32, ParameterDirection.Input);
                    var dataQuery = await _dapper.SelectAsync<ActivityDataFromQuery>(_crmDbConn, "BBB03_COUNT_ACTIVITY_DAILY_BY_MONTH", param, _sqlTimeout);
                    dataEvent = dataQuery != null ? dataQuery.ToList() : new List<ActivityDataFromQuery>();
                }

                for (var i = 1; i <= days; i++)
                {
                    ActivityDataMonthCheckResponse dayCheck = new ActivityDataMonthCheckResponse();
                    string day = new DateTime(today.Year, today.Month, i).ToString("dd/MM/yyyy");
                    int hasEvent = 0;
                    int hasWork = 0;
                    var dataCheckEnvent = dataEvent.FirstOrDefault(e => e.DateWork.Equals(day));
                    if (dataCheckEnvent != null)
                    {
                        hasEvent = dataCheckEnvent.CountActivity;
                    }
                    var dataCheckWork = dataWork.FirstOrDefault(e => e.DateWork.Equals(day));
                    if (dataCheckWork != null)
                    {
                        hasWork = dataCheckWork.CountActivity;
                    }

                    dayCheck.EventCount = hasEvent;
                    dayCheck.WorkCount = hasWork;
                    dayCheck.DateWork = day;
                    data.Add(dayCheck);
                }

                return new Response<ActivityDataMonthCheckResponse>
                {
                    Code = ((int)ErrorCodeDetail.Success).ErrorCodeFormat(),
                    Message = ErrorCodeDetail.Success.ToEnumDescription(),
                    Data = data
                };

            }
            catch (Exception ex)
            {
                LogException(model.UserName,
                    $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
                return new Response<ActivityDataMonthCheckResponse> {
                    Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                    Message = ex.Message
                };
            }
        }
        public async Task<Response<ActivityTypeReponse>> GetActivityType(BaseRequest model)
        {
            try
            {
                var param = new DynamicParameters();

                return new Response<ActivityTypeReponse>
                {
                    Code = ((int)ErrorCodeDetail.Success).ErrorCodeFormat(),
                    Message = ErrorCodeDetail.Success.ToEnumDescription(),
                    Data = await _dapper.SelectAsync<ActivityTypeReponse>(_crmDbConn, "BBB28_GET_LIST_ACTIVITY_TYPE", param, _sqlTimeout)
                };
            }
            catch (Exception ex)
            {
                LogException(model.UserName,
                    $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
                return new Response<ActivityTypeReponse> {
                    Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                    Message = ex.Message
                };
            }
        }
    }
}
