﻿using Dapper;
using Helper.DataAccess.Dapper;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using TVSI.Crm.API.Common;
using TVSI.Crm.API.Common.Enums;
using TVSI.Crm.API.Lib.Models;
using TVSI.Crm.API.Lib.Models.Request.Dashboard;
using TVSI.Crm.API.Lib.Models.Response.Dashboard;
using TVSI.TWork.API.Common.ExtensionMethods;

namespace TVSI.Crm.API.Lib.Services.Impls
{
    public class DashboardService : BaseService<DashboardService>, IDashboardService
    {
        private readonly IDapperHelper _dapper;
        private readonly string _crmDbConn;
        private readonly string _emsDbConn;
        private readonly string _innoDbConn;
        private readonly int _sqlTimeout;

        public DashboardService(IConfiguration config, ILogger<DashboardService> logger, IDapperHelper dapper) : base(config, logger)
        {
            var connStr = _config.GetConnectionString("CRMDB_CONNECTION");
            var connEmsStr = _config.GetConnectionString("EMSDB_CONNECTION");
            var connInnoStr = _config.GetConnectionString("INNODB_CONNECTION");
            _crmDbConn = connStr;
            _emsDbConn = connEmsStr;
            _innoDbConn = connInnoStr;
            _dapper = dapper;
            _sqlTimeout = _config["Timeout:Database"] == null
                ? CommonConstants.SqlServerTimeout
                : int.Parse(_config["Timeout:Database"]);
        }

        public async Task<ResponseSingle<DashboardChartResult>> TradeInfo(DashboardChartRequest model)
        {
            try
            {
                DateTime today = DateTime.ParseExact(model.DateIndex, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                DayOfWeek currentDay = today.DayOfWeek;
                int daysTillCurrentDay = currentDay - DayOfWeek.Monday;
                DateTime currentWeekStartDate = today.AddDays(-daysTillCurrentDay - 1);
                DateTime currentWeekEndDate = today.AddDays(1);
                DateTime lastWeekStartDate = currentWeekStartDate.AddDays(-7);
                DateTime lastWeekEndDate = currentWeekStartDate.AddDays(1);
                DateTime StartDateLastMonth = new DateTime(today.Month == 1 ? today.Year - 1 : today.Year, today.Month == 1 ? 12 : today.Month - 1, 1);
                DateTime StartDateMonth = new DateTime(today.Year, today.Month, 1);
                DateTime EndDateMonth = new DateTime(today.Month == 12 ? today.Year + 1 : today.Year, today.Month == 12 ? 1 : today.Month + 1, 1);
                string date = DateTime.ParseExact(model.DateIndex, "dd/MM/yyyy", CultureInfo.InvariantCulture).ToString("yyyy-MM-dd");

                var dataReponse = new DashboardChartResult();

                var param = new DynamicParameters();

                param.Add("@end", model.Status == 1 ? lastWeekEndDate.ToString("yyyy-MM-dd") + " 00:00:00.000" : StartDateMonth.ToString("yyyy-MM-dd") + " 00:00:00.000", DbType.String, ParameterDirection.Input);
                param.Add("@start", model.Status == 1 ? lastWeekStartDate.ToString("yyyy-MM-dd") + " 00:00:00.000" : StartDateLastMonth.AddDays(-1).ToString("yyyy-MM-dd") + " 00:00:00.000", DbType.String, ParameterDirection.Input);
                param.Add("@endChar", model.Status == 1 ? currentWeekEndDate.ToString("yyyy-MM-dd") + " 00:00:00.000" : EndDateMonth.ToString("yyyy-MM-dd") + " 00:00:00.000", DbType.String, ParameterDirection.Input);
                param.Add("@startChar", model.Status == 1 ? currentWeekStartDate.ToString("yyyy-MM-dd") + " 00:00:00.000" : StartDateMonth.ToString("yyyy-MM-dd") + " 00:00:00.000", DbType.String, ParameterDirection.Input);
                param.Add("@userCode", model.UserName, DbType.String, ParameterDirection.Input);
                param.Add("@DateIndex", date + " 00:00:00.000", DbType.String, ParameterDirection.Input);

                var data = await _dapper.GetAsync<DashboardSql>(_emsDbConn, "BBB05_GET_SUM_FEE", param, _sqlTimeout);

                double sumThisWeek = 0.0;
                double sumLastWeek = 0.0;

                dataReponse.Transaction_Data = JsonConvert.DeserializeObject<List<DashboardChartDetail>>(data.SumTradeInfo);
                dataReponse.TodayFee = data.TodayFee;

                sumLastWeek = data.SumFeeLastWeek;

                foreach (DashboardChartDetail dataSum in dataReponse.Transaction_Data)
                {
                    sumThisWeek += dataSum.Transaction_Fee;
                }

                dataReponse.PercentOfWeek = sumLastWeek == 0.0 ? 100 : Math.Round(((sumThisWeek / sumLastWeek) * 100), 2);

                return new ResponseSingle<DashboardChartResult>
                {
                    Code = ((int)ErrorCodeDetail.Success).ErrorCodeFormat(),
                    Message = ErrorCodeDetail.Success.ToEnumDescription(),
                    Data = dataReponse
                };
            }
            catch (Exception ex)
            {
                LogException(model.UserName,
                    $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
                return new ResponseSingle<DashboardChartResult> { };
            }
        }

        public async Task<ResponseSingle<DashboardSummaryResult>> TradeTotalHistory(BaseRequest model)
        {
            try
            {
                var param = new DynamicParameters();

                param.Add("@userName", model.UserName, DbType.String, ParameterDirection.Input);
                param.Add("@systemLevel", 0, DbType.Int32, ParameterDirection.Input);
                param.Add("@branchID", "", DbType.String, ParameterDirection.Input);
                param.Add("@saleList", "", DbType.String, ParameterDirection.Input);
                return new ResponseSingle<DashboardSummaryResult>
                {
                    Code = ((int)ErrorCodeDetail.Success).ErrorCodeFormat(),
                    Message = ErrorCodeDetail.Success.ToEnumDescription(),
                    Data = await _dapper.GetAsync<DashboardSummaryResult>(_crmDbConn, "TVSI_sDashboard_TotalInfo", param, _sqlTimeout)
                };
            }
            catch (Exception ex)
            {
                LogException(model.UserName,
                     $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
                return new ResponseSingle<DashboardSummaryResult> { };
            }
        }

        public async Task<ResponseSingle<DashboardTradeResult>> TradeTotalInfoHistory(DashboardTradeRequest model)
        {
            try
            {
                string date = DateTime.ParseExact(model.DateIndex, "dd/MM/yyyy", CultureInfo.InvariantCulture).ToString("yyyy-MM-dd");

                var param = new DynamicParameters();

                param.Add("@userCode", model.UserName, DbType.String, ParameterDirection.Input);
                param.Add("@saleId", model.UserName.Substring(0, 4), DbType.String, ParameterDirection.Input);
                param.Add("@DateIndex", date, DbType.String, ParameterDirection.Input);

                var data = await _dapper.GetAsync<DashboardTradeResultSql>(_emsDbConn, "BBB06_GET_TRANDE_INFO", param, _sqlTimeout);

                var dataDailyTrade = JsonConvert.DeserializeObject<List<DashboardDailyTradeInfoData>>(data.TotalLastMonth);

                DashboardTradeResult dataReponse = new DashboardTradeResult();

                dataReponse.MonthVolume = dataDailyTrade.Count > 0 ? dataDailyTrade[0].MonthVolume : 0;
                dataReponse.MonthAmount = dataDailyTrade.Count > 0 ? dataDailyTrade[0].MonthAmount : 0.0;
                dataReponse.MonthFee = dataReponse.MonthAmount * 0.003;
                dataReponse.MonthPercent = data.TotalAmountMonthBefore == 0.0 ? 100 : Math.Round(((dataReponse.MonthAmount / data.TotalAmountMonthBefore) * 100), 2);
                dataReponse.OpenAccount = data.CountAccount;

                return new ResponseSingle<DashboardTradeResult>
                {
                    Code = ((int)ErrorCodeDetail.Success).ErrorCodeFormat(),
                    Message = ErrorCodeDetail.Success.ToEnumDescription(),
                    Data = dataReponse
                };
            }
            catch (Exception ex)
            {
                LogException(model.UserName,
                    $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
                return new ResponseSingle<DashboardTradeResult> { };
            }
        }

        public async Task<ResponseSingle<TotalCustLeadResponse>> TotalCustLead(BaseRequest model)
        {
            try
            {
                var today = DateTime.Now;
                var param = new DynamicParameters();

                param.Add("@userName", model.UserName, DbType.String, ParameterDirection.Input);
                param.Add("@Date", today.ToString("yyyy-MM-dd"), DbType.String, ParameterDirection.Input);
                return new ResponseSingle<TotalCustLeadResponse>
                {
                    Code = ((int)ErrorCodeDetail.Success).ErrorCodeFormat(),
                    Message = ErrorCodeDetail.Success.ToEnumDescription(),
                    Data = await _dapper.GetAsync<TotalCustLeadResponse>(_crmDbConn, "BBB36_GET_Total_Cust_lead", param, _sqlTimeout)
                };
            }
            catch (Exception ex)
            {
                LogException(model.UserName,
                    $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
                return new ResponseSingle<TotalCustLeadResponse> { };
            }
        }

        public async Task<ResponseSingle<TotalActitityReponse>> TotalAcitvity(BaseRequest model)
        {
            try
            {
                var today = DateTime.Now;
                var param = new DynamicParameters();

                param.Add("@userName", model.UserName, DbType.String, ParameterDirection.Input);
                param.Add("@systemLevel", 0, DbType.Int32, ParameterDirection.Input);
                param.Add("@branchID", "", DbType.String, ParameterDirection.Input);
                param.Add("@saleList", "", DbType.String, ParameterDirection.Input);
                param.Add("@Date", today.ToString("yyyy-MM-dd"), DbType.String, ParameterDirection.Input);
                return new ResponseSingle<TotalActitityReponse>
                {
                    Code = ((int)ErrorCodeDetail.Success).ErrorCodeFormat(),
                    Message = ErrorCodeDetail.Success.ToEnumDescription(),
                    Data = await _dapper.GetAsync<TotalActitityReponse>(_crmDbConn, "BBB37_GET_Total_Activity", param, _sqlTimeout)
                };
            }
            catch (Exception ex)
            {
                LogException(model.UserName,
                    $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
                return new ResponseSingle<TotalActitityReponse> { };
            }
        }
    }
}
