﻿using Dapper;
using Helper.DataAccess.Dapper;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using TVSI.Crm.API.Common;
using TVSI.Crm.API.Lib.Models;
using TVSI.TWork.API.Common.Enums;
using TVSI.TWork.API.Common.ExtensionMethods;
using static TVSI.Crm.API.Lib.Models.Request.Contact.ContactRequest;
using static TVSI.Crm.API.Lib.Models.Response.Contact.ContactRequest;

namespace TVSI.Crm.API.Lib.Services.Impls
{
    public class ContactService : BaseService<ContactService>, IContactService
    {
        private readonly IDapperHelper _dapper;
        private readonly string _crmDbConn;
        private readonly string _emsDbConn;
        private readonly string _innoDbConn;
        private readonly int _sqlTimeout;

        public ContactService(IConfiguration config, ILogger<ContactService> logger, IDapperHelper dapper) : base(config, logger)
        {
            var connStr = _config.GetConnectionString("CRMDB_CONNECTION");
            var connEmsStr = _config.GetConnectionString("EMSDB_CONNECTION");
            var connInnoStr = _config.GetConnectionString("INNODB_CONNECTION");
            _crmDbConn = connStr;
            _emsDbConn = connEmsStr;
            _innoDbConn = connInnoStr;
            _dapper = dapper;
            _sqlTimeout = _config["Timeout:Database"] == null
                ? CommonConstants.SqlServerTimeout
                : int.Parse(_config["Timeout:Database"]);
        }

        public async Task<ResponseSingle<ContactInfoCustomer>> GetInfoContactCust(ContactinfoCustRequest model)
        {
            try
            {

                var param = new DynamicParameters();

                param.Add("@SaleId", model.UserName, DbType.String, ParameterDirection.Input);

                var dataUser = await _dapper.GetAsync<ContactInfoStaff>(_emsDbConn, "BBB01_GET_STAFF_INFO", param, _sqlTimeout);

                var param1 = new DynamicParameters();

                param1.Add("@CustCode", model.CustCode, DbType.String, ParameterDirection.Input);
                param1.Add("@AssignUser", model.UserName, DbType.String, ParameterDirection.Input);

                var dataCust = await _dapper.GetAsync<ContactInfoCustomer>(_crmDbConn, "BBB12_GET_CUST_INFO", param1, _sqlTimeout);

                dataCust.SaleID = dataUser.SaleID;
                dataCust.SaleName = dataUser.SaleName;

                return new ResponseSingle<ContactInfoCustomer>
                {
                    Code = ((int)ErrorCodeDetail.Success).ErrorCodeFormat(),
                    Message = ErrorCodeDetail.Success.ToEnumDescription(),
                    Data = dataCust
                };
            }
            catch (Exception ex)
            {
                LogException(model.UserName,
                    $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
                return new ResponseSingle<ContactInfoCustomer> {
                    Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                    Message = ex.Message
                };
            }
        }

        public async Task<ResponseSingle<ContactInfoLead>> GetInfoContactLead(ContactinfoLeadRequest model)
        {
            try
            {

                var param = new DynamicParameters();

                param.Add("@SaleId", model.UserName, DbType.String, ParameterDirection.Input);

                var dataUser = await _dapper.GetAsync<ContactInfoStaff>(_emsDbConn, "BBB01_GET_STAFF_INFO", param, _sqlTimeout);

                var param1 = new DynamicParameters();

                param1.Add("@LeadID", model.LeadID, DbType.Int32, ParameterDirection.Input);
                param1.Add("@AssignUser", model.UserName, DbType.String, ParameterDirection.Input);

                var dataCust = await _dapper.GetAsync<ContactInfoLead>(_crmDbConn, "BBB13_GET_LEAD_INFO", param1, _sqlTimeout);

                dataCust.SaleID = dataUser.SaleID;
                dataCust.SaleName = dataUser.SaleName;

                return new ResponseSingle<ContactInfoLead>
                {
                    Code = ((int)ErrorCodeDetail.Success).ErrorCodeFormat(),
                    Message = ErrorCodeDetail.Success.ToEnumDescription(),
                    Data = dataCust
                };
            }
            catch (Exception ex)
            {
                LogException(model.UserName,
                    $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
                return new ResponseSingle<ContactInfoLead> {
                    Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                    Message = ex.Message
                };
            }
        }

        public async Task<ResponseSingle<ContactInfoStaff>> GetInfoContactStaff(ContactinfoStaffRequest model)
        {
            try
            {

                var param = new DynamicParameters();

                param.Add("@SaleId", model.UserName, DbType.String, ParameterDirection.Input);

                var dataUser = await _dapper.GetAsync<ContactInfoStaff>(_emsDbConn, "BBB01_GET_STAFF_INFO", param, _sqlTimeout);
                return new ResponseSingle<ContactInfoStaff>
                {
                    Code = ((int)ErrorCodeDetail.Success).ErrorCodeFormat(),
                    Message = ErrorCodeDetail.Success.ToEnumDescription(),
                    Data = dataUser
                };
            }
            catch (Exception ex)
            {
                LogException(model.UserName,
                    $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
                return new ResponseSingle<ContactInfoStaff> {
                    Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                    Message = ex.Message
                };
            }
        }

        public async Task<ResponseSingle<ContactCustomerResponse>> GetListContactCust(ContactStaffRequest model)
        {
            try
            {
                var start = (model.PageIndex - 1) * model.PageSize;
                var param = new DynamicParameters();

                param.Add("@UserName", model.UserName.Substring(0, 4), DbType.String, ParameterDirection.Input);
                param.Add("@KeySearch", model.KeySearch, DbType.String, ParameterDirection.Input);
                param.Add("@CustCode", model.CustCode, DbType.String, ParameterDirection.Input);
                param.Add("@Phone", model.Phone, DbType.String, ParameterDirection.Input);
                param.Add("@Start", start, DbType.Int32, ParameterDirection.Input);
                param.Add("@PageSize", model.PageSize, DbType.Int32, ParameterDirection.Input);

                var param1 = new DynamicParameters();
                param1.Add("@UserName", model.UserName.Substring(0, 4), DbType.String, ParameterDirection.Input);
                param1.Add("@KeySearch", model.KeySearch, DbType.String, ParameterDirection.Input);
                param1.Add("@CustCode", model.CustCode, DbType.String, ParameterDirection.Input);
                param1.Add("@Phone", model.Phone, DbType.String, ParameterDirection.Input);

                ContactCustomerResponse data = new ContactCustomerResponse();

                var dataUser = await _dapper.SelectAsync<ContactCustomer>(_innoDbConn, "BBB01_GET_LIST_CONTACT_CUSTOMER", param, _sqlTimeout);
                var count = await _dapper.GetAsync<int>(_innoDbConn, "BBB02_GET_COUNT_CONTACT_CUSTOMER", param1, _sqlTimeout);

                data.ListContactCustomers = dataUser.ToList();
                data.TotalContact = count;

                return new ResponseSingle<ContactCustomerResponse>
                {
                    Code = ((int)ErrorCodeDetail.Success).ErrorCodeFormat(),
                    Message = ErrorCodeDetail.Success.ToEnumDescription(),
                    Data = data
                };
            }
            catch (Exception ex)
            {
                LogException(model.UserName,
                    $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
                return new ResponseSingle<ContactCustomerResponse> {
                    Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                    Message = ex.Message
                };
            }
        }

        public async Task<ResponseSingle<ContactStaffResponse>> GetListContactStaff(ContactStaffRequest model)
        {
            try
            {
                var start = (model.PageIndex - 1) * model.PageSize;

                var filterFunctionList = new List<string> { "CRM_BO", "CRM_SALE", "CRM_BB", "CRM_HR", "CRM_CF" };

                var param = new DynamicParameters();

                param.Add("@KeySearch", model.KeySearch, DbType.String, ParameterDirection.Input);
                param.Add("@Start", start, DbType.Int32, ParameterDirection.Input);
                param.Add("@PageSize", model.PageSize, DbType.Int32, ParameterDirection.Input);

                var param1 = new DynamicParameters();
                param1.Add("@KeySearch", model.KeySearch, DbType.String, ParameterDirection.Input);

                ContactStaffResponse data = new ContactStaffResponse();

                List<ContactStaff> dataStaff = new List<ContactStaff>();

                var ListContactStaff = await _dapper.SelectAsync<ContactStaff>(_emsDbConn, "BBB02_GET_INFO_LIST_STAFF", param, _sqlTimeout);
                var count = await _dapper.GetAsync<int>(_emsDbConn, "BBB03_COUNT_LIST_STAFF", param1, _sqlTimeout);

                string listUserNames = "";
                foreach (var item in ListContactStaff)
                {
                    listUserNames += item.UserName + ",";
                }

                dataStaff = ListContactStaff.ToList();

                var param2 = new DynamicParameters();
                param2.Add("@tenDangNhap", listUserNames.Substring(0, listUserNames.Length - 1), DbType.String, ParameterDirection.Input);

                var ListRole = await _dapper.SelectAsync<ContactRole>(_emsDbConn, "BBB04_GET_ROLE_STAFF", param2, _sqlTimeout);

                foreach (var roleItem in ListRole)
                {
                    if (filterFunctionList.Contains(roleItem.ma_chuc_nang))
                    {
                        ContactStaff contactStaff = dataStaff.FirstOrDefault(e => e.UserName == roleItem.ten_dang_nhap);
                        contactStaff.UserRole = roleItem.ma_chuc_nang;

                        if ("CR M_HR".Equals(roleItem.ma_chuc_nang) || "CRM_CF".Equals(roleItem.ma_chuc_nang))
                            contactStaff.UserRole = "CRM_BO";
                    }
                }

                data.ListContactStaffs = dataStaff;
                data.TotalContact = count;

                return new ResponseSingle<ContactStaffResponse>
                {
                    Code = ((int)ErrorCodeDetail.Success).ErrorCodeFormat(),
                    Message = ErrorCodeDetail.Success.ToEnumDescription(),
                    Data = data
                };
            }
            catch (Exception ex)
            {
                LogException(model.UserName,
                    $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
                return new ResponseSingle<ContactStaffResponse> {
                    Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                    Message = ex.Message
                };
            }
        }
    }
}
