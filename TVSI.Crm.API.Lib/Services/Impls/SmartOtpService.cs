﻿using Dapper;
using Helper.DataAccess.Dapper;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using OtpNet;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using TVSI.Crm.API.Common;
using TVSI.Crm.API.Common.Enums;
using TVSI.Crm.API.Lib.Models;
using TVSI.Crm.API.Lib.Models.Request.SmartOtp;
using TVSI.Crm.API.Lib.Models.Response.SmartOtp;
using TVSI.Crm.API.Lib.Models.Response.User;
using TVSI.Crm.API.Lib.Utilities;
using TVSI.TWork.API.Common.ExtensionMethods;

namespace TVSI.Crm.API.Lib.Services.Impls
{
    public class SmartOtpService : BaseService<SmartOtpService>, ISmartOtpService
    {
        private readonly IDapperHelper _dapper;
        private readonly string _centralizeDbConn;
        private readonly string _notificationDbConn;
        private readonly int _sqlTimeout;

        public SmartOtpService(IConfiguration config, ILogger<SmartOtpService> logger, IDapperHelper dapper) : base(config, logger)
        {
            var connStr = _config.GetConnectionString("CENTRALIZEDB_CONNECTION");
            var connStrNoti = _config.GetConnectionString("NOTIFICATIONDB_CONNECTION");
            _centralizeDbConn = connStr;
            _notificationDbConn = connStrNoti;
            _dapper = dapper;
            _sqlTimeout = _config["Timeout:Database"] == null
                ? CommonConstants.SqlServerTimeout
                : int.Parse(_config["Timeout:Database"]);
        }
        public async Task<ResponseSingle<bool>> CheckDeviceSmartOtp(CheckSmartOtpDeviceRequest model)
        {
            try
            {
                var param = new DynamicParameters();
                param.Add("@UserLogin", model.UserName, DbType.String, ParameterDirection.Input);
                param.Add("@DeviceId", model.deviceId, DbType.String, ParameterDirection.Input);
                param.Add("@status", 2, DbType.Int32, ParameterDirection.Input);

                var data = await _dapper.GetAsync<SmartOtpModel>(_centralizeDbConn, "AAA01_SmartOtp_GetDeviceInfo", param, _sqlTimeout);

                if(data == null)
                {
                    return new ResponseSingle<bool>
                    {
                        Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                        Message = "Tài khoản chưa được kích hoạt Otp",
                        Data = false
                    };
                }

                return new ResponseSingle<bool>
                {
                    Code = ((int)ErrorCodeDetail.Success).ErrorCodeFormat(),
                    Message = "Tài khoản đã được kích hoạt Otp",
                    Data = true
                };

            }
            catch (Exception ex)
            {
                LogException(model.UserName,
                    $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
                return new ResponseSingle<bool> 
                {
                    Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                    Message = ex.Message,
                    Data = false
                };
            }
        }
        public async Task<ResponseSingle<SmartOtpModelResponse>> RegisterSmartOtp(RegisterDeviceRequest model)
        {
            try 
            {
                var paramGet = new DynamicParameters();
                paramGet.Add("@UserLogin", model.UserName, DbType.String, ParameterDirection.Input);
                paramGet.Add("@DeviceId", model.deviceId, DbType.String, ParameterDirection.Input);
                paramGet.Add("@status", 1, DbType.Int32, ParameterDirection.Input);

                var data = await _dapper.GetAsync<SmartOtpModel>(_centralizeDbConn, "AAA01_SmartOtp_GetDeviceInfo", paramGet, _sqlTimeout);

                var dataReponse = new SmartOtpModelResponse();

                var secretKey = Base32Url.ToBase32String(Encoding.ASCII.GetBytes($"{model.UserName}#{model.deviceId}#{Guid.NewGuid()}"));
                var encryptKeyAndIV = SmartOtpConst.GetMd5Hash($"{model.UserName}#{model.deviceId}#{model.smartPin}");
                var encryptKey = !string.IsNullOrEmpty(encryptKeyAndIV) && encryptKeyAndIV.Length > 16 ? encryptKeyAndIV.Substring(0, 16) : string.Empty;
                var encryptIV = !string.IsNullOrEmpty(encryptKeyAndIV) && encryptKeyAndIV.Length > 16 ? encryptKeyAndIV.Substring(16) : string.Empty;
                var secretKeyHash = SmartOtpConst.EncryptCBC(secretKey, encryptKey, encryptIV);

                dataReponse.SecretKey = secretKeyHash;

                var paramUser = new DynamicParameters();
                paramUser.Add("@UserLogin", model.UserName, DbType.String, ParameterDirection.Input);
                var dataUser = await _dapper.GetAsync<UserInfo>(_centralizeDbConn, "AAA03_Get_User_Info", paramUser, _sqlTimeout);

                if (dataUser == null || dataUser.Phone == null)
                {
                    dataReponse.SecretKey = "";
                    return new ResponseSingle<SmartOtpModelResponse>
                    {
                        Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                        Message = "Đăng ký smartOtp thất bại",
                        Data = dataReponse
                    };
                }

                var totp = new Totp(Base32Encoding.ToBytes(secretKey), step: 300);

                var content = "TVSI tran trong thong bao. Ma OTP cua ban la: " + totp.ComputeTotp(DateTime.UtcNow) + ". OTP co hieu luc 5 phut. Tran trong!";

                var paramSms = new DynamicParameters();
                paramSms.Add("@so_dien_thoai", dataUser.Phone, DbType.String, ParameterDirection.Input);
                paramSms.Add("@ma_dich_vu", "SMS_OTP", DbType.String, ParameterDirection.Input);
                paramSms.Add("@trang_thai", 0, DbType.Int32, ParameterDirection.Input);
                

                if (data != null && data.SecretKey != null)
                {
                    secretKeyHash = SmartOtpConst.EncryptCBC(data.SecretKey, encryptKey, encryptIV);
                    
                    dataReponse.SecretKey = secretKeyHash;

                    totp = new Totp(Base32Encoding.ToBytes(data.SecretKey), step: 300);
                    content = "TVSI tran trong thong bao. Ma OTP cua ban la: " + totp.ComputeTotp(DateTime.UtcNow) + ". OTP co hieu luc 5 phut. Tran trong!";

                    paramSms.Add("@noi_dung", content, DbType.String, ParameterDirection.Input);

                    await _dapper.ExecuteAsync(_notificationDbConn, "TVSI_sHANG_DOI_SMS_INSERT_SMART_OTP", paramSms, _sqlTimeout);
                    return new ResponseSingle<SmartOtpModelResponse>
                    {
                        Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                        Message = "Bạn đã đăng ký smartOtp vui lòng kích hoạt otp",
                        Data = dataReponse
                    };
                }

                paramSms.Add("@noi_dung", content, DbType.String, ParameterDirection.Input);
                await _dapper.ExecuteAsync(_notificationDbConn, "TVSI_sHANG_DOI_SMS_INSERT_SMART_OTP", paramSms, _sqlTimeout);

                var param = new DynamicParameters();
                param.Add("@UserLogin", model.UserName, DbType.String, ParameterDirection.Input);
                param.Add("@DeviceId", model.deviceId, DbType.String, ParameterDirection.Input);
                param.Add("@SecretKey", secretKey, DbType.String, ParameterDirection.Input);
                await _dapper.ExecuteAsync(_centralizeDbConn, "AAA02_Create_SmartOtp", param, _sqlTimeout);

                return new ResponseSingle<SmartOtpModelResponse>
                {
                    Code = ((int)ErrorCodeDetail.Success).ErrorCodeFormat(),
                    Message = "Đăng ký smartOtp thành công",
                    Data = dataReponse
                };
            }
            catch (Exception ex)
            {
                LogException(model.UserName,
                    $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
                return new ResponseSingle<SmartOtpModelResponse>
                {
                    Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                    Message = ex.Message,
                    Data = new SmartOtpModelResponse()
            };
            }
        }
        public async Task<ResponseSingle<bool>> ActiveSmartOtp(ActiveOtpRequest model)
        {
            try
            {
                var param = new DynamicParameters();
                param.Add("@UserLogin", model.UserName, DbType.String, ParameterDirection.Input);
                param.Add("@DeviceId", model.deviceId, DbType.String, ParameterDirection.Input);
                param.Add("@status", 1, DbType.Int32, ParameterDirection.Input);

                var data = await _dapper.GetAsync<SmartOtpModel>(_centralizeDbConn, "AAA01_SmartOtp_GetDeviceInfo", param, _sqlTimeout);

                if (data == null)
                {
                    return new ResponseSingle<bool>
                    {
                        Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                        Message = "Tài khoản chưa được đăng ký smartOtp",
                        Data = false
                    };
                }

                var totp = new Totp(Base32Encoding.ToBytes(data.SecretKey), step: 300);

                var otp = totp.ComputeTotp(DateTime.UtcNow);

                if (!model.otp.Contains(otp))
                {
                    return new ResponseSingle<bool>
                    {
                        Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                        Message = "Otp không đúng vui lòng thử lại",
                        Data = false
                    };
                }

                var paramUpdate = new DynamicParameters();

                paramUpdate.Add("@UserLogin", model.UserName, DbType.String, ParameterDirection.Input);
                paramUpdate.Add("@DeviceId", model.deviceId, DbType.String, ParameterDirection.Input);
                paramUpdate.Add("@status", 2, DbType.Int32, ParameterDirection.Input);

                await _dapper.ExecuteAsync(_centralizeDbConn, "AAA03_UpdateStatus_SmartOtp", paramUpdate, _sqlTimeout);

                return new ResponseSingle<bool>
                {
                    Code = ((int)ErrorCodeDetail.Success).ErrorCodeFormat(),
                    Message = "Tài khoản kích hoạt Otp thành công",
                    Data = true
                };

            }
            catch (Exception ex)
            {
                LogException(model.UserName,
                    $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
                return new ResponseSingle<bool>
                {
                    Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                    Message = ex.Message,
                    Data = false
                };
            }
        }
        public async Task<ResponseSingle<bool>> UnRegisterSmartOtp(ActiveOtpRequest model)
        {
            try
            {
                var param = new DynamicParameters();
                param.Add("@UserLogin", model.UserName, DbType.String, ParameterDirection.Input);
                param.Add("@DeviceId", model.deviceId, DbType.String, ParameterDirection.Input);
                param.Add("@status", 2, DbType.Int32, ParameterDirection.Input);

                var data = await _dapper.GetAsync<SmartOtpModel>(_centralizeDbConn, "AAA01_SmartOtp_GetDeviceInfo", param, _sqlTimeout);

                if (data == null)
                {
                    return new ResponseSingle<bool>
                    {
                        Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                        Message = "Tài khoản chưa được đăng ký smartOtp",
                        Data = false
                    };
                }

                var totp = new Totp(Base32Encoding.ToBytes(data.SecretKey), step: 30);

                var otp = totp.ComputeTotp(DateTime.UtcNow);

                if (!model.otp.Contains(otp))
                {
                    return new ResponseSingle<bool>
                    {
                        Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                        Message = "Otp không đúng vui lòng thử lại",
                        Data = false
                    };
                }

                var paramUpdate = new DynamicParameters();

                paramUpdate.Add("@UserLogin", model.UserName, DbType.String, ParameterDirection.Input);
                paramUpdate.Add("@DeviceId", model.deviceId, DbType.String, ParameterDirection.Input);
                paramUpdate.Add("@status", 99, DbType.Int32, ParameterDirection.Input);

                await _dapper.GetAsync<SmartOtpModel>(_centralizeDbConn, "AAA03_UpdateStatus_SmartOtp", paramUpdate, _sqlTimeout);

                return new ResponseSingle<bool>
                {
                    Code = ((int)ErrorCodeDetail.Success).ErrorCodeFormat(),
                    Message = "Tài khoản hủy otp thành công",
                    Data = true
                };
            }
            catch (Exception ex)
            {
                LogException(model.UserName,
                    $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
                return new ResponseSingle<bool>
                {
                    Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                    Message = ex.Message,
                    Data = false
                };
            }
        }
        public async Task<ResponseSingle<bool>> VerifySmartOtp(SmartOtpRequest model)
        {
            try
            {
                var param = new DynamicParameters();
                param.Add("@UserLogin", model.UserName, DbType.String, ParameterDirection.Input);
                param.Add("@DeviceId", model.deviceId, DbType.String, ParameterDirection.Input);
                param.Add("@status", 2, DbType.Int32, ParameterDirection.Input);

                var data = await _dapper.GetAsync<SmartOtpModel>(_centralizeDbConn, "AAA01_SmartOtp_GetDeviceInfo", param, _sqlTimeout);

                if(data == null)
                {
                    return new ResponseSingle<bool>
                    {
                        Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                        Message = "Tài khoản chưa được đăng ký smartOtp",
                        Data = false
                    };
                }

                var totp = new Totp(Base32Encoding.ToBytes(data.SecretKey), step: 30);

                var otp = totp.ComputeTotp(DateTime.UtcNow);

                if(!string.IsNullOrEmpty(model.otpTime))
                {
                    var dateTime = DateTime.ParseExact(model.otpTime, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);

                    var otpClient = totp.ComputeTotp(dateTime);

                    if (!model.otp.Contains(otpClient))
                    {
                        return new ResponseSingle<bool>
                        {
                            Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                            Message = "Otp không đúng vui long kiểm tra lại",
                            Data = false
                        };
                    } else
                    {
                        return new ResponseSingle<bool>
                        {
                            Code = ((int)ErrorCodeDetail.Success).ErrorCodeFormat(),
                            Message = "Thành công time client",
                            Data = true
                        };
                    }
                }

                if (!model.otp.Contains(otp))
                {
                    return new ResponseSingle<bool>
                    {
                        Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                        Message = "Thời gian server lệch",
                        Data = false
                    };
                }

                return new ResponseSingle<bool>
                {
                    Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                    Message = "Thành công",
                    Data = true
                };
            }
            catch (Exception ex)
            {
                LogException(model.UserName,
                    $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
                return new ResponseSingle<bool>
                {
                    Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                    Message = ex.Message,
                    Data = false
                };
            }
        }
    }
}
