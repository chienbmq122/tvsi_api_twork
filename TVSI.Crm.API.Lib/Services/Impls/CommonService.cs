using System.Data;
using System.Reflection;
using Dapper;
using Helper.DataAccess.Dapper;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using TVSI.Crm.API.Common;
using TVSI.Crm.API.Common.Enums;
using TVSI.Crm.API.Lib.Models;
using TVSI.Crm.API.Lib.Models.Request.Common;
using TVSI.Crm.API.Lib.Models.Response.Common;
using TVSI.TWork.API.Common.ExtensionMethods;

namespace TVSI.Crm.API.Lib.Services.Impls;

public class CommonService : BaseService<CommonService>, ICommonService
{
    private readonly IDapperHelper _dapper;
    private readonly string _crmDbConn;
    private readonly string _commonDbConn;
    private readonly string _emsDbConn;
    private readonly string _innoDbConn;
    private readonly string _priceinfoDbConn;
    private readonly int _sqlTimeout;

    public CommonService(IConfiguration config, ILogger<CommonService> logger, IDapperHelper dapper) : base(config,
        logger)
    {
        var connStr = _config.GetConnectionString("CRMDB_CONNECTION");
        var commonStr = _config.GetConnectionString("COMMONDB_CONNECTION");
        var emsStr = _config.GetConnectionString("EMSDB_CONNECTION");
        var innoStr = _config.GetConnectionString("INNODB_CONNECTION");
        var priceStr = _config.GetConnectionString("PRICEINOFO_CONNECTION");
        _crmDbConn = connStr;
        _commonDbConn = commonStr;
        _emsDbConn = emsStr;
        _innoDbConn = innoStr;
        _priceinfoDbConn = priceStr;
        _dapper = dapper;
        _sqlTimeout = _config["Timeout:Database"] == null
            ? CommonConstants.SqlServerTimeout
            : int.Parse(_config["Timeout:Database"]);
    }

    public async Task<Response<dynamic>> GetProfileStatus(BaseRequest model)
    {
        try
        {
            var param = new DynamicParameters();
            param.Add("@Type", 1, DbType.Int32, ParameterDirection.Input); // Value 1 trạng thái hồ sơ
            return new Response<dynamic>
            {
                Code = ((int)ErrorCodeDetail.Success).ErrorCodeFormat(),
                Message = ErrorCodeDetail.Success.ToEnumDescription(),
                Data = await _dapper.SelectAsync<dynamic>(_commonDbConn,
                    "TVSI_sGET_STATUS", param, _sqlTimeout)
            };
        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
            return new Response<dynamic>
            {
                Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                Message = ErrorCodeDetail.Exception.ToEnumDescription()
            };
        }
    }

    public async Task<Response<dynamic>> GetAccountStatus(BaseRequest model)
    {
        try
        {
            var param = new DynamicParameters();
            param.Add("@Type", 2, DbType.Int32, ParameterDirection.Input); // Value 2 trạng thái tài khoản
            return new Response<dynamic>
            {
                Code = ((int)ErrorCodeDetail.Success).ErrorCodeFormat(),
                Message = ErrorCodeDetail.Success.ToEnumDescription(),
                Data = await _dapper.SelectAsync<dynamic>(_commonDbConn,
                    "TVSI_sGET_STATUS", param, _sqlTimeout)
            };
        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
            return new Response<dynamic>
            {
                Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                Message = ErrorCodeDetail.Exception.ToEnumDescription()
            };
        }
    }

    public async Task<Response<dynamic>> GetActionStatus(BaseRequest model)
    {
        try
        {
            var param = new DynamicParameters();
            param.Add("@Type", 3, DbType.Int32, ParameterDirection.Input); // Value 3 trạng thái tương tác
            return new Response<dynamic>
            {
                Code = ((int)ErrorCodeDetail.Success).ErrorCodeFormat(),
                Message = ErrorCodeDetail.Success.ToEnumDescription(),
                Data = await _dapper.SelectAsync<dynamic>(_commonDbConn,
                    "TVSI_sGET_STATUS", param, _sqlTimeout)
            };
        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
            return new Response<dynamic>
            {
                Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                Message = ErrorCodeDetail.Exception.ToEnumDescription()
            };
        }
    }

    public async Task<Response<dynamic>> GetChannelOpenAccount(BaseRequest model)
    {
        try
        {
            var param = new DynamicParameters();
            param.Add("@Type", 4, DbType.Int32, ParameterDirection.Input); // Value 4 Kênh MTK
            return new Response<dynamic>
            {
                Code = ((int)ErrorCodeDetail.Success).ErrorCodeFormat(),
                Message = ErrorCodeDetail.Success.ToEnumDescription(),
                Data = await _dapper.SelectAsync<dynamic>(_commonDbConn,
                    "TVSI_sGET_STATUS", param, _sqlTimeout)
            };
        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
            return new Response<dynamic>
            {
                Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                Message = ErrorCodeDetail.Exception.ToEnumDescription()
            };
        }
    }

    public async Task<Response<dynamic>> GetProvinceList(BaseRequest model)
    {
        try
        {
            return new Response<dynamic>
            {
                Code = ((int)ErrorCodeDetail.Success).ErrorCodeFormat(),
                Message = ErrorCodeDetail.Success.ToEnumDescription(),
                Data = await _dapper.SelectAsync<dynamic>(_crmDbConn,
                    "TVSI_sGET_PROVINCE_LIST", null!, _sqlTimeout)
            };
        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
            return new Response<dynamic>
            {
                Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                Message = ErrorCodeDetail.Exception.ToEnumDescription()
            };
        }
    }

    public async Task<Response<dynamic>> GetDistrictList(GetDistrictListRequest model)
    {
        try
        {
            var param = new DynamicParameters();
            param.Add("@ProvinceCode", model.ProvinceCode, DbType.String, ParameterDirection.Input);
            return new Response<dynamic>
            {
                Code = ((int)ErrorCodeDetail.Success).ErrorCodeFormat(),
                Message = ErrorCodeDetail.Success.ToEnumDescription(),
                Data = await _dapper.SelectAsync<dynamic>(_crmDbConn,
                    "TVSI_sGET_DISTRICT_LIST", param, _sqlTimeout)
            };
        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
            return new Response<dynamic>
            {
                Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                Message = ErrorCodeDetail.Exception.ToEnumDescription()
            };
        }
    }

    public async Task<Response<dynamic>> GetWardList(GetWardListRequest model)
    {
        try
        {
            var param = new DynamicParameters();
            param.Add("@DistrictCode", model.DistrictCode, DbType.String, ParameterDirection.Input);
            return new Response<dynamic>
            {
                Code = ((int)ErrorCodeDetail.Success).ErrorCodeFormat(),
                Message = ErrorCodeDetail.Success.ToEnumDescription(),
                Data = await _dapper.SelectAsync<dynamic>(_crmDbConn,
                    "TVSI_sGET_WARD_LIST", param, _sqlTimeout)
            };
        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
            return new Response<dynamic>
            {
                Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                Message = ErrorCodeDetail.Exception.ToEnumDescription()
            };
        }
    }
    

    public async Task<Response<ExtendResponse>> GetExtendInfoList(BaseRequest model)
    {
        try
        {
            var result = new List<ExtendResponse>();
            var data = await _dapper.SelectAsync<ExtendCategoryResponse>(_crmDbConn,
                "TVSI_sGET_EXTEND_INFO_LIST", null, _sqlTimeout);
            var dataNameType = await _dapper.SelectAsync<ExtendCategoryResponse>(_crmDbConn,
                "TVSI_sGET_EXTEND_NAME_INFO_LIST", null, _sqlTimeout);
            
            if (data != null && data.Any() && dataNameType != null && dataNameType.Any())
            {
                foreach (var itemType in dataNameType)
                {
                    var dataCategory = new List<ExtendKeyValueResponse>();
                    foreach (var item in data)
                        if (itemType.CategoryType == item.CategoryType)
                            dataCategory.Add(new ExtendKeyValueResponse()
                                { Value = item.CategoryId, Name = item.CategoryName });
                    result.Add(new ExtendResponse()
                        {
                            ExtendName = itemType.CategoryName,
                            CategoryType = itemType.CategoryType,
                            ExtendValue = dataCategory,
                            CrmBb = itemType.CrmBb,
                            CrmSale = itemType.CrmSale
                        }
                    );
                }
            }


            return new Response<ExtendResponse>
            {
                Code = ((int)ErrorCodeDetail.Success).ErrorCodeFormat(),
                Message = ErrorCodeDetail.Success.ToEnumDescription(),
                Data = result
            };
        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
            return new Response<ExtendResponse>
            {
                Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                Message = ErrorCodeDetail.Exception.ToEnumDescription()
            };
        }
    }

    public async Task<Response<KycInfoResponse>> GetKycInfoList(BaseRequest request)
    {
        try
        {
            var result = new List<KycInfoResponse>();
            var kycLists = await _dapper.SelectAsync<CustomerKycResponse>(_crmDbConn,
                "TVSI_sGET_KYC_LIST", null, _sqlTimeout);
            var kycTypes = await _dapper.SelectAsync<CustomerKycResponse?>(_crmDbConn,
                "TVSI_sGET_KYC_TYPE_LIST", null, _sqlTimeout);

            if (kycTypes != null && kycTypes.Any() && kycLists != null && kycLists.Any())
            {
                foreach (var kycType in kycTypes)
                {
                    var kycdata = new List<KycNameValue>();
                    foreach (var item in kycLists)
                    {
                        if (kycType?.KycType == item.KycType)
                            kycdata.Add(new KycNameValue()
                            {
                                Value = item.KycId,
                                Name = item.KycName
                            });    
                    }
                    
                    result.Add( new KycInfoResponse
                    {
                        KycName = kycType?.KycTypeName,
                        KycValue = kycdata,
                        KycType = kycType?.KycType,
                        TypeSeleted = kycType?.TypeSeleted
                    });
                }   
            }
            return new Response<KycInfoResponse>
            {
                Code = ((int)ErrorCodeDetail.Success).ErrorCodeFormat(),
                Message = ErrorCodeDetail.Success.ToEnumDescription(),
                Data = result
            };
        }
        catch (Exception ex)
        {
            LogException(request.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
            return new Response<KycInfoResponse>
            {
                Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                Message = ErrorCodeDetail.Exception.ToEnumDescription()
            };
        }
    }

    public async Task<Response<dynamic>> GetCustomerTypeList(BaseRequest request)
    {
        try
        {
            var param = new DynamicParameters();
            param.Add("@Type", 5, DbType.Int32, ParameterDirection.Input); // Value 5 loại hình tài khoản
            return new Response<dynamic>
            {
                Code = ((int)ErrorCodeDetail.Success).ErrorCodeFormat(),
                Message = ErrorCodeDetail.Success.ToEnumDescription(),
                Data = await _dapper.SelectAsync<dynamic>(_commonDbConn,
                    "TVSI_sGET_STATUS", param, _sqlTimeout)
            };
        }
        catch (Exception ex)
        {
            LogException(request.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
            return new Response<dynamic>
            {
                Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                Message = ErrorCodeDetail.Exception.ToEnumDescription()
            };
        }
    }

    public async Task<ResponseSingle<dynamic>> CheckCustCodeCustomerInfo(BaseRequest request)
    {
        try
        {
            return new ResponseSingle<dynamic>
            {
                Code = ((int)ErrorCodeDetail.Success).ErrorCodeFormat(),
                Message = ErrorCodeDetail.Success.ToEnumDescription(),
                Data = null
            }; 
        }
        catch (Exception ex)
        {
            LogException(request.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
            return new ResponseSingle<dynamic>
            {
                Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                Message = ErrorCodeDetail.Exception.ToEnumDescription()
            };
        }
    }

    public async Task<Response<dynamic>> GetPlaceOfIssue(BaseRequest request)
    {
        try
        {
            return new Response<dynamic>
            {
                Code = ((int)ErrorCodeDetail.Success).ErrorCodeFormat(),
                Message = ErrorCodeDetail.Success.ToEnumDescription(),
                Data = await _dapper.SelectAsync<dynamic>(_crmDbConn,
                    "TVSI_sGET_PLACE_OF_ISSUE", null!, _sqlTimeout)
            };
        }
        catch (Exception ex)
        {
            LogException(request.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
            return new Response<dynamic>
            {
                Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                Message = ErrorCodeDetail.Exception.ToEnumDescription()
            };
        }
    }

    public async Task<ResponseSingle<KycCustomerInfomationResponse>> GetKycCustomerInfo(GetKycCustomerRequest model)
    {
        try
        {
            var data = new KycCustomerInfomationResponse();
            var param = new DynamicParameters();
            param.Add("@CustCode", model.CustCode, DbType.String, ParameterDirection.Input);
            var dataType = await _dapper.SelectAsync<KycTypeResponse>(_crmDbConn,
                "TVSI_sGET_KYC_TYPE", null!, _sqlTimeout);
            var dataKycCustomer  = await _dapper.SelectAsync<KycInfo>(_crmDbConn,
                "TVSI_sGET_KYC_CUSTOMERKYC", param, _sqlTimeout);
            
            var dataKycJoined = await _dapper.GetAsync<KycJoined>(_crmDbConn,
                "TVSI_sGET_KYC_JOINED", param, _sqlTimeout);
            if (dataKycJoined != null)
                data.KycJoined = dataKycJoined;

            if (dataType != null && dataType.Any())
            {
                var dataList = dataType.ToList();
                var dataCustomer = dataKycCustomer.ToList();
                
                for (int i = 0; i < dataList.Count; i++)
                {
                    var items = new List<KycNameValue>();
                    if (dataKycCustomer != null && dataKycCustomer.Any())
                    {
                        for (int j = 0; j < dataCustomer.Count; j++)
                        {
                            if (dataList[i].KycType == dataCustomer[j].KycType)
                            {
                                items.Add(new KycNameValue
                                {
                                    Name = dataCustomer[j].Name,
                                    Value = dataCustomer[j].Value,
                                });
                            }
                        }
                    }

                    if (items.Count > 0)
                    {
                        data.KycSelecteds.Add(new KycSelected
                        {
                            KycName = dataList[i].KycTypeName,
                            KycType = dataList[i].KycType,
                            KycValue = items,
                            TypeSeleted = dataList[i].TypeSeleted
                        });   
                    }
                }
            }

            
            return new ResponseSingle<KycCustomerInfomationResponse>
            {
                Code = ((int)ErrorCodeDetail.Success).ErrorCodeFormat(),
                Message = ErrorCodeDetail.Success.ToEnumDescription(),
                Data = data
            };
        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
            return new ResponseSingle<KycCustomerInfomationResponse>
            {
                Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                Message = ErrorCodeDetail.Exception.ToEnumDescription()
            };
        }
        
        
    }

    public async Task<Response<dynamic>> GetCustomerStatusList(BaseRequest model)
    {
        try
        {
            var param = new DynamicParameters();
            param.Add("@Type", 7, DbType.Int32, ParameterDirection.Input); // Value 7 trạng thái hoạt động hoặc đóng của danh sách khách hàng
            return new Response<dynamic>
            {
                Code = ((int)ErrorCodeDetail.Success).ErrorCodeFormat(),
                Message = ErrorCodeDetail.Success.ToEnumDescription(),
                Data = await _dapper.SelectAsync<dynamic>(_commonDbConn,
                    "TVSI_sGET_STATUS", param, _sqlTimeout)
            };
        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
            return new Response<dynamic>
            {
                Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                Message = ErrorCodeDetail.Exception.ToEnumDescription()
            };
        }
    }

    public async Task<Response<dynamic>> GetAllRightByFun(GetAllRightByFunctionRequest model)
    {
        try
        {
            var param = new DynamicParameters();
            param.Add("@UserName", model.UserName, DbType.String, ParameterDirection.Input);
            param.Add("@Function", model.Function, DbType.String, ParameterDirection.Input); // quyền view màn hình CustInfo CRM
            return new Response<dynamic>
            {
                Code = ((int)ErrorCodeDetail.Success).ErrorCodeFormat(),
                Message = ErrorCodeDetail.Success.ToEnumDescription(),
                Data = await _dapper.SelectAsync<dynamic>(_crmDbConn,
                    "TVSI_sGET_GET_ALL_RIGHT_BY_FUNCTION", param, _sqlTimeout)
            };
        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
            return new Response<dynamic>
            {
                Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                Message = ErrorCodeDetail.Exception.ToEnumDescription()
            };
        }
    }
    public async Task<Response<dynamic>> GetBranchList(BaseRequest model)
    {
        try
        {
            return new Response<dynamic>
            {
                Code = ((int)ErrorCodeDetail.Success).ErrorCodeFormat(),
                Message = ErrorCodeDetail.Success.ToEnumDescription(),
                Data = await _dapper.SelectAsync<dynamic>(_emsDbConn,
                    "TVSI_sGetBranchList", new DynamicParameters(new {}), _sqlTimeout)
            };
        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
            return new Response<dynamic>
            {
                Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                Message = ErrorCodeDetail.Exception.ToEnumDescription()
            };
        }
    }
}