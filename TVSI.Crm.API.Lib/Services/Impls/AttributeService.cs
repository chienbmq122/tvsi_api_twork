﻿using Dapper;
using Helper.DataAccess.Dapper;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using TVSI.Crm.API.Common;
using TVSI.Crm.API.Lib.Models;
using TVSI.Crm.API.Lib.Models.Response.Attribute;
using TVSI.TWork.API.Common.Enums;
using TVSI.TWork.API.Common.ExtensionMethods;

namespace TVSI.Crm.API.Lib.Services.Impls
{
    public class AttributeService : BaseService<AttributeService>, IAttributeService
    {
        private readonly IDapperHelper _dapper;
        private readonly string _crmDbConn;
        private readonly int _sqlTimeout;

        public AttributeService(IConfiguration config, ILogger<AttributeService> logger, IDapperHelper dapper) : base(config, logger)
        {
            var connStr = _config.GetConnectionString("CRMDB_CONNECTION");
            _crmDbConn = connStr;
            _dapper = dapper;
            _sqlTimeout = _config["Timeout:Database"] == null
                ? CommonConstants.SqlServerTimeout
                : int.Parse(_config["Timeout:Database"]);
        }

        public async Task<Response<AttributeReponse>> GetAttribute(BaseRequest model)
        {
            try
            {
                var param = new DynamicParameters();
                return new Response<AttributeReponse>
                {
                    Code = ((int)ErrorCodeDetail.Success).ErrorCodeFormat(),
                    Message = ErrorCodeDetail.Success.ToEnumDescription(),
                    Data = await _dapper.SelectAsync<AttributeReponse>(_crmDbConn, "BBB25_GET_LIST_ATTRIBUTE", param, _sqlTimeout)
                };
            }
            catch (Exception ex)
            {
                LogException(model.UserName,
                    $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
                return new Response<AttributeReponse> { };
            }
        }

        public async Task<Response<ExtendCategoryReponse>> GetExtendCategory(BaseRequest model)
        {
            try
            {
                var param = new DynamicParameters();
                return new Response<ExtendCategoryReponse>
                {
                    Code = ((int)ErrorCodeDetail.Success).ErrorCodeFormat(),
                    Message = ErrorCodeDetail.Success.ToEnumDescription(),
                    Data = await _dapper.SelectAsync<ExtendCategoryReponse>(_crmDbConn, "BBB26_GET_LIST_CATEGORY_ATTRIBUTE", param, _sqlTimeout)
                };
            }
            catch (Exception ex)
            {
                LogException(model.UserName,
                    $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
                return new Response<ExtendCategoryReponse> { };
            }
        }
    }
}
