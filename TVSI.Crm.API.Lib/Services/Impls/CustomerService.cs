﻿using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Net;
using System.Reflection;
using Dapper;
using Helper.DataAccess.Dapper;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using TVSI.Crm.API.Common;
using TVSI.Crm.API.Common.Enums;
using TVSI.Crm.API.Lib.Models;
using TVSI.Crm.API.Lib.Models.Request.Customer;
using TVSI.Crm.API.Lib.Models.Response.Account;
using TVSI.Crm.API.Lib.Models.Response.Activity;
using TVSI.Crm.API.Lib.Models.Response.Customer;
using TVSI.Crm.API.Lib.Models.Response.Lead;
using TVSI.Crm.API.Lib.Utilities;
using TVSI.TWork.API.Common.ExtensionMethods;

namespace TVSI.Crm.API.Lib.Services.Impls;

public class CustomerService : BaseService<CustomerService>, ICustomerService
{
    private readonly IDapperHelper _dapper;
    private readonly string _crmDbConn;
    private readonly string _commonDbConn;
    private readonly string _innoDbConn;
    private readonly string _emsDbConn;
    private readonly string _signatureDbConn;
    private readonly string _urlHome;
    private readonly string _apiBond;
    private readonly int _sqlTimeout;
    private readonly IBankService _bankService;

    public CustomerService(IConfiguration config, ILogger<CustomerService> logger, IDapperHelper dapper,
        IBankService bankService) : base(config,
        logger)
    {
        var connStr = _config.GetConnectionString("CRMDB_CONNECTION");
        var commonStr = _config.GetConnectionString("COMMONDB_CONNECTION");
        var emsStr = _config.GetConnectionString("EMSDB_CONNECTION");
        var innoStr = _config.GetConnectionString("INNODB_CONNECTION");
        var _signatureDbConn = _config.GetConnectionString("SIGNATUREDB_CONNECTION");
        var urlHome = _config["Tvsi:Url_Home"];
        var apiBond = _config["Tvsi:BOND_API"];

        _crmDbConn = connStr;
        _commonDbConn = commonStr;
        _emsDbConn = emsStr;
        _innoDbConn = innoStr;
        _bankService = bankService;
        _dapper = dapper;
        _urlHome = urlHome;
        _apiBond = apiBond;
        _sqlTimeout = _config["Timeout:Database"] == null
            ? CommonConstants.SqlServerTimeout
            : int.Parse(_config["Timeout:Database"]);
    }
    
    public async Task<ResponseSingle<dynamic>> GetActionStatusDetail(GetActionStatusDetailRequest model)
    {
        try
        {
            var param = new DynamicParameters();
            param.Add("@Id", model.Id, DbType.Int32, ParameterDirection.Input);
            
            return new ResponseSingle<dynamic>
            {
                Code = ((int)ErrorCodeDetail.Success).ErrorCodeFormat(),
                Message = ErrorCodeDetail.Success.ToEnumDescription(),
                Data = await _dapper.SelectAsync<dynamic>(_crmDbConn,
                    "TVSI_sGET_ACTION_STATUS_INFO", param, _sqlTimeout)
            };
        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
            return new ResponseSingle<dynamic>
            {
                Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                Message = ErrorCodeDetail.Exception.ToEnumDescription()
            };
        }
    }

    public async Task<Response<CustomerOnlineListResponse>> GetCustomerOnlineList(CustomerOnlineListRequest model,
        string lang)
    {
        try
        {
            var start = (model.PageIndex - 1) * model.PageSize;
            var param = new DynamicParameters();
            param.Add("@UserName", model.UserName, DbType.String, ParameterDirection.Input);
            param.Add("@CardID", model.CardId, DbType.String, ParameterDirection.Input);
            param.Add("@CustCode", model.CustCode, DbType.String, ParameterDirection.Input);
            param.Add("@FullName", model.FullName, DbType.String, ParameterDirection.Input);
            param.Add("@Phone", model.Phone, DbType.String, ParameterDirection.Input);
            param.Add("@SaleID", model.SaleID, DbType.String, ParameterDirection.Input);
            param.Add("@OpenSource", model.OpenSource, DbType.Int32, ParameterDirection.Input);
            param.Add("@AccountStatus", model.AccountStatus, DbType.String, ParameterDirection.Input);
            param.Add("@ProfileStatus", model.ProfileStatus, DbType.String, ParameterDirection.Input);
            param.Add("@ActionStatus", model.ActionStatus, DbType.String, ParameterDirection.Input);
            param.Add("@start", start, DbType.Int32, ParameterDirection.Input);
            param.Add("@PageSize", model.PageSize, DbType.Int32, ParameterDirection.Input);
            return new Response<CustomerOnlineListResponse>
            {
                Code = ((int)ErrorCodeDetail.Success).ErrorCodeFormat(),
                Message = ErrorCodeDetail.Success.ToEnumDescription(),
                Data = await _dapper.SelectAsync<CustomerOnlineListResponse>(_crmDbConn,
                    "TVSI_sGetCustomerOnlineList", param, _sqlTimeout)
            };
        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
            return new Response<CustomerOnlineListResponse>
            {
                Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                Message = ErrorCodeDetail.Exception.ToEnumDescription()
            };
        }
    }

    public async Task<ResponseSingle<CustomerInfoAllResponse>> GetCustomerOnlineDetailInfo(
        CustomerOnlineDetailInfoRequest model, string lang)
    {
        try
        {
            var param = new DynamicParameters();
            param.Add("@Id", model.Id, DbType.Int32, ParameterDirection.Input);
            param.Add("@UserName", model.UserName, DbType.String, ParameterDirection.Input);
            var data = await _dapper.GetAsync<CustomerInfoAllResponse>(_crmDbConn,
                "TVSI_sGetCustomerDetailInfo", param, _sqlTimeout);

            var paramName = new DynamicParameters();
            if (!string.IsNullOrEmpty(data?.SaleID))
            {
                paramName.Add("@SaleID",data.SaleID,DbType.String,ParameterDirection.Input);
                data.SaleName = await _dapper.GetAsync<string>(_emsDbConn, "TVSI_sGetSaleName", paramName, _sqlTimeout);
            }

            if (data != null)
            {
                //if ((data.OpenSource == 2 || data.OpenSource == 3 || data.OpenSource == 7)  && !string.IsNullOrEmpty(data.Custcode))
                //{
                //    var paramiImg = new DynamicParameters();
                //    paramiImg.Add("@CustCode", data.Custcode, DbType.String, ParameterDirection.Input);
                //    paramiImg.Add("@UserName", model.UserName, DbType.String, ParameterDirection.Input);
                //    var dataImage = await _dapper.GetAsync<ImageDataByte>(_commonDbConn,
                //        "GET_IMAGE_CARDID_CUSTOMER", paramiImg, _sqlTimeout); // lấy data ảnh từ commondb
                //    if (dataImage != null && dataImage.FileDataBack.Length > 0 && dataImage.FileDataFront.Length > 0)
                //    {
                //        data.ImageFront = Convert.ToBase64String(dataImage.FileDataFront);
                //        data.ImageBack = Convert.ToBase64String(dataImage.FileDataBack);
                //    }
                //}
                //if (data.OpenSource == 4)
                //{

                //    //trên test cần đồng bộ
                //    try
                //    {
                //        var urlFront =  _urlHome + "EkycCustomer/" + model.Id + "/" + model.Id + "_Front.jpg";
                //        var urlBack =  _urlHome + "EkycCustomer/" + model.Id + "/" +  model.Id + "_Back.jpg";
                //        var urlFace =  _urlHome + "EkycCustomer/" + model.Id + "/" +  model.Id + "_Face.jpg";

                //        var frontByte = GetImageByte.GetImageByteUrl(urlFront);
                //        var backByte = GetImageByte.GetImageByteUrl(urlBack);
                //        var faceByte = GetImageByte.GetImageByteUrl(urlFace);
                //        if (frontByte != null && frontByte.Length > 0 && backByte != null && backByte.Length > 0 && faceByte != null && faceByte.Length > 0)
                //        {
                //            data.ImageFront = Convert.ToBase64String(frontByte);
                //            data.ImageBack = Convert.ToBase64String(backByte);
                //            data.ImageFace = Convert.ToBase64String(faceByte);
                //        }
                //    }
                //    catch (Exception ex)
                //    {
                //        LogException(model.UserName,
                //            $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
                //    }
                //} 

                //// to be continued
                //if (data.OpenSource == 6 && !string.IsNullOrEmpty(data.CardId))
                //{
                //    try
                //    {
                //        var paramImage = new
                //        {
                //            CardId = data.CardId
                //        };
                //        var responseData = CallApiHelper.GetImageCardId(data.CardId,paramImage,_apiBond);
                //        if (responseData != null 
                //            && !string.IsNullOrEmpty(responseData.RetData.Front_Image) 
                //            && !string.IsNullOrEmpty(responseData.RetData.Back_Image)
                //            && !string.IsNullOrEmpty(responseData.RetData.Face_Image))
                //        {
                //            data.ImageFront = responseData.RetData.Front_Image;
                //            data.ImageBack = responseData.RetData.Back_Image;
                //            data.ImageFace = responseData.RetData.Face_Image;
                //        }
                //    }
                //    catch (Exception ex)
                //    {
                //        LogException(model.UserName,
                //            $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
                //    }
                //}

                // 20220929 CuongLM Add Xu ly du lieu anh tap trung
                // BEGIN
                try
                {
                    var imageDataList = await _dapper.SelectAsync<ImageFileData>(_signatureDbConn, "TVSI_sGET_IMAGE_DATA",
                        new DynamicParameters(new { CustCode = data.Custcode }), _sqlTimeout);
                    if (imageDataList != null)
                    {
                        foreach (var item in imageDataList)
                        {
                            if (item.Type == 1)
                                data.ImageFront = Convert.ToBase64String(item.FileData);

                            if (item.Type == 2)
                                data.ImageBack = Convert.ToBase64String(item.FileData);

                            if (item.Type == 3)
                                data.ImageFace = Convert.ToBase64String(item.FileData);
                        }
                    }
                }
                catch (Exception ex)
                {
                    LogException(model.UserName,
                        $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
                }
                // END
            }
            return new ResponseSingle<CustomerInfoAllResponse>
            {
                Code = ((int)ErrorCodeDetail.Success).ErrorCodeFormat(),
                Message = ErrorCodeDetail.Success.ToEnumDescription(),
                Data = data
            };
        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
            return new ResponseSingle<CustomerInfoAllResponse>
            {
                Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                Message = ErrorCodeDetail.Exception.ToEnumDescription()
            };
        }
    }

    public async Task<Response<dynamic>> GetCustomerList(CustomerListRequest model)
    {
        try
        {
            var start = (model.PageIndex - 1) * model.PageSize;
            var param = new DynamicParameters();
            param.Add("@UserName", model.UserName, DbType.String, ParameterDirection.Input);
            param.Add("@Custcode", model.Custcode, DbType.String, ParameterDirection.Input);
            param.Add("@FullName", model.FullName, DbType.String, ParameterDirection.Input);
            param.Add("@Phone", model.Phone, DbType.String, ParameterDirection.Input);
            param.Add("@Email", model.Email, DbType.String, ParameterDirection.Input);
            param.Add("@CustomerType", model.CustomerType, DbType.Int32, ParameterDirection.Input);
            param.Add("@Status", model.Status, DbType.Int32, ParameterDirection.Input);
            param.Add("@start", start, DbType.Int32, ParameterDirection.Input);
            param.Add("@PageSize", model.PageSize, DbType.Int32, ParameterDirection.Input);
            return new Response<dynamic>
            {
                Code = ((int)ErrorCodeDetail.Success).ErrorCodeFormat(),
                Message = ErrorCodeDetail.Success.ToEnumDescription(),
                Data = await _dapper.SelectAsync<dynamic>(_crmDbConn,
                    "TVSI_sGET_CUSTOMER_LIST", param, _sqlTimeout)
            };
        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
            return new Response<dynamic>
            {
                Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                Message = ErrorCodeDetail.Exception.ToEnumDescription()
            };
        }
    }

    public async Task<ResponseSingle<dynamic>> GetCustomerDetailInfo(CustomerDetailRequest model)
    {
        try
        {
            var param = new DynamicParameters();
            param.Add("@UserName", model.UserName, DbType.String, ParameterDirection.Input);
            param.Add("@Custcode", model.Custcode, DbType.String, ParameterDirection.Input);
            var data = await _dapper.GetAsync<CustomerDetailResponse>(_crmDbConn,
                "TVSI_sGET_CUSTOMER_DETAIL_INFO", param, _sqlTimeout); // data crm
            if (data != null)
            {
                
                var paramBank = new DynamicParameters();
                paramBank.Add("@Custcode", model.Custcode, DbType.String, ParameterDirection.Input);
                var dataBank = await _dapper.SelectAsync<CustomerBankInfo?>(_innoDbConn,
                    "TW_GET_CUSTOMER_BANK_INFO", paramBank, _sqlTimeout); // Lấy thông tin bank inno
                if (dataBank != null && dataBank.Any())
                    data.CustomerBankInfo = dataBank;

                var paramInternal = new DynamicParameters();
                paramInternal.Add("@Custcode", model.Custcode, DbType.String, ParameterDirection.Input);
                var dataInternal = await _dapper.SelectAsync<InternalAccountInfo?>(_innoDbConn,
                    "TW_GET_CUSTOMER_INTERNALBANK_INFO", paramInternal,
                    _sqlTimeout); // Lấy thông tin Internal tk đối ứng inno
                if (dataInternal != null && dataInternal.Any())
                    data.CustomerInternalInfo = dataInternal;

                var paramExtend = new DynamicParameters();
                paramExtend.Add("@Custcode", model.Custcode, DbType.String, ParameterDirection.Input);
                var dataExtend = await _dapper.SelectAsync<CustomerExtendInfo>(_crmDbConn,
                    "TVSI_sGetExtendAccountInfo", paramExtend, _sqlTimeout); // lấy thông tin mở rộng
                if (dataExtend != null && dataExtend.Any())
                {
                    foreach (var item in dataExtend)
                    {
                        data.CustomerExtend.Add(new CustomerExtend()
                        {
                            CategoryType = item.CategoryType,
                            Value = item.CategoryId
                        });
                    }
                }


                var paramShareHolder = new DynamicParameters();
                paramShareHolder.Add("@Custcode", model.Custcode, DbType.String, ParameterDirection.Input);
                var dataShareHolder = await _dapper.SelectAsync<ShareHolderInfoResponsive>(_crmDbConn,
                    "TVSI_sGET_SHAREHOLDER_INFO_CUSTOMER", paramShareHolder, _sqlTimeout);
                if (dataShareHolder?.Count() > 0)
                {
                    data.IsShareHolder = true;
                    List<ShareHolderInfoResponsive> lstShareHolder = new List<ShareHolderInfoResponsive>();
                    foreach (var item in dataShareHolder)
                    {
                        lstShareHolder.Add(item);
                    }

                    data.ShareHolderInfo = lstShareHolder;
                }
                else
                {
                    data.IsShareHolder = false;
                }
            }

            return new ResponseSingle<dynamic>
            {
                Code = ((int)ErrorCodeDetail.Success).ErrorCodeFormat(),
                Message = ErrorCodeDetail.Success.ToEnumDescription(),
                Data = data
            };
        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
            return new ResponseSingle<dynamic>
            {
                Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                Message = ErrorCodeDetail.Exception.ToEnumDescription()
            };
        }
    }

    public async Task<ResponseSingle<bool>> CancelCustomerInfo(CancelCustomerInfoRequest model)
    {
        try
        {
            var param = new DynamicParameters();
            param.Add("@Id", model.Id, DbType.String, ParameterDirection.Input);
            param.Add("@Result", null, DbType.Int32, ParameterDirection.Output);
            await _dapper.ExecuteAsync(_crmDbConn, "TVSI_sCANCEL_CUSTOMER_INFO", param,
                _sqlTimeout);
            var result = param.Get<int>("@Result");
            return new ResponseSingle<bool>
            {
                Code = (result is (int)ErrorCodeDetail.Success
                    ? (int)ErrorCodeDetail.Success
                    : (int)ErrorCodeDetail.Failed).ErrorCodeFormat(),
                Message = result is (int)ErrorCodeDetail.Success
                    ? ErrorCodeDetail.Success.ToEnumDescription()
                    : ErrorCodeDetail.Failed.ToEnumDescription(),
                Data = result is (int)ErrorCodeDetail.Success
            };
        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
            return new ResponseSingle<bool>
            {
                Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                Message = ErrorCodeDetail.Exception.ToEnumDescription()
            };
        }
    }

    public async Task<ResponseSingle<bool>> UpdateExtendInfo(UpdateCustomerExtendInfoRequest model)
    {
        try
        {
            var result = -1;
            var paramGetExtend = new DynamicParameters();
            paramGetExtend.Add("@Custcode", model.Custcode, DbType.String, ParameterDirection.Input);
            var extedData = await _dapper.SelectAsync<string>(_crmDbConn, "TVSI_sGET_INFO_EXTEND", paramGetExtend,
                _sqlTimeout);
            if (extedData != null && extedData.Any())
            {
                // DELETE thông tin mở rộng.
                var paramDelExtend = new DynamicParameters();
                paramDelExtend.Add("@Custcode", model.Custcode, DbType.String, ParameterDirection.Input);
                paramDelExtend.Add("@Result", null, DbType.Int32, ParameterDirection.Output);
                await _dapper.ExecuteAsync(_crmDbConn, "TVSI_sDELETE_INFO_EXTEND", paramDelExtend, _sqlTimeout);
                result = paramDelExtend.Get<int>("@Result");
                if (result == 0)
                {
                    //INSERT THÔNG TIN MỞ RỘNG MỚI
                    if (model.ExtendCust?.Count() > 0)
                    {
                        foreach (var item in model.ExtendCust)
                        {
                            var paramInsetExtend = new DynamicParameters();
                            paramInsetExtend.Add("@Custcode", model.Custcode, DbType.String, ParameterDirection.Input);
                            paramInsetExtend.Add("@CategoryId", item.Value, DbType.String, ParameterDirection.Input);
                            paramInsetExtend.Add("@CreateBy", model.UserName, DbType.String,
                                ParameterDirection.Input);
                            paramInsetExtend.Add("@Result", null, DbType.Int32, ParameterDirection.Output);
                            await _dapper.ExecuteAsync(_crmDbConn, "TVSI_sINSERT_INFO_EXTEND", paramInsetExtend,
                                _sqlTimeout);
                            result = paramInsetExtend.Get<int>("@Result");
                            if (result == -1)
                                LogInformation(model.UserName,
                                    $"INSERT thông tin mở rộng thất bại custcode {model.Custcode},CategoryId {item.Value} , ");
                        }
                    }
                }
                if (result == -1)
                    LogInformation(model.UserName, $"Xóa thông tin mở rộng thất bại custcode {model.Custcode}");
            }else{
                if (model.ExtendCust?.Count() > 0)
                {
                    foreach (var item in model.ExtendCust)
                    {
                        var paramInsetExtend = new DynamicParameters();
                        paramInsetExtend.Add("@Custcode", model.Custcode, DbType.String, ParameterDirection.Input);
                        paramInsetExtend.Add("@CategoryId", item.Value, DbType.String, ParameterDirection.Input);
                        paramInsetExtend.Add("@CreateBy", model.UserName, DbType.String,
                            ParameterDirection.Input);
                        paramInsetExtend.Add("@Result", null, DbType.Int32, ParameterDirection.Output);
                        await _dapper.ExecuteAsync(_crmDbConn, "TVSI_sINSERT_INFO_EXTEND", paramInsetExtend,
                            _sqlTimeout);
                        result = paramInsetExtend.Get<int>("@Result");
                        if (result == -1)
                            LogInformation(model.UserName,
                                $"INSERT thông tin mở rộng thất bại custcode {model.Custcode},CategoryId {item.Value} , ");
                    }
                }
            }
            
            

            //THông tin cổ đông 
            if (model.IsShareHolder)
            {
                foreach (var item in model.ShareHolderInfo!)
                {
                    var paramShareHolderInsert = new DynamicParameters();
                    paramShareHolderInsert.Add("@CustCode", model.Custcode, DbType.String, ParameterDirection.Input);
                    paramShareHolderInsert.Add("@CustName", model.FullName, DbType.String, ParameterDirection.Input);
                    paramShareHolderInsert.Add("@Address", model.Address, DbType.String, ParameterDirection.Input);
                    paramShareHolderInsert.Add("@ShareType1", item.ShareType_1, DbType.Boolean,
                        ParameterDirection.Input);
                    paramShareHolderInsert.Add("@ShareType2", item.ShareType_2, DbType.Boolean,
                        ParameterDirection.Input);
                    paramShareHolderInsert.Add("@ShareType3", item.ShareType_3, DbType.Boolean,
                        ParameterDirection.Input);
                    paramShareHolderInsert.Add("@ShareType4", item.ShareType_4, DbType.Boolean,
                        ParameterDirection.Input);
                    paramShareHolderInsert.Add("@StockCode", item.StockCode, DbType.String, ParameterDirection.Input);
                    paramShareHolderInsert.Add("@CreatedBy", model.UserName, DbType.String, ParameterDirection.Input);
                    paramShareHolderInsert.Add("@Result", dbType: DbType.Int32, direction: ParameterDirection.Output);
                    await _dapper.ExecuteAsync(_crmDbConn, "TVSI_sInsert_shareholder_customer", paramShareHolderInsert,
                        _sqlTimeout);
                    result = paramShareHolderInsert.Get<int>("@Result");
                    /*await _dapper.ExecuteAsync(_crmDbConn,
                        "TVSI_sCREATE_SHAREHOLDER", paramShareHolderInsert, _sqlTimeout);*/
                    if (result == -1)
                        LogInformation(model.UserName,
                            $"Insert thông tin dữ liệu thông tin cổ đông thất bại : CustomerCode {model.Custcode}, StockCode {item.StockCode}");
                }
            }


            return new ResponseSingle<bool>
            {
                Code = (result is (int)ErrorCodeDetail.Success
                    ? (int)ErrorCodeDetail.Success
                    : (int)ErrorCodeDetail.Failed).ErrorCodeFormat(),
                Message = result is (int)ErrorCodeDetail.Success
                    ? ErrorCodeDetail.Success.ToEnumDescription()
                    : ErrorCodeDetail.Failed.ToEnumDescription(),
                Data = result is (int)ErrorCodeDetail.Success
            };
        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
            return new ResponseSingle<bool>
            {
                Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                Message = ErrorCodeDetail.Exception.ToEnumDescription()
            };
        }
    }

    public async Task<ResponseSingle<bool>> UpdateCustomerKycInfo(UpdateKycCustomerInfoRequest model)
    {
        try
        {
            var result = -1;
            
            var paramDel = new DynamicParameters();
            paramDel.Add("@Custcode", model.Custcode, DbType.String, ParameterDirection.Input);
            paramDel.Add("@Result", null, DbType.Int32, ParameterDirection.Output);
            await _dapper.ExecuteAsync(_crmDbConn, "TVSI_sDELETE_CUSTOMER_KYC_INFO", paramDel,
                _sqlTimeout);
            var resultDel = paramDel.Get<int>("@Result");
            if (resultDel == 0)
            {
                if (model.KycId != null && model.KycId.Any())
                {
                    foreach (var item in model.KycId)
                    {
                        var param = new DynamicParameters();
                        param.Add("@Custcode", model.Custcode, DbType.String, ParameterDirection.Input);
                        param.Add("@Id", item, DbType.Int32, ParameterDirection.Input);
                        param.Add("@UserName", model.UserName, DbType.String, ParameterDirection.Input);
                        param.Add("@Result", null, DbType.Int32, ParameterDirection.Output);
                        await _dapper.ExecuteAsync(_crmDbConn, "TVSI_sINSERT_CUSTOMER_KYC_INFO", param,
                            _sqlTimeout);
                        result = param.Get<int>("@Result");
                    }
                }

                if (result == 0)
                {
                    var param = new DynamicParameters();
                    param.Add("@Custcode", model.Custcode, DbType.String, ParameterDirection.Input);
                    param.Add("@FinancialTaget", model.FinancialTaget, DbType.String, ParameterDirection.Input);
                    param.Add("@OtherNote", model.OtherNote, DbType.String, ParameterDirection.Input);
                    param.Add("@ProductIntro", model.ProductIntro, DbType.String, ParameterDirection.Input);
                    param.Add("@UserName", model.UserName, DbType.String, ParameterDirection.Input);
                    param.Add("@Result", null, DbType.Int32, ParameterDirection.Output);
                    await _dapper.ExecuteAsync(_crmDbConn, "TVSI_sINSERT_CUSTOMER_JOINED_KYC_INFO", param,
                        _sqlTimeout);
                    result = param.Get<int>("@Result");
                }
   
            }

            return new ResponseSingle<bool>
            {
                Code = (result is (int)ErrorCodeDetail.Success
                    ? (int)ErrorCodeDetail.Success
                    : (int)ErrorCodeDetail.Failed).ErrorCodeFormat(),
                Message = result is (int)ErrorCodeDetail.Success
                    ? ErrorCodeDetail.Success.ToEnumDescription()
                    : ErrorCodeDetail.Failed.ToEnumDescription(),
                Data = result is (int)ErrorCodeDetail.Success
            };
        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
            return new ResponseSingle<bool>
            {
                Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                Message = ErrorCodeDetail.Exception.ToEnumDescription()
            };
        }
    }

    public async Task<ResponseSingle<bool>> UpdateActionStatus(UpdateCustomerActionStatusRequest model)
    {
        try
        {
            var param = new DynamicParameters();
            param.Add("@Id", model.Id, DbType.Int32, ParameterDirection.Input);
            param.Add("@ActionStatus", model.ActionStatus, DbType.String, ParameterDirection.Input);
            param.Add("@UserName", model.UserName, DbType.String, ParameterDirection.Input);
            param.Add("@SaleAssign", model.SaleAssign, DbType.String, ParameterDirection.Input);
            param.Add("@Description", model.Description, DbType.String, ParameterDirection.Input);
            param.Add("@Result", null, DbType.Int32, ParameterDirection.Output);
            await _dapper.ExecuteAsync(_crmDbConn, "TVSI_sUPDATE_ACTION_CUSTOMER", param,
                _sqlTimeout);
            var result = param.Get<int>("@Result");
            return new ResponseSingle<bool>
            {
                Code = (result is (int)ErrorCodeDetail.Success
                    ? (int)ErrorCodeDetail.Success
                    : (int)ErrorCodeDetail.Failed).ErrorCodeFormat(),
                Message = result is (int)ErrorCodeDetail.Success
                    ? ErrorCodeDetail.Success.ToEnumDescription()
                    : ErrorCodeDetail.Failed.ToEnumDescription(),
                Data = result is (int)ErrorCodeDetail.Success
            };
        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
            return new ResponseSingle<bool>
            {
                Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                Message = ErrorCodeDetail.Exception.ToEnumDescription()
            };
        }
    }

    public async Task<ResponseSingle<bool>> ConvertCustomerToAccount(ConvertCustomerToAccountRequest model)
    {
        try
        {
            var description = "HĐ thường";
            var addMargin = model.AccountMargin ? "1" : "0";
            if (addMargin.Contains("1"))
                description += ", Margin";

            var branchNo = CommonConstants.Branchiddefault;
            var branchName = CommonConstants.Branchnamedefault;
            var branchId = "00"; // lấy data từ API login
            var paramBranch = new DynamicParameters();
            paramBranch.Add("@SaleID", model.SaleID, DbType.String, ParameterDirection.Input);
            var branchInfo =
                await _dapper.GetAsync<BranchInfoResponse>(_emsDbConn, "TVSI_sGetBranchInfo", paramBranch, _sqlTimeout);
            if (branchInfo != null)
            {
                if (!string.IsNullOrEmpty(branchInfo.BranchName))
                    branchName = branchInfo.BranchName;
                if (!string.IsNullOrEmpty(branchInfo.BranchNo))
                    branchNo = branchInfo.BranchNo;
            }

            var serviceRegister = CommonConstants.BankNotRegister;
            var transferType = "";

            var methodReceive = "";


            var bank01 = CommonConstants.BankNotRegister;
            var bank02 = CommonConstants.BankNotRegister;
            var bank03 = CommonConstants.BankNotRegister;
            var bank04 = CommonConstants.BankNotRegister;
            for (int i = 0; i < model.BankInfo?.Count; i++)
            {
                if (i <= 4)
                {
                    if (i == 0)
                        if (!string.IsNullOrEmpty(model.BankInfo?[i].BankNo))
                            bank01 = 0;
                    if (i == 1)
                        if (!string.IsNullOrEmpty(model.BankInfo?[i].BankNo))
                            bank02 = 0;
                    if (i == 2)
                        if (!string.IsNullOrEmpty(model.BankInfo?[i].BankNo))
                            bank03 = 0;
                    if (i == 3)
                        if (!string.IsNullOrEmpty(model.BankInfo?[i].BankNo))
                            bank04 = 0;
                }
            }

            if (bank01 == 0 || bank02 == 0 || bank03 == 0 || bank04 == 0)
            {
                serviceRegister = 0;
                transferType += 1;
            }

            var profileStatus = ProfileStatusConst.Process;
            var accountStatus = "";
            if (model.ContractStatus == 2 ) //  Nợ hồ sơ
            {
                profileStatus = ProfileStatusConst.ProfileDebt; // trạng thái hồ 101 Nợ hồ sơ
                if (branchNo! == "00" || branchNo! == "00")
                    accountStatus = AccountStatusConst.BDSaleApprove; // trạng thái tài khoản GDMG duyệt
                else
                    accountStatus = AccountStatusConst.WaitingBuMen; // trạng thái tk Chờ BU MAN duyệt	
            }
            else
                accountStatus = AccountStatusConst.Process; // trạng thái tk đang xử lý


            var birthDay = (!string.IsNullOrEmpty(model.BirthDay)) ?
                DateTime.ParseExact(model.BirthDay!, CommonConstants.DateFormat, CultureInfo.CurrentCulture) : (DateTime?)null;
            var issueDate = (!string.IsNullOrEmpty(model.IssueDate)) ?
                DateTime.ParseExact(model.IssueDate!, CommonConstants.DateFormat, CultureInfo.CurrentCulture) : (DateTime?)null;
            var manIssueDate = (!string.IsNullOrEmpty(model.ManIssueDate)) ?
                DateTime.ParseExact(model.ManIssueDate!, CommonConstants.DateFormat, CultureInfo.CurrentCulture) : (DateTime?)null;
            var manDateAnPower = (!string.IsNullOrEmpty(model.ManDateAnPower)) ?
                DateTime.ParseExact(model.ManDateAnPower!, CommonConstants.DateFormat, CultureInfo.CurrentCulture) : (DateTime?)null;
            
            var ChannelStatusInfo = CommonConstants.BankNotRegister;

            var param = new DynamicParameters();
            param.Add("@UserName", model.UserName, DbType.String, ParameterDirection.Input); // UserName
            param.Add("@Custcode", model.Custcode, DbType.String, ParameterDirection.Input); // so tk
            param.Add("@AccountNo", "044C" + model.Custcode, DbType.String,
                ParameterDirection.Input); // số tài khoản 044cxxxxxx
            param.Add("@ProfileType", (int)CommonDetail.ProfileType.ProfileOpen, DbType.Int32,
                ParameterDirection.Input); // loại hồ sơ
            param.Add("@AccountType", (int)CommonDetail.CustTypeConst.INDIVIDUAL_DOMESTIC_INVEST, DbType.Int32,
                ParameterDirection.Input); // loại tài khoản
            param.Add("@FullName", model.FullName!.ToLower(), DbType.String, ParameterDirection.Input); // họ và tên
            param.Add("@BirthDay", birthDay, DbType.DateTime, ParameterDirection.Input); // ngày sinh
            param.Add("@Gender", model.Gender, DbType.Int32, ParameterDirection.Input); // giới tính
            param.Add("@NationalityName", Nationality.NationalityName, DbType.String,
                ParameterDirection.Input); // quốc tịch
            param.Add("@NationalityCode", Nationality.NationalityCode, DbType.String,
                ParameterDirection.Input); // quốc tịch
            param.Add("@CardId", model.CardId, DbType.String, ParameterDirection.Input); // số cmt/cccd
            param.Add("@IssueDate", issueDate, DbType.DateTime, ParameterDirection.Input); // ngày cấp cmt/cccd
            param.Add("@IssuePlace", model.IssuePlace, DbType.String, ParameterDirection.Input); // nơi cấp cmt/cccd
            param.Add("@Address", model.Address, DbType.String, ParameterDirection.Input); // địa chỉ liên hệ
            param.Add("@ContractAdd", addMargin, DbType.String, ParameterDirection.Input); // HĐ bổ sung
            param.Add("@ContractMain", true, DbType.String, ParameterDirection.Input); // HĐ Chính
            param.Add("@TradingResultMethod", model.TradingResultMethod, DbType.String,
                ParameterDirection.Input); // Phương thức nhận KQ giao dịch
            param.Add("@TransferType", transferType, DbType.String,
                ParameterDirection.Input); // Dịch vụ chuyển tiền
            param.Add("@Phone_01", model.Phone01, DbType.String, ParameterDirection.Input); // SĐT 01 
            param.Add("@Phone_02", model.Phone02, DbType.String, ParameterDirection.Input); // SĐT 02 
            param.Add("@Email", model.Email, DbType.String, ParameterDirection.Input); // Email
            for (int i = 0; i < model.BankInfo?.Count; i++)
            {
                if (i < 4)
                {
                    param.Add("@BankAccountName_0" + (i + 1), model.BankInfo?[i].BankAccountName, DbType.String,
                        ParameterDirection.Input); // tên chủ tk ngân hàng
                    param.Add("@BankAccountNo_0" + (i + 1), model.BankInfo?[i].BankAccountNo, DbType.String,
                        ParameterDirection.Input); // số tk ngân hàng

                    if (!string.IsNullOrEmpty(model.BankInfo?[i].BankNo))
                        param.Add("@BankName_0" + (i + 1), // tên NH
                            _bankService.GetBankName(model.BankInfo?[i].BankNo).Result.Data ?? "", DbType.String,
                            ParameterDirection.Input); // tên ngân hàng
                    if (!string.IsNullOrEmpty(model.BankInfo?[i].BankNo) &&
                        !string.IsNullOrEmpty(model.BankInfo?[i].SubBranchNo))
                        param.Add("@SubBranchName_0" + (i + 1), // tên nhánh NH
                            _bankService.GetSubBranchName(model.BankInfo?[i].BankNo, model.BankInfo?[i].SubBranchNo)
                                .Result.Data ?? "", DbType.String, ParameterDirection.Input); // tên chi nhánh

                    param.Add("@SubBranchNo_0" + (i + 1), model.BankInfo?[i].SubBranchNo, DbType.String,
                        ParameterDirection.Input); // mã chi nhánh
                    param.Add("@BankNo_0" + (i + 1), model.BankInfo?[i].BankNo, DbType.String,
                        ParameterDirection.Input); // mã NH
                }
            }

            param.Add("@AccountStatus", accountStatus, DbType.String, ParameterDirection.Input); // trang thai tk
            param.Add("@ProfileStatus", profileStatus, DbType.String, ParameterDirection.Input); // trang thai ho so
            param.Add("@ServiceStatus", serviceRegister, DbType.Int32, ParameterDirection.Input); // trạng thái dịch vụ

            param.Add("@Bank01Status", bank01, DbType.Int32, ParameterDirection.Input); // trạng thái ngân hàng 1
            param.Add("@Bank02Status", bank02, DbType.Int32, ParameterDirection.Input); // trạng thái ngân hàng 2
            param.Add("@Bank03Status", bank03, DbType.Int32, ParameterDirection.Input); // trạng thái ngân hàng 3
            param.Add("@Bank04Status", bank04, DbType.Int32, ParameterDirection.Input); // trạng thái ngân hàng 4

            param.Add("@ChannelStatusInfo", ChannelStatusInfo, DbType.Int32,
                ParameterDirection.Input); // trạng thái kênh thông tin

            param.Add("@Description", description, DbType.String, ParameterDirection.Input); // Mô tả
            param.Add("@SaleID", model.SaleID, DbType.String, ParameterDirection.Input); // SaleID
            param.Add("@BranchID", branchNo, DbType.String, ParameterDirection.Input); // mã chi nhánh
            param.Add("@BranchName", branchName, DbType.String, ParameterDirection.Input); // tên chi nhánh
            param.Add("@DataSource", 2, DbType.String, ParameterDirection.Input); // Nguồn dữ liệu

            param.Add("@InternalStatus01", -1, DbType.Int32, ParameterDirection.Input); // trạng thái DV đối ứng 01 
            param.Add("@InternalStatus02", -1, DbType.Int32, ParameterDirection.Input); // trạng thái DV đối ứng 02

            // Thông tin đại diện tvsi
            param.Add("@ManTvsi", model.ManTvsi, DbType.String, ParameterDirection.Input); // đại diện tvsi
            param.Add("@ManPosition", model.ManPosition, DbType.String, ParameterDirection.Input); // vị trí
            param.Add("@ManAnPower", model.ManAnPower, DbType.String, ParameterDirection.Input); // giấy ủy quền
            param.Add("@ManDateAnPower", manDateAnPower, DbType.DateTime,
                ParameterDirection.Input); // ngày cấp giấy ủy quyền
            param.Add("@ManCardId", model.ManCardId, DbType.String, ParameterDirection.Input); // số cmt/cccd
            param.Add("@ManIssueDate", manIssueDate, DbType.DateTime,
                ParameterDirection.Input); // ngày cấp cmt/cccd
            param.Add("@ManIssuePlace", model.ManIssuePlace, DbType.String,
                ParameterDirection.Input); // nơi cấp cmt /cccd

            param.Add("@InfoUS01", model.Info01 ? 1 : 0, DbType.Int32, ParameterDirection.Input); // thông tin us
            param.Add("@InfoUS02", model.Info02 ? 1 : 0, DbType.Int32, ParameterDirection.Input); // thông tin us
            param.Add("@InfoUS03", model.Info03 ? 1 : 0, DbType.Int32, ParameterDirection.Input); // thông tin us
            param.Add("@InfoUS04", model.Info04 ? 1 : 0, DbType.Int32, ParameterDirection.Input); // thông tin us
            param.Add("@InfoUS05", model.Info05 ? 1 : 0, DbType.Int32, ParameterDirection.Input); // thông tin us
            param.Add("@InfoUS06", model.Info06 ? 1 : 0, DbType.Int32, ParameterDirection.Input); // thông tin us
            param.Add("@InfoUS07", model.Info07 ? 1 : 0, DbType.Int32, ParameterDirection.Input); // thông tin us
            param.Add("@Id", null, DbType.Int32, ParameterDirection.Output);
            await _dapper.ExecuteAsync(_commonDbConn, "TVSI_sCONVERT_CUSTOMER_TO_ACCOUNT", param,
                _sqlTimeout);
            var id = param.Get<int>("@Id");

            if (model.ContractStatus == 2 && model.AccountMargin && id > 0)
            {
                var paramDebtInsert = new DynamicParameters();
                paramDebtInsert.Add("@Id", id, DbType.Int32, ParameterDirection.Input); // Id
                paramDebtInsert.Add("@CreditLimit", model.CreditLimit, DbType.Decimal,
                    ParameterDirection.Input); // Hạn mức dư nợ
                paramDebtInsert.Add("@DebtTerm", model.DebtTerm, DbType.Int32,
                    ParameterDirection.Input); // Thời gian ghi nợ
                paramDebtInsert.Add("@DepositRate", model.DepositRate, DbType.Double,
                    ParameterDirection.Input); // Ty lệ ký quỹ
                paramDebtInsert.Add("@Result", dbType: DbType.Int32,
                    direction: ParameterDirection.Output); // OUTPUT
                await _dapper.ExecuteAsync(_commonDbConn, "TVSI_sINSERT_CONTRACT_FS_INFO", paramDebtInsert,
                    _sqlTimeout); // Insert du liệu ghi nợ margin 
                var debtStatus = paramDebtInsert.Get<int>("@Result");
                if (debtStatus == -1)
                    LogInformation(model.UserName,
                        $"Insert thông tin dữ liệu ghi nợ lỗi id {id},CreditLimit{model.CreditLimit},DebtTerm{model.DebtTerm},DepositRate{model.DepositRate}");

                if (model.ImageFront != null && model.ImageBack != null)
                {
                    byte[] dataCardIdImage = Convert.FromBase64String(model.ImageFront!);
                    byte[] dataCardIdBackImage = Convert.FromBase64String(model.ImageBack!);
                    var cardIdFileName = id + ".jpg";
                    var cardIdBackFileName = id + "_ms.jpg";

                    var paramImageInsert = new DynamicParameters();
                    paramImageInsert.Add("@OpenId", id, DbType.Int64, ParameterDirection.Input);
                    paramImageInsert.Add("@CustomerCode", model.Custcode, DbType.String, ParameterDirection.Input);
                    paramImageInsert.Add("@FileName", cardIdFileName, DbType.String, ParameterDirection.Input);
                    paramImageInsert.Add("@FileData", dataCardIdImage, DbType.Binary, ParameterDirection.Input);
                    paramImageInsert.Add("@FileBackName", cardIdBackFileName, DbType.String, ParameterDirection.Input);
                    paramImageInsert.Add("@FileBackData", dataCardIdBackImage, DbType.Binary, ParameterDirection.Input);
                    paramImageInsert.Add("@CreateBy", model.UserName, DbType.String, ParameterDirection.Input);
                    paramImageInsert.Add("@Result", dbType: DbType.Int32, direction: ParameterDirection.Output);

                    await _dapper.ExecuteAsync(_commonDbConn, "TVSI_sOpen_Account_CardId_Image", paramImageInsert,
                        _sqlTimeout);
                    var iInsertImage = paramImageInsert.Get<int>("@Result");
                    if (iInsertImage == -1)
                        LogInformation(model.UserName,
                            $"Insert thông tin ảnh cmnd lỗi id {id}, CustomerCode {model.Custcode}");
                    else
                    {
                        AddCentralizeImageDB(id, 7, model.Custcode, model.UserName, cardIdFileName,
                            model.ImageFront, cardIdBackFileName, model.ImageBack);
                    }
                }

            }

            var result = id > 0 ? 0 : -1;
            return new ResponseSingle<bool>
            {
                Code = (result is (int)ErrorCodeDetail.Success
                    ? (int)ErrorCodeDetail.Success
                    : (int)ErrorCodeDetail.Failed).ErrorCodeFormat(),
                Message = result is (int)ErrorCodeDetail.Success
                    ? ErrorCodeDetail.Success.ToEnumDescription()
                    : ErrorCodeDetail.Failed.ToEnumDescription(),
                Data = result is (int)ErrorCodeDetail.Success
            };
        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
            return new ResponseSingle<bool>
            {
                Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                Message = ErrorCodeDetail.Exception.ToEnumDescription()
            };
        }
    }

    public async Task<ResponseSingle<bool>> CheckValidateCustcode(CheckValidateCustCodeRequest model)
    {
        try
        {
            var param = new DynamicParameters();
            param.Add("@CustCode", model.Custcode, DbType.String, ParameterDirection.Input); // Số tk khách hàng
            param.Add("@CustType", model.CustomerType, DbType.Int32, ParameterDirection.Input); // Loại hình KH
            param.Add("@Result", null, DbType.Int32, ParameterDirection.Output);
            await _dapper.GetAsync<string>(_commonDbConn, "TVSI_sCHECK_CUSTCODE_CUSTOMER", param,
                _sqlTimeout);
            var result = param.Get<int>("@Result");
            return new ResponseSingle<bool>
            {
                Code = (result is (int)ErrorCodeDetail.Success
                    ? (int)ErrorCodeDetail.Success
                    : (int)ErrorCodeDetail.Failed).ErrorCodeFormat(),
                Message = result is (int)ErrorCodeDetail.Success
                    ? ErrorCodeDetail.Success.ToEnumDescription()
                    : ErrorCodeDetail.Failed.ToEnumDescription(),
                Data = result is (int)ErrorCodeDetail.Success
            };
        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
            return new ResponseSingle<bool>
            {
                Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                Message = ErrorCodeDetail.Exception.ToEnumDescription()
            };
        }
    }

    public async Task<ResponseSingle<ListCustcodeReponse>> ListCustcode(GetCustcodeRequest model)
    {
        try
        {
            var start = (model.PageIndex - 1) * model.PageSize;

            var param = new DynamicParameters();
            param.Add("@userName", model.UserName, DbType.String, ParameterDirection.Input);
            param.Add("@Start", start, DbType.Int32, ParameterDirection.Input);
            param.Add("@PageSize", model.PageSize, DbType.Int32, ParameterDirection.Input);
            param.Add("@CustCode", model.CustCode, DbType.String, ParameterDirection.Input);
            var data = await _dapper.GetAsync<ListCustcodeSql>(_crmDbConn, "BBB38_GET_All_CUST", param, _sqlTimeout);

            if (data != null)
            {
                var dataResponse = new ListCustcodeReponse();

                dataResponse.TotalItem = data.TotalItem;

                if (!string.IsNullOrEmpty(data.Items))
                    dataResponse.Items = JsonConvert.DeserializeObject<List<ListCustcode>>(data.Items);

                return new ResponseSingle<ListCustcodeReponse>
                {
                    Code = ((int)ErrorCodeDetail.Success).ErrorCodeFormat(),
                    Message = ErrorCodeDetail.Success.ToEnumDescription(),
                    Data = dataResponse
                };
            }

            return new ResponseSingle<ListCustcodeReponse>
            {
                Code = ((int)ErrorCodeDetail.Success).ErrorCodeFormat(),
                Message = ErrorCodeDetail.Success.ToEnumDescription(),
                Data = null
            };
        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
            return new ResponseSingle<ListCustcodeReponse> {
                Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                Message = ErrorCodeDetail.Exception.ToEnumDescription()
            };
        }
    }

    public async Task<ResponseSingle<LeadActivityReponse>> GetCustomerActivity(ListCustomerActivityRequest model)
    {
        try
        {
            var start = (model.PageIndex - 1) * model.PageSize;

            var param = new DynamicParameters();
            param.Add("@CustCode", model.CustCode, DbType.String, ParameterDirection.Input);
            param.Add("@Start", start, DbType.Int32, ParameterDirection.Input);
            param.Add("@PageSize", model.PageSize, DbType.Int32, ParameterDirection.Input);
            var data = await _dapper.GetAsync<LeadActivitySQL>(_crmDbConn, "BBB39_GET_LIST_CUSTOMER_ACTIVITY", param,
                _sqlTimeout);

            var dataResponse = new LeadActivityReponse();

            if (String.IsNullOrEmpty(data.Items))
            {
                return new ResponseSingle<LeadActivityReponse>
                {
                    Code = ((int)ErrorCodeDetail.Success).ErrorCodeFormat(),
                    Message = ErrorCodeDetail.Success.ToEnumDescription(),
                    Data = dataResponse
                };
            }

            dataResponse.TotalItem = data.TotalItem;
            dataResponse.Items = JsonConvert.DeserializeObject<List<ActivityResult>>(data.Items);

            return new ResponseSingle<LeadActivityReponse>
            {
                Code = ((int)ErrorCodeDetail.Success).ErrorCodeFormat(),
                Message = ErrorCodeDetail.Success.ToEnumDescription(),
                Data = dataResponse
            };
        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
            return new ResponseSingle<LeadActivityReponse>
            {
                Code = ((int)ErrorCodeDetail.Failed).ErrorCodeFormat(),
                Message = ErrorCodeDetail.Failed.ToEnumDescription()
            };
        }
    }

    public async Task<ResponseSingle<CustomerReviewRateStarReponse>> GetCustomerReviews(CustomerReviewsRequest model)
    {
        try
        {
            var result = new CustomerReviewRateStarReponse();
            var param = new DynamicParameters();
            param.Add("@CustCode", model.CustCode, DbType.String, ParameterDirection.Input);
            param.Add("@UserName", model.UserName, DbType.String, ParameterDirection.Input);
            var data = await _dapper.SelectAsync<CustomerReviewsResponse>(_crmDbConn, "TVSI_sGET_CUSTOMER_REVIEWS",
                param, _sqlTimeout);

            if (data != null && data.Any())
            {
                result.RateStar!.StarOne = data.Count(x => x.Star == 1);

                result.RateStar!.StarTwo = data.Count(x => x.Star == 2);

                result.RateStar!.StarThree = data.Count(x => x.Star == 3);

                result.RateStar!.StarFour = data.Count(x => x.Star == 4);

                result.RateStar!.StarFive = data.Count(x => x.Star == 5);


                result.RateStar!.AvgStar = (5 * result.RateStar!.StarFive +
                                            4 * result.RateStar!.StarFour +
                                            3 * result.RateStar!.StarThree +
                                            2 * result.RateStar!.StarTwo +
                                            1 * result.RateStar!.StarOne) / data.Select(x => x.Star).Count();

                var dataList = data.Select(x => x.Id).ToList();
                var dataReply =
                    _dapper.SelectAsync<Reply>(_crmDbConn, "TVSI_sGET_CUSTOMER_REPLY", null!, _sqlTimeout).Result!
                        .Where(a => dataList.Any(x => x == a.Id));
                foreach (var item in data)
                {
                    if (dataReply != null && dataReply.Any())
                    {
                        foreach (var reply in dataReply)
                        {
                            if (item.Id == reply.Id)
                            {
                                item.Replies?.Add(new Reply()
                                {
                                    Content = reply.Content,
                                    FullName = reply.FullName
                                });
                            }
                        }
                    }

                    result.CustomerReviews?.Add(item);
                }
            }

            return new ResponseSingle<CustomerReviewRateStarReponse>
            {
                Code = ((int)ErrorCodeDetail.Success).ErrorCodeFormat(),
                Message = ErrorCodeDetail.Success.ToEnumDescription(),
                Data = result
            };
        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                 $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
            return new ResponseSingle<CustomerReviewRateStarReponse>
            {
                Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                Message = ErrorCodeDetail.Exception.ToEnumDescription()
            };
        }
    }

    public async Task<ResponseSingle<dynamic>> ConfirmEkycRequest(ConfirmEkycRequest model)
    {
        try
        {
            var param = new DynamicParameters();
            param.Add("@ConfirmCode", model.ConfirmCode, DbType.String, ParameterDirection.Input);
            var result = 0;
            if (model.OpenSource == 4)
            {
                var data = await _dapper.GetAsync<ConfirmEkycResponse>(_crmDbConn,"TVSI_sCONFIRM_EKYC",param,_sqlTimeout);
                result = data != null && data.ReturnCode == 1 ? result = 0 : result = -1;
            }
            if (model.OpenSource == 6)
            {
                param.Add("@ho_so_mo_tk_id", null, DbType.Int32, ParameterDirection.Output);
                param.Add("@crm_id_cf", null, DbType.Int32, ParameterDirection.Output);
                var data = await _dapper.GetAsync<ConfirmEkycResponse>(_crmDbConn,"TVSI_sCONFIRM_EKYC_API_MOBILE",param,_sqlTimeout);
                result = data != null && data.ReturnCode == 1 ? result = 0 : result = -1;
            }
            return new ResponseSingle<dynamic>
            {
                Code = (result is (int)ErrorCodeDetail.Success
                    ? (int)ErrorCodeDetail.Success
                    : (int)ErrorCodeDetail.Failed).ErrorCodeFormat(),
                Message = result is (int)ErrorCodeDetail.Success
                    ? ErrorCodeDetail.Success.ToEnumDescription()
                    : ErrorCodeDetail.Failed.ToEnumDescription(),
                Data = result is (int)ErrorCodeDetail.Success
            };
        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
            return new ResponseSingle<dynamic>
            {
                Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                Message = ErrorCodeDetail.Exception.ToEnumDescription()
            };
        }
    }
    
      private void AddCentralizeImageDB(long openAccID, int? sourceType, string? CustCode, string userName, 
                                        string frontName, string frontData, string backName, string backData)
    {
        try
        {
            using (var imageConn = new SqlConnection(_signatureDbConn))
            {
                var sql = @"Insert into TVSI_OPENACCOUNT_FILES(CustCode, OpenAccID, SourceType, Type, FileName, FileData, Status, CreatedBy, CreatedDate)
                            values(@CustCode, @OpenAccID, @SourceType, @Type, @FileName, @FileData, @Status, @CreatedBy, @CreatedDate)"; ;


                imageConn.Execute(sql, new
                {
                    CustCode = CustCode,
                    OpenAccID = openAccID,
                    SourceType = sourceType,
                    Type = 1,
                    FileName = frontName,
                    FileData = frontData,
                    Status = 1,
                    CreatedBy = userName,
                    CreatedDate = DateTime.Now
                });

                imageConn.Execute(sql, new
                {
                    CustCode = CustCode,
                    OpenAccID = openAccID,
                    SourceType = sourceType,
                    Type = 2,
                    FileName = backName,
                    FileData = backData,
                    Status = 1,
                    CreatedBy = userName,
                    CreatedDate = DateTime.Now
                });
            }
        }
        catch (Exception ex)
        {
            LogException(userName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
        }
    }


}