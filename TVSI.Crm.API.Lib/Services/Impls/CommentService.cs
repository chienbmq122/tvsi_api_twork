﻿using Dapper;
using Helper.DataAccess.Dapper;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using TVSI.Crm.API.Common;
using TVSI.Crm.API.Common.Enums;
using TVSI.Crm.API.Lib.Models;
using TVSI.Crm.API.Lib.Models.Request.Comment;
using TVSI.Crm.API.Lib.Models.Response.Comment;
using TVSI.TWork.API.Common.ExtensionMethods;

namespace TVSI.Crm.API.Lib.Services.Impls
{
    public class CommentService : BaseService<CommentService>, ICommentService
    {
        private readonly IDapperHelper _dapper;
        private readonly string _crmDbConn;
        private readonly int _sqlTimeout;

        public CommentService(IConfiguration config, ILogger<CommentService> logger, IDapperHelper dapper) : base(config, logger)
        {
            var connStr = _config.GetConnectionString("CRMDB_CONNECTION");
            _crmDbConn = connStr;
            _dapper = dapper;
            _sqlTimeout = _config["Timeout:Database"] == null
                ? CommonConstants.SqlServerTimeout
                : int.Parse(_config["Timeout:Database"]);
        }

        public async Task<Response<CommentDBModel>> GetListComment(CommentModelRequest model)
        {
            try
            {
                var param = new DynamicParameters();
                if(string.IsNullOrEmpty(model.CustCode))
                {
                    param.Add("@LeadId", model.LeadID, DbType.Int32, ParameterDirection.Input);
                    param.Add("@Custcode", model.CustCode, DbType.String, ParameterDirection.Input);
                    param.Add("@DBType", 1, DbType.Int32, ParameterDirection.Input);
                }
                else
                {
                    param.Add("@LeadId", model.LeadID, DbType.Int32, ParameterDirection.Input);
                    param.Add("@Custcode", model.CustCode, DbType.String, ParameterDirection.Input);
                    param.Add("@DBType", 2, DbType.Int32, ParameterDirection.Input);
                }

                var data = await _dapper.SelectAsync<CommentDBModel>(_crmDbConn, "BBB28_GET_LIST_COMMENT", param, _sqlTimeout);

                var idComment = "";

                if(data?.ToList().Count > 0)
                {
                    foreach (var comment in data)
                    {
                        idComment += comment.CommentID + ",";
                    }
                } else
                {
                    return new Response<CommentDBModel>
                    {
                        Code = ((int)ErrorCodeDetail.Success).ErrorCodeFormat(),
                        Message = ErrorCodeDetail.Success.ToEnumDescription(),
                        Data = data
                    };
                }

                var param1 = new DynamicParameters();
                
                param1.Add("@CommentIDs", idComment.Substring(0, idComment.Length - 1), DbType.String, ParameterDirection.Input);
                var dataReaply = await _dapper.SelectAsync<ReplyModel>(_crmDbConn, "BBB29_GET_LIST_Reaply", param1, _sqlTimeout);

                if(dataReaply != null)
                {
                    foreach (var comment in data)
                    {
                        comment.ListReply = dataReaply.Where(reply => reply.CommentID == comment.CommentID).ToList();
                    }
                }

                return new Response<CommentDBModel>
                {
                    Code = ((int)ErrorCodeDetail.Success).ErrorCodeFormat(),
                    Message = ErrorCodeDetail.Success.ToEnumDescription(),
                    Data = data
                };
            }
            catch (Exception ex)
            {
                LogException(model.UserName,
                    $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
                return new Response<CommentDBModel> {
                    Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                    Message = ex.Message
                };
            }
        }
        public async Task<ResponseSingle<bool>> CreateComent(CreateCommentRequest model)
        {
            try
            {
                var param = new DynamicParameters();
                param.Add("@UserName", model.UserName, DbType.String, ParameterDirection.Input);
                param.Add("@LeadID", model.LeadID, DbType.Int32, ParameterDirection.Input);
                param.Add("@CustCode", model.CustCode, DbType.String, ParameterDirection.Input);
                param.Add("@Content", model.Content, DbType.String, ParameterDirection.Input);
                param.Add("@FullName", model.FullName, DbType.String, ParameterDirection.Input);
                param.Add("@Rate", model.Rate, DbType.Int32, ParameterDirection.Input);
                var data = await _dapper.ExecuteAsync(_crmDbConn, "BBB30_CREATE_COMMENT", param, _sqlTimeout);
                return new ResponseSingle<bool>
                {
                    Code = ((int)ErrorCodeDetail.Success).ErrorCodeFormat(),
                    Message = ErrorCodeDetail.Success.ToEnumDescription(),
                    Data = true
                };
            }
            catch (Exception ex)
            {
                LogException(model.UserName,
                    $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
                return new ResponseSingle<bool>
                {
                    Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                    Message = ex.Message
                };
            }
        }
        public async Task<ResponseSingle<bool>> CreateReply(CreateReplyRequest model)
        {
            try
            {
                var param = new DynamicParameters();
                param.Add("@UserName", model.UserName, DbType.String, ParameterDirection.Input);
                param.Add("@CommentID", model.CommentID, DbType.Int32, ParameterDirection.Input);
                param.Add("@FullName", model.FullName, DbType.String, ParameterDirection.Input);
                param.Add("@Reply", model.Reply, DbType.String, ParameterDirection.Input);
                var data = await _dapper.ExecuteAsync(_crmDbConn, "BBB31_CREATE_REAPLY", param, _sqlTimeout);
                return new ResponseSingle<bool>
                {
                    Code = ((int)ErrorCodeDetail.Success).ErrorCodeFormat(),
                    Message = ErrorCodeDetail.Success.ToEnumDescription(),
                    Data = true
                };
            }
            catch (Exception ex)
            {
                LogException(model.UserName,
                    $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
                return new ResponseSingle<bool>
                {
                    Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                    Message = ex.Message
                };
            }
        }

        public async Task<ResponseSingle<TotalRateResponse>> GetTotalReply(GetRateRequest model)
        {
            try
            {
                var param = new DynamicParameters();
                param.Add("@CustCode", model.CustCode, DbType.String, ParameterDirection.Input);
                param.Add("@LeadID", model.LeadID, DbType.Int32, ParameterDirection.Input);
                if (string.IsNullOrEmpty(model.UserName))
                {
                    param.Add("@CustType", 2, DbType.Int32, ParameterDirection.Input);
                } else
                {
                    param.Add("@CustType", 1, DbType.Int32, ParameterDirection.Input);
                }
                var data = await _dapper.GetAsync<TotalRateResponse>(_crmDbConn, "BBB41_GET_TOTAL_RATE", param, _sqlTimeout);
                return new ResponseSingle<TotalRateResponse>
                {
                    Code = ((int)ErrorCodeDetail.Success).ErrorCodeFormat(),
                    Message = ErrorCodeDetail.Success.ToEnumDescription(),
                    Data = data
                };
            }
            catch (Exception ex)
            {
                LogException(model.UserName,
                    $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
                return new ResponseSingle<TotalRateResponse>
                {
                    Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                    Message = ex.Message
                };
            }
        }
    }
}
