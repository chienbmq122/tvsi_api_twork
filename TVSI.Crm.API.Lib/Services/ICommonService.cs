using TVSI.Crm.API.Lib.Models;
using TVSI.Crm.API.Lib.Models.Request.Common;
using TVSI.Crm.API.Lib.Models.Response.Common;

namespace TVSI.Crm.API.Lib.Services;

public interface ICommonService
{
    Task<Response<dynamic>> GetProfileStatus(BaseRequest model);
    Task<Response<dynamic>> GetAccountStatus(BaseRequest model);
    Task<Response<dynamic>> GetActionStatus(BaseRequest model);
    Task<Response<dynamic>> GetChannelOpenAccount(BaseRequest model);
    Task<Response<dynamic>> GetProvinceList(BaseRequest model);
    Task<Response<dynamic>> GetDistrictList(GetDistrictListRequest model);
    Task<Response<dynamic>> GetWardList(GetWardListRequest model);
    /*
    Task<Response<dynamic>> GetTypeAccountList(BaseRequest model);
    */
    Task<Response<ExtendResponse>> GetExtendInfoList(BaseRequest request);
    Task<Response<KycInfoResponse>> GetKycInfoList(BaseRequest request);
    /// <summary>
    /// Loại hình tài khoản
    /// </summary>
    /// <param name="request"></param>
    /// <returns></returns>
    Task<Response<dynamic>> GetCustomerTypeList(BaseRequest request);
    
    /// <summary>
    /// Kiểm tra mã KH có được phép xem k?
    /// </summary>
    /// <param name="request"></param>
    /// <returns></returns>
    Task<ResponseSingle<dynamic>> CheckCustCodeCustomerInfo(BaseRequest request);
    Task<Response<dynamic>> GetPlaceOfIssue(BaseRequest request);
    Task<Response<dynamic>> GetCustomerStatusList(BaseRequest model);
    Task<Response<dynamic>> GetAllRightByFun(GetAllRightByFunctionRequest model);
    
    Task<ResponseSingle<KycCustomerInfomationResponse>> GetKycCustomerInfo(GetKycCustomerRequest model);

    Task<Response<dynamic>> GetBranchList(BaseRequest model);
}