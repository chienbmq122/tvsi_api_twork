﻿namespace TVSI.Crm.API.Lib.Settings
{
    public class LogAdvanceSettings
    {
        public string? Path { get; set; }
        public bool DisableAll { get; set; } = false;
        public List<string> DisableControllers { get; set; } = new();
        public List<string> DisableActions { get; set; } = new();
    }
}
