﻿using System;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace TVSI.Crm.API.Filters.SwaggerFilters;

public class SwaggerOptions : IConfigureOptions<SwaggerGenOptions>
{
    private readonly IApiVersionDescriptionProvider _provider;

    public SwaggerOptions(IApiVersionDescriptionProvider provider) => _provider = provider;

    public void Configure(SwaggerGenOptions options)
    {
        foreach (var description in _provider.ApiVersionDescriptions)
        {
            options.SwaggerDoc(description.GroupName, CreateApiVersion(description));
        }
    }

    private static OpenApiInfo CreateApiVersion(ApiVersionDescription description)
    {
        var info = new OpenApiInfo
        {
            Title = "TVSI CRM API",
            Description = "API for CRM",
            Version = description.ApiVersion.ToString(),
            Contact = new OpenApiContact
            {
                Name = "TVSI Securities",
                Url = new Uri("https://www.tvsi.com.vn"),
            }
        };

        if (description.IsDeprecated)
        {
            info.Description += " - This API version has been deprecated.";
        }

        return info;
    }
}