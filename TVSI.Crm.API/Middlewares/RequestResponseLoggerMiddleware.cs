﻿using Serilog;

namespace TVSI.Crm.API.Middlewares
{
    
public class RequestResponseLoggerMiddleware
{
    private readonly RequestDelegate _next;
    private readonly bool _isRequestResponseLoggingEnabled;

    public RequestResponseLoggerMiddleware(RequestDelegate next, IConfiguration config)
    {
        _next = next;
        _isRequestResponseLoggingEnabled = config.GetValue<bool>("EnableRequestResponseLogging", false);
    }

    public async Task InvokeAsync(HttpContext httpContext)
    {
        // Middleware is enabled only when the EnableRequestResponseLogging config value is set.
        if (_isRequestResponseLoggingEnabled)
        {
            var requestBody = await ReadBodyFromRequest(httpContext.Request);
            var requestInfo = $"HTTP request information:\r\n" +
                              $"\tMethod: {httpContext.Request.Method}\r\n" +
                              $"\tPath: {httpContext.Request.Path}\r\n" +
                              $"\tQueryString: {httpContext.Request.QueryString}\r\n" +
                              $"\tHeaders: {FormatHeaders(httpContext.Request.Headers)}\r\n" +
                              $"\tSchema: {httpContext.Request.Scheme}\r\n" +
                              $"\tHost: {httpContext.Request.Host}\r\n" +
                              $"\tBody: {requestBody}";

            //var baseRequest = JsonConvert.DeserializeObject<BaseRequest>(requestBody);
            //var userName = baseRequest != null && !string.IsNullOrEmpty(baseRequest.UserName) ? baseRequest.UserName : "Other";

            //Log.ForContext("Name", userName).Information(requestInfo);
            Log.ForContext("Name", DateTime.Now.ToString("yyyyMMdd") + "\\"  + "middleware").Information(requestInfo);

            // Temporarily replace the HttpResponseStream, which is a write-only stream, with a MemoryStream to capture it's value in-flight.
            var originalResponseBody = httpContext.Response.Body;
            using var newResponseBody = new MemoryStream();
            httpContext.Response.Body = newResponseBody;

            // Call the next middleware in the pipeline
            await _next(httpContext);

            newResponseBody.Seek(0, SeekOrigin.Begin);
            var responseBodyText = await new StreamReader(httpContext.Response.Body).ReadToEndAsync();

            var responseInfo = $"HTTP response information:\r\n" +
                               $"\tStatusCode: {httpContext.Response.StatusCode}\r\n" +
                               $"\tContentType: {httpContext.Response.ContentType}\r\n" +
                               $"\tHeaders: {FormatHeaders(httpContext.Response.Headers)}\r\n" +
                               $"\tBody: {responseBodyText}";

            
            //var baseResponse = JsonConvert.DeserializeObject<ResponseSingle<object>>(responseBodyText);
            //userName = baseResponse != null && !string.IsNullOrEmpty(baseResponse.UserName) ? baseResponse.UserName : "Other";
            
            //Log.ForContext("Name", userName).Information(responseInfo);
            Log.ForContext("Name", DateTime.Now.ToString("yyyyMMdd") + "\\" + "middleware").Information(responseInfo);

            newResponseBody.Seek(0, SeekOrigin.Begin);
            await newResponseBody.CopyToAsync(originalResponseBody);
        }
        else
        {
            await _next(httpContext);
        }
    }
    private static string FormatHeaders(IHeaderDictionary headers) => string.Join(", ",
        headers.Select(kvp => $"{{{kvp.Key}: {string.Join(", ", kvp.Value)}}}"));

    private static async Task<string> ReadBodyFromRequest(HttpRequest request)
    {
        // Ensure the request's body can be read multiple times (for the next middlewares in the pipeline).
        request.EnableBuffering();

        using var streamReader = new StreamReader(request.Body, leaveOpen: true);
        var requestBody = await streamReader.ReadToEndAsync();

        // Reset the request's body stream position for next middleware in the pipeline.
        request.Body.Position = 0;
        return requestBody;
    }
}
}