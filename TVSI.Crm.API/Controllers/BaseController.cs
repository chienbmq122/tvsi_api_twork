﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Serilog;
using TVSI.Crm.API.Lib.Models;

namespace TVSI.Crm.API.Controllers;

[Produces("application/json")]
[Route("api/[controller]")]
[ApiController]
public class BaseController : ControllerBase
{
    #region "Logging"

    /// <summary>
    /// Log Request
    /// </summary>
    /// <param name="userName"></param>
    /// <param name="requestModel"></param>
    ///  /// <param name="apiName"></param>
    protected void LogRequest(string userName, BaseRequest requestModel, string apiName = "")
    {
        if (string.IsNullOrEmpty(apiName))
            apiName = ControllerContext.ActionDescriptor.ActionName;

        if (!string.IsNullOrEmpty(userName))
        {
            var requestJson = JsonConvert.SerializeObject(requestModel, Formatting.None, new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore
            });
            Log.ForContext("Name", DateTime.Now.ToString("yyyyMMdd") + "\\" + userName)
                .Information("API=" + apiName + "\t Request=" + requestJson);
        }
    }

    protected void LogResponse<T>(string userName, ResponseSingle<T> responseModel, string apiName = "")
    {
        if (string.IsNullOrEmpty(apiName))
            apiName = ControllerContext.ActionDescriptor.ActionName;

        if (!string.IsNullOrEmpty(userName))
        {
            var responseJson = JsonConvert.SerializeObject(responseModel, Formatting.None, new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore
            });
            Log.ForContext("Name", DateTime.Now.ToString("yyyyMMdd") + "\\" + userName)
                .Information("API=" + apiName + "\t Response=" + responseJson);
        }
    }

    protected void LogResponse<T>(string userName, Response<T> responseModel, string apiName = "")
    {
        if (string.IsNullOrEmpty(apiName))
            apiName = ControllerContext.ActionDescriptor.ActionName;

        if (!string.IsNullOrEmpty(userName))
        {
            var responseJson = JsonConvert.SerializeObject(responseModel, Formatting.None, new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore
            });
            Log.ForContext("Name", DateTime.Now.ToString("yyyyMMdd") + "\\" + userName)
                .Information("API=" + apiName + "\t Response=" + responseJson);
        }
    }

    /// <summary>
    /// Log Exception
    /// </summary>
    /// <param name="userName"></param>
    /// <param name="apiName"></param>
    /// <param name="message"></param>
    protected void LogException(string userName, string message, string apiName = "")
    {
        if (string.IsNullOrEmpty(apiName))
            apiName = ControllerContext.ActionDescriptor.ActionName;

        Log.ForContext("Name", DateTime.Now.ToString("yyyyMMdd") + "\\" + "exception").Error(message);
        if (!string.IsNullOrEmpty(userName))
        {
            Log.ForContext("Name", DateTime.Now.ToString("yyyyMMdd") + "\\" + userName)
                .Error("API=" + apiName + "\t Message=" + message);
        }
    }

    protected void LogInformation(string userName, string apiName, string message)
    {
        if (string.IsNullOrEmpty(apiName))
            apiName = ControllerContext.ActionDescriptor.ActionName;

        if (!string.IsNullOrEmpty(userName))
        {
            Log.ForContext("Name", DateTime.Now.ToString("yyyyMMdd") + "\\" + userName)
                .Information("API=" + apiName + "\t Message=" + message);
        }
    }
    #endregion
}