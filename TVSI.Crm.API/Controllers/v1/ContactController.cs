﻿using Microsoft.AspNetCore.Mvc;
using System.Reflection;
using TVSI.Crm.API.Lib.Models;
using TVSI.Crm.API.Lib.Services;
using static TVSI.Crm.API.Lib.Models.Request.Contact.ContactRequest;

namespace TVSI.Crm.API.Controllers.v1
{
    [ApiVersion("1.0")]
    public class ContactController : BaseController
    {
        private readonly IContactService _contactService;

        /// <summary>
        /// Controller Constructor
        /// </summary>
        public ContactController(IContactService contactService)
        {
            _contactService = contactService;
        }

        /// <summary>
        /// API Lấy thông tin customer
        /// </summary>
        /// <param name="model">Thông tin Request</param>
        /// <param name="src">Nguồn yêu cầu (M:Mobile, W:Web)</param>
        /// <param name="lang">Ngôn ngữ (EN: English, VI: VietNam)</param>
        /// <returns>Thông tin customer</returns>
        /// <remarks>
        /// Sample request:
        /// 
        ///     POST /Contact/GetInfoContactCust
        ///     {
        ///        "UserName": "1268-001",
        ///        "UserRole": "CRM_SALE",
        ///        "CustCode": "000135",
        ///     }
        ///
        /// </remarks>
        [HttpPost("GetInfoContactCust")]
        public async Task<IActionResult> GetInfoContactCust(ContactinfoCustRequest model, [FromQuery] string? src = "M", [FromQuery] string? lang = "VI")
        {
            try
            {
                LogRequest(model.UserName, model, "GetInfoContactCust");

                var response = await _contactService.GetInfoContactCust(model);

                LogResponse(model.UserName, response, "GetInfoContactCust");

                return Ok(response);
            }
            catch (Exception ex)
            {
                LogException(model.UserName, $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}", "GetInfoContactCust");
                return StatusCode(StatusCodes.Status500InternalServerError, new SystemError());
            }
        }

        /// <summary>
        /// API Lấy thông tin danh sách staff
        /// </summary>
        /// <param name="model">Thông tin Request</param>
        /// <param name="src">Nguồn yêu cầu (M:Mobile, W:Web)</param>
        /// <param name="lang">Ngôn ngữ (EN: English, VI: VietNam)</param>
        /// <returns>Thông tin danh sách staff</returns>
        /// <remarks>
        /// Sample request:
        /// 
        ///     POST /Contact/GetListContactStaff
        ///     {
        ///        "UserName": "1268-001",
        ///        "UserRole": "CRM_SALE",
        ///        "PageIndex": 1,
        ///        "PageSize": 10,
        ///        "KeySearch: ""
        ///     }
        ///
        /// </remarks>
        [HttpPost("GetListContactStaff")]
        public async Task<IActionResult> GetListContactStaff(ContactStaffRequest model, [FromQuery] string? src = "M", [FromQuery] string? lang = "VI")
        {
            try
            {
                LogRequest(model.UserName, model, "GetListContactStaff");

                var response = await _contactService.GetListContactStaff(model);

                LogResponse(model.UserName, response, "GetListContactStaff");

                return Ok(response);
            }
            catch (Exception ex)
            {
                LogException(model.UserName, $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}", "GetListContactStaff");
                return StatusCode(StatusCodes.Status500InternalServerError, new SystemError());
            }
        }

        /// <summary>
        /// API Lấy thông tin lead
        /// </summary>
        /// <param name="model">Thông tin Request</param>
        /// <param name="src">Nguồn yêu cầu (M:Mobile, W:Web)</param>
        /// <param name="lang">Ngôn ngữ (EN: English, VI: VietNam)</param>
        /// <returns>Thông tin lead</returns>
        /// <remarks>
        /// Sample request:
        /// 
        ///     POST /Contact/ContactinfoLeadRequest
        ///     {
        ///        "UserName": "1268-001",
        ///        "UserRole": "CRM_SALE",
        ///        "LeadID": 90230,
        ///     }
        ///
        /// </remarks>
        [HttpPost("GetInfoContactLead")]
        public async Task<IActionResult> GetInfoContactLead(ContactinfoLeadRequest model, [FromQuery] string? src = "M", [FromQuery] string? lang = "VI")
        {
            try
            {
                LogRequest(model.UserName, model, "GetInfoContactLead");

                var response = await _contactService.GetInfoContactLead(model);

                LogResponse(model.UserName, response, "GetInfoContactLead");

                return Ok(response);
            }
            catch (Exception ex)
            {
                LogException(model.UserName, $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}", "GetInfoContactLead");
                return StatusCode(StatusCodes.Status500InternalServerError, new SystemError());
            }
        }

        /// <summary>
        /// API Lấy thông tin staff
        /// </summary>
        /// <param name="model">Thông tin Request</param>
        /// <param name="src">Nguồn yêu cầu (M:Mobile, W:Web)</param>
        /// <param name="lang">Ngôn ngữ (EN: English, VI: VietNam)</param>
        /// <returns>Thông tin staff</returns>
        /// <remarks>
        /// Sample request:
        /// 
        ///     POST /Contact/GetInfoContactStaff
        ///     {
        ///        "UserName": "1268-001",
        ///        "UserRole": "CRM_SALE",
        ///        "StaffID": "CRM_SALE",
        ///     }
        ///
        /// </remarks>
        [HttpPost("GetInfoContactStaff")]
        public async Task<IActionResult> GetInfoContactStaff(ContactinfoStaffRequest model, [FromQuery] string? src = "M", [FromQuery] string? lang = "VI")
        {
            try
            {
                LogRequest(model.UserName, model, "GetInfoContactStaff");

                var response = await _contactService.GetInfoContactStaff(model);

                LogResponse(model.UserName, response, "GetInfoContactStaff");

                return Ok(response);
            }
            catch (Exception ex)
            {
                LogException(model.UserName, $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}", "GetInfoContactStaff");
                return StatusCode(StatusCodes.Status500InternalServerError, new SystemError());
            }
        }

        /// <summary>
        /// API Lấy thông tin danh sách customer
        /// </summary>
        /// <param name="model">Thông tin Request</param>
        /// <param name="src">Nguồn yêu cầu (M:Mobile, W:Web)</param>
        /// <param name="lang">Ngôn ngữ (EN: English, VI: VietNam)</param>
        /// <returns>Thông tin danh sách customer</returns>
        /// <remarks>
        /// Sample request:
        /// 
        ///     POST /Contact/GetListContactCust
        ///     {
        ///        "UserName": "1268-001",
        ///        "UserRole": "CRM_SALE",
        ///        "PageIndex": 1,
        ///        "PageSize": 10,
        ///        "KeySearch": "",
        ///        "CustCode": "",
        ///        "Phone": ""
        ///     }
        ///
        /// </remarks>
        [HttpPost("GetListContactCust")]
        public async Task<IActionResult> GetListContactCust(ContactStaffRequest model, [FromQuery] string? src = "M", [FromQuery] string? lang = "VI")
        {
            try
            {
                LogRequest(model.UserName, model, "GetListContactCust");

                var response = await _contactService.GetListContactCust(model);

                LogResponse(model.UserName, response, "GetListContactCust");

                return Ok(response);
            }
            catch (Exception ex)
            {
                LogException(model.UserName, $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}", "GetListContactCust");
                return StatusCode(StatusCodes.Status500InternalServerError, new SystemError());
            }
        }
    }
}
