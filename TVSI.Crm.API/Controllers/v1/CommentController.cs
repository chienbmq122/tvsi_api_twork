﻿using Microsoft.AspNetCore.Mvc;
using System.Reflection;
using TVSI.Crm.API.Lib.Models;
using TVSI.Crm.API.Lib.Models.Request.Comment;
using TVSI.Crm.API.Lib.Services;

namespace TVSI.Crm.API.Controllers.v1
{
    [ApiVersion("1.0")]
    public class CommentController : BaseController
    {
        private readonly ICommentService _commentService;

        /// <summary>
        /// Controller Constructor
        /// </summary>
        public CommentController(ICommentService commentService)
        {
            _commentService = commentService;
        }

        /// <summary>
        /// API Lấy danh sách comment và reaply
        /// </summary>
        /// <param name="model">Thông tin Request</param>
        /// <param name="src">Nguồn yêu cầu (M:Mobile, W:Web)</param>
        /// <param name="lang">Ngôn ngữ (EN: English, VI: VietNam)</param>
        /// <returns>Ddanh sách comment và reaply</returns>
        /// <remarks>
        /// Sample request:
        /// 
        ///     POST /Common/GetListComment
        ///     {
        ///        "UserName": "0824-001",
        ///        "UserRole": "CRM_SALE"
        ///     }
        ///
        /// </remarks>
        [HttpPost("GetListComment")]
        public async Task<IActionResult> GetListComment(CommentModelRequest model, [FromQuery] string? src = "M",
            [FromQuery] string? lang = "VI")
        {
            try
            {
                LogRequest(model.UserName, model, "GetListComment");

                var response = await _commentService.GetListComment(model);

                LogResponse(model.UserName, response, "GetListComment");

                return Ok(response);
            }
            catch (Exception ex)
            {
                LogException(model.UserName, $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}", "GetListComment");
                return StatusCode(StatusCodes.Status500InternalServerError, new SystemError());
            }
        }

        /// <summary>
        /// API Tạo mới comment
        /// </summary>
        /// <param name="model">Thông tin Request</param>
        /// <param name="src">Nguồn yêu cầu (M:Mobile, W:Web)</param>
        /// <param name="lang">Ngôn ngữ (EN: English, VI: VietNam)</param>
        /// <returns>Tạo mới comment</returns>
        /// <remarks>
        /// Sample request:
        /// 
        ///     POST /Common/CreateComent
        ///     {
        ///        "UserName": "0824-001",
        ///        "UserRole": "CRM_SALE"
        ///     }
        ///
        /// </remarks>
        [HttpPost("CreateComent")]
        public async Task<IActionResult> CreateComent(CreateCommentRequest model, [FromQuery] string? src = "M",
            [FromQuery] string? lang = "VI")
        {
            try
            {
                LogRequest(model.UserName, model, "CreateComent");

                var response = await _commentService.CreateComent(model);

                LogResponse(model.UserName, response, "CreateComent");

                return Ok(response);
            }
            catch (Exception ex)
            {
                LogException(model.UserName, $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}", "CreateComent");
                return StatusCode(StatusCodes.Status500InternalServerError, new SystemError());
            }
        }

        /// <summary>
        /// API Tạo mới reply
        /// </summary>
        /// <param name="model">Thông tin Request</param>
        /// <param name="src">Nguồn yêu cầu (M:Mobile, W:Web)</param>
        /// <param name="lang">Ngôn ngữ (EN: English, VI: VietNam)</param>
        /// <returns>Tạo mới reply</returns>
        /// <remarks>
        /// Sample request:
        /// 
        ///     POST /Common/CreateReply
        ///     {
        ///        "UserName": "0824-001",
        ///        "UserRole": "CRM_SALE"
        ///     }
        ///
        /// </remarks>
        [HttpPost("CreateReply")]
        public async Task<IActionResult> CreateReply(CreateReplyRequest model, [FromQuery] string? src = "M",
            [FromQuery] string? lang = "VI")
        {
            try
            {
                LogRequest(model.UserName, model, "CreateReply");

                var response = await _commentService.CreateReply(model);

                LogResponse(model.UserName, response, "CreateReply");

                return Ok(response);
            }
            catch (Exception ex)
            {
                LogException(model.UserName, $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}", "CreateReply");
                return StatusCode(StatusCodes.Status500InternalServerError, new SystemError());
            }
        }

        /// <summary>
        /// API get total rate
        /// </summary>
        /// <param name="model">Thông tin Request</param>
        /// <param name="src">Nguồn yêu cầu (M:Mobile, W:Web)</param>
        /// <param name="lang">Ngôn ngữ (EN: English, VI: VietNam)</param>
        /// <returns>Tạo get total rate</returns>
        /// <remarks>
        /// Sample request:
        /// 
        ///     POST /Common/GetTotalReply
        ///     {
        ///        "UserName": "0824-001",
        ///        "UserRole": "CRM_SALE",
        ///        "CustCode": "356689",
        ///        "LeadID": 0
        ///     }
        ///
        /// </remarks>
        [HttpPost("GetTotalReply")]
        public async Task<IActionResult> GetTotalReply(GetRateRequest model, [FromQuery] string? src = "M",
            [FromQuery] string? lang = "VI")
        {
            try
            {
                LogRequest(model.UserName, model, "GetTotalReply");

                var response = await _commentService.GetTotalReply(model);

                LogResponse(model.UserName, response, "GetTotalReply");

                return Ok(response);
            }
            catch (Exception ex)
            {
                LogException(model.UserName, $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}", "GetTotalReply");
                return StatusCode(StatusCodes.Status500InternalServerError, new SystemError());
            }
        }
    }
}
