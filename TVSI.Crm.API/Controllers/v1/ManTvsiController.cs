using System.Reflection;
using Microsoft.AspNetCore.Mvc;
using TVSI.Crm.API.Lib.Models;
using TVSI.Crm.API.Lib.Models.Request.ManTvsi;
using TVSI.Crm.API.Lib.Services;

namespace TVSI.Crm.API.Controllers.v1;


/// <summary>
/// MANTVSI Module API
/// </summary>
[ApiVersion("1.0")]
public class ManTvsiController : BaseController
{
    private readonly IManTvsiService _manTvsiService;

    
    /// <summary>
    /// Controller Constructor
    /// </summary>
    public ManTvsiController(IManTvsiService manTvsiService)
    {
        _manTvsiService = manTvsiService;
    }
    
    /// <summary>
    /// Lây danh sách man TVSI 
    /// </summary>
    /// <param name="model"></param>
    /// <param name="src"></param>
    /// <param name="lang"></param>
    /// <returns>Lây danh sách man TVSI </returns>
    /// <remarks>
    /// Sample request:
    ///
    ///     POST /ManTvsi/GetManTvsiInfoList
    ///      {
    ///        "UserName": "6245-001",
    ///        "UserRole": "CRM_SALE"
    ///      }
    /// </remarks>
    [HttpPost("GetManTvsiInfoList")]
    public async Task<IActionResult> GetManTvsiInfoList(BaseRequest model, [FromQuery] string? src = "M",
        [FromQuery] string? lang = "VI")
    {
        try
        {
            LogRequest(model.UserName, model, "GetManTvsiInfoList");

            var response = await _manTvsiService.GetManInfoList(model);

            LogResponse(model.UserName, response, "GetManTvsiInfoList");

            return Ok(response);
        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}", "GetManTvsiInfoList");
            return StatusCode(StatusCodes.Status500InternalServerError, new SystemError());
        }
    } 
    
    /// <summary>
    /// Lây chi tiết hồ sơ man TVSI 
    /// </summary>
    /// <param name="model"></param>
    /// <param name="src"></param>
    /// <param name="lang"></param>
    /// <returns> Lây chi tiết hồ sơ man TVSI </returns>
    /// <remarks>
    /// Sample request:
    ///
    ///     POST /ManTvsi/GetManTvsiInfoDetail
    ///      {
    ///        "UserName": "6245-001",
    ///        "UserRole": "CRM_SALE",
    ///        "ManCode": "102"
    ///      }
    /// </remarks>
    [HttpPost("GetManTvsiInfoDetail")]
    public async Task<IActionResult> GetManTvsiInfoDetail(ManTvsiRequest model, [FromQuery] string? src = "M",
        [FromQuery] string? lang = "VI")
    {
        try
        {
            LogRequest(model.UserName, model, "GetManTvsiInfoDetail");

            var response = await _manTvsiService.GetManInfoDetail(model);

            LogResponse(model.UserName, response, "GetManTvsiInfoDetail");

            return Ok(response);
        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}", "GetManTvsiInfoDetail");
            return StatusCode(StatusCodes.Status500InternalServerError, new SystemError());
        }
    }
    
    
}