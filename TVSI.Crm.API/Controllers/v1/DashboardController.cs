﻿using Microsoft.AspNetCore.Mvc;
using System.Reflection;
using TVSI.Crm.API.Lib.Models;
using TVSI.Crm.API.Lib.Models.Request.Dashboard;
using TVSI.Crm.API.Lib.Services;

namespace TVSI.Crm.API.Controllers.v1
{
    [ApiVersion("1.0")]
    public class DashboardController : BaseController
    {
        private readonly IDashboardService _dashboardService;


        /// <summary>
        /// Controller Constructor
        /// </summary>
        public DashboardController(IDashboardService dashboardService)
        {
            _dashboardService = dashboardService;
        }

        /// <summary>
        /// API lấy thông tin giao dịch ngày hôm nay và tháng hiện tại
        /// </summary>
        /// <param name="model">Thông tin Request</param>
        /// <param name="src">Nguồn yêu cầu (M:Mobile, W:Web)</param>
        /// <param name="lang">Ngôn ngữ (EN: English, VI: VietNam)</param>
        /// <returns>thông tin giao dịch ngày hôm nay và tháng hiện tại</returns>
        /// <remarks>
        /// Sample request:
        /// 
        ///     POST /Dashboard/FeeInfo
        ///     {
        ///        "UserName": "0824-001",
        ///        "UserRole": "CRM_SALE",
        ///        "Status": 1,
        ///        "DateIndex": "23/02/2022",
        ///     }
        ///
        /// </remarks>
        [HttpPost("FeeInfo")]
        public async Task<IActionResult> FeeInfo(DashboardChartRequest model, [FromQuery] string? src = "M",
            [FromQuery] string? lang = "VI")
        {
            try
            {
                LogRequest(model.UserName, model, "FeeInfo");

                var response = await _dashboardService.TradeInfo(model);

                LogResponse(model.UserName, response, "FeeInfo");

                return Ok(response);
            }
            catch (Exception ex)
            {
                LogException(model.UserName, $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}", "FeeInfo");
                return StatusCode(StatusCodes.Status500InternalServerError, new SystemError());
            }
        }

        /// <summary>
        /// API lấy thông tin tổng hợp Cơ hội / KH tiềm năng / KH / KH active
        /// </summary>
        /// <param name="model">Thông tin Request</param>
        /// <param name="src">Nguồn yêu cầu (M:Mobile, W:Web)</param>
        /// <param name="lang">Ngôn ngữ (EN: English, VI: VietNam)</param>
        /// <returns>thông tin tổng hợp Cơ hội / KH tiềm năng / KH / KH active</returns>
        /// <remarks>
        /// Sample request:
        /// 
        ///     POST /Dashboard/SummaryInfo
        ///     {
        ///        "UserName": "0824-001",
        ///        "UserRole": "CRM_SALE",
        ///     }
        ///
        /// </remarks>
        [HttpPost("SummaryInfo")]
        public async Task<IActionResult> SummaryInfo(BaseRequest model, [FromQuery] string? src = "M",
            [FromQuery] string? lang = "VI")
        {
            try
            {
                LogRequest(model.UserName, model, "SummaryInfo");

                var response = await _dashboardService.TradeTotalHistory(model);

                LogResponse(model.UserName, response, "SummaryInfo");

                return Ok(response);
            }
            catch (Exception ex)
            {
                LogException(model.UserName, $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}", "SummaryInfo");
                return StatusCode(StatusCodes.Status500InternalServerError, new SystemError());
            }
        }

        /// <summary>
        /// API lấy thông tin giao dịch ngày hôm nay và tháng hiện tại
        /// </summary>
        /// <param name="model">Thông tin Request</param>
        /// <param name="src">Nguồn yêu cầu (M:Mobile, W:Web)</param>
        /// <param name="lang">Ngôn ngữ (EN: English, VI: VietNam)</param>
        /// <returns>thông tin giao dịch ngày hôm nay và tháng hiện tại</returns>
        /// <remarks>
        /// Sample request:
        /// 
        ///     POST /Dashboard/TradeInfo
        ///     {
        ///        "UserName": "0824-001",
        ///        "UserRole": "CRM_SALE",
        ///        "DateIndex": "23/02/2022"
        ///     }
        ///
        /// </remarks>
        [HttpPost("TradeInfo")]
        public async Task<IActionResult> TradeInfo(DashboardTradeRequest model, [FromQuery] string? src = "M",
            [FromQuery] string? lang = "VI")
        {
            try
            {
                LogRequest(model.UserName, model, "TradeInfo");

                var response = await _dashboardService.TradeTotalInfoHistory(model);

                LogResponse(model.UserName, response, "TradeInfo");

                return Ok(response);
            }
            catch (Exception ex)
            {
                LogException(model.UserName, $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}", "TradeInfo");
                return StatusCode(StatusCodes.Status500InternalServerError, new SystemError());
            }
        }

        /// <summary>
        /// API lấy thông tin tổng hợp Khách hàng tiềm năng / khách hàng
        /// </summary>
        /// <param name="model">Thông tin Request</param>
        /// <param name="src">Nguồn yêu cầu (M:Mobile, W:Web)</param>
        /// <param name="lang">Ngôn ngữ (EN: English, VI: VietNam)</param>
        /// <returns>thông tin tổng hợp Khách hàng tiềm năng / khách hàng</returns>
        /// <remarks>
        /// Sample request:
        /// 
        ///     POST /Dashboard/TotalCustLead
        ///     {
        ///        "UserName": "0824-001",
        ///        "UserRole": "CRM_SALE",
        ///     }
        ///
        /// </remarks>
        [HttpPost("TotalCustLead")]
        public async Task<IActionResult> TotalCustLead(BaseRequest model, [FromQuery] string? src = "M",
            [FromQuery] string? lang = "VI")
        {
            try
            {
                LogRequest(model.UserName, model, "TotalCustLead");

                var response = await _dashboardService.TotalCustLead(model);

                LogResponse(model.UserName, response, "TotalCustLead");

                return Ok(response);
            }
            catch (Exception ex)
            {
                LogException(model.UserName, $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}", "TotalCustLead");
                return StatusCode(StatusCodes.Status500InternalServerError, new SystemError());
            }
        }

        /// <summary>
        /// API lấy thông tin tổng hợp hoạt động
        /// </summary>
        /// <param name="model">Thông tin Request</param>
        /// <param name="src">Nguồn yêu cầu (M:Mobile, W:Web)</param>
        /// <param name="lang">Ngôn ngữ (EN: English, VI: VietNam)</param>
        /// <returns>thông tin tổng hợp hoạt động / khách hàng</returns>
        /// <remarks>
        /// Sample request:
        /// 
        ///     POST /Dashboard/TotalAcitvity
        ///     {
        ///        "UserName": "0824-001",
        ///        "UserRole": "CRM_SALE",
        ///     }
        ///
        /// </remarks>
        [HttpPost("TotalAcitvity")]
        public async Task<IActionResult> TotalAcitvity(BaseRequest model, [FromQuery] string? src = "M",
            [FromQuery] string? lang = "VI")
        {
            try
            {
                LogRequest(model.UserName, model, "TotalAcitvity");

                var response = await _dashboardService.TotalAcitvity(model);

                LogResponse(model.UserName, response, "TotalAcitvity");

                return Ok(response);
            }
            catch (Exception ex)
            {
                LogException(model.UserName, $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}", "TotalAcitvity");
                return StatusCode(StatusCodes.Status500InternalServerError, new SystemError());
            }
        }
    }
}
