using System.Reflection;
using Microsoft.AspNetCore.Mvc;
using TVSI.Crm.API.Lib.Models;
using TVSI.Crm.API.Lib.Models.Request.Shareholder;
using TVSI.Crm.API.Lib.Services;

namespace TVSI.Crm.API.Controllers.v1;

/// <summary>
/// Quản lý cổ đông Module API
/// </summary>
[ApiVersion("1.0")]
public class ShareholderController : BaseController
{
    private readonly IShareholderService _shareholderService;


    /// <summary>
    /// Controller Constructor
    /// </summary>
    /// <param name="shareholderService"></param>
    public ShareholderController(IShareholderService shareholderService)
    {
        _shareholderService = shareholderService;
    }


    /// <summary>
    /// Lấy danh sách cổ đông 
    /// </summary>
    /// <param name="model"></param>
    /// <param name="src"></param>
    /// <param name="lang"></param>
    /// <returns>Danh sách màn hình cổ đông</returns>
    /// <remarks>
    /// Sample request:
    ///
    ///     POST /Shareholder/GetShareholderList
    ///      {
    ///         "UserName": "6245-001",
    ///         "UserRole": "string",
    ///         "Custcode": "",
    ///         "StockCode": "",
    ///         "FullName": "",
    ///         "Status": 0,
    ///         "ShareholderType": 0
    ///      }
    /// </remarks>
    [HttpPost("GetShareholderList")]
    public async Task<IActionResult> GetShareholderList(ShareholderListRequest model, [FromQuery] string? src = "M",
        [FromQuery] string? lang = "VI")
    {
        try
        {
            LogRequest(model.UserName, model, "GetShareholderList");

            var response = await _shareholderService.GetShareholderList(model);

            LogResponse(model.UserName, response, "GetShareholderList");

            return Ok(response);
        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}", "GetShareholderList");
            return StatusCode(StatusCodes.Status500InternalServerError, new SystemError());
        }
    } 
    
    
    /// <summary>
    ///  Thêm thông tin cổ đông
    /// </summary>
    /// <param name="model"></param>
    /// <param name="src"></param>
    /// <param name="lang"></param>
    /// <returns>Thêm thông tin cổ đông</returns>
    /// <remarks>
    /// Sample request:
    ///
    ///     POST /Shareholder/CreateShareholder
    ///      {
    ///         "UserName": "1001-001",
    ///         "FullName": "Tạ Minh Chiến",
    ///         "Custcode": "000005",
    ///         "Address": "Việt Vinh",
    ///         "StockCode": "VCB,VIB",
    ///         "ShareholderType01": true,
    ///         "ShareholderType02": true,
    ///         "ShareholderType03": true,
    ///         "ShareholderType04": true,
    ///         "UserRole": "string"
    ///      }
    /// </remarks>
    [HttpPost("CreateShareholder")]
    public async Task<IActionResult> CreateShareholder(CreateShareholderRequest model, [FromQuery] string? src = "M",
        [FromQuery] string? lang = "VI")
    {
        try
        {
            LogRequest(model.UserName, model, "CreateShareholder");

            var response = await _shareholderService.CreateShareholder(model);

            LogResponse(model.UserName, response, "CreateShareholder");

            return Ok(response);
        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}", "CreateShareholder");
            return StatusCode(StatusCodes.Status500InternalServerError, new SystemError());
        }
    } 
    
    
    
    /// <summary>
    ///  thông tin chi tiết cổ đông
    /// </summary>
    /// <param name="model"></param>
    /// <param name="src"></param>
    /// <param name="lang"></param>
    /// <returns>Thêm thông tin cổ đông</returns>
    /// <remarks>
    /// Sample request:
    ///
    ///     POST /Shareholder/GetShareholderDetailInfo
    ///      {
    ///         "UserName": "6245-001",
    ///         "Custcode": "000005",
    ///         "UserRole": "string",
    ///         "Id":3035
    ///      }
    /// </remarks>
    [HttpPost("GetShareholderDetailInfo")]
    public async Task<IActionResult> GetShareholderDetailInfo(ShareholderDetailRequest model, [FromQuery] string? src = "M",
        [FromQuery] string? lang = "VI")
    {
        try
        {
            LogRequest(model.UserName, model, "GetShareholderDetailInfo");

            var response = await _shareholderService.GetShareholderDetail(model);

            LogResponse(model.UserName, response, "GetShareholderDetailInfo");

            return Ok(response);
        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
            return StatusCode(StatusCodes.Status500InternalServerError, new SystemError());
        }
    }
    /// <summary>
    ///  Check thông tin tài khoản có được thêm mới hay ko
    /// </summary>
    /// <param name="model"></param>
    /// <param name="src"></param>
    /// <param name="lang"></param>
    /// <returns>Check thông tin tài khoản có được thêm mới hay ko</returns>
    /// <remarks>
    /// Sample request:
    ///
    ///     POST /Shareholder/CheckCustcodeShareholder
    ///      {
    ///         "UserName": "6245-001",
    ///         "Custcode": "000005",
    ///         "UserRole": "string"
    ///         
    ///      }
    /// </remarks>
    [HttpPost("CheckCustcodeShareholder")]
    public async Task<IActionResult> CheckCustcodeShareholder(CheckCustcodeRequest model, [FromQuery] string? src = "M",
        [FromQuery] string? lang = "VI")
    {
        try
        {
            LogRequest(model.UserName, model, "CheckCustcodeShareholder");

            var response = await _shareholderService.CheckCustcodeShareholder(model.Custcode,model.UserName);

            LogResponse(model.UserName, response, "CheckCustcodeShareholder");

            return Ok(response);
        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}", "CheckCustcodeShareholder");
            return StatusCode(StatusCodes.Status500InternalServerError, new SystemError());
        }
    }  
    
    /// <summary>
    /// danh sách  thông tin mã ck
    /// </summary>
    /// <param name="model"></param>
    /// <param name="src"></param>
    /// <param name="lang"></param>
    /// <returns>lấy thông tin mã ck</returns>
    /// <remarks>
    /// Sample request:
    ///
    ///     POST /Shareholder/GetStockInfoList
    ///      {
    ///         "UserName": "6245-001",
    ///         "UserRole": "string"
    ///         
    ///      }
    /// </remarks>
    [HttpPost("GetStockInfoList")]
    public async Task<IActionResult> GetStockInfoList(BaseRequest model, [FromQuery] string? src = "M",
        [FromQuery] string? lang = "VI")
    {
        try
        {
            LogRequest(model.UserName, model, "GetStockInfoList");

            var response = await _shareholderService.GetStockInfoList(model);

            LogResponse(model.UserName, response, "GetStockInfoList");

            return Ok(response);
        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}", "GetStockInfoList");
            return StatusCode(StatusCodes.Status500InternalServerError, new SystemError());
        }
    }  
    
    /// <summary>
    /// API  Hủy thông tin cổ đông
    /// </summary>
    /// <param name="model"></param>
    /// <param name="src"></param>
    /// <param name="lang"></param>
    /// <returns>API  Hủy thông tin cổ đông</returns>
    /// <remarks>
    /// Sample request:
    ///
    ///     POST /Shareholder/CancelShareholderInfo
    ///      {
    ///          "UserName": "6245-001",
    ///          "UserRole": "string"
    ///          "Id": 3032,
    ///          "Custcode": "000001"
    ///      }
    /// </remarks>
    [HttpPost("CancelShareholderInfo")]
    public async Task<IActionResult> CancelShareholderInfo(CancelShareholderRequest model, [FromQuery] string? src = "M",
        [FromQuery] string? lang = "VI")
    {
        try
        {
            LogRequest(model.UserName, model, "CancelShareholderInfo");

            var response = await _shareholderService.CancelShareholderInfo(model);

            LogResponse(model.UserName, response, "CancelShareholderInfo");

            return Ok(response);
        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}", "CancelShareholderInfo");
            return StatusCode(StatusCodes.Status500InternalServerError, new SystemError());
        }
    }  
    
    /// <summary>
    /// API  Cập nhật thông tin cổ đông
    /// </summary>
    /// <param name="model"></param>
    /// <param name="src"></param>
    /// <param name="lang"></param>
    /// <returns>API  Cập nhật thông tin cổ đông</returns>
    /// <remarks>
    /// Sample request:
    ///
    ///     POST /Shareholder/UpdateShareholderInfo
    ///      {
    ///         "UserName": "6245-001",
    ///         "UserRole": "string",
    ///          "Id": 3044,
    ///          "ShareholderType01": true,
    ///          "ShareholderType02": false,
    ///          "ShareholderType03": true,
    ///          "ShareholderType04": true,
    ///          "StockCode": "VIC,BID,VCB"
    ///      }
    /// </remarks>
    [HttpPost("UpdateShareholderInfo")]
    public async Task<IActionResult> UpdateShareholderInfo(UpdateShareholderRequest model, [FromQuery] string? src = "M",
        [FromQuery] string? lang = "VI")
    {
        try
        {
            LogRequest(model.UserName, model, "UpdateShareholderInfo");

            var response = await _shareholderService.UpdateShareholder(model);

            LogResponse(model.UserName, response, "UpdateShareholderInfo");

            return Ok(response);
        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}", "UpdateShareholderInfo");
            return StatusCode(StatusCodes.Status500InternalServerError, new SystemError());
        }
    }
}