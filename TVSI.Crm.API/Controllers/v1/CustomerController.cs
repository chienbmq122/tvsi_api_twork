﻿using System.Net;
using System.Reflection;
using Microsoft.AspNetCore.Mvc;
using TVSI.Crm.API.Lib.Models;
using TVSI.Crm.API.Lib.Models.Request.Common;
using TVSI.Crm.API.Lib.Models.Request.Customer;
using TVSI.Crm.API.Lib.Models.Response.Customer;
using TVSI.Crm.API.Lib.Models.Response.Lead;
using TVSI.Crm.API.Lib.Services;
using TVSI.Crm.API.Lib.Services.Impls;

namespace TVSI.Crm.API.Controllers.v1;

/// <summary>
/// Customer Module API
/// </summary>
[ApiVersion("1.0")]
public class CustomerController : BaseController
{
    private readonly ICustomerService _customerService;
    private readonly ICommonService _commonService;


    /// <summary>
    /// Controller Constructor
    /// </summary>
    public CustomerController(ICustomerService customerService,ICommonService commonService)
    {
        _customerService = customerService;
        _commonService = commonService;
    }


    /// <summary>
    /// API lấy hồ sơ mở tài khoản trực tuyến
    /// </summary>
    /// <param name="model">Thông tin Request</param>
    /// <param name="src">Nguồn yêu cầu (M:Mobile, W:Web)</param>
    /// <param name="lang">Ngôn ngữ (EN: English, VI: VietNam)</param>
    /// <returns>API lấy danh sách hồ sơ mở tài khoản trực tuyến</returns>
    /// <remarks>
    /// Sample request:
    /// 
    ///     POST /Customer/GetCustomerOnlineDetailInfo
    ///     {
    ///        "UserName": "1268-001",
    ///        "Id":10935,
    ///        "UserRole":"CRM_SALE"
    ///     }
    ///
    /// </remarks>
    [HttpPost("GetCustomerOnlineDetailInfo")]
    public async Task<IActionResult> GetCustomerOnlineDetailInfo(CustomerOnlineDetailInfoRequest model,
        [FromQuery] string? src = "M",
        [FromQuery] string? lang = "VI")
    {
        try
        {
            LogRequest(model.UserName, model, "GetCustomerOnlineDetailInfo");

            var response = await _customerService.GetCustomerOnlineDetailInfo(model, lang);

            LogResponse(model.UserName, response, "GetCustomerOnlineDetailInfo");

            return Ok(response);
        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}", "GetCustomerOnlineDetailInfo");
            return StatusCode(StatusCodes.Status500InternalServerError, new SystemError());
        }
    }


    /// <summary>
    /// API Danh sách khách hàng mở tài khoản trực tuyến
    /// </summary>
    /// <param name="model">Thông tin Request</param>
    /// <param name="src">Nguồn yêu cầu (M:Mobile, W:Web)</param>
    /// <param name="lang">Ngôn ngữ (EN: English, VI: VietNam)</param>
    /// <returns>API Danh sách khách hàng mở tài khoản trực tuyến</returns>
    /// <remarks>
    /// Sample request:
    /// 
    ///     POST /Customer/GetCustomerOnlineList
    ///     {
    ///        "UserName": "1268-001",
    ///        "CardId": "",
    ///        "CustCode": "",
    ///        "FullName": "",
    ///        "Phone": "",
    ///        "SaleID": "",
    ///        "OpenSource": 4,
    ///        "AccountStatus": "",
    ///        "ProfileStatus": "",
    ///        "ActionStatus": "",
    ///        "PageIndex": 1,
    ///        "PageSize": 10,
    ///        "UserRole":"CRM_SALE"
    ///     }
    ///
    /// </remarks>
    [HttpPost("GetCustomerOnlineList")]
    [ProducesResponseType(typeof(Response<CustomerOnlineListResponse>), (int)HttpStatusCode.OK)]
    public async Task<IActionResult> GetCustomerOnlineList(CustomerOnlineListRequest model,
        [FromQuery] string? src = "M",
        [FromQuery] string? lang = "VI")
    {
        try
        {
            LogRequest(model.UserName, model, "GetCustomerOnlineList");

            var response = await _customerService.GetCustomerOnlineList(model, lang);

            LogResponse(model.UserName, response, "GetCustomerOnlineList");

            return Ok(response);
        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}", "GetCustomerOnlineList");
            return StatusCode(StatusCodes.Status500InternalServerError, new SystemError());
        }
    }

    /// <summary>
    /// API Hủy hồ sơ MTK trực tuyếtn
    /// </summary>
    /// <param name="model">Thông tin Request</param>
    /// <param name="src">Nguồn yêu cầu (M:Mobile, W:Web)</param>
    /// <param name="lang">Ngôn ngữ (EN: English, VI: VietNam)</param>
    /// <returns>API Hủy hồ sơ MTK trực tuyếtn</returns>
    /// <remarks>
    /// Sample request:
    /// 
    ///     POST /Customer/CancelCustomerInfo
    ///     {
    ///        "UserName": "1268-001",
    ///        "UserRole":"CRM_SALE",
    ///         "Id": 12344
    ///     }
    ///
    /// </remarks>
    [HttpPost("CancelCustomerInfo")]
    [ProducesResponseType(typeof(Response<CustomerOnlineListResponse>), (int)HttpStatusCode.OK)]
    public async Task<IActionResult> CancelCustomerInfo(CancelCustomerInfoRequest model, [FromQuery] string? src = "M",
        [FromQuery] string? lang = "VI")
    {
        try
        {
            LogRequest(model.UserName, model, "CancelCustomerInfo");

            var response = await _customerService.CancelCustomerInfo(model);

            LogResponse(model.UserName, response, "CancelCustomerInfo");

            return Ok(response);
        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}", "CancelCustomerInfo");
            return StatusCode(StatusCodes.Status500InternalServerError, new SystemError());
        }
    }


    /// <summary>
    /// API Quản lý KH ->  Danh sách khách hàng 
    /// </summary>
    /// <param name="model">Thông tin Request</param>
    /// <param name="src">Nguồn yêu cầu (M:Mobile, W:Web)</param>
    /// <param name="lang">Ngôn ngữ (EN: English, VI: VietNam)</param>
    /// <returns> API Quản lý KH ->  Danh sách khách hàng </returns>
    /// <remarks>
    /// Sample request:
    /// 
    ///     POST /Customer/GetCustomerList
    ///     {
    ///         "UserName": "1001-001",
    ///         "UserRole": "string",
    ///         "Custcode": "943940",
    ///         "FullName": "",
    ///         "Phone": "",
    ///         "Email": "",
    ///         "CustomerType": 0,
    ///         "Status": 0,
    ///         "PageIndex": 1,
    ///         "PageSize": 10
    ///     }
    ///
    /// </remarks>
    [HttpPost("GetCustomerList")]
    public async Task<IActionResult> GetCustomerList(CustomerListRequest model, [FromQuery] string? src = "M",
        [FromQuery] string? lang = "VI")
    {
        try
        {
            LogRequest(model.UserName, model, "GetCustomerList");

            var response = await _customerService.GetCustomerList(model);

            LogResponse(model.UserName, response, "GetCustomerList");

            return Ok(response);
        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
            return StatusCode(StatusCodes.Status500InternalServerError, new SystemError());
        }
    }

    /// <summary>
    /// API Quản lý KH ->  Danh sách khách hàng -> Chi tiết KH
    /// </summary>
    /// <param name="model">Thông tin Request</param>
    /// <param name="src">Nguồn yêu cầu (M:Mobile, W:Web)</param>
    /// <param name="lang">Ngôn ngữ (EN: English, VI: VietNam)</param>
    /// <returns> API Quản lý KH ->  Danh sách khách hàng  -> Chi tiết KH </returns>
    /// <remarks>
    /// Sample request:
    /// 
    ///     POST /Customer/GetCustomerDetailInfo
    ///     {
    ///         "UserName": "1001-001",
    ///         "UserRole": "string",
    ///         "Custcode": "943940"
    ///     }
    ///
    /// </remarks>
    [HttpPost("GetCustomerDetailInfo")]
    public async Task<IActionResult> GetCustomerDetailInfo(CustomerDetailRequest model, [FromQuery] string? src = "M",
        [FromQuery] string? lang = "VI")
    {
        try
        {
            LogRequest(model.UserName, model, "GetCustomerDetailInfo");

            var response = await _customerService.GetCustomerDetailInfo(model);

            LogResponse(model.UserName, response, "GetCustomerDetailInfo");

            return Ok(response);
        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
            return StatusCode(StatusCodes.Status500InternalServerError, new SystemError());
        }
    }

    /// <summary>
    /// API Quản lý KH ->  Danh sách khách hàng -> Chi tiết KH -> Cập nhật thông tin mở rộng
    /// </summary>
    /// <param name="model">Thông tin Request</param>
    /// <param name="src">Nguồn yêu cầu (M:Mobile, W:Web)</param>
    /// <param name="lang">Ngôn ngữ (EN: English, VI: VietNam)</param>
    /// <returns> API Quản lý KH ->  Danh sách khách hàng  -> Chi tiết KH -> Cập nhật thông tin mở rộng </returns>
    /// <remarks>
    /// Sample request:
    /// 
    ///     POST /Customer/UpdateCustomerExtendInfo
    ///     {
    ///         "UserName": "1001-001",
    ///         "UserRole": "string",
    ///         "FullName": "Chiến minh",
    ///         "Custcode": "000515",
    ///         "Address": "string",
    ///         "IsShareHolder": true,
    ///         "ShareHolderInfo": [
    ///         {
    ///        "ShareType_1": true,
    ///        "ShareType_2": true,
    ///        "ShareType_3": true,
    ///        "ShareType_4": true,
    ///        "StockCode": "FPT"
    ///         },
    ///         {
    ///         "ShareType_1": true,
    ///         "ShareType_2": true,
    ///         "ShareType_3": true,
    ///         "ShareType_4": true,
    ///         "StockCode": "VNM"
    ///         }
    ///         ],
    ///      "ExtendCust": [
    ///         {
    ///        "CategoryType": 5,
    ///        "Value": 110
    ///         },
    ///         {
    ///        "CategoryType": 6,
    ///         "Value": 220
    ///         }
    ///         ]
    ///     }
    /// </remarks>
    [HttpPost("UpdateCustomerExtendInfo")]
    public async Task<IActionResult> UpdateCustomerExtendInfo(UpdateCustomerExtendInfoRequest model,
        [FromQuery] string? src = "M",
        [FromQuery] string? lang = "VI")
    {
        try
        {
            LogRequest(model.UserName, model, "UpdateCustomerExtendInfo");

            var response = await _customerService.UpdateExtendInfo(model);

            LogResponse(model.UserName, response, "UpdateCustomerExtendInfo");

            return Ok(response);
        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}", "UpdateCustomerExtendInfo");
            return StatusCode(StatusCodes.Status500InternalServerError, new SystemError());
        }
    }

    /// <summary>
    /// API Quản lý KH ->  Danh sách khách hàng -> Chi tiết KH -> Cập nhật thông tin KYC
    /// </summary>
    /// <param name="model">Thông tin Request</param>
    /// <param name="src">Nguồn yêu cầu (M:Mobile, W:Web)</param>
    /// <param name="lang">Ngôn ngữ (EN: English, VI: VietNam)</param>
    /// <returns> API Quản lý KH ->  Danh sách khách hàng  -> Chi tiết KH -> Cập nhật thông tin KYC </returns>
    /// <remarks>
    /// Sample request:
    /// 
    ///     POST /Customer/UpdateCustomerKycInfo
    ///     {
    ///         "UserName": "1001-001",
    ///         "UserRole": "string",
    ///         "Custcode": "943940"
    ///     }
    ///
    /// </remarks>
    [HttpPost("UpdateCustomerKycInfo")]
    public async Task<IActionResult> UpdateCustomerKycInfo(UpdateKycCustomerInfoRequest model,
        [FromQuery] string? src = "M",
        [FromQuery] string? lang = "VI")
    {
        try
        {
            LogRequest(model.UserName, model, "UpdateCustomerKycInfo");

            var response = await _customerService.UpdateCustomerKycInfo(model);

            LogResponse(model.UserName, response, "UpdateCustomerKycInfo");

            return Ok(response);
        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}", "UpdateCustomerKycInfo");
            return StatusCode(StatusCodes.Status500InternalServerError, new SystemError());
        }
    }

    /// <summary>
    /// API Danh sách MTK trực tuyến -> cập nhật trạng thái tương tác
    /// </summary>
    /// <param name="model">Thông tin Request</param>
    /// <param name="src">Nguồn yêu cầu (M:Mobile, W:Web)</param>
    /// <param name="lang">Ngôn ngữ (EN: English, VI: VietNam)</param>
    /// <returns> API Danh sách MTK trực tuyến -> cập nhật trạng thái tương tác </returns>
    /// <remarks>
    /// Sample request:
    /// 
    ///     POST /Customer/UpdateCustomerActionStatus
    ///     {
    ///         "UserName": "1001-001",
    ///         "UserRole": "string",
    ///         "Id": 4393,
    ///          "ActionStatus": "300",
    ///           "SaleAssign": "1001-001",
    ///             "Description": "AHUHUHU"
    ///     }
    ///
    /// </remarks>
    [HttpPost("UpdateCustomerActionStatus")]
    public async Task<IActionResult> UpdateCustomerActionStatus(UpdateCustomerActionStatusRequest model,
        [FromQuery] string? src = "M",
        [FromQuery] string? lang = "VI")
    {
        try
        {
            LogRequest(model.UserName, model, "UpdateCustomerActionStatus");
            var response = await _customerService.UpdateActionStatus(model);
            LogResponse(model.UserName, response, "UpdateCustomerActionStatus");

            return Ok(response);
        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}", "UpdateCustomerActionStatus");
            return StatusCode(StatusCodes.Status500InternalServerError, new SystemError());
        }
    } 
    
    /*/// <summary>
    /// API Check mã khách hàng có được phép đăng ký 
    /// </summary>
    /// <param name="model">Thông tin Request</param>
    /// <param name="src">Nguồn yêu cầu (M:Mobile, W:Web)</param>
    /// <param name="lang">Ngôn ngữ (EN: English, VI: VietNam)</param>
    /// <returns> API Check validate Custcode </returns>
    /// <remarks>
    /// Sample request:
    /// 
    ///     POST /Customer/CheckValidateCustcode
    ///     {
    ///         "UserName": "1001-001",
    ///         "UserRole": "string",
    ///         "Custcode": "475869",
    ///         "CustomerType": "1"
    ///     }
    ///
    /// </remarks>
    [HttpPost("CheckValidateCustcode")]
    public async Task<IActionResult> CheckValidateCustcode(CheckValidateCustCodeRequest model,
        [FromQuery] string? src = "M",
        [FromQuery] string? lang = "VI")
    {
        try
        {
            LogRequest(model.UserName, model, "CheckValidateCustcode");

            var response = await _customerService.CheckValidateCustcode(model);

            LogResponse(model.UserName, response, "CheckValidateCustcode");

            return Ok(response);
        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}", "CheckValidateCustcode");
            return StatusCode(StatusCodes.Status500InternalServerError, new SystemError());
        }
    } */
    
    /// <summary>
    /// API chuyển đổi hồ sơ Khách hàng sang hồ sơ tài khoản
    /// </summary>
    /// <param name="model">Thông tin Request</param>
    /// <param name="src">Nguồn yêu cầu (M:Mobile, W:Web)</param>
    /// <param name="lang">Ngôn ngữ (EN: English, VI: VietNam)</param>
    /// <returns> API Check validate Custcode </returns>
    /// <remarks>
    /// Sample request:
    /// 
    ///     POST /Customer/ConvertCustomerToAccount
    ///     {
    ///      "UserName": "1268-001",
    ///      "UserRole": "CRM_SALE",
    ///      "BankInfo": [
    ///            {
    ///           "BankAccountNo": "123123",
    ///          "BankAccountName": "Chiến Proo",
    ///          "BankNo": "001",
    ///          "SubBranchNo": "101"
    ///            }
    ///         ],
    ///      "Custcode": "258698",
    ///      "FullName": "Chén prooooo",
    ///      "BirthDay": "19/05/1998",
    ///      "Gender": 1,
    ///      "CardId": "58475965",
    ///      "IssueDate": "19/05/1998",
    ///      "IssuePlace": "Hà Giang",
    ///      "Address": "Hà Noại",
    ///      "SaleID": "1268",
    ///      "ContractStatus": 2,
    ///      "AccountMargin": true,
    ///      "CreditLimit": 2,
    ///      "DebtTerm": 4,
    ///      "DepositRate": 2,
    ///      "ImageFront": "string",
    ///      "ImageBack": "string",
    ///      "ImageFace": "string",
    ///      "ManTvsi": "AHIHI",
    ///      "ManPosition": "Giasm đốc",
    ///      "ManAnPower": "12C12/222",
    ///      "ManDateAnPower": "12/05/2016",
    ///      "ManCardId": "123123123",
    ///      "ManIssueDate": "12/06/2016",
    ///      "ManIssuePlace": "HÀ NỘI",
    ///      "TradingResultSms": true,
    ///      "TradingResultEmail": true,
    ///      "Phone01": "123123123",
    ///      "Phone02": "",
    ///      "Email": "CHientm@tvsi.com.vn",
    ///      "Info01": true,
    ///      "Info02": true,
    ///      "Info03": true,
    ///      "Info04": true,
    ///      "Info05": true,
    ///      "Info06": true,
    ///      "Info07": true
    ///     }
    ///
    /// </remarks>
    [HttpPost("ConvertCustomerToAccount")]
    public async Task<IActionResult> ConvertCustomerToAccount(ConvertCustomerToAccountRequest model,
        [FromQuery] string? src = "M",
        [FromQuery] string? lang = "VI")
    {
        try
        {
            LogRequest(model.UserName, model, "ConvertCustomerToAccount");

            var response = await _customerService.ConvertCustomerToAccount(model);

            LogResponse(model.UserName, response, "ConvertCustomerToAccount");

            return Ok(response);
        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}", "ConvertCustomerToAccount");
            return StatusCode(StatusCodes.Status500InternalServerError, new SystemError());
        }
    }

    /// <summary>
    /// API Get list custcode
    /// </summary>
    /// <param name="model">Thông tin Request</param>
    /// <param name="src">Nguồn yêu cầu (M:Mobile, W:Web)</param>
    /// <param name="lang">Ngôn ngữ (EN: English, VI: VietNam)</param>
    /// <returns> API Get list custcode </returns>
    /// <remarks>
    /// Sample request:
    /// 
    ///     POST /Customer/GetListCustcode
    ///     {
    ///         "UserName": "1001-001",
    ///         "Custcode": "",
    ///         "PageIndex": 1,
    ///         "PageSize": 20
    ///     }
    ///
    /// </remarks>
    [HttpPost("GetListCustcode")]
    public async Task<IActionResult> GetListCustcode(GetCustcodeRequest model,
        [FromQuery] string? src = "M",
        [FromQuery] string? lang = "VI")
    {
        try
        {
            LogRequest(model.UserName, model, "GetListCustcode");

            var response = await _customerService.ListCustcode(model);

            LogResponse(model.UserName, response, "GetListCustcode");

            return Ok(response);
        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}", "GetListCustcode");
            return StatusCode(StatusCodes.Status500InternalServerError, new SystemError());
        }
    }

    /// <summary>
    /// API Get list activity custcode
    /// </summary>
    /// <param name="model">Thông tin Request</param>
    /// <param name="src">Nguồn yêu cầu (M:Mobile, W:Web)</param>
    /// <param name="lang">Ngôn ngữ (EN: English, VI: VietNam)</param>
    /// <returns> API Get list activity custcode </returns>
    /// <remarks>
    /// Sample request:
    /// 
    ///     POST /Customer/GetCustomerActivity
    ///     {
    ///         "UserName": "1001-001",
    ///         "UserRole": "",
    ///         "Custcode": "666966",
    ///         "PageIndex": 1,
    ///         "PageSize": 20
    ///     }
    ///
    /// </remarks>
    [HttpPost("GetCustomerActivity")]
    public async Task<IActionResult> GetCustomerActivity(ListCustomerActivityRequest model,
        [FromQuery] string? src = "M",
        [FromQuery] string? lang = "VI")
    {
        try
        {
            LogRequest(model.UserName, model, "GetCustomerActivity");

            var response = await _customerService.GetCustomerActivity(model);

            LogResponse(model.UserName, response, "GetCustomerActivity");

            return Ok(response);
        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}", "GetCustomerActivity");
            return StatusCode(StatusCodes.Status500InternalServerError, new SystemError());
        }
    }  
      
     /// <summary>
    /// API lấy danh sách đánh giá KH
    /// </summary>
    /// <param name="model">Thông tin Request</param>
    /// <param name="src">Nguồn yêu cầu (M:Mobile, W:Web)</param>
    /// <param name="lang">Ngôn ngữ (EN: English, VI: VietNam)</param>
    /// <returns> API lấy danh sách đánh giá KH </returns>
    /// <remarks>
    /// Sample request:
    /// 
    ///     POST /Customer/GetCustomerReviews
    ///     {
    ///         "UserName": "1001-001",
    ///         "UserRole": "CRM_SALE",
    ///         "Custcode": "000015"
     ///     }
    ///
    /// </remarks>
    [HttpPost("GetCustomerReviews")]
    public async Task<IActionResult> GetCustomerReviews(CustomerReviewsRequest model,
        [FromQuery] string? src = "M",
        [FromQuery] string? lang = "VI")
    {
        try
        {
            LogRequest(model.UserName, model, "GetCustomerReviews");

            var response = await _customerService.GetCustomerReviews(model);

            LogResponse(model.UserName, response, "GetCustomerReviews");

            return Ok(response);
        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
            return StatusCode(StatusCodes.Status500InternalServerError, new SystemError());
        }
    } 
     
     /// <summary>
    /// API lay thong tin chi tiet kyc
    /// </summary>
    /// <param name="model">Thông tin Request</param>
    /// <param name="src">Nguồn yêu cầu (M:Mobile, W:Web)</param>
    /// <param name="lang">Ngôn ngữ (EN: English, VI: VietNam)</param>
     /// <returns> API lấy danh sách đánh giá KH </returns>
     /// <remarks>
     /// Sample request:
     /// 
     ///     POST /Customer/GetKycCustomerDetailInfo
     ///     {
     ///        "UserName": "1268-001",
     ///        "UserRole": "CRM_SALE",
     ///        "CustCode": "000515"
     ///      }
     ///
     /// </remarks>
    [HttpPost("GetKycCustomerDetailInfo")]
    public async Task<IActionResult> GetKycCustomerDetailInfo(GetKycCustomerRequest model,
        [FromQuery] string? src = "M",
        [FromQuery] string? lang = "VI")
    {
        try
        {
            LogRequest(model.UserName, model, "GetKycCustomerDetailInfo");

            var response = await _commonService.GetKycCustomerInfo(model);

            LogResponse(model.UserName, response, "GetKycCustomerDetailInfo");
            return Ok(response);
        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}", "GetKycCustomerDetailInfo");
            return StatusCode(StatusCodes.Status500InternalServerError, new SystemError());
        }
    }   
     
     /// <summary>
    /// API lay thong tin trạng thái tương tác
    /// </summary>
    /// <param name="model">Thông tin Request</param>
    /// <param name="src">Nguồn yêu cầu (M:Mobile, W:Web)</param>
    /// <param name="lang">Ngôn ngữ (EN: English, VI: VietNam)</param>
     /// <returns> API lấy danh sách đánh giá KH </returns>
     /// <remarks>
     /// Sample request:
     /// 
     ///     POST /Customer/GetActionStatusDetail
     ///     {
     ///        "UserName": "1268-001",
     ///        "Id": 123123
     ///      }
     ///
     /// </remarks>
    [HttpPost("GetActionStatusDetail")]
    public async Task<IActionResult> GetActionStatusDetail(GetActionStatusDetailRequest model,
        [FromQuery] string? src = "M",
        [FromQuery] string? lang = "VI")
    {
        try
        {
            LogRequest(model.UserName, model, "GetKycCustomerDetailInfo");

            var response = await _customerService.GetActionStatusDetail(model);

            LogResponse(model.UserName, response, "GetKycCustomerDetailInfo");
            return Ok(response);
        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}", "GetKycCustomerDetailInfo");
            return StatusCode(StatusCodes.Status500InternalServerError, new SystemError());
        }
    }
     
     
     
     
     /// <summary>
    /// API kích hoạt tài khoản khách hàng eKYC
    /// </summary>
    /// <param name="model">Thông tin Request</param>
    /// <param name="src">Nguồn yêu cầu (M:Mobile, W:Web)</param>
    /// <param name="lang">Ngôn ngữ (EN: English, VI: VietNam)</param>
     /// <returns> API kích hoạt tài khoản khách hàng eKYC </returns>
     /// <remarks>
     /// Sample request:
     /// 
     ///     POST /Customer/ConfirmEkyc
     ///     {
     ///        "UserName": "1268-001",
     ///        "OpenSource": 4,
     ///        "ConfirmCode": "57acb0cf8723a7c03faf9196252b8d8c"
     ///      }
     ///
     /// </remarks>
    [HttpPost("ConfirmEkyc")]
    public async Task<IActionResult> ConfirmEkyc(ConfirmEkycRequest model,
        [FromQuery] string? src = "M",
        [FromQuery] string? lang = "VI")
    {
        try
        {
            LogRequest(model.UserName, model, "GetKycCustomerDetailInfo");

            var response = await _customerService.ConfirmEkycRequest(model);

            LogResponse(model.UserName, response, "GetKycCustomerDetailInfo");
            return Ok(response);
        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}", "GetKycCustomerDetailInfo");
            return StatusCode(StatusCodes.Status500InternalServerError, new SystemError());
        }
    }
    
}