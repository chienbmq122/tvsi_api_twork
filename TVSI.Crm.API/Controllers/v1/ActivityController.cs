﻿using Microsoft.AspNetCore.Mvc;
using System.Reflection;
using TVSI.Crm.API.Lib.Models;
using TVSI.Crm.API.Lib.Models.Request.Activity;
using TVSI.Crm.API.Lib.Services;

namespace TVSI.Crm.API.Controllers.v1;

[ApiVersion("1.0")]
public class ActivityController : BaseController
{
    private readonly IActivityService _activityService;


    /// <summary>
    /// Controller Constructor
    /// </summary>
    public ActivityController(IActivityService activityService)
    {
        _activityService = activityService;
    }

    /// <summary>
    /// API Lấy danh sách hoạt động
    /// </summary>
    /// <param name="model">Thông tin Request</param>
    /// <param name="src">Nguồn yêu cầu (M:Mobile, W:Web)</param>
    /// <param name="lang">Ngôn ngữ (EN: English, VI: VietNam)</param>
    /// <returns>Danh sách hoạt động</returns>
    /// <remarks>
    /// Sample request:
    /// 
    ///     POST /Activity/GetActivity
    ///     {
    ///        "UserName": "1268-001",
    ///        "UserRole": "CRM_SALE",
    ///        "EventType": 1,
    ///        "DateIndex": "07/07/2022",
    ///     }
    ///
    /// </remarks>
    [HttpPost("GetActivity")]
    public async Task<IActionResult> GetActivity(ActivityRequestByEventType model, [FromQuery] string? src = "M",
        [FromQuery] string? lang = "VI")
    {
        try
        {
            LogRequest(model.UserName, model, "GetActivity");

            var response = await _activityService.GetActivity(model);

            LogResponse(model.UserName, response, "GetActivity");

            return Ok(response);
        }
        catch (Exception ex)
        {
            LogException(model.UserName, $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}", "GetActivity");
            return StatusCode(StatusCodes.Status500InternalServerError, new SystemError());
        }
    }

    /// <summary>
    /// API Lấy filter hoạt động
    /// </summary>
    /// <param name="model">Thông tin Request</param>
    /// <param name="src">Nguồn yêu cầu (M:Mobile, W:Web)</param>
    /// <param name="lang">Ngôn ngữ (EN: English, VI: VietNam)</param>
    /// <returns>filter hoạt động</returns>
    /// <remarks>
    /// Sample request:
    /// 
    ///     POST /Activity/GetDetailActivity
    ///     {
    ///        "UserName": "1268-001",
    ///        "UserRole": "CRM_SALE",
    ///        "FromDate": "2022-07-07",
    ///        "ToDate": "2022-07-07",
    ///        "EventType": 1,
    ///        "Assigned": "1268-001",
    ///        "Priority": 1,
    ///        "Status": 0,
    ///        "PageIndex": 1,
    ///        "PageSize": 10,
    ///     }
    ///
    /// </remarks>
    [HttpPost("GetActivityByFiltter")]
    public async Task<IActionResult> GetActivityByFiltter(FilterActivityRequest model, [FromQuery] string? src = "M",
        [FromQuery] string? lang = "VI")
    {
        try
        {
            LogRequest(model.UserName, model, "GetActivityByFiltter");

            var response = await _activityService.GetActivityByFiltter(model);

            LogResponse(model.UserName, response, "GetActivityByFiltter");

            return Ok(response);
        }
        catch (Exception ex)
        {
            LogException(model.UserName, $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}", "GetActivityByFiltter");
            return StatusCode(StatusCodes.Status500InternalServerError, new SystemError());
        }
    }

    /// <summary>
    /// API Lấy chi tiết hoạt động
    /// </summary>
    /// <param name="model">Thông tin Request</param>
    /// <param name="src">Nguồn yêu cầu (M:Mobile, W:Web)</param>
    /// <param name="lang">Ngôn ngữ (EN: English, VI: VietNam)</param>
    /// <returns>chi tiết hoạt động</returns>
    /// <remarks>
    /// Sample request:
    /// 
    ///     POST /Activity/GetDetailActivity
    ///     {
    ///        "UserName": "1268-001",
    ///        "UserRole": "CRM_SALE",
    ///        "ActivityId": 90292,
    ///     }
    ///
    /// </remarks>
    [HttpPost("GetDetailActivity")]
    public async Task<IActionResult> GetDetailActivity(DeleteActivityRequest model, [FromQuery] string? src = "M",
        [FromQuery] string? lang = "VI")
    {
        try
        {
            LogRequest(model.UserName, model, "GetDetailActivity");

            var response = await _activityService.GetDetailActivity(model);

            LogResponse(model.UserName, response, "GetDetailActivity");

            return Ok(response);
        }
        catch (Exception ex)
        {
            LogException(model.UserName, $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}", "GetDetailActivity");
            return StatusCode(StatusCodes.Status500InternalServerError, new SystemError());
        }
    }

    /// <summary>
    /// API thêm mới hoạt động
    /// </summary>
    /// <param name="model">Thông tin Request</param>
    /// <param name="src">Nguồn yêu cầu (M:Mobile, W:Web)</param>
    /// <param name="lang">Ngôn ngữ (EN: English, VI: VietNam)</param>
    /// <returns>thêm mới hoạt động</returns>
    /// <remarks>
    /// Sample request:
    /// 
    ///     POST /Activity/CreateActivity
    ///     {
    ///        "UserName": "string",
    ///        "UserRole": "string",
    ///        "LeadId": 0,
    ///        "Custcode": "string",
    ///        "Title": "string",
    ///        "Type": 0,
    ///        "EventType": 0,
    ///        "StartDate": "string",
    ///        "EndDate": "string",
    ///        "Location": "string",
    ///        "Priority": 0,
    ///        "Status": 0,
    ///        "Approved": "string",
    ///        "Assigned": "string",
    ///        "Detail": "string",
    ///        "ReasonType": 0
    ///     }
    ///
    /// </remarks>
    [HttpPost("CreateActivity")]
    public async Task<IActionResult> CreateActivity(CreateActivityRequest model, [FromQuery] string? src = "M",
        [FromQuery] string? lang = "VI")
    {
        try
        {
            LogRequest(model.UserName, model, "CreateActivity");

            var response = await _activityService.CreateActivity(model);

            LogResponse(model.UserName, response, "CreateActivity");

            return Ok(response);
        }
        catch (Exception ex)
        {
            LogException(model.UserName, $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}", "CreateActivity");
            return StatusCode(StatusCodes.Status500InternalServerError, new SystemError());
        }
    }

    /// <summary>
    /// API update hoạt động
    /// </summary>
    /// <param name="model">Thông tin Request</param>
    /// <param name="src">Nguồn yêu cầu (M:Mobile, W:Web)</param>
    /// <param name="lang">Ngôn ngữ (EN: English, VI: VietNam)</param>
    /// <returns>update hoạt động</returns>
    /// <remarks>
    /// Sample request:
    /// 
    ///     POST /Activity/UpdateActivity
    ///     {
    ///        "UserName": "1268-001",
    ///        "UserRole": "CRM_SALE",
    ///        "ActivityId": 90292,
    ///     }
    ///
    /// </remarks>
    [HttpPost("UpdateActivity")]
    public async Task<IActionResult> UpdateActivity(UpdateActivityRequest model, [FromQuery] string? src = "M",
        [FromQuery] string? lang = "VI")
    {
        try
        {
            LogRequest(model.UserName, model, "UpdateActivity");

            var response = await _activityService.UpdateActivity(model);

            LogResponse(model.UserName, response, "UpdateActivity");

            return Ok(response);
        }
        catch (Exception ex)
        {
            LogException(model.UserName, $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}", "UpdateActivity");
            return StatusCode(StatusCodes.Status500InternalServerError, new SystemError());
        }
    }

    /// <summary>
    /// API delete hoạt động
    /// </summary>
    /// <param name="model">Thông tin Request</param>
    /// <param name="src">Nguồn yêu cầu (M:Mobile, W:Web)</param>
    /// <param name="lang">Ngôn ngữ (EN: English, VI: VietNam)</param>
    /// <returns>delete hoạt động</returns>
    /// <remarks>
    /// Sample request:
    /// 
    ///     POST /Activity/DeleteActivity
    ///     {
    ///        "UserName": "1268-001",
    ///        "UserRole": "CRM_SALE",
    ///        "ActivityId": 90292,
    ///     }
    ///
    /// </remarks>
    [HttpPost("DeleteActivity")]
    public async Task<IActionResult> DeleteActivity(DeleteActivityRequest model, [FromQuery] string? src = "M",
        [FromQuery] string? lang = "VI")
    {
        try
        {
            LogRequest(model.UserName, model, "DeleteActivity");

            var response = await _activityService.DeleteActivity(model);

            LogResponse(model.UserName, response, "DeleteActivity");

            return Ok(response);
        }
        catch (Exception ex)
        {
            LogException(model.UserName, $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}", "DeleteActivity");
            return StatusCode(StatusCodes.Status500InternalServerError, new SystemError());
        }
    }

    /// <summary>
    /// API update hoặc delete nhiều task
    /// </summary>
    /// <param name="model">Thông tin Request</param>
    /// <param name="src">Nguồn yêu cầu (M:Mobile, W:Web)</param>
    /// <param name="lang">Ngôn ngữ (EN: English, VI: VietNam)</param>
    /// <returns>update hoặc delete nhiều task</returns>
    /// <remarks>
    /// Sample request:
    /// 
    ///     POST /Activity/UpdateOrDeleteListActivity
    ///     {
    ///        "UserName": "1268-001",
    ///        "UserRole": "CRM_SALE",
    ///        "ActivityId": 90292,
    ///     }
    ///
    /// </remarks>
    [HttpPost("UpdateOrDeleteListActivity")]
    public async Task<IActionResult> UpdateOrDeleteListActivity(UpdateListActivityRequest model, [FromQuery] string? src = "M",
        [FromQuery] string? lang = "VI")
    {
        try
        {
            LogRequest(model.UserName, model, "UpdateOrDeleteListActivity");

            var response = await _activityService.UpdateOrDeleteListActivity(model);

            LogResponse(model.UserName, response, "UpdateOrDeleteListActivity");

            return Ok(response);
        }
        catch (Exception ex)
        {
            LogException(model.UserName, $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}", "UpdateOrDeleteListActivity");
            return StatusCode(StatusCodes.Status500InternalServerError, new SystemError());
        }
    }

    /// <summary>
    /// API confirm hoặc reject task
    /// </summary>
    /// <param name="model">Thông tin Request</param>
    /// <param name="src">Nguồn yêu cầu (M:Mobile, W:Web)</param>
    /// <param name="lang">Ngôn ngữ (EN: English, VI: VietNam)</param>
    /// <returns>confirm hoặc reject task</returns>
    /// <remarks>
    /// Sample request:
    /// 
    ///     POST /Activity/ConfirmOrRejectActivity
    ///     {
    ///        "UserName": "1268-001",
    ///        "UserRole": "CRM_SALE",
    ///        "ActivityId": 90292,
    ///     }
    ///
    /// </remarks>
    [HttpPost("ConfirmOrRejectActivity")]
    public async Task<IActionResult> ConfirmOrRejectActivity(ConfirmOrRejectRequest model, [FromQuery] string? src = "M",
        [FromQuery] string? lang = "VI")
    {
        try
        {
            LogRequest(model.UserName, model, "ConfirmOrRejectActivity");

            var response = await _activityService.ConfirmOrRejectActivity(model);

            LogResponse(model.UserName, response, "ConfirmOrRejectActivity");

            return Ok(response);
        }
        catch (Exception ex)
        {
            LogException(model.UserName, $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}", "ConfirmOrRejectActivity");
            return StatusCode(StatusCodes.Status500InternalServerError, new SystemError());
        }
    }


    /// <summary>
    /// API đếm số hoạt độngh từng ngày trong tháng
    /// </summary>
    /// <param name="model">Thông tin Request</param>
    /// <param name="src">Nguồn yêu cầu (M:Mobile, W:Web)</param>
    /// <param name="lang">Ngôn ngữ (EN: English, VI: VietNam)</param>
    /// <returns>đếm số hoạt độngh từng ngày trong tháng</returns>
    /// <remarks>
    /// Sample request:
    /// 
    ///     POST /Activity/GetHisTimeWorkStaffByMonth
    ///     {
    ///        "UserName": "1268-001",
    ///        "UserRole": "CRM_SALE",
    ///        "DateIndex": "07/07/2022"
    ///     }
    ///
    /// </remarks>
    [HttpPost("GetHisTimeWorkStaffByMonth")]
    public async Task<IActionResult> GetHisTimeWorkStaffByMonth(ActivityDataMonthRequest model, [FromQuery] string? src = "M",
        [FromQuery] string? lang = "VI")
    {
        try
        {
            LogRequest(model.UserName, model, "GetHisTimeWorkStaffByMonth");

            var response = await _activityService.GetHisTimeWorkStaffByMonth(model);

            LogResponse(model.UserName, response, "GetHisTimeWorkStaffByMonth");

            return Ok(response);
        }
        catch (Exception ex)
        {
            LogException(model.UserName, $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}", "GetHisTimeWorkStaffByMonth");
            return StatusCode(StatusCodes.Status500InternalServerError, new SystemError());
        }
    }

    /// <summary>
    /// API type hoạt động
    /// </summary>
    /// <param name="model">Thông tin Request</param>
    /// <param name="src">Nguồn yêu cầu (M:Mobile, W:Web)</param>
    /// <param name="lang">Ngôn ngữ (EN: English, VI: VietNam)</param>
    /// <returns>type hoạt động</returns>
    /// <remarks>
    /// Sample request:
    /// 
    ///     POST /Activity/GetActivityType
    ///     {
    ///        "UserName": "1268-001",
    ///        "UserRole": "CRM_SALE",
    ///     }
    ///
    /// </remarks>
    [HttpPost("GetActivityType")]
    public async Task<IActionResult> GetActivityType(BaseRequest model, [FromQuery] string? src = "M",
        [FromQuery] string? lang = "VI")
    {
        try
        {
            LogRequest(model.UserName, model, "GetActivityType");

            var response = await _activityService.GetActivityType(model);

            LogResponse(model.UserName, response, "GetActivityType");

            return Ok(response);
        }
        catch (Exception ex)
        {
            LogException(model.UserName, $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}", "GetActivityType");
            return StatusCode(StatusCodes.Status500InternalServerError, new SystemError());
        }
    }
}