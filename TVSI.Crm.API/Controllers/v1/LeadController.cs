﻿using System;
using System.Net;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TVSI.Crm.API.Lib.Models;
using TVSI.Crm.API.Lib.Models.Request.Lead;
using TVSI.Crm.API.Lib.Services;

namespace TVSI.Crm.API.Controllers.v1;

/// <summary>
/// Lead Module API
/// </summary>
[ApiVersion("1.0")]
public class LeadController : BaseController
{
    private readonly ILeadService _leadService;


    /// <summary>
    /// Controller Constructor
    /// </summary>
    public LeadController(ILeadService leadService)
    {
        _leadService = leadService;
    }

    /// <summary>
    /// API Lấy danh sách khách hàng tiềm năng
    /// </summary>
    /// <param name="model">Thông tin Request</param>
    /// <param name="src">Nguồn yêu cầu (M:Mobile, W:Web)</param>
    /// <param name="lang">Ngôn ngữ (EN: English, VI: VietNam)</param>
    /// <returns>Danh sách khách hàng tiềm năng</returns>
    /// <remarks>
    /// Sample request:
    /// 
    ///     POST /Lead/GetLeadListInfo
    ///     {
    ///        "UserName": "0824-001",
    ///        "UserRole": "CRM_SALE",
    ///        "Name": "",
    ///        "Phone": "",
    ///        "Email": "",
    ///        "LeadSourceID": 0,
    ///        "ProfileType": 0,
    ///        "Status": 0,
    ///        "PageIndex": 1,
    ///        "PageSize": 20
    ///     }
    ///
    /// </remarks>
    [HttpPost("GetLeadListInfo")]
    public async Task<IActionResult> GetLeadListInfo(LeadListRequest model, [FromQuery] string? src = "M",
        [FromQuery] string? lang = "VI")
    {
        try
        {
            LogRequest(model.UserName, model, "GetLeadListInfo");

            var response = await _leadService.GetLeadList(model, lang);

            LogResponse(model.UserName, response, "GetLeadListInfo");

            return Ok(response);
        }
        catch (Exception ex)
        {
            LogException(model.UserName, $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}", "GetLeadListInfo");
            return StatusCode(StatusCodes.Status500InternalServerError, new SystemError());
        }
    }

    /// <summary>
    /// API tạo mới khách hàng tiềm năng
    /// </summary>
    /// <param name="model">Thông tin Request</param>
    /// <param name="src">Nguồn yêu cầu (M:Mobile, W:Web)</param>
    /// <param name="lang">Ngôn ngữ (EN: English, VI: VietNam)</param>
    /// <returns>Tạo mới khách hàng tiềm năng</returns>
    /// <remarks>
    /// Sample request:
    /// 
    ///     POST /Lead/CreateLead
    ///     {
    ///        "UserName": "0824-001",
    ///        "UserRole": "CRM_SALE",
    ///        "Name": "Hứa Văn Chiến",
    ///        "Phone": "09999999999",
    ///        "Email": "khongBietDungMail@gmail.com",
    ///        "Address": "Gầm cầu",
    ///        "ProfileType": 1,
    ///        "BranchID": 1,
    ///        "AssignUser": "0824-001",
    ///        "Description": "Khách hàng nhiều tiền",
    ///        "LeadSourceID": 1,
    ///        "Attributes": [
    ///          {
    ///             "AttributeID": 1,
    ///             "Value": "29/02/1998",
    ///             "Priority": 1
    ///          }
    ///        ]
    ///     }
    ///
    /// </remarks>
    [HttpPost("CreateLead")]
    public async Task<IActionResult> CreateLead(CreateLeadRequest model, [FromQuery] string? src = "M",
        [FromQuery] string? lang = "VI")
    {
        try
        {
            LogRequest(model.UserName, model, "CreateLead");

            var response = await _leadService.CreateLead(model, lang);

            LogResponse(model.UserName, response, "CreateLead");

            return Ok(response);
        }
        catch (Exception ex)
        {
            LogException(model.UserName, $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}", "CreateLead");
            return StatusCode(StatusCodes.Status500InternalServerError, new SystemError());
        }
    }

    /// <summary>
    /// API Lấy danh sách hoạt động khách hàng tiềm năng
    /// </summary>
    /// <param name="model">Thông tin Request</param>
    /// <param name="src">Nguồn yêu cầu (M:Mobile, W:Web)</param>
    /// <param name="lang">Ngôn ngữ (EN: English, VI: VietNam)</param>
    /// <returns>Danh sách hoạt động khách hàng tiềm năng</returns>
    /// <remarks>
    /// Sample request:
    /// 
    ///     POST /Lead/GetLeadActivity
    ///     {
    ///        "UserName": "0824-001",
    ///        "UserRole": "CRM_SALE",
    ///        "LeadID": 90110,
    ///        "PageIndex": 1,
    ///        "PageSize": 20
    ///     }
    ///
    /// </remarks>
    [HttpPost("GetLeadActivity")]
    public async Task<IActionResult> GetLeadActivity(ListActivityRequest model, [FromQuery] string? src = "M",
        [FromQuery] string? lang = "VI")
    {
        try
        {
            LogRequest(model.UserName, model, "GetLeadActivity");

            var response = await _leadService.GetLeadActivity(model, lang);

            LogResponse(model.UserName, response, "GetLeadActivity");

            return Ok(response);
        }
        catch (Exception ex)
        {
            LogException(model.UserName, $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}", "GetLeadActivity");
            return StatusCode(StatusCodes.Status500InternalServerError, new SystemError());
        }
    }

    /// <summary>
    /// API Lấy chi tiết khách hàng tiềm năng
    /// </summary>
    /// <param name="model">Thông tin Request</param>
    /// <param name="src">Nguồn yêu cầu (M:Mobile, W:Web)</param>
    /// <param name="lang">Ngôn ngữ (EN: English, VI: VietNam)</param>
    /// <returns>Chi tiết khách hàng tiềm năng</returns>
    /// <remarks>
    /// Sample request:
    /// 
    ///     POST /Lead/GetDetailLead
    ///     {
    ///        "UserName": "0824-001",
    ///        "UserRole": "CRM_SALE",
    ///        "LeadID": LeadID
    ///     }
    ///
    /// </remarks>
    [HttpPost("GetDetailLead")]
    public async Task<IActionResult> GetDetailLead(DetailLeadRequest model, [FromQuery] string? src = "M",
        [FromQuery] string? lang = "VI")
    {
        try
        {
            LogRequest(model.UserName, model, "GetDetailLead");

            var response = await _leadService.GetDetailLead(model, lang);

            LogResponse(model.UserName, response, "GetDetailLead");

            return Ok(response);
        }
        catch (Exception ex)
        {
            LogException(model.UserName, $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}", "GetDetailLead");
            return StatusCode(StatusCodes.Status500InternalServerError, new SystemError());
        }
    }

    /// <summary>
    /// API Lấy danh sách SRC khách hàng tiềm năng
    /// </summary>
    /// <param name="model">Thông tin Request</param>
    /// <param name="src">Nguồn yêu cầu (M:Mobile, W:Web)</param>
    /// <param name="lang">Ngôn ngữ (EN: English, VI: VietNam)</param>
    /// <returns>Danh sách SRC khách hàng tiềm năng</returns>
    /// <remarks>
    /// Sample request:
    /// 
    ///     POST /Lead/GetSrcLead
    ///     {
    ///        "UserName": "0824-001",
    ///        "UserRole": "CRM_SALE",
    ///        "LeadID": 90110,
    ///        "PageIndex": 1,
    ///        "PageSize": 20
    ///     }
    ///
    /// </remarks>
    [HttpPost("GetSrcLead")]
    public async Task<IActionResult> GetSrcLead(BaseRequest model, [FromQuery] string? src = "M",
        [FromQuery] string? lang = "VI")
    {
        try
        {
            LogRequest(model.UserName, model, "GetSrcLead");

            var response = await _leadService.GetSrcLead(model);

            LogResponse(model.UserName, response, "GetSrcLead");

            return Ok(response);
        }
        catch (Exception ex)
        {
            LogException(model.UserName, $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}", "GetSrcLead");
            return StatusCode(StatusCodes.Status500InternalServerError, new SystemError());
        }
    }

    /// <summary>
    /// API DELETE lead
    /// </summary>
    /// <param name="model">Thông tin Request</param>
    /// <param name="src">Nguồn yêu cầu (M:Mobile, W:Web)</param>
    /// <param name="lang">Ngôn ngữ (EN: English, VI: VietNam)</param>
    /// <returns> DELETE lead</returns>
    /// <remarks>
    /// Sample request:
    /// 
    ///     POST /Lead/DeleteLead
    ///     {
    ///        "UserName": "0824-001",
    ///        "UserRole": "CRM_SALE",
    ///        "LeadID": 90110,
    ///        "PageIndex": 1,
    ///        "PageSize": 20
    ///     }
    ///
    /// </remarks>
    [HttpPost("DeleteLead")]
    public async Task<IActionResult> DeleteLead(DeleteLead model, [FromQuery] string? src = "M",
        [FromQuery] string? lang = "VI")
    {
        try
        {
            LogRequest(model.UserName, model, "DeleteLead");

            var response = await _leadService.DeleteLead(model);

            LogResponse(model.UserName, response, "DeleteLead");

            return Ok(response);
        }
        catch (Exception ex)
        {
            LogException(model.UserName, $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}", "DeleteLead");
            return StatusCode(StatusCodes.Status500InternalServerError, new SystemError());
        }
    }

    /// <summary>
    /// API UPDATE lead
    /// </summary>
    /// <param name="model">Thông tin Request</param>
    /// <param name="src">Nguồn yêu cầu (M:Mobile, W:Web)</param>
    /// <param name="lang">Ngôn ngữ (EN: English, VI: VietNam)</param>
    /// <returns> UPDATE lead</returns>
    /// <remarks>
    /// Sample request:
    /// 
    ///     POST /Lead/UpdateLead
    ///     {
    ///        "UserName": "0824-001",
    ///        "UserRole": "CRM_SALE",
    ///        "LeadID": 90110,
    ///        "PageIndex": 1,
    ///        "PageSize": 20
    ///     }
    ///
    /// </remarks>
    [HttpPost("UpdateLead")]
    public async Task<IActionResult> UpdateLead(UpdateLeadRequest model, [FromQuery] string? src = "M",
        [FromQuery] string? lang = "VI")
    {
        try
        {
            LogRequest(model.UserName, model, "UpdateLead");

            var response = await _leadService.UpdateLead(model);

            LogResponse(model.UserName, response, "UpdateLead");

            return Ok(response);
        }
        catch (Exception ex)
        {
            LogException(model.UserName, $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}", "UpdateLead");
            return StatusCode(StatusCodes.Status500InternalServerError, new SystemError());
        }
    }

    /// <summary>
    /// API covert lead to customer
    /// </summary>
    /// <param name="model">Thông tin Request</param>
    /// <param name="src">Nguồn yêu cầu (M:Mobile, W:Web)</param>
    /// <param name="lang">Ngôn ngữ (EN: English, VI: VietNam)</param>
    /// <returns>covert lead to customer</returns>
    /// <remarks>
    /// Sample request:
    /// 
    ///     POST /Lead/UpdateLead
    ///     {
    ///        "UserName": "0824-001",
    ///        "UserRole": "CRM_SALE",
    ///        "LeadID": 90110,
    ///        "CustCode": "222222"
    ///     }
    ///
    /// </remarks>
    [HttpPost("CovertLead")]
    public async Task<IActionResult> CovertLead(ConverLeadToCust model, [FromQuery] string? src = "M",
        [FromQuery] string? lang = "VI")
    {
        try
        {
            LogRequest(model.UserName, model, "CovertLead");

            var response = await _leadService.CovertLead(model);

            LogResponse(model.UserName, response, "CovertLead");

            return Ok(response);
        }
        catch (Exception ex)
        {
            LogException(model.UserName, $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}", "CovertLead");
            return StatusCode(StatusCodes.Status500InternalServerError, new SystemError());
        }
    }
}