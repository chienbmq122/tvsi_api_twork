﻿using Microsoft.AspNetCore.Mvc;
using System.Reflection;
using TVSI.Crm.API.Lib.Models;
using TVSI.Crm.API.Lib.Models.Request.SmartOtp;
using TVSI.Crm.API.Lib.Services;

namespace TVSI.Crm.API.Controllers.v1
{
    [ApiVersion("1.0")]
    public class SmartOtpController : BaseController
    {
        private readonly ISmartOtpService _smartOtpService;

        /// <summary>
        /// Controller Constructor
        /// </summary>
        public SmartOtpController(ISmartOtpService smartOtpService)
        {
            _smartOtpService = smartOtpService;
        }

        /// <summary>
        /// API đăng ký smartotp nhưng chưa kích hoạt
        /// </summary>
        /// <param name="model">Thông tin Request</param>
        /// <param name="src">Nguồn yêu cầu (M:Mobile, W:Web)</param>
        /// <param name="lang">Ngôn ngữ (EN: English, VI: VietNam)</param>
        /// <returns>Đăng ký smartotp nhưng chưa kích hoạt</returns>
        /// <remarks>
        /// Sample request:
        /// 
        ///     POST /SmartOtp/RegisterSmartOtp
        ///     {
        ///        "UserName": "1917-001",
        ///        "UserRole": "CRM_SALE",
        ///        "deviceId": "",
        ///        "smartPin": ""
        ///     }
        ///
        /// </remarks>
        [HttpPost("RegisterSmartOtp")]
        public async Task<IActionResult> RegisterSmartOtp(RegisterDeviceRequest model, [FromQuery] string? src = "M",
            [FromQuery] string? lang = "VI")
        {
            try
            {
                var response = await _smartOtpService.RegisterSmartOtp(model);

                return Ok(response);
            }
            catch (Exception ex)
            {
                LogException(model.UserName, $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
                return StatusCode(StatusCodes.Status500InternalServerError, new SystemError());
            }
        }

        /// <summary>
        /// API check đăng ký hay chưa
        /// </summary>
        /// <param name="model">Thông tin Request</param>
        /// <param name="src">Nguồn yêu cầu (M:Mobile, W:Web)</param>
        /// <param name="lang">Ngôn ngữ (EN: English, VI: VietNam)</param>
        /// <returns>check đăng ký hay chưa</returns>
        /// <remarks>
        /// Sample request:
        /// 
        ///     POST /SmartOtp/CheckDeviceSmartOtp
        ///     {
        ///        "UserName": "1917-001",
        ///        "UserRole": "CRM_SALE",
        ///        "deviceId": ""
        ///     }
        ///
        /// </remarks>
        [HttpPost("CheckDeviceSmartOtp")]
        public async Task<IActionResult> CheckDeviceSmartOtp(CheckSmartOtpDeviceRequest model, [FromQuery] string? src = "M",
            [FromQuery] string? lang = "VI")
        {
            try
            {
                var response = await _smartOtpService.CheckDeviceSmartOtp(model);

                return Ok(response);
            }
            catch (Exception ex)
            {
                LogException(model.UserName, $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
                return StatusCode(StatusCodes.Status500InternalServerError, new SystemError());
            }
        }

        /// <summary>
        /// API kích hoạt otp
        /// </summary>
        /// <param name="model">Thông tin Request</param>
        /// <param name="src">Nguồn yêu cầu (M:Mobile, W:Web)</param>
        /// <param name="lang">Ngôn ngữ (EN: English, VI: VietNam)</param>
        /// <returns>kích hoạt otp</returns>
        /// <remarks>
        /// Sample request:
        /// 
        ///     POST /SmartOtp/ActiveSmartOtp
        ///     {
        ///        "UserName": "1917-001",
        ///        "UserRole": "CRM_SALE",
        ///        "deviceId": "",
        ///        "otp": ""
        ///     }
        ///
        /// </remarks>
        [HttpPost("ActiveSmartOtp")]
        public async Task<IActionResult> ActiveSmartOtp(ActiveOtpRequest model, [FromQuery] string? src = "M",
            [FromQuery] string? lang = "VI")
        {
            try
            {
                var response = await _smartOtpService.ActiveSmartOtp(model);

                return Ok(response);
            }
            catch (Exception ex)
            {
                LogException(model.UserName, $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
                return StatusCode(StatusCodes.Status500InternalServerError, new SystemError());
            }
        }

        /// <summary>
        /// API Hủy đăng ký
        /// </summary>
        /// <param name="model">Thông tin Request</param>
        /// <param name="src">Nguồn yêu cầu (M:Mobile, W:Web)</param>
        /// <param name="lang">Ngôn ngữ (EN: English, VI: VietNam)</param>
        /// <returns>Hủy đăng ký</returns>
        /// <remarks>
        /// Sample request:
        /// 
        ///     POST /SmartOtp/UnRegisterSmartOtp
        ///     {
        ///        "UserName": "1917-001",
        ///        "UserRole": "CRM_SALE",
        ///        "deviceId": "",
        ///        "otp": ""
        ///     }
        ///
        /// </remarks>
        [HttpPost("UnRegisterSmartOtp")]
        public async Task<IActionResult> UnRegisterSmartOtp(ActiveOtpRequest model, [FromQuery] string? src = "M",
            [FromQuery] string? lang = "VI")
        {
            try
            {
                var response = await _smartOtpService.UnRegisterSmartOtp(model);

                return Ok(response);
            }
            catch (Exception ex)
            {
                LogException(model.UserName, $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
                return StatusCode(StatusCodes.Status500InternalServerError, new SystemError());
            }
        }

        /// <summary>
        /// API check otp
        /// </summary>
        /// <param name="model">Thông tin Request</param>
        /// <param name="src">Nguồn yêu cầu (M:Mobile, W:Web)</param>
        /// <param name="lang">Ngôn ngữ (EN: English, VI: VietNam)</param>
        /// <returns>check otp</returns>
        /// <remarks>
        /// Sample request:
        /// 
        ///     POST /SmartOtp/VerifySmartOtp
        ///     {
        ///        "UserName": "1917-001",
        ///        "UserRole": "CRM_SALE",
        ///        "deviceId": "",
        ///        "otp": "",
        ///        "otpTime": "26/08/2022 10:10:10"
        ///     }
        ///
        /// </remarks>
        [HttpPost("VerifySmartOtp")]
        public async Task<IActionResult> VerifySmartOtp(SmartOtpRequest model, [FromQuery] string? src = "M",
            [FromQuery] string? lang = "VI")
        {
            try
            {
                var response = await _smartOtpService.VerifySmartOtp(model);

                return Ok(response);
            }
            catch (Exception ex)
            {
                LogException(model.UserName, $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
                return StatusCode(StatusCodes.Status500InternalServerError, new SystemError());
            }
        }

    }
}
