﻿using System;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TVSI.Crm.API.Lib.Models;
using TVSI.Crm.API.Lib.Services;

namespace TVSI.Crm.API.Controllers.v1;

/// <summary>
/// API Tạm
/// </summary>
public class TempController :  BaseController
{
    private readonly IDistributeCacheService _distributeCacheService;

    public TempController(IDistributeCacheService distributeCacheService)
    {
        _distributeCacheService = distributeCacheService;
    }

    /// <summary>
    /// Dựng tạm API để load data vào Redis Cache :))
    /// </summary>
    /// <param name="model"></param>
    /// <returns></returns>
    [HttpPost("LoadRedisCache")]
    public async Task<IActionResult> LoadRedisCache(BaseRequest model)
    {
        try
        {
            await _distributeCacheService.LoadDataToCache();

            return Ok();
        }
        catch (Exception ex)
        {
            LogException(model.UserName, $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
            return StatusCode(StatusCodes.Status500InternalServerError, new SystemError());
        }
    }
}