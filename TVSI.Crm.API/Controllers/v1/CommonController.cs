using System.Reflection;
using Microsoft.AspNetCore.Mvc;
using TVSI.Crm.API.Lib.Models;
using TVSI.Crm.API.Lib.Models.Request.Common;
using TVSI.Crm.API.Lib.Services;

namespace TVSI.Crm.API.Controllers.v1;


/// <summary>
/// Common Module API
/// </summary>
public class CommonController : BaseController
{
    private readonly ICommonService _commonService;

    
    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="statusService"></param>
    public CommonController(ICommonService commonService)
    {
        _commonService = commonService;
    }
    
    /// <summary>
    /// danh sách  trạng thái hồ sơ
    /// </summary>
    /// <param name="model"></param>
    /// <param name="src"></param>
    /// <param name="lang"></param>
    /// <returns>danh sách  trạng thái hồ sơ</returns>
    /// <remarks>
    /// Sample request:
    ///
    ///     POST /Common/GetProfileStatusList
    ///      {
    ///         "UserName": "6245-001",
    ///         "UserRole": "string"
    ///         
    ///      }
    /// </remarks>
    [HttpPost("GetProfileStatusList")]
    public async Task<IActionResult> GetProfileStatusList(BaseRequest model, [FromQuery] string? src = "M",
        [FromQuery] string? lang = "VI")
    {
        try
        {
            LogRequest(model.UserName, model, "GetProfileStatusList");

            var response = await _commonService.GetProfileStatus(model);

            LogResponse(model.UserName, response, "GetProfileStatusList");
            return Ok(response);
        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}", "GetProfileStatusList");
            return StatusCode(StatusCodes.Status500InternalServerError, new SystemError());
        }
    }
    
    /// <summary>
    /// danh sách  trạng thái Tài khoản
    /// </summary>
    /// <param name="model"></param>
    /// <param name="src"></param>
    /// <param name="lang"></param>
    /// <returns>danh sách  trạng thái Tài khoản</returns>
    /// <remarks>
    /// Sample request:
    ///
    ///     POST /Common/GetAccountStatusList
    ///      {
    ///         "UserName": "6245-001",
    ///         "UserRole": "string"
    ///         
    ///      }
    /// </remarks>
    [HttpPost("GetAccountStatusList")]
    public async Task<IActionResult> GetAccountStatusList(BaseRequest model, [FromQuery] string? src = "M",
        [FromQuery] string? lang = "VI")
    {
        try
        {
            LogRequest(model.UserName, model, "GetAccountStatusList");

            var response = await _commonService.GetAccountStatus(model);

            LogResponse(model.UserName, response, "GetAccountStatusList");

            return Ok(response);
        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}", "GetAccountStatusList");
            return StatusCode(StatusCodes.Status500InternalServerError, new SystemError());
        }
    } 
    
    /// <summary>
    /// danh sách  trạng thái tương tác
    /// </summary>
    /// <param name="model"></param>
    /// <param name="src"></param>
    /// <param name="lang"></param>
    /// <returns>danh sách trạng thái tương tác</returns>
    /// <remarks>
    /// Sample request:
    ///
    ///     POST /Common/GetActionStatusList
    ///      {
    ///         "UserName": "6245-001",
    ///         "UserRole": "string"
    ///         
    ///      }
    /// </remarks>
    [HttpPost("GetActionStatusList")]
    public async Task<IActionResult> GetActionStatusList(BaseRequest model, [FromQuery] string? src = "M",
        [FromQuery] string? lang = "VI")
    {
        try
        {
            LogRequest(model.UserName, model, "GetActionStatusList");

            var response = await _commonService.GetActionStatus(model);

            LogResponse(model.UserName, response, "GetActionStatusList");

            return Ok(response);
        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}", "GetActionStatusList");
            return StatusCode(StatusCodes.Status500InternalServerError, new SystemError());
        }
    }   
    
    /// <summary>
    /// danh sách  KÊNH mtK
    /// </summary>
    /// <param name="model"></param>
    /// <param name="src"></param>
    /// <param name="lang"></param>
    /// <returns>danh sách  KÊNH mtK</returns>
    /// <remarks>
    /// Sample request:
    ///
    ///     POST /Common/GetActionStatusList
    ///      {
    ///         "UserName": "6245-001",
    ///         "UserRole": "string"
    ///         
    ///      }
    /// </remarks>
    [HttpPost("GetChannelOpenAccountList")]
    public async Task<IActionResult> GetChannelOpenAccountList(BaseRequest model, [FromQuery] string? src = "M",
        [FromQuery] string? lang = "VI")
    {
        try
        {
            LogRequest(model.UserName, model, "GetChannelOpenAccountList");

            var response = await _commonService.GetChannelOpenAccount(model);

            LogResponse(model.UserName, response, "GetChannelOpenAccountList");

            return Ok(response);
        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}", "GetChannelOpenAccountList");
            return StatusCode(StatusCodes.Status500InternalServerError, new SystemError());
        }
    }  
    
    /// <summary>
    /// API lấy danh sách tỉnh TP
    /// </summary>
    /// <param name="model"></param>
    /// <param name="src"></param>
    /// <param name="lang"></param>
    /// <returns>API lấy danh sách tỉnh TP</returns>
    /// <remarks>
    /// Sample request:
    ///
    ///     POST /Common/GetProvinceList
    ///      {
    ///         "UserName": "6245-001",
    ///         "UserRole": "string"
    ///         
    ///      }
    /// </remarks>
    [HttpPost("GetProvinceList")]
    public async Task<IActionResult> GetProvinceList(BaseRequest model, [FromQuery] string? src = "M",
        [FromQuery] string? lang = "VI")
    {
        try
        {
            LogRequest(model.UserName, model, "GetProvinceList");

            var response = await _commonService.GetProvinceList(model);

            LogResponse(model.UserName, response, "GetProvinceList");

            return Ok(response);
        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}", "GetProvinceList");
            return StatusCode(StatusCodes.Status500InternalServerError, new SystemError());
        }
    } 
    /// <summary>
    /// API lấy danh sách Quận/Huyện
    /// </summary>
    /// <param name="model"></param>
    /// <param name="src"></param>
    /// <param name="lang"></param>
    /// <returns>API lấy danh sách Quận/Huyện</returns>
    /// <remarks>
    /// Sample request:
    ///
    ///     POST /Common/GetDistrictList
    ///      {
    ///         "UserName": "6245-001",
    ///         "UserRole": "string",
    ///         "ProvinceCode": "95"
    ///         
    ///      }
    /// </remarks>
    [HttpPost("GetDistrictList")]
    public async Task<IActionResult> GetDistrictList(GetDistrictListRequest model, [FromQuery] string? src = "M",
        [FromQuery] string? lang = "VI")
    {
        try
        {
            LogRequest(model.UserName, model, "GetDistrictList");

            var response = await _commonService.GetDistrictList(model);

            LogResponse(model.UserName, response, "GetDistrictList");

            return Ok(response);
        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}", "GetDistrictList");
            return StatusCode(StatusCodes.Status500InternalServerError, new SystemError());
        }
    } 
    
    /// <summary>
    /// API lấy danh sách Xã/Phường
    /// </summary>
    /// <param name="model"></param>
    /// <param name="src"></param>
    /// <param name="lang"></param>
    /// <returns>API lấy danh sách Xã/Phường</returns>
    /// <remarks>
    /// Sample request:
    ///
    ///     POST /Common/GetDistrictList
    ///      {
    ///         "UserName": "6245-001",
    ///         "UserRole": "string",
    ///         "DistrictCode": "961",
    ///         
    ///      }
    /// </remarks>
    [HttpPost("GetWardList")]
    public async Task<IActionResult> GetWardList(GetWardListRequest model, [FromQuery] string? src = "M",
        [FromQuery] string? lang = "VI")
    {
        try
        {
            LogRequest(model.UserName, model, "GetWardList");

            var response = await _commonService.GetWardList(model);

            LogResponse(model.UserName, response, "GetWardList");

            return Ok(response);
        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}", "GetWardList");
            return StatusCode(StatusCodes.Status500InternalServerError, new SystemError());
        }
    }
    /*/// <summary>
    /// danh sách  loại hình mở tài khoản
    /// </summary>
    /// <param name="model"></param>
    /// <param name="src"></param>
    /// <param name="lang"></param>
    /// <returns>danh sách  loại hình mở tài khoản</returns>
    /// <remarks>
    /// Sample request:
    ///
    ///     POST /Common/GetTypeAccountList
    ///      {
    ///         "UserName": "6245-001",
    ///         "UserRole": "ABC"
    ///         
    ///      }
    /// </remarks>
    [HttpPost("GetTypeAccountList")]
    public async Task<IActionResult> GetTypeAccountList(BaseRequest model, [FromQuery] string? src = "M",
        [FromQuery] string? lang = "VI")
    {
        try
        {
            var response = await _commonService.GetTypeAccountList(model);
            return Ok(response);
        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
            return StatusCode(StatusCodes.Status500InternalServerError, new SystemError());
        }
    }*/
    
    /// <summary>
    /// API lấy danh sách thông tin mở rộng
    /// </summary>
    /// <param name="model"></param>
    /// <param name="src"></param>
    /// <param name="lang"></param>
    /// <returns>API lấy danh sách thông tin mở rộng</returns>
    /// <remarks>
    /// Sample request:
    ///
    ///     POST /Common/GetExtendInfoList
    ///      {
    ///         "UserName": "6245-001",
    ///         "UserRole": "string"
    ///         
    ///      }
    /// </remarks>
    [HttpPost("GetExtendInfoList")]
    public async Task<IActionResult> GetExtendInfoList(BaseRequest model, [FromQuery] string? src = "M",
        [FromQuery] string? lang = "VI")
    {
        try
        {
            LogRequest(model.UserName, model, "GetExtendInfoList");

            var response = await _commonService.GetExtendInfoList(model);

            LogResponse(model.UserName, response, "GetExtendInfoList");

            return Ok(response);
        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}", "GetExtendInfoList");
            return StatusCode(StatusCodes.Status500InternalServerError, new SystemError());
        }
    } 
    
    /// <summary>
    /// API lấy danh sách thông tin KYC
    /// </summary>
    /// <param name="model"></param>
    /// <param name="src"></param>
    /// <param name="lang"></param>
    /// <returns>API lấy danh sách thông tin KYC</returns>
    /// <remarks>
    /// Sample request:
    ///
    ///     POST /Common/GetKycInfoList
    ///      {
    ///         "UserName": "6245-001",
    ///         "UserRole": "string"
    ///         
    ///      }
    /// </remarks>
    [HttpPost("GetKycInfoList")]
    public async Task<IActionResult> GetKycInfoList(BaseRequest model, [FromQuery] string? src = "M",
        [FromQuery] string? lang = "VI")
    {
        try
        {
            LogRequest(model.UserName, model, "GetKycInfoList");

            var response = await _commonService.GetKycInfoList(model);

            LogResponse(model.UserName, response, "GetKycInfoList");

            return Ok(response);
        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}", "GetKycInfoList");
            return StatusCode(StatusCodes.Status500InternalServerError, new SystemError());
        }
    } 
    
    /// <summary>
    /// API lấy danh sách loại hình tài khoản
    /// </summary>
    /// <param name="model"></param>
    /// <param name="src"></param>
    /// <param name="lang"></param>
    /// <returns>API lấy danh sách loại hình tài khoản </returns>
    /// <remarks>
    /// Sample request:
    ///
    ///     POST /Common/GetCustomerTypeList
    ///      {
    ///         "UserName": "6245-001",
    ///         "UserRole": "string"
    ///         
    ///      }
    /// </remarks>
    [HttpPost("GetCustomerTypeList")]
    public async Task<IActionResult> GetCustomerTypeList(BaseRequest model, [FromQuery] string? src = "M",
        [FromQuery] string? lang = "VI")
    {
        try
        {
            LogRequest(model.UserName, model, "GetCustomerTypeList");

            var response = await _commonService.GetCustomerTypeList(model);

            LogResponse(model.UserName, response, "GetCustomerTypeList");

            return Ok(response);
        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}", "GetCustomerTypeList");
            return StatusCode(StatusCodes.Status500InternalServerError, new SystemError());
        }
    }  
    
    /// <summary>
    /// API lấy danh sách nơi cấp CMT/CCCD
    /// </summary>
    /// <param name="model"></param>
    /// <param name="src"></param>
    /// <param name="lang"></param>
    /// <returns> API lấy danh sách nơi cấp CMT/CCCD </returns>
    /// <remarks>
    /// Sample request:
    ///
    ///     POST /Common/GetPlaceOfIssueList
    ///      {
    ///         "UserName": "6245-001",
    ///         "UserRole": "string"
    ///         
    ///      }
    /// </remarks>
    [HttpPost("GetPlaceOfIssueList")]
    public async Task<IActionResult> GetPlaceOfIssueList(BaseRequest model, [FromQuery] string? src = "M",
        [FromQuery] string? lang = "VI")
    {
        try
        {
            LogRequest(model.UserName, model, "GetPlaceOfIssueList");

            var response = await _commonService.GetPlaceOfIssue(model);

            LogResponse(model.UserName, response, "GetPlaceOfIssueList");

            return Ok(response);
        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}", "GetPlaceOfIssueList");
            return StatusCode(StatusCodes.Status500InternalServerError, new SystemError());
        }
    }
    
    /// <summary>
    /// API lấy danh sách trạng thái hoạt động hoặc đóng của danh sách khách hàng
    /// </summary>
    /// <param name="model"></param>
    /// <param name="src"></param>
    /// <param name="lang"></param>
    /// <returns> API lấy danh sách trạng thái hoạt động hoặc đóng của danh sách khách hàng</returns>
    /// <remarks>
    /// Sample request:
    ///
    ///     POST /Common/GetCustomerStatus
    ///      {
    ///         "UserName": "6245-001",
    ///         "UserRole": "string"
    ///         
    ///      }
    /// </remarks>
    [HttpPost("GetCustomerStatus")]
    public async Task<IActionResult> GetCustomerStatus(BaseRequest model, [FromQuery] string? src = "M",
        [FromQuery] string? lang = "VI")
    {
        try
        {
            LogRequest(model.UserName, model, "GetCustomerStatus");

            var response = await _commonService.GetCustomerStatusList(model);

            LogResponse(model.UserName, response, "GetCustomerStatus");

            return Ok(response);
        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}", "GetCustomerStatus");
            return StatusCode(StatusCodes.Status500InternalServerError, new SystemError());
        }
    }   
    
    
    /// <summary>
    /// API  Lấy danh sách phân quyền
    /// </summary>
    /// <param name="model"></param>
    /// <param name="src"></param>
    /// <param name="lang"></param>
    /// <returns> API lấy danh sách trạng thái hoạt động hoặc đóng của danh sách khách hàng</returns>
    /// <remarks>
    /// Sample request:
    ///
    ///     POST /Common/GetCustomerStatus
    ///      {
    ///         "UserName": "6245-001",
    ///         "UserRole": "string"
    ///         
    ///      }
    /// </remarks>
    [HttpPost("GetAllRightByFun")]
    public async Task<IActionResult> GetAllRightByFun(GetAllRightByFunctionRequest model, [FromQuery] string? src = "M",
        [FromQuery] string? lang = "VI")
    {
        try
        {
            LogRequest(model.UserName, model, "GetAllRightByFun");

            var response = await _commonService.GetAllRightByFun(model);

            LogResponse(model.UserName, response, "GetAllRightByFun");

            return Ok(response);
        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}", "GetAllRightByFun");
            return StatusCode(StatusCodes.Status500InternalServerError, new SystemError());
        }
    }


    /// <summary>
    /// API lấy danh sách chi nhánh TVSI
    /// </summary>
    /// <param name="model"></param>
    /// <param name="src"></param>
    /// <param name="lang"></param>
    /// <returns> API lấy danh sách chi nhánh TVSI</returns>
    /// <remarks>
    /// Sample request:
    ///
    ///     POST /Common/GetBranchList
    ///      {
    ///         "UserName": "6245-001",
    ///         "UserRole": "string"
    ///         
    ///      }
    /// </remarks>
    [HttpPost("GetBranchList")]
    public async Task<IActionResult> GetBranchList(BaseRequest model, [FromQuery] string? src = "M",
        [FromQuery] string? lang = "VI")
    {
        try
        {
            LogRequest(model.UserName, model, "GetBranchList");

            var response = await _commonService.GetBranchList(model);

            LogResponse(model.UserName, response, "GetBranchList");

            return Ok(response);
        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}", "GetBranchList");
            return StatusCode(StatusCodes.Status500InternalServerError, new SystemError());
        }
    }
}
