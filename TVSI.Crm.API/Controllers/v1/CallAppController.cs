﻿using System.Net;
using Microsoft.AspNetCore.Mvc;
using System.Reflection;
using TVSI.Crm.API.Lib.Models;
using TVSI.Crm.API.Lib.Models.Request.CallInApp;
using TVSI.Crm.API.Lib.Models.Response.CallInApp;
using TVSI.Crm.API.Lib.Services;

namespace TVSI.Crm.API.Controllers.v1
{
    [ApiVersion("1.0")]
    public class CallAppController : BaseController
    {
        private readonly ICallInAppService _callInAppService;


        /// <summary>
        /// Controller Constructor
        /// </summary>
        public CallAppController(ICallInAppService callInAppService)
        {
            _callInAppService = callInAppService;
        }

        /// <summary>
        /// API Lấy danh sách customer contact
        /// </summary>
        /// <param name="model">Thông tin Request</param>
        /// <param name="src">Nguồn yêu cầu (M:Mobile, W:Web)</param>
        /// <param name="lang">Ngôn ngữ (EN: English, VI: VietNam)</param>
        /// <returns>Danh sách customer contact</returns>
        /// <remarks>
        /// Sample request:
        /// 
        ///     POST /CallApp/GetListCustomerContact
        ///     {
        ///        "UserName": "1917-001",
        ///        "UserRole": "CRM_SALE",
        ///        "Custcode": "",
        ///        "Name": "",
        ///        "Phone": "",
        ///        "PageIndex": 1,
        ///        "PageSize": 20
        ///     }
        ///
        /// </remarks>
        [HttpPost("GetListCustomerContact")]
        public async Task<IActionResult> GetListCustomerContact(CustomerContactInfoRequest model, [FromQuery] string? src = "M",
            [FromQuery] string? lang = "VI")
        {
            try
            {
                LogRequest(model.UserName, model, "GetListCustomerContact");

                var response = await _callInAppService.GetListCustomerContact(model);

                LogResponse(model.UserName, response, "GetListCustomerContact");

                return Ok(response);
            }
            catch (Exception ex)
            {
                LogException(model.UserName, $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}", "GetListCustomerContact");
                return StatusCode(StatusCodes.Status500InternalServerError, new SystemError());
            }
        }

        /// <summary>
        /// API Tạo mới call app
        /// </summary>
        /// <param name="model">Thông tin Request</param>
        /// <param name="src">Nguồn yêu cầu (M:Mobile, W:Web)</param>
        /// <param name="lang">Ngôn ngữ (EN: English, VI: VietNam)</param>
        /// <returns>Tạo mới call app</returns>
        /// <remarks>
        /// Sample request:
        /// 
        ///     POST /CallApp/CreateCallApp
        ///     {
        ///         "UserName": "1917-001",
        ///         "UserRole": "CRM_SALE",
        ///         "CallId": "000001",
        ///         "CustCode": "115888",
        ///         "LeadId": 0,
        ///         "OpenAccId": 0,
        ///         "CustName": "Mr. Bùi Đình Đức",
        ///         "Direction": 0,
        ///         "Mobile": "0333333333",
        ///     }
        ///
        /// </remarks>
        [HttpPost("CreateCallApp")]
        public async Task<IActionResult> CreateCallApp(CreateCallAppRequest model, [FromQuery] string? src = "M",
            [FromQuery] string? lang = "VI")
        {
            try
            {
                LogRequest(model.UserName, model, "CreateCallApp");

                var response = await _callInAppService.CreateCallApp(model);
                
                LogResponse(model.UserName, response, "CreateCallApp");

                return Ok(response);
            }
            catch (Exception ex)
            {
                LogException(model.UserName, $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}", "CreateCallApp");
                return StatusCode(StatusCodes.Status500InternalServerError, new SystemError());
            }
        }

        /// <summary>
        /// API Update call app
        /// </summary>
        /// <param name="model">Thông tin Request</param>
        /// <param name="src">Nguồn yêu cầu (M:Mobile, W:Web)</param>
        /// <param name="lang">Ngôn ngữ (EN: English, VI: VietNam)</param>
        /// <returns>Update call app</returns>
        /// <remarks>
        /// Sample request:
        /// 
        ///     POST /CallApp/UpdateCallApp
        ///     {
        ///         "UserName": "1917-001",
        ///         "UserRole": "CRM_SALE",
        ///         "CallId": "000001",
        ///         "Note": "string  123",
        ///         "Comment": "test test",
        ///         "Status": 1,
        ///         "Rate": 5,
        ///         "CallAppTypeList": "1,2,3,4",
        ///         "TimeCall": "10:10:00"
        ///     }
        ///
        /// </remarks>
        [HttpPost("UpdateCallApp")]
        public async Task<IActionResult> UpdateCallApp(UpdateHistoryCallRequest model, [FromQuery] string? src = "M",
            [FromQuery] string? lang = "VI")
        {
            try
            {
                LogRequest(model.UserName, model, "UpdateCallApp");

                var response = await _callInAppService.UpdateCallApp(model);

                LogResponse(model.UserName, response, "UpdateCallApp");

                return Ok(response);
            }
            catch (Exception ex)
            {
                LogException(model.UserName, $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}", "UpdateCallApp");
                return StatusCode(StatusCodes.Status500InternalServerError, new SystemError());
            }
        }

        /// <summary>
        /// API Lấy danh sách lịch sử cuộc gọi
        /// </summary>
        /// <param name="model">Thông tin Request</param>
        /// <param name="src">Nguồn yêu cầu (M:Mobile, W:Web)</param>
        /// <param name="lang">Ngôn ngữ (EN: English, VI: VietNam)</param>
        /// <returns>Danh sách lịch sử cuộc gọi</returns>
        /// <remarks>
        /// Sample request:
        /// 
        ///     POST /CallApp/GetListHistoryCall
        ///     {
        ///         "UserName": "1917-001",
        ///         "UserRole": "CRM_SALE",
        ///         "CustCode": "",
        ///         "SaleId": "",
        ///         "LeadId" : 0,
        ///         "CustType": -1,
        ///         "FromDate": "10/01/2022",
        ///         "ToDate": "05/08/2022",
        ///         "PageIndex": 1,
        ///         "PageSize": 10
        ///      }
        ///
        /// </remarks>
        [HttpPost("GetListHistoryCall")]
        [ProducesResponseType(typeof(ResponseSingle<HistoryCallAppInfoResponse>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetListHistoryCall(GetListHistoryCallRequest model, [FromQuery] string? src = "M",
            [FromQuery] string? lang = "VI")
        {
            try
            {
                LogRequest(model.UserName, model, "GetListHistoryCall");

                var response = await _callInAppService.GetListHistoryCall(model);

                LogResponse(model.UserName, response, "GetListHistoryCall");

                return Ok(response);
            }
            catch (Exception ex)
            {
                LogException(model.UserName, $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}", "GetListHistoryCall");
                return StatusCode(StatusCodes.Status500InternalServerError, new SystemError());
            }
        }

        /// <summary>
        /// API Lấy danh sách booking cuộc gọi
        /// </summary>
        /// <param name="model">Thông tin Request</param>
        /// <param name="src">Nguồn yêu cầu (M:Mobile, W:Web)</param>
        /// <param name="lang">Ngôn ngữ (EN: English, VI: VietNam)</param>
        /// <returns>Danh sách booking cuộc gọi</returns>
        /// <remarks>
        /// Sample request:
        /// 
        ///     POST /CallApp/GetListBookingCall
        ///     {
        ///         "UserName": "1917-001",
        ///         "UserRole": "CRM_SALE",
        ///         "CustCode": "",
        ///         "SaleId": "",
        ///         "CustType": -1,
        ///         "FromDate": "10/01/2022",
        ///         "ToDate": "05/08/2022",
        ///         "PageIndex": 1,
        ///         "PageSize": 10
        ///     }
        ///
        /// </remarks>
        [HttpPost("GetListBookingCall")]
        [ProducesResponseType(typeof(ResponseSingle<BookingCallAppResponse>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetListBookingCall(GetListBookingCallRequest model, [FromQuery] string? src = "M",
            [FromQuery] string? lang = "VI")
        {
            try
            {
                LogRequest(model.UserName, model, "GetListBookingCall");

                var response = await _callInAppService.GetListBookingCall(model);

                LogResponse(model.UserName, response, "GetListBookingCall");

                return Ok(response);
            }
            catch (Exception ex)
            {
                LogException(model.UserName, $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}", "GetListBookingCall");
                return StatusCode(StatusCodes.Status500InternalServerError, new SystemError());
            }
        }

        /// <summary>
        /// API Tạo mới booking call
        /// </summary>
        /// <param name="model">Thông tin Request</param>
        /// <param name="src">Nguồn yêu cầu (M:Mobile, W:Web)</param>
        /// <param name="lang">Ngôn ngữ (EN: English, VI: VietNam)</param>
        /// <returns>Tạo mới booking call</returns>
        /// <remarks>
        /// Sample request:
        /// 
        ///     POST /CallApp/CreateBookingCall
        ///     {
        ///         "UserName": "1917-001",
        ///         "UserRole": "CRM_SALE",
        ///         "CallAppType": 1,
        ///         "StartTime": "2022-07-22 00:00:00",
        ///         "EndTime": "2022-07-23 07:00:00",
        ///         "RemindTime": "2022-07-23 06:55:00",
        ///         "Description": "string",
        ///         "BookCallType": 1,
        ///         "AssignUser": "1917-001",
        ///         "CustCode": "115888",
        ///         "LeadId": 0,
        ///         "OpenAccId": 0,
        ///         "CustName": "Mr. Bùi Đình Đức",
        ///         "Mobile": "033333333"
        ///     }
        ///
        /// </remarks>
        [HttpPost("CreateBookingCall")]
        public async Task<IActionResult> CreateBookingCall(CreateBookingCallRequest model, [FromQuery] string? src = "M",
            [FromQuery] string? lang = "VI")
        {
            try
            {
                LogRequest(model.UserName, model, "CreateBookingCall");

                var response = await _callInAppService.CreateBookingCall(model);

                LogResponse(model.UserName, response, "CreateBookingCall");

                return Ok(response);
            }
            catch (Exception ex)
            {
                LogException(model.UserName, $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}", "CreateBookingCall");
                return StatusCode(StatusCodes.Status500InternalServerError, new SystemError());
            }
        }

        /// <summary>
        /// API Update booking call
        /// </summary>
        /// <param name="model">Thông tin Request</para
        /// <param name="src">Nguồn yêu cầu (M:Mobile, W:Web)</param>
        /// <param name="lang">Ngôn ngữ (EN: English, VI: VietNam)</param>
        /// <returns>Update booking call</returns>
        /// <remarks>
        /// Sample request:
        /// 
        ///     POST /CallApp/UpdateBookingCall
        ///     {
        ///         "UserName": "1917-001",
        ///         "UserRole": "CRM_SALE",
        ///         "BookApptCallId": 1,
        ///         "CallAppTypeList": "1,2,3,4",
        ///         "StartTime": "2022-07-22 15:00:00",
        ///         "EndTime": "2022-07-23 15:00:00",
        ///         "RemindTime": "2022-07-23 14:55:00",
        ///         "Description": "Update tesst",
        ///         "BookCallType": 3,
        ///         "AssignUser": "1917-001",
        ///         "Status": 2
        ///     }
        ///
        /// </remarks>
        [HttpPost("UpdateBookingCall")]
        public async Task<IActionResult> UpdateBookingCall(UpdateBookingCallRequest model, [FromQuery] string? src = "M",
            [FromQuery] string? lang = "VI")
        {
            try
            {
                LogRequest(model.UserName, model, "UpdateBookingCall");

                var response = await _callInAppService.UpdateBookingCall(model);

                LogResponse(model.UserName, response, "UpdateBookingCall");

                return Ok(response);
            }
            catch (Exception ex)
            {
                LogException(model.UserName, $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}", "UpdateBookingCall");
                return StatusCode(StatusCodes.Status500InternalServerError, new SystemError());
            }
        }


        /// <summary>
        /// API status booking call
        /// </summary>
        /// <param name="model">Thông tin Request</param>
        /// <param name="src">Nguồn yêu cầu (M:Mobile, W:Web)</param>
        /// <param name="lang">Ngôn ngữ (EN: English, VI: VietNam)</param>
        /// <returns>Update status booking call</returns>
        /// <remarks>
        /// Sample request:
        /// 
        ///     POST /CallApp/GetListStatus
        ///     {
        ///         "UserName": "1917-001",
        ///         "UserRole": "CRM_SALE"
        ///     }
        ///
        /// </remarks>
        [HttpPost("GetListStatus")]
        public async Task<IActionResult> GetListStatus(BaseRequest model, [FromQuery] string? src = "M",
            [FromQuery] string? lang = "VI")
        {
            try
            {
                LogRequest(model.UserName, model, "GetListStatus");

                var response = await _callInAppService.GetListStatus(model);

                LogResponse(model.UserName, response, "GetListStatus");

                return Ok(response);
            }
            catch (Exception ex)
            {
                LogException(model.UserName, $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}", "GetListStatus");
                return StatusCode(StatusCodes.Status500InternalServerError, new SystemError());
            }
        }

        /// <summary>
        /// API delete booking call
        /// </summary>
        /// <param name="model">Thông tin Request</param>
        /// <param name="src">Nguồn yêu cầu (M:Mobile, W:Web)</param>
        /// <param name="lang">Ngôn ngữ (EN: English, VI: VietNam)</param>
        /// <returns>delete booking call</returns>
        /// <remarks>
        /// Sample request:
        /// 
        ///     POST /CallApp/DeleteBookingCall
        ///     {
        ///         "UserName": "1917-001",
        ///         "UserRole": "CRM_SALE",
        ///         "BookApptCallId": 1
        ///     }
        ///
        /// </remarks>
        [HttpPost("DeleteBookingCall")]
        public async Task<IActionResult> DeleteBookingCall(DeleteBookingCallRequest model, [FromQuery] string? src = "M",
            [FromQuery] string? lang = "VI")
        {
            try
            {
                LogRequest(model.UserName, model, "DeleteBookingCall");

                var response = await _callInAppService.DeleteBookingCall(model);

                LogResponse(model.UserName, response, "DeleteBookingCall");

                return Ok(response);
            }
            catch (Exception ex)
            {
                LogException(model.UserName, $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}", "DeleteBookingCall");
                return StatusCode(StatusCodes.Status500InternalServerError, new SystemError());
            }
        }


        /// <summary>
        /// API get list user assign
        /// </summary>
        /// <param name="model">Thông tin Request</param>
        /// <param name="src">Nguồn yêu cầu (M:Mobile, W:Web)</param>
        /// <param name="lang">Ngôn ngữ (EN: English, VI: VietNam)</param>
        /// <returns>get list user assign</returns>
        /// <remarks>
        /// Sample request:
        /// 
        ///     POST /CallApp/GetListSaleInfo
        ///     {
        ///         "UserName": "1917-001",
        ///         "UserRole": "CRM_SALE",
        ///         "BookApptCallId": 1
        ///     }
        ///
        /// </remarks>
        [HttpPost("GetListSaleInfo")]
        public async Task<IActionResult> GetListSaleInfo(SaleInfoRequestModel model, [FromQuery] string? src = "M",
            [FromQuery] string? lang = "VI")
        {
            try
            {
                var response = await _callInAppService.GetListSaleInfo(model);

                return Ok(response);
            }
            catch (Exception ex)
            {
                LogException(model.UserName, $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
                return StatusCode(StatusCodes.Status500InternalServerError, new SystemError());
            }
        }

        /// <summary>
        /// API det detail call
        /// </summary>
        /// <param name="model">Thông tin Request</param>
        /// <param name="src">Nguồn yêu cầu (M:Mobile, W:Web)</param>
        /// <param name="lang">Ngôn ngữ (EN: English, VI: VietNam)</param>
        /// <returns>get list user assign</returns>
        /// <remarks>
        /// Sample request:
        /// 
        ///     POST /CallApp/GetDetailCall
        ///     {
        ///         "UserName": "1917-001",
        ///         "CallId": "call-vn-1-M4S3S666NO-1663006341782",
        ///     }
        ///
        /// </remarks>
        [HttpPost("GetDetailCall")]
        public async Task<IActionResult> GetDetailCall(GetDetailHistoryCall model, [FromQuery] string? src = "M",
            [FromQuery] string? lang = "VI")
        {
            try
            {
                var response = await _callInAppService.GetDetailCall(model);

                return Ok(response);
            }
            catch (Exception ex)
            {
                LogException(model.UserName, $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
                return StatusCode(StatusCodes.Status500InternalServerError, new SystemError());
            }
        }
    }
}
