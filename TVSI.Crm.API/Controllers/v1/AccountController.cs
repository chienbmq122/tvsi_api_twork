using System.Reflection;
using Microsoft.AspNetCore.Mvc;
using TVSI.Crm.API.Lib.Models;
using TVSI.Crm.API.Lib.Models.Request.Account;
using TVSI.Crm.API.Lib.Models.Response.Account;
using TVSI.Crm.API.Lib.Services;

namespace TVSI.Crm.API.Controllers.v1;

/// <summary>
/// Customer Module API
/// </summary>
[ApiVersion("1.0")]
public class AccountController : BaseController
{
    private readonly IAccountService _accountService;

    /// <summary>
    /// Controller Constructor
    /// </summary>
    public AccountController(IAccountService accountService)
    {
        _accountService = accountService;
    }


    /// <summary>
    /// Quản lý hồ sơ mở tài khoản MTK
    /// </summary>
    /// <param name="model"></param>
    /// <param name="src"></param>
    /// <param name="lang"></param>
    /// <returns>Quản lý hồ sơ mở tài khoản MTK</returns>
    /// <remarks>
    /// Sample request:
    ///
    ///     POST /Account/GetAccountOpenList
    ///     {
    ///         "UserName": "1268-001",
    ///         "FullName": "Chi",
    ///         "CustCode": "",
    ///         "AccountStatus": "",
    ///         "ProfileStatus": "",
    ///         "UserRole":"CRM_SALE",
    ///         "PageIndex": 1,
    ///         "PageSize": 10
    ///     } 
    /// </remarks>
    [HttpPost("GetAccountOpenList")]
    public async Task<IActionResult> GetAccountOpenList(AccountListRequest model, [FromQuery] string? src = "M",
        [FromQuery] string? lang = "VI")
    {
        try
        {
            LogRequest(model.UserName, model, "GetAccountOpenList");

            var response = await _accountService.GetAccountOpenList(model);

            LogResponse(model.UserName, response, "GetAccountOpenList");

            return Ok(response);
        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}", "GetAccountOpenList");
            return StatusCode(StatusCodes.Status500InternalServerError, new SystemError());
        }
    }  
    
    /// <summary>
    /// danh sách droplist custcode
    /// </summary>
    /// <param name="model"></param>
    /// <param name="src"></param>
    /// <param name="lang"></param>
    /// <returns> danh sách droplist custcode</returns>
    /// <remarks>
    /// Sample request:
    ///
    ///     POST /Account/GetCustCodeOAccountOpenList
    ///     {
    ///         "UserName": "0011-001"
    ///     } 
    /// </remarks>
    [HttpPost("GetCustCodeOAccountOpenList")]
    public async Task<IActionResult> GetCustCodeOAccountOpenList(BaseRequest model, [FromQuery] string? src = "M",
        [FromQuery] string? lang = "VI")
    {
        try
        {
            LogRequest(model.UserName, model, "GetCustCodeOAccountOpenList");

            var response = await _accountService.GetCustCodeOpenList(model);

            LogResponse(model.UserName, response, "GetCustCodeOAccountOpenList");

            return Ok(response);
        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}", "GetCustCodeOAccountOpenList");
            return StatusCode(StatusCodes.Status500InternalServerError, new SystemError());
        }
    }


    /// <summary>
    /// Chi tiết hồ sơ
    /// </summary>
    /// <param name="model"></param>
    /// <param name="src"></param>
    /// <param name="lang"></param>
    /// <returns>Danh sách mở tài khoản MTK</returns>
    /// <remarks>
    /// Sample request:
    ///
    ///     POST /Account/GetAccountOpenDetailInfo
    ///      {
    ///        "UserName": "6245-001",
    ///        "UserRole": "CRM_SALE",
    ///        "Id": 131505,
    ///        "Custcode": "765688"
    ///      }
    /// </remarks>
    [HttpPost("GetAccountOpenDetailInfo")]
    public async Task<IActionResult> GetAccountOpenDetailInfo(AccountOpenDetailRequest model, [FromQuery] string? src = "M",
        [FromQuery] string? lang = "VI")
    {
        try
        {
            LogRequest(model.UserName, model, "GetAccountOpenDetailInfo");
            var response = await _accountService.GetAccountOpenDetailInfo(model);
            LogResponse(model.UserName, response, "GetAccountOpenDetailInfo");
            return Ok(response);
        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}", "GetAccountOpenDetailInfo");
            return StatusCode(StatusCodes.Status500InternalServerError, new SystemError());
        }
    }


    /// <summary>
    /// cập nhật chi tiết hồ sơ
    /// </summary>
    /// <param name="model"></param>
    /// <param name="src"></param>
    /// <param name="lang"></param>
    /// <returns>Danh sách mở tài khoản MTK ( câp nhật hồ sơ)</returns>
    /// <remarks>
    /// Sample request:
    ///
    ///     POST /Account/UpdateAccountDetail
    ///      {
    ///        "UserName": "6245-001",
    ///        "UserRole": "CRM_SALE",
    ///        "Id": 131505,
    ///        "Custcode": "765688"
    ///      }
    /// </remarks>
    [HttpPost("UpdateAccountDetail")]
    public async Task<IActionResult> UpdateAccountDetail(UpdateAccountDetailRequest model, [FromQuery] string? src = "M",
        [FromQuery] string? lang = "VI")
    {
        try
        {
            LogRequest(model.UserName, model, "UpdateAccountDetail");

            var response = await _accountService.UpdateAccountDetail(model);

            LogResponse(model.UserName, response, "UpdateAccountDetail");

            return Ok(response);
        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}", "UpdateAccountDetail");
            return StatusCode(StatusCodes.Status500InternalServerError, new SystemError());
        }
    } 
    
    /// <summary>
    /// API Hủy hồ sơ tài khoản
    /// </summary>
    /// <param name="model"></param>
    /// <param name="src"></param>
    /// <param name="lang"></param>
    /// <returns>API Hủy hồ sơ tài khoản</returns>
    /// <remarks>
    /// Sample request:
    ///
    ///     POST /Account/CancelOpenAccount
    ///      {
    ///        "UserName": "6245-001",
    ///        "UserRole": "CRM_SALE",
    ///        "Id": 131505
    ///      }
    /// </remarks>
    [HttpPost("CancelOpenAccount")]
    public async Task<IActionResult> CancelOpenAccount(CancelOpenAccountRequest model, [FromQuery] string? src = "M",
        [FromQuery] string? lang = "VI")
    {
        try
        {
            LogRequest(model.UserName, model, "CancelOpenAccount");

            var response = await _accountService.CancelOpenAccount(model);

            LogResponse(model.UserName, response, "CancelOpenAccount");

            return Ok(response);
        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}", "CancelOpenAccount");
            return StatusCode(StatusCodes.Status500InternalServerError, new SystemError());
        }
    }

    /// <summary>
    /// Check số điện thoại/ CardId đã đăng ký mở tài khoản 
    /// </summary>
    /// <param name="model"></param>
    /// <param name="src"></param>
    /// <param name="lang"></param>
    /// <returns>Check số điện thoại/ CardId đã đăng ký mở tài khoản </returns>
    /// <remarks>
    /// Sample request:
    ///
    ///     POST /Account/CheckPhoneCardIdAccount
    ///      {
    ///        "UserName": "6245-001",
    ///        "UserRole": "CRM_SALE",
    ///        "Phone": "83600008",
    ///        "CardId": "188798989",
    ///        "CardIssueDate":"16/09/2022"
    ///      }
    ///       Data:
    ///         True: Chưa đăng ký
    ///         False: Đã đăng ký
    /// </remarks>
    [HttpPost("CheckPhoneCardIdAccount")]
    public async Task<IActionResult> CheckPhoneCardIdAccount(CheckPhoneCardIdAccountRequest model, [FromQuery] string? src = "M",
        [FromQuery] string? lang = "VI")
    {
        try
        {
            LogRequest(model.UserName, model, "CheckPhoneCardIdAccount");

            var response = await _accountService.CheckPhoneCardIdAccount(model);

            LogResponse(model.UserName, response, "CheckPhoneCardIdAccount");

            return Ok(response);
        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}", "CheckPhoneCardIdAccount");
            return StatusCode(StatusCodes.Status500InternalServerError, new SystemError());
        }
    }

    /// <summary>
    /// Kiểm tra mã KH
    /// </summary>
    /// <param name="model"></param>
    /// <param name="src"></param>
    /// <param name="lang"></param>
    /// <returns> Kiểm tra mã KH </returns>
    /// <remarks>
    /// Sample request:
    ///
    ///     POST /Account/CheckCustomerCode
    ///      {
    ///        "UserName": "6245-001",
    ///        "UserRole": "CRM_SALE",
    ///        "TypeAccount": 1,
    ///        "CustomerCode": "271084"
    ///      }
    ///       Data:
    ///           0:   Chưa mở
    ///           1:   Đã mở nhưng chưa mở TK marin => chuyển sang màn hình mở tk bổ sung
    ///           33:  Số lượng HĐ nợ quá số lượng
    ///           10:  Đã mở
    ///           88:  Khách hàng đã đóng tài khoản.
    /// </remarks>
    [HttpPost("CheckCustomerCode")]
    public async Task<IActionResult> CheckCustomerCode(CheckCustomerCodeRequest model, [FromQuery] string? src = "M",
        [FromQuery] string? lang = "VI")
    {
        try
        {
            LogRequest(model.UserName, model, "CheckCustomerCode");

            var response = await _accountService.CheckCustomerCode(model);

            LogResponse(model.UserName, response, "CheckCustomerCode");

            return Ok(response);
        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}", "CheckCustomerCode");
            return StatusCode(StatusCodes.Status500InternalServerError, new SystemError());
        }
    }

    /// <summary>
    /// Mở tài khoản cá nhân trong nước  ( mở mới)
    /// </summary>
    /// <param name="model"></param>
    /// <param name="src"></param>
    /// <param name="lang"></param>
    /// <returns>Mở tài khoản cá nhân trong nước ( mở mởi)</returns>
    /// <remarks>
    /// Sample request:
    ///
    ///     POST /Account/OpenIndividualDomesticAccount
    ///      {
    ///         "UserName": "6245-001",
    ///         "UserRole": "CRM_SALE",
    ///         "ProfileType": 1,
    ///         "CustomerCode": "234466",
    ///         "FullName": "ACCCC",
    ///         "BirthDay": "12/12/1212",
    ///         "Gender": 1,
    ///         "CardId": "1234324434",
    ///         "IssueDate": "12/12/1212",
    ///         "IssuePlace": "aaaaaaaaaaa",
    ///         "Address": "AVC",
    ///         "ManTvsi": "ABC",
    ///         "ManPosition": "aaa",
    ///         "ManAnPower": "Aaa",
    ///         "ManDateAnPower": "12/12/1212",
    ///         "ManCardId": "12343223",
    ///         "ManIssueDate": "12/12/1212",
    ///         "ManIssuePlace": "AAAA",
    ///         "ImageFront": "",
    ///         "ImageBack": "",
    ///         "SaleID": "123",
    ///         "AccountMargin": true,
    ///         "CreditLimit": 12121212,
    ///         "DebtTerm": null,
    ///         "DepositRate": 0.2,
    ///         "TradingResultSms": true,
    ///         "TradingResultEmail": true,
    ///         "Phone01": "090909090",
    ///         "Phone02": "09090909",
    ///         "Email": "a@aaa.com",
    ///         "TransferCashService": true,
    ///         "BankInfo": [
    ///                         {
    ///                             "BankAccountNo": "122323646",
    ///                             "BankAccountName": "Nguyễn Văn Nam",
    ///                             "BankNo": "143",
    ///                             "SubBranchNo": "121"
    ///                         }
    ///                     ],
    ///          "ExtendCust": [
    ///                           {
    ///                              "CategoryType": 1,
    ///                              "Value": 1
    ///                           },
    ///                           {
    ///                              "CategoryType": 2,
    ///                              "Value": 88
    ///                           }
    ///                        ],
    ///             "IsInfoUsa": true,
    ///             "UsaInfo":
    ///                 {
    ///                     "Info01": true,
    ///                     "Info02": true,
    ///                     "Info03": true,
    ///                     "Info04": true,
    ///                     "Info05": true,
    ///                     "Info06": true,
    ///                     "Info07": true
    ///                 },
    ///             "IsShareHolder": true,
    ///             "ShareHolderInfo": [
    ///             {"stockCode": "mã chứng khoán1", "ShareType_1": true, "ShareType_2": true, "ShareType_3": true, "ShareType_4": true }
    ///             ],
    ///             "IsProfileCompletion" : true,
    ///             "SourceAccount" : 7,
    ///      }
    /// </remarks>
    [HttpPost("OpenIndividualDomesticAccount")]
    public async Task<IActionResult> OpenIndividualDomesticAccount(OpenIndividualDomesticAccountRequest model, [FromQuery] string? src = "M",
        [FromQuery] string? lang = "VI")
    {
        try
        {
            LogRequest(model.UserName, model, "OpenIndividualDomesticAccount");

            var response = await _accountService.OpenIndividualDomesticAccount(model);

            LogResponse(model.UserName, response, "OpenIndividualDomesticAccount");

            return Ok(response);
        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}", "OpenIndividualDomesticAccount");
            return StatusCode(StatusCodes.Status500InternalServerError, new SystemError());
        }
    }

    /// <summary>
    /// Chi tiết hồ sơ để mở tk bổ sung
    /// </summary>
    /// <param name="model"></param>
    /// <param name="src"></param>
    /// <param name="lang"></param>
    /// <returns>Chi tiết hồ sơ để mở tk bổ sung</returns>
    /// <remarks>
    /// Sample request:
    ///
    ///     POST /Account/GetOpenAccountAdditionalDetail
    ///      {
    ///        "UserName": "6245-001",
    ///        "UserRole": "CRM_SALE",
    ///        "Custcode": "100282"
    ///      }
    /// </remarks>
    [HttpPost("GetOpenAccountAdditionalDetail")]
    public async Task<IActionResult> GetOpenAccountAdditionalDetail(OpenAccountAdditionalDetailRequest model, [FromQuery] string? src = "M",
        [FromQuery] string? lang = "VI")
    {
        try
        {
            LogRequest(model.UserName, model, "GetOpenAccountAdditionalDetail");

            var response = await _accountService.GetOpenAccountAdditionalDetail(model);

            LogResponse(model.UserName, response, "GetOpenAccountAdditionalDetail");

            return Ok(response);
        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}", "GetOpenAccountAdditionalDetail");
            return StatusCode(StatusCodes.Status500InternalServerError, new SystemError());
        }
    }

    /// <summary>
    /// Mở bổ sung tài khoản cá nhân trong nước
    /// </summary>
    /// <param name="model"></param>
    /// <param name="src"></param>
    /// <param name="lang"></param>
    /// <returns> Mở bổ sung tài khoản cá nhân trong nước</returns>
    /// <remarks>
    /// Sample request:
    ///
    ///     POST /Account/OpenIndividualDomesticAdditionalAccount
    ///      {
    ///         "UserName": "6245-001",
    ///         "UserRole": "CRM_SALE",
    ///         "CustomerCode": "234466",
    ///         "FullName": "ACCCC",
    ///         "BirthDay": "12/12/1212",
    ///         "Gender": 1,
    ///         "CardId": "1234324434",
    ///         "IssueDate": "12/12/1212",
    ///         "IssuePlace": "aaaaaaaaaaa",
    ///         "Address": "AVC",  
    ///         "SaleID": "123",
    ///         "AccountMargin": true,
    ///         "SourceAccount" : 7
    ///      }
    /// </remarks>
    [HttpPost("OpenIndividualDomesticAdditionalAccount")]
    public async Task<IActionResult> OpenIndividualDomesticAdditionalAccount(OpenIndividualDomesticAdditionalAccountRequest model, [FromQuery] string? src = "M",
        [FromQuery] string? lang = "VI")
    {
        try
        {
            LogRequest(model.UserName, model, "OpenIndividualDomesticAdditionalAccount");

            var response = await _accountService.OpenIndividualDomesticAdditionalAccount(model);

            LogResponse(model.UserName, response, "OpenIndividualDomesticAdditionalAccount");

            return Ok(response);
        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}", "OpenIndividualDomesticAdditionalAccount");
            return StatusCode(StatusCodes.Status500InternalServerError, new SystemError());
        }
    }
    
    /// <summary>
    /// API Cập nhật thông tin tài khoản bổ sung Margin
    /// </summary>
    /// <param name="model"></param>
    /// <param name="src"></param>
    /// <param name="lang"></param>
    /// <returns>Cập nhật thông tin tài khoản bổ sung Margin</returns>
    /// <remarks>
    /// Sample request:
    ///
    ///     POST /Account/UpdateAdditionalAccount
    ///      {
    ///         "UserName": "string",
    ///         "UserRole": "string",
    ///         "Id": 131505,
    ///         "MarginAccount": true,
    ///         "CreditLimit": 10,
    ///         "DebtTerm": 5,
    ///         "DepositRate": 10
    ///      }
    /// </remarks>
    [HttpPost("UpdateAdditionalAccount")]
    public async Task<IActionResult> UpdateAdditionalAccount(UpdateAdditionalAccountRequest model, [FromQuery] string? src = "M",
        [FromQuery] string? lang = "VI")
    {
        try
        {
            LogRequest(model.UserName, model, "UpdateAdditionalAccount");

            var response = await _accountService.UpdateAdditionalAccount(model);

            LogResponse(model.UserName, response, "UpdateAdditionalAccount");

            return Ok(response);
        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}", "UpdateAdditionalAccount");
            return StatusCode(StatusCodes.Status500InternalServerError, new SystemError());
        }
    }


    /// <summary>
    /// Search danh sách nợ hồ sơ mở tk để Bu duyệt (mở mới, mở bổ sung)/ Lịch sử
    /// </summary>
    /// <param name="model"></param>
    /// <param name="src"></param>
    /// <param name="lang"></param>
    /// <returns>Danh sách hồ sơ mở tk để duyệt (mở mới, mở bổ sung)/ Lịch sử</returns>
    /// <remarks>
    /// Sample request:
    ///
    ///     POST /Account/GetOpenAccountDebtDocumentList
    ///     {
    ///         "UserName": "1268-001",
    ///         "UserRole":"CRM_SALE",
    ///         "CustCode": "",
    ///         "FullName": "",
    ///         "SaleId": "1268",
    ///         "AccountStatus": "",
    ///         "SearchType" : 1 // : 1-Search duyệt, 2-Lịch sử
    ///         "PageIndex": 1,
    ///         "PageSize": 10
    ///     } 
    /// </remarks>
    [HttpPost("GetOpenAccountDebtDocumentList")]
    public async Task<IActionResult> GetOpenAccountDebtDocumentList(OpenAccountDebtDocumentListRequest model, [FromQuery] string? src = "M",
        [FromQuery] string? lang = "VI")
    {
        try
        {
            LogRequest(model.UserName, model, "GetOpenAccountDebtDocumentList");

            var response = await _accountService.GetOpenAccountDebtDocumentList(model);

            LogResponse(model.UserName, response, "GetOpenAccountDebtDocumentList");

            return Ok(response);
        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}", "GetOpenAccountList");
            return StatusCode(StatusCodes.Status500InternalServerError, new SystemError());
        }
    }

    /// <summary>
    /// Chi tiết hồ sơ duyệt/ lịch sử duyệt
    /// </summary>
    /// <param name="model"></param>
    /// <param name="src"></param>
    /// <param name="lang"></param>
    /// <returns>Chi tiết hồ sơ duyệt/ lịch sử duyệt</returns>
    /// <remarks>
    /// Sample request:
    ///
    ///     POST /Account/GetOpenAccountDetail
    ///      {
    ///        "UserName": "6245-001",
    ///        "UserRole": "CRM_SALE",
    ///        "Id": "12345",
    ///         "SearchType" : 1 // : 1-Search duyệt, 2-Lịch sử
    ///      }
    /// </remarks>
    [HttpPost("GetOpenAccountDetail")]
    public async Task<IActionResult> GetOpenAccountDetail(OpenAccountDetailRequest model, [FromQuery] string? src = "M",
        [FromQuery] string? lang = "VI")
    {
        try
        {
            LogRequest(model.UserName, model, "GetOpenAccountDetail");

            var response = await _accountService.GetOpenAccountDetail(model);

            LogResponse(model.UserName, response, "GetOpenAccountDetail");

            return Ok(response);
        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}", "GetOpenAccountDetail");
            return StatusCode(StatusCodes.Status500InternalServerError, new SystemError());
        }
    }

    /// <summary>
    /// Cập nhật trạng thái mở tài khoản ( duyệt/ từ chối mở tài khoản)
    /// </summary>
    /// <param name="model">Thông tin Request</param>
    /// <param name="src">Nguồn yêu cầu (M:Mobile, W:Web)</param>
    /// <param name="lang">Ngôn ngữ (EN: English, VI: VietNam)</param>
    /// <returns> Cập nhật trạng thái mở tài khoản ( duyệt/ từ chối mở tài khoản) </returns>
    /// <remarks>
    /// Sample request:
    /// 
    ///     POST /Account/UpdateAccountStatus
    ///     {
    ///         "UserName": "1001-001",
    ///         "UserRole": "string",
    ///         "Id": 4393,
    ///         "IsConfirm": false,
    ///         "AccountStatus" : "001", ( 001: chờ BUMAN duyệt, 003: chờ GĐMG duyệt)
    ///         "AddContract": "1", (nếu có 1 thì là có margin)
    ///         "Description": "Từ chối",
    ///         "SaleId" : "1977"
    ///     }
    ///
    /// </remarks>
    [HttpPost("UpdateAccountStatus")]
    public async Task<IActionResult> UpdateAccountStatus(UpdateAccountStatusRequest model, [FromQuery] string? src = "M", [FromQuery] string? lang = "VI")
    {
        try
        {
            LogRequest(model.UserName, model, "UpdateAccountStatus");

            var response = await _accountService.UpdateAccountStatus(model);

            LogResponse(model.UserName, response, "UpdateAccountStatus");

            return Ok(response);
        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}", "UpdateAccountStatus");
            return StatusCode(StatusCodes.Status500InternalServerError, new SystemError());
        }
    }

    /// <summary>
    /// API Get list custcode
    /// </summary>
    /// <param name="model">Thông tin Request</param>
    /// <param name="src">Nguồn yêu cầu (M:Mobile, W:Web)</param>
    /// <param name="lang">Ngôn ngữ (EN: English, VI: VietNam)</param>
    /// <returns> API Get list custcode </returns>
    /// <remarks>
    /// Sample request:
    /// 
    ///     POST /Account/GetListCustcode
    ///     {
    ///         "UserName": "1001-001",
    ///         "Custcode": "",
    ///         "PageIndex": 1,
    ///         "PageSize": 20
    ///     }
    ///
    /// </remarks>
    [HttpPost("GetListCustcode")]
    public async Task<IActionResult> GetListCustcode(GetCustcodeRequestAccount model,
        [FromQuery] string? src = "M",
        [FromQuery] string? lang = "VI")
    {
        try
        {
            LogRequest(model.UserName, model, "GetListCustcode");

            var response = await _accountService.ListCustcode(model);

            LogResponse(model.UserName, response, "GetListCustcode");

            return Ok(response);
        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}", "GetListCustcode");
            return StatusCode(StatusCodes.Status500InternalServerError, new SystemError());
        }
    }

    /// <summary>
    /// API Get Accecpt Token Stingee
    /// </summary>
    /// <param name="model">Thông tin Request</param>
    /// <param name="src">Nguồn yêu cầu (M:Mobile, W:Web)</param>
    /// <param name="lang">Ngôn ngữ (EN: English, VI: VietNam)</param>
    /// <returns> API Get list custcode </returns>
    /// <remarks>
    /// Sample request:
    /// 
    ///     POST /Account/GetAccecptTokenStingee
    ///     {
    ///         "UserName": "1001-001",
    ///         "Custcode": "",
    ///         "PageIndex": 1,
    ///         "PageSize": 20
    ///     }
    ///
    /// </remarks>
    [HttpPost("GetAccecptTokenStingee")]
    public async Task<IActionResult> GetAccecptTokenStingee(BaseRequest model,
        [FromQuery] string? src = "M",
        [FromQuery] string? lang = "VI")
    {
        try
        {
            LogRequest(model.UserName, model, "GetStringeeToken");

            var response = await _accountService.GetStringeeToken(model);

            LogResponse(model.UserName, response, "GetStringeeToken");

            return Ok(response);
        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}", "GetListCustcode");
            return StatusCode(StatusCodes.Status500InternalServerError, new SystemError());
        }
    }
}