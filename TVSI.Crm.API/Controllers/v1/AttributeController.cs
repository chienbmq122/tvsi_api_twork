﻿using Microsoft.AspNetCore.Mvc;
using System.Reflection;
using TVSI.Crm.API.Lib.Models;
using TVSI.Crm.API.Lib.Services;

namespace TVSI.Crm.API.Controllers.v1
{
    [ApiVersion("1.0")]
    public class AttributeController : BaseController
    {
        private readonly IAttributeService _attributeService;

        /// <summary>
        /// Controller Constructor
        /// </summary>
        public AttributeController(IAttributeService attributeService)
        {
            _attributeService = attributeService;
        }

        /// <summary>
        /// API Lấy danh sách Attribute
        /// </summary>
        /// <param name="model">Thông tin Request</param>
        /// <param name="src">Nguồn yêu cầu (M:Mobile, W:Web)</param>
        /// <param name="lang">Ngôn ngữ (EN: English, VI: VietNam)</param>
        /// <returns>Danh sách Attribute</returns>
        /// <remarks>
        /// Sample request:
        /// 
        ///     POST /Common/GetAttribute
        ///     {
        ///        "UserName": "0824-001",
        ///        "UserRole": "CRM_SALE"
        ///     }
        ///
        /// </remarks>
        [HttpPost("GetAttribute")]
        public async Task<IActionResult> GetAttribute(BaseRequest model, [FromQuery] string? src = "M",
            [FromQuery] string? lang = "VI")
        {
            try
            {
                LogRequest(model.UserName, model, "GetAttribute");

                var response = await _attributeService.GetAttribute(model);

                LogResponse(model.UserName, response, "GetAttribute");

                return Ok(response);
            }
            catch (Exception ex)
            {
                LogException(model.UserName, $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}", "GetAttribute");
                return StatusCode(StatusCodes.Status500InternalServerError, new SystemError());
            }
        }

        /// <summary>
        /// API Lấy danh sách category của attribute
        /// </summary>
        /// <param name="model">Thông tin Request</param>
        /// <param name="src">Nguồn yêu cầu (M:Mobile, W:Web)</param>
        /// <param name="lang">Ngôn ngữ (EN: English, VI: VietNam)</param>
        /// <returns>Danh sách category của attribute</returns>
        /// <remarks>
        /// Sample request:
        /// 
        ///     POST /Common/GetExtendCategory
        ///     {
        ///        "UserName": "0824-001",
        ///        "UserRole": "CRM_SALE"
        ///     }
        ///
        /// </remarks>
        [HttpPost("GetExtendCategory")]
        public async Task<IActionResult> GetExtendCategory(BaseRequest model, [FromQuery] string? src = "M",
            [FromQuery] string? lang = "VI")
        {
            try
            {
                LogRequest(model.UserName, model, "GetExtendCategory");

                var response = await _attributeService.GetExtendCategory(model);

                LogResponse(model.UserName, response, "GetExtendCategory");

                return Ok(response);
            }
            catch (Exception ex)
            {
                LogException(model.UserName, $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}", "GetExtendCategory");
                return StatusCode(StatusCodes.Status500InternalServerError, new SystemError());
            }
        }
    }
}
