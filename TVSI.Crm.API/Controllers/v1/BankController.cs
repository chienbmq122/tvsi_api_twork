using System.Reflection;
using Microsoft.AspNetCore.Mvc;
using TVSI.Crm.API.Lib.Models;
using TVSI.Crm.API.Lib.Models.Request.Account;
using TVSI.Crm.API.Lib.Models.Request.Bank;
using TVSI.Crm.API.Lib.Services;

namespace TVSI.Crm.API.Controllers.v1;


/// <summary>
/// Bank Module API
/// </summary>
public class BankController : BaseController
{
    public readonly IBankService _bankService;

    
    
    /// <summary>
    /// Controller Constructor
    /// </summary>
    public BankController(IBankService bankService)
    {
        _bankService = bankService;
    }
    
    
    /// <summary>
    /// Lay bank list
    /// </summary>
    /// <param name="model"></param>
    /// <param name="src"></param>
    /// <param name="lang"></param>
    /// <returns>lấy danh sách thông tin ngân hàng</returns>
    /// <remarks>
    /// Sample request:
    ///
    ///     POST /Bank/GetBankList
    ///     {
    ///         "UserName": "1268-001",
    ///         "UserRole":"CRM_SALE"
    ///     } 
    /// </remarks>
    [HttpPost("GetBankList")]
    public async Task<IActionResult> GetBankList(BankListRequest model, [FromQuery] string? src = "M",
        [FromQuery] string? lang = "VI")
    {
        try
        {
            LogRequest(model.UserName, model, "GetBankList");

            var response = await _bankService.GetBankList(model);

            LogResponse(model.UserName, response, "GetBankList");

            return Ok(response);
        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}", "GetBankList");
            return StatusCode(StatusCodes.Status500InternalServerError, new SystemError());
        }
    } 
    /// <summary>
    /// Lay bank chi nhánh ngân hàng
    /// </summary>
    /// <param name="model"></param>
    /// <param name="src"></param>
    /// <param name="lang"></param>
    /// <returns>lấy danh sách chi nhánh ngân hàng</returns>
    /// <remarks>
    /// Sample request:
    ///
    ///     POST /Bank/GetBankList
    ///     {
    ///         "UserName": "1268-001",
    ///         "UserRole":"CRM_SALE",
    ///         "BankNo" : "001"
    ///     } 
    /// </remarks>
    [HttpPost("GetBranchList")]
    public async Task<IActionResult> GetBranchList(BranchListRequest model, [FromQuery] string? src = "M",
        [FromQuery] string? lang = "VI")
    {
        try
        {
            LogRequest(model.UserName, model, "GetBranchList");

            var response = await _bankService.GetBranchList(model);

            LogResponse(model.UserName, response, "GetBranchList");

            return Ok(response);
        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}", "GetBranchList");
            return StatusCode(StatusCodes.Status500InternalServerError, new SystemError());
        }
    }  
    
}