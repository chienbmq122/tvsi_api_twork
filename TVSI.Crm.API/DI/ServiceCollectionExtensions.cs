﻿using Helper.DataAccess.Dapper;
using Microsoft.Extensions.Options;
using Swashbuckle.AspNetCore.SwaggerGen;
using TVSI.Crm.API.Filters.SwaggerFilters;
using TVSI.Crm.API.Lib.Services;
using TVSI.Crm.API.Lib.Services.Impls;

namespace TVSI.Crm.API.DI;

public static class ServiceCollectionExtensions
{
    public static IServiceCollection AddServices(this IServiceCollection services)
    {
        #region Core Service
        services.AddScoped<IDapperHelper, DapperHelper>();
        services.AddTransient<IConfigureOptions<SwaggerGenOptions>, SwaggerOptions>();
        #endregion Core Service
        services.AddScoped<IRedisService, RedisService>();
        services.AddScoped<IDistributeCacheService, DistributeCacheService>();
        services.AddScoped<ICustomerService, CustomerService>();
        services.AddScoped<IActivityService, ActivityService>();
        services.AddScoped<IContactService, ContactService>();
        services.AddScoped<ICallInAppService, CallInAppService>();
        services.AddScoped<IAccountService, AccountService>();
        services.AddScoped<ILeadService, LeadService>();
        services.AddScoped<IAttributeService, AttributeService>();
        services.AddScoped<IBankService, BankService>();
        services.AddScoped<IManTvsiService, ManTvsiService>();
        services.AddScoped<IShareholderService, ShareholderService>();
        services.AddScoped<ICommonService, CommonService>();
        services.AddScoped<IDashboardService, DashboardService>();
        services.AddScoped<ICommentService, CommentService>();
        services.AddScoped<ISmartOtpService, SmartOtpService>();
        return services;
    }
}