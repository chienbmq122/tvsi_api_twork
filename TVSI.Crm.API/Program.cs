using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.AspNetCore.Mvc.Versioning;
using Serilog;
using StackExchange.Redis;
using TVSI.Crm.API.DI;
using TVSI.Crm.API.Filters.SwaggerFilters;
using TVSI.Crm.API.Lib.Settings;
using TVSI.Crm.API.Middlewares;

var env = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");

var builder = WebApplication.CreateBuilder(args);
var configuration = builder.Configuration;
var services = builder.Services;

builder.Services.AddControllers().AddJsonOptions(options =>
{
    options.JsonSerializerOptions.PropertyNamingPolicy = null;
});

#region AppSettings
configuration
    .AddJsonFile($"appsettings.json", false, true)
    .AddJsonFile($"appsettings.{env}.json", true, true)
    .AddJsonFile($"serilogconfigs.json", false, true)
    .AddEnvironmentVariables();
#endregion

#region Logging

// Config serilog
var logAdvance = configuration.GetSection("LoggingAdvance").Get<LogAdvanceSettings>();

builder.Services.AddSingleton(
    Log.Logger = new LoggerConfiguration()
        .MinimumLevel.Information()
        .ReadFrom.Configuration(
            new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .Build())
        .WriteTo.Map("Name", "other", (name, wt) 
            => wt.File($"{logAdvance.Path}\\{name}_cmrapi_.log", rollingInterval: RollingInterval.Day))
        .CreateLogger()
);
#endregion

#region Cache

//var EncryRedisPass = Encryption.Encrypt(configuration["Redis:Pwd"], CommonConstants.EncryptionKeys);

var redisConf = $"{configuration["Redis:Ip"]},password={configuration["Redis:Pwd"]}";
services.AddStackExchangeRedisCache(options => { options.Configuration = redisConf; });

var cm = ConnectionMultiplexer.Connect(redisConf);
services.AddSingleton<IConnectionMultiplexer>(cm);

#endregion Cache


#region ApiVersioning
services.AddApiVersioning(config =>
{
    config.DefaultApiVersion = new ApiVersion(1, 0);
    config.AssumeDefaultVersionWhenUnspecified = true;
    config.ReportApiVersions = true;
    config.ApiVersionReader = ApiVersionReader.Combine(
        new HeaderApiVersionReader("x-api-version"),
        new QueryStringApiVersionReader("v"));
});

services.AddVersionedApiExplorer(options =>
{
    options.GroupNameFormat = "'v'VVV";
    options.SubstituteApiVersionInUrl = true;
});
#endregion ApiVersioning

#region Swagger

services.AddEndpointsApiExplorer();
services.AddSwaggerGen(c =>
{
    c.OperationFilter<OperationFilter>();

    var xmlPath = Path.Combine(AppContext.BaseDirectory, "TVSI.Crm.API.Document.xml");
    if (File.Exists(xmlPath))
        c.IncludeXmlComments(xmlPath);

    var xmlLibPath = Path.Combine(AppContext.BaseDirectory, "TVSI.Crm.API.Lib.Document.xml");
    if (File.Exists(xmlLibPath))
        c.IncludeXmlComments(xmlLibPath);
});

#endregion Swagger

// Add services to the container.
services.AddServices();
services.AddControllers();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment() || app.Environment.IsStaging())
{
    app.UseSwagger();
    app.UseSwaggerUI(c =>
    {
        var apiVersionDescriptions
            = app.Services.GetService<IApiVersionDescriptionProvider>()?.ApiVersionDescriptions;
        if (apiVersionDescriptions == null) return;
        foreach (var description in apiVersionDescriptions)
            c.SwaggerEndpoint($"/swagger/{description.GroupName}/swagger.json",
                description.GroupName);
    });
}


// Configure the HTTP request pipeline.
app.UseAuthorization();
//app.UseMiddleware<RequestResponseLoggerMiddleware>();
app.UseCors(x => x
    .AllowAnyOrigin()
    .AllowAnyMethod()
    .AllowAnyHeader());

app.MapControllers();
app.Run();

