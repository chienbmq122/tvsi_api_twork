﻿CREATE TABLE [dbo].[SaleInfo](
	[UserName] [varchar](50) NOT NULL,
	[SaleID] [varchar](10) NOT NULL,
	[FullName] [nvarchar](200) NOT NULL,
	[BranchCode] [varchar](5) NOT NULL,
	[Status] [int] NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedBy] [varchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
	[BranchName] [nvarchar](200) NULL,
	[Position] [int] NULL,
	[Phone] [varchar](50) NULL,
	[Email] [varchar](100) NULL,
 CONSTRAINT [PK_SaleInfo] PRIMARY KEY CLUSTERED 
(
	[UserName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]