﻿ALTER PROCEDURE [dbo].[TVSI_sCustInfor_Sync] 	
AS
BEGIN
	CREATE TABLE #TEMP_TABLE (ID bigint identity(1,1), ma_khach_hang varchar(25), ten_khach_hang nvarchar(250),
			loai_khach_hang int, mobile varchar(15), email varchar(150), dia_chi nvarchar(300), 
			ma_chi_nhanh varchar(5), trang_thai int,
			loai_vip int,
			mo_ta nvarchar(50),
			ngay_mo_tai_khoan datetime,
			ngay_cap_nhat datetime,
			ma_nhan_vien varchar(50),
			sale_bb varchar(50),
                        phone_sms varchar(50),
                        phone_01 varchar(50),
                        phone_02 varchar(50),
                        birthday date,
                        gender varchar(2),
						card_id varchar(50),
						card_issuer nvarchar(100),
						card_issue_date datetime)
	DECLARE @previousYear int
	SET @previousYear = YEAR(getdate()) - 1

    SELECT CustCode, SaleID SaleID_BB
        INTO #TEMP_BB
        FROM OPENDATASOURCE ('SQLOLEDB', 'Data Source=10.0.140.117;User ID=sa;Password=gxgiemuoTvsi$;' ).TVSI_TBMDB.dbo.CustInfo 
        WHERE Status IN (0,1,2) AND CustType = 1 AND SaleID NOT IN ('88888','66666','0102')

   SELECT CustomerID, Birthday, Sex Gender
        INTO #TEMP_INNO_CUSTOMER
        FROM  OPENDATASOURCE('SQLOLEDB', 'Data Source=10.0.140.25;User ID=innootws;Password=1nn0t3ch$;').TEST_INNOTRADEDB_PRO.dbo.CUSTOMER 
    WHERE CustomerID <> ''

    SELECT ISNULL(A.CustomerID, B.CustomerID) CustomerID, A.SMS, CC1, CC2, CC3 
        INTO #TEMP_INNO
        FROM (
            SELECT CustomerID, Phone SMS 
                FROM  OPENDATASOURCE('SQLOLEDB', 'Data Source=10.0.140.25;User ID=innootws;Password=1nn0t3ch$;').TEST_INNOTRADEDB_PRO.dbo.CUSTOMERPHONE 
                WHERE IsActive = 1 AND IsSMS = 1 AND CustomerID <> ''
        ) AS A
        FULL OUTER JOIN
        (
            SELECT CustomerID, CC1, CC2, CC3
            FROM
            (
                SELECT Phone, CustomerID, Rank FROM (
                    SELECT Phone, CustomerID, 'CC' + CAST(Rank AS Varchar) Rank 
                    FROM (
                        SELECT CustomerID, Phone, CAST(row_number() over (partition by CustomerID order by DateCreate desc) aS Varchar) as Rank 
                        FROM   OPENDATASOURCE('SQLOLEDB', 'Data Source=10.0.140.25;User ID=innootws;Password=1nn0t3ch$;').TEST_INNOTRADEDB_PRO.dbo.CUSTOMERPHONE 
                        WHERE IsActive = 1 AND IsSMS = 0 AND CustomerID <> ''
                    ) AS A WHERE rank <= 3
                ) AS A
            ) d
            PIVOT
            (
              max(Phone)
              for Rank in (CC1,CC2,CC3)
            ) piv
        ) AS B
        ON A.CustomerID = B.CustomerID

        INSERT INTO #TEMP_TABLE (ma_khach_hang, ten_khach_hang, loai_khach_hang, 
				mobile, email, dia_chi, ma_chi_nhanh, 
				trang_thai, loai_vip, mo_ta, ngay_mo_tai_khoan,
				ngay_cap_nhat, ma_nhan_vien, sale_bb, phone_sms, phone_01, phone_02,
                                birthday, gender, card_id, card_issuer, card_issue_date)

		SELECT A.ma_khach_hang, CAST(A.ten_khach_hang AS NVarchar(250)), A.loai_khach_hang, 
				CAST(A.so_dien_thoai_mb AS Varchar(15)), CAST(A.dia_chi_email AS Varchar(150)), 
                                CAST(D.dia_chi_khach_hang AS NVarchar(300)), A.ma_chi_nhanh,
				coalesce(B.trang_thai, 0) trang_thai, V.loai_vip, V.mo_ta, C.ngay_mo_tai_khoan,
				B.ngay_cap_nhat_cuoi, B.ma_nhan_vien_quan_ly, BB.SaleID_BB,
                                INNO.SMS phone_sms, INNO.CC1 phone_01, INNO.CC2 phone_02,
                                IC.Birthday, IC.Gender, A.so_cmnd_passport, A.noi_cap, A.ngay_cap        
		FROM TVSI_COMMONDB.dbo.TVSI_TAI_KHOAN_KHACH_HANG A
		LEFT JOIN TVSI_COMMONDB.dbo.TVSI_THONG_TIN_LIEN_HE_KHACH_HANG D ON D.so_tai_khoan = A.ma_khach_hang + '1'
		LEFT JOIN TVSI_EMSDB.dbo.TVSI_THONG_TIN_KHACH_HANG B ON A.ma_khach_hang = B.ma_khach_hang
		LEFT JOIN TVSI_EMSDB.dbo.TVSI_THONG_TIN_TAI_KHOAN C ON A.ma_khach_hang = C.ma_khach_hang AND C.loai_tai_khoan = 1
		LEFT JOIN (SELECT loai_vip, mo_ta, ma_khach_hang FROM TVSI_COMMONDB.dbo.TVSI_KHACH_HANG_VIP WHERE YEAR(ngay_tao) = @previousYear) V 
                ON A.ma_khach_hang = V.ma_khach_hang
                LEFT JOIN #TEMP_BB BB ON A.ma_khach_hang = BB.CustCode
                LEFT JOIN #TEMP_INNO INNO ON A.ma_khach_hang = INNO.CustomerID
                LEFT JOIN #TEMP_INNO_CUSTOMER IC ON A.ma_khach_hang = IC.CustomerID

        UPDATE CustInfo
            SET CustName = B.ten_khach_hang, ProfileType = B.loai_khach_hang, Mobile = B.mobile,
                Email = B.email, [Address] = B.dia_chi, [BranchID] = B.ma_chi_nhanh, Status = B.trang_thai,
                VipType = B.loai_vip, VipDesc = B.mo_ta, OpenAccDate = B.ngay_mo_tai_khoan,
                SaleID_BB = B.sale_bb, PhoneSMS = B.phone_sms, PhoneCC_01 = B.phone_01, PhoneCC_02 = B.phone_02,
                Birthday = B.Birthday, Gender = B.Gender, CardID = B.card_id, CardIssuer = B.card_issuer, CardIssueDate= B.card_issue_date,
                AssignUser = B.ma_nhan_vien, UpdatedBy = 'WINSERVICES', UpdatedDate = B.ngay_cap_nhat
            FROM CustInfo A
            INNER JOIN #TEMP_TABLE B
            ON A.CustCode = B.ma_khach_hang
                
        INSERT INTO CustInfo (CustCode, CustName, Mobile, Email, [Address], ProfileType,[Status], VipType, VipDesc, OpenAccDate, 
            BranchID, AssignUser, SaleID_BB, CreatedBy, CreatedDate, PhoneSMS, PhoneCC_01, PhoneCC_02, Birthday, Gender, CardID, CardIssueDate, CardIssuer)
			SELECT DISTINCT B.ma_khach_hang, B.ten_khach_hang, B.mobile, B.email, B.dia_chi, B.loai_khach_hang, B.trang_thai,B.loai_vip, 
                    B.mo_ta, B.ngay_mo_tai_khoan, B.ma_chi_nhanh, B.ma_nhan_vien, B.sale_bb ,'WINSERVICES', GETDATE(), PhoneSMS, PhoneCC_01, PhoneCC_02,
                    B.Birthday, B.Gender, B.card_id, B.card_issue_date, B.card_issuer
			FROM #TEMP_TABLE B
                LEFT JOIN CustInfo A
                ON A.CustCode = B.ma_khach_hang
                WHERE A.CustCode IS NULL

                DROP TABLE #TEMP_BB
                DROP TABLE #TEMP_INNO
                DROP TABLE #TEMP_TABLE
END