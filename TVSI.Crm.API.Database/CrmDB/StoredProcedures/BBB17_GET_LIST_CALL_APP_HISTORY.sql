﻿ALTER PROC [dbo].[BBB17_GET_LIST_CALL_APP_HISTORY]  
    @CustCode varchar(20),	-- Mã khách hàng
	@SaleID varchar(10),	-- Sale ID
	@CustType int,			-- Loại khách hàng
	@FromDate date,			-- Từ ngày
	@ToDate date,			-- Đến ngày
	@Start int,
	@PageSize int,
	@ViewType int			-- 0: Nhân viên, 1: Giám đốc, 2: Trưởng nhóm
AS
/* **********************************************************************
Author:  cuonglm-Lê Minh Cương
Creation Date: 04/08/2022
Desc: Lấy danh sách lịch sử cuộc gọi
Sample: exec BBB17_GET_LIST_CALL_APP_HISTORY '', '', -1, '2022-01-10', '2022-08-05', 0, 10, 0
*/
BEGIN
	IF @ViewType = 0
	BEGIN
		SELECT
			(SELECT A.CallAppHistId, A.CallId, A.CustCode, A.CustName, A.CustType, A.CallAppTypeList, A.SaleID, D.FullName SaleName, A.Rate,
				A.Direction, A.Note, A.Status, A.TimeCall, A.CreatedBy, A.CreatedDate, A.UpdatedBy, A.UpdatedDate,
				B.FullName CreatedName, B.FullName UpdatedName
				FROM CallAppHist A
				LEFT JOIN SaleInfo B on A.CreatedBy = B.UserName
				LEFT JOIN SaleInfo C on A.UpdatedBy = C.UserName
				LEFT JOIN SaleInfo D on A.SaleID = D.SaleID
				WHERE (CustCode = @CustCode OR @CustCode = '')
					AND (A.SaleID = @SaleID OR @SaleID='')
					AND (A.CustType = @CustType OR @CustType = -1)
					AND (A.CreatedDate >= @FromDate AND A.CreatedDate <= @ToDate)
				ORDER BY A.CreatedDate DESC OFFSET @Start ROWS 
				FETCH NEXT @PageSize ROWS ONLY 
				FOR JSON PATH) Items,

			(SELECT Count(*)
				FROM CallAppHist A
				LEFT JOIN SaleInfo B on A.CreatedBy = B.UserName
				LEFT JOIN SaleInfo C on A.CreatedBy = C.UserName
				LEFT JOIN SaleInfo D on A.SaleID = D.SaleID
				WHERE (A.CustCode = @CustCode OR @CustCode = '')
					AND (A.SaleID = @SaleID OR @SaleID='')
					AND (A.CustType = @CustType OR @CustType = -1)
					AND (A.CreatedDate >= @FromDate AND A.CreatedDate <= @ToDate)) TotalItem
	END
END
