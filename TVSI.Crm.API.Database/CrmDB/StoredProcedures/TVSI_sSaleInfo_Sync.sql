﻿CREATE PROCEDURE TVSI_sSaleInfo_Sync
AS
BEGIN
	CREATE TABLE #TEMP_TABLE (ID bigint identity, user_name varchar(50), sale_id varchar(20), 
		full_name nvarchar(200), branch_code varchar(50), status int, position int, 
		branch_name nvarchar(200), phone varchar(50), email varchar(200))

	INSERT INTO #TEMP_TABLE (user_name, sale_id, full_name, branch_code, status,
					position, branch_name, phone, email)
			SELECT A.ten_dang_nhap as user_name, B.ma_quan_ly as sale_id,
				 B.ho_va_ten as full_name, C.ma_chi_nhanh as branch_code, B.trang_thai status,
				 B.level, C.ten_cap_do_he_thong, B.dien_thoai_lien_he, B.dia_chi_email
                 FROM TVSI_EMSDB.dbo.TVSI_THONG_TIN_TRUY_CAP A 
            INNER JOIN TVSI_EMSDB.dbo.TVSI_NHAN_SU B on A.nhan_su_id = B.nhan_su_id 
            LEFT JOIN TVSI_EMSDB.dbo.TVSI_CAP_DO_HE_THONG C on A.cap_do_he_thong_id = C.cap_do_he_thong_id

	update SaleInfo
		SET SaleID = B.sale_id, FullName = B.full_name, BranchCode = B.branch_code, 
		Status = B.status, Position = B.position, BranchName = B.branch_name,
		Phone = B.phone, Email = B.email,
		UpdatedBy = 'Winservices', UpdatedDate = GETDATE()
		FROM SaleInfo A
		INNER JOIN #TEMP_TABLE B ON A.UserName = B.user_name

	INSERT INTO SaleInfo(UserName, SaleID, FullName, BranchCode, Status, 
		Position, BranchName, Phone, Email, CreatedBy, CreatedDate)
		SELECT B.user_name, B.sale_id, B.full_name, B.branch_code, B.status, 
			B.position, B.branch_name, B.phone, B.email,
			'Winservices', GETDATE() FROM #TEMP_TABLE B
			LEFT JOIN SaleInfo A on B.user_name = A.UserName
		WHERE A.UserName IS NULL

	DROP TABLE #TEMP_TABLE;
END