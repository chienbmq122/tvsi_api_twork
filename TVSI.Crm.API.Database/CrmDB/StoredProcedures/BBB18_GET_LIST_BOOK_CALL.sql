﻿ALTER PROC [dbo].[BBB18_GET_LIST_BOOK_CALL] 
    @CustCode varchar(20),	-- Mã khách hàng
	@SaleID varchar(10),	-- Sale ID
	@CustType int,			-- Loại khách hàng
	@FromDate date,			-- Từ ngày
	@ToDate date,			-- Đến ngày
	@Start int,
	@PageSize int,
	@ViewType int			-- 0: Nhân viên, 1: Giám đốc, 2: Trưởng nhóm
AS
/* **********************************************************************
Author:  cuonglm-Lê Minh Cương
Creation Date: 04/08/2022
Desc: Lấy danh sách lịch sử cuộc gọi
Sample: exec BBB18_GET_LIST_BOOK_CALL '', '', -1, '2022-01-10', '2022-08-05', 0, 10, 0
*/
BEGIN
	IF @ViewType = 0
	BEGIN
		SELECT
			(SELECT A.BookApptCallId, A.CustCode, A.CustName, A.CustType, A.CallAppTypeList, A.BookCallType,
				A.AssignUser, D.FullName AssignUserName, D.BranchName BranchName,
				A.Description, A.Status, A.StartTime, A.EndTime, A.RemindTime, A.CreatedBy, A.CreatedDate, A.UpdatedBy, A.UpdatedDate,
				B.FullName CreatedName, B.FullName UpdatedName
				FROM BookApptCall A
				LEFT JOIN SaleInfo B on A.CreatedBy = B.UserName
				LEFT JOIN SaleInfo C on A.UpdatedBy = C.UserName
				LEFT JOIN SaleInfo D on A.AssignUser = D.UserName
				WHERE (CustCode = @CustCode OR @CustCode = '')
					AND (A.SaleID = @SaleID OR @SaleID='')
					AND (A.CustType = @CustType OR @CustType = -1)
					AND (A.CreatedDate >= @FromDate AND A.CreatedDate <= @ToDate)
				ORDER BY A.CreatedDate DESC OFFSET @Start ROWS 
				FETCH NEXT @PageSize ROWS ONLY 
				FOR JSON PATH) Items,

			(SELECT Count(*)
				FROM BookApptCall A
				LEFT JOIN SaleInfo B on A.CreatedBy = B.UserName
				LEFT JOIN SaleInfo C on A.UpdatedBy = C.UserName
				LEFT JOIN SaleInfo D on A.AssignUser = D.SaleID
				WHERE (A.CustCode = @CustCode OR @CustCode = '')
					AND (A.SaleID = @SaleID OR @SaleID='')
					AND (A.CustType = @CustType OR @CustType = -1)
					AND (A.CreatedDate >= @FromDate AND A.CreatedDate <= @ToDate)) TotalItem
	END

END
