﻿CREATE PROCEDURE SYS001_GET_ALL_LANGUAGES

AS
/* **********************************************************************
Author:  cuonglm-Lê Minh Cương
Creation Date: 04/08/2022
Desc: Lấy tất cả multi language
Sample: exec SYS001_GET_ALL_LANGUAGES 
*/
BEGIN
	SELECT ItemCode, Vietnamese, English 
	FROM Language
END