using System.ComponentModel;

namespace TVSI.Crm.API.Common.Enums;

public enum ErrorCodeDetail
{
    [Description("Thành công.")]
    Success = 0,
    [Description("Thất bại.")]
    Failed = -1,
    [Description("Hệ thống bị gián đoạn. Vui lòng liên hệ 19001900 để được hỗ trợ!")]
    Exception = -99999,
    [Description("Tài khoản đã được đăng ký.")]
    Registered = -2,
    [Description("Tài khoản đã được mở margin hoặc chưa mở HĐ chính.")]
    MarginRegistered = -3,

    #region Code for Authen

    AccountHasBeenLocked = 69901,
    IncorrectInfoLogin = 69902,
    NotAllowedToAccessApplication = 69903,
    NotAllowedToAccessAction = 69904,
    TokenRequired = 69905,
    TokenNotFound = 69906,
    InvalidToken = 69907,
    NoDataFound = 69908,
    InvalidUserInfo = 69909,

    #endregion Code for Authen
}
