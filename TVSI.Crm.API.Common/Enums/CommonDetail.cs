using System.ComponentModel;

namespace TVSI.Crm.API.Common.Enums;

public class CommonDetail
{
       /// <summary>
    /// Indicates whether the sample is used for request or response
    /// </summary>
    public enum SampleDirection
    {
        Request = 0,
        Response
    }

    public enum TypeLeave
    {
        TimeKeeping = 1, // Chấm công
        OverTime = 2, // Làm thêm giờ
        LeaveApplications = 3 // Nghỉ phép
    }

    public enum ApprovedStatus
    {
        PendingApproved = 0, // Tạo mới
        Approved = 1, // Đã duyệt
        ExecutiveBoardApproved = 2, //DB đã duyệt
        Deleted = 99, // Đã xóa
        Reject = -1, // Từ chối
    }

    public enum ProfileStatus
    {
        Pending = 000, // chờ xử lý
        PendingApproved = 100 ,// chờ duyệt
        DebtProfile = 101, // Nợ hồ sơ
        CompleteProfile = 200, // Hoàn thành
        Reject = 199 // hoặc 299 là trạng thái từ chối
    }

    public enum Activated
    {
        [Description("000")]
        Pending = 000, // chờ xử lý
        [Description("001")]
        PendingBUMANApproved = 001, // chờ BU_MAN duyệt
        [Description("002")]
        PengdingFSApproved = 002, // Chờ FS duyệt
        [Description("099")]
        BUMANReject  = 099, // BU_MAN từ chối
        [Description("100")]
        Processing  = 100, // Đang xử lý
        [Description("102")]
        ActivatedKSV = 102, // Kích hoạt(KSV SS)
        [Description("200")]
        CompleteAccount = 200 , // Hoàn thành
        [Description("399")]
        FSReject = 399, // FS từ chối
        [Description("999")]
        CancelAccount = 999 // Đã hủy
    }

    /*public enum TypeContact
    {
        OpenAccountCRM = 2, // mở tài khoản trên CRM
        OpeneKYCMobile = 6, // mở tài khoản eKYC trên Mobile
        OpeneKYCWeb = 4, // mở tài khoản eKYC trên Website
        OpenAccountEMS = 1, // mở tài khoản trên EMS
        OpenWebsite = 3 // mở tài khoản thường trên website
    }  */ 
    public enum SourceAccount
    {
        OpenAccountCRM = 2, // mở tài khoản trên CRM
        OpeneKYCMobile = 6, // mở tài khoản eKYC trên Mobile
        OpeneKYCWeb = 4, // mở tài khoản eKYC trên Website
        OpenAccountEMS = 1, // mở tài khoản trên EMS
        OpenWebsite = 3, // mở tài khoản form trên website
        TWork = 7, // mở tài khoản trên TWork
    }
    public enum CustTypeConst
    {
        INDIVIDUAL_DOMESTIC_INVEST = 1,     // CN trong nuoc
        ORGANIZATION_DOMESTIC_INVEST = 2,   // TC trong nuoc
        INDIVIDUAL_FOREIGN_INVEST = 3,       // CN nuoc ngoai
        ORGANIZATION_FOREIGN_INVEST = 4     // TC nuoc ngoai
    }

    public enum OpenContact
    {
        OpenContactDefault = 0,// hợp đồng thường
        OpenContactMargin = 1, // hợp đồng margin
        OpenContactDerivative = 2 //Hợp đồng phái sinh
    }

    public enum Member
    {
        Employee = 0, // nhân viên 1, trưởng phòng, 2 giám đốc
        Manager =1, // trưởng phòng
        GerenalManager = 2 // giám đốc
        
    }
    /// <summary>
    /// Phương thức giao dịch
    /// </summary>
    public enum TradeMethodConst
    {
        Phone = 1,
        Internet = 2
    }

    /// <summary>
    /// Phương thức nhận kết quả giao dịch
    /// </summary>
    public enum TradeResultMethodConst
    {
        SMS = 1,            // SMS
        Email = 2,          // Email
        AtCounter = 3       // Tại quày
    }

    /// <summary>
    /// Phương thức sao kê
    /// </summary>
    public enum AccStatementMethodConst
    {
        RequisteredLetter = 1,  // Thư đảm bảo
        Email = 2,              // Email
        AtCounter = 3           // Tại quầy
    }

    public enum TransferMoneyConst
    {
        Bank = 1, // sang tk ngan hang
        Account = 2, // chuyen tien noi bo
        Bidv = 3, // tai khoan nuoc ngoai
        Phone = 4,
        iTradeHome = 5
    }

    public enum Categories
    {
        Job = 1, // Ngề nghiệp
        FinancialCapability = 2,//Khả năng tài chính
        InvestmentExperience = 8, // Kinh nghiệm đầu tư
        WorkPlace = 6, // nơi làm việc
        JonedBond = 7, // Tham gia TP từ
        TakeRisks = 9, // chấp nhận rủi ro
        InvestmentPrefer = 10 , // sở thích đầu tư
        NoteCommunication = 11 , // Lưu ý khi giao tiếp
        SaleRelationship = 12 , // mối quan hệ với sale
    }

    public enum Gender
    {
        Male = 1, // Nam
        Female = 2, // Nu
        NotGender = 3 // ko xác định
    }

    public enum PolicyLeave
    {
        YearLeave = 1 ,// Nghỉ theo chế độ phép năm
        SpecialLeave = 2 ,// Nghỉ phép đặc biệt
        CompensatoryLeave = 3 ,// Nghỉ bù
        SickLeave = 4,// Nghỉ ốm
        SickChildrenLeave = 5, // Nghỉ con ốm
        NoPayLeave =6, // Nghỉ không lương
        Other = 7 // Khác
    }

    public enum Activity
    {
        WorkOut = 1,// Công việc ngoài văn phòng (thay chấm công vân tay)
        Phone = 2, // Gọi điện thoại/Email
        Other = 4, // Khac
    }

    public enum Status
    {
        Plan = 11, // Đã lập kế hoạch
        organization = 12, // Đã tổ chức
        Ongoing = 13, //Đang tổ chức
        Complete = 14 // Hoàn thành
    }

    public enum StatuseKYC
    {
        NotActivated = 0, // Chưa kích hoạt
        Activated = 1 //Đã kích hoạt
    }

    /// <summary>
    /// dropdown lý do
    /// </summary>
    public enum Reasons
    {
        MeetCustomer = 1, // Đi gặp gỡ KH hiện tại/tiềm năng
        OpenAccount = 2, //Đi mở tài khoản chứng khoán (cơ sở/ phái sinh)
        ContractBonds = 3,//Đi ký hợp đồng trái phiếu
        CustomerPay = 4, // Đi hỗ trợ khách hàng nộp tiền
        CustomerProblems = 5, // Đi hỗ trợ khách hàng gặp sự cố giao dịch
        ContractAdditional = 6, // Đi ký hợp đồng bổ sung
        OrderList = 7, // Hoàn thành danh sách đặt lệnh
        CustomerGifts = 8,//Đi tặng quà khách hàng (sinh nhật, tết, các sự kiện khác)
        BusinessVisit = 9, // Đi thăm doanh nghiệp/ họp ĐHCĐ
        PersonalWork = 10, // đi việc cá nhân
        Other = 11, // Khác
    }
    
    
    public enum ProfileType
    {
        ProfileOpen = 1,
        ProfileExtend = 2
    }

}