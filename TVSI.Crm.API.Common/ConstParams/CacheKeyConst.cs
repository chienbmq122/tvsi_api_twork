﻿namespace TVSI.Crm.API.Common.ConstParams;

public class CacheKeyConst
{
    public const string RootPath = "TVSI_CRM_API";
    public const string Language = RootPath + ":LANGUAGE";
}