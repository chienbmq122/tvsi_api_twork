﻿using System.ComponentModel;

namespace TVSI.Crm.API.Common.ConstParams;

public class CommonConst
{
    
}
public enum ValidateAccountConst
{
    ACCOUNT_NOT_EXIST = 0,                          // Chưa mở
    ACCOUNT_EXIST_NOT_MARGIN = 1,                   // Đã mở nhưng chưa mở TK marin
    ACCOUNT_EXIST_MARGIN = 2,                       // Đã mở TK marin
    ACCOUNT_EXIST_DIF_CUSTYPE = 3,                  // Đã mở nhưng loại hình khác 
    OPEN_CONTRACT_EXSIT = 4,                        // HĐ mở TK đã có hoặc đang xử lý
    ACCOUNT_EXIST_DERIVATIVE = 5,                   // Đã mở tài khoản phái sinh
    ACCOUNT_EXIST_MARGIN_AND_DERIVATIVE = 6,        // Đã mở TK margin và phái sinh
    CUSTOMER_EXIST_BUT_ACCOUNT_NOT_EXIST = 7,       // Trường hợp mới mở Customer, chưa mở Account (Đuôi 1)
    SYSTEM_ERROR = 9,                               // Lỗi API
    SYSTEM_PERMISSION = 99,                         // Trường hợp Sales không có quyền truy khuất tài khoản này
    ACCOUNT_EXIST = 10,                             // Đã mở
    ACCOUNT_MAX_DEBT = 33,                           // Số lượng hợp đồng mở nợ quá giới hạn
    ACCOUNT_CLOSED = 88                           // Tk đã đóng
}

/// <summary>
/// Phân loại khách hàng
/// </summary>
public enum CustTypeConst
{
    /// <summary>
    /// Cá nhân trong nước
    /// </summary>
    [Description("Cá nhân Trong nước")]
    INDIVIDUAL_DOMESTIC_INVEST = 1,

    /// <summary>
    /// Cá nhân trong nước bond
    /// </summary>
    [Description("Cá nhân Trong nước trái phiếu")]
    INDIVIDUAL_DOMESTIC_INVEST_BOND = 11,

    /// <summary>
    /// Tổ chức trong nước
    /// </summary>
    [Description("Tổ chức Trong nước")]
    ORGANIZATION_DOMESTIC_INVEST = 2,

    /// <summary>
    /// Cá nhân nước ngoài
    /// </summary>
    [Description("Cá nhân nước ngoài")]
    INDIVIDUAL_FOREIGN_INVEST = 3,

    /// <summary>
    /// Tổ chức nước ngoài
    /// </summary>
    [Description("Tổ chức nước ngoài")]
    ORGANIZATION_FOREIGN_INVEST = 4
}

/// <summary>
/// Loại hồ sơ
/// </summary>
public enum DocumentTypeConst
{
    /// <summary>
    /// Thêm mới hợp đồng mở tài khoản
    /// </summary>
    [Description("Thêm mới")]
    OPEN_DOCUMENT = 1,

    /// <summary>
    /// Bổ sung hồ sơ mở tài khoản
    /// </summary>
    [Description("Bổ sung")]
    ADDITION_DOCUMENT = 2
}

/// <summary>
/// Loại hợp đồng mở
/// </summary>
public enum AdditionContractConst
{
    Normal = 0,     // HĐ thường
    Margin = 1     // HĐ Margin
}
/// <summary>
/// Trạng thái dịch vụ
/// </summary>
public enum ServiceStatusConst
{
    [Description("Không đăng ký")]
    KO_DANG_KY = -1,
    [Description("Chờ xử lý")]
    CHO_XU_LY = 0,
    [Description("Chờ HO xác nhận")]
    CHO_HO_XAC_NHAN = 1,
    [Description("Chờ KH xác nhận")]
    CHO_KH_XAC_NHAN = 2,
    [Description("Từ chối")]
    BUMAN_TU_CHOI = 99,
    [Description("Từ chối")]
    KSV_TU_CHOI = 98
}
/// <summary>
/// Giới tính
/// </summary>
public enum SexConst
{
    [Description("Không xác định")]
    Unknown = 0,
    [Description("Nam")]
    Male = 1,
    [Description("Nữ")]
    Female = 2
}