﻿namespace TVSI.Crm.API.Common;

public class CommonConstants
{
    #region Times

    public const int _1Day = 1;
    public const int _7Days = 7;
    public const int _30Days = 30;

    public const int _1Hours = 1;
    public const int _4Hours = 4;
    public const int _8Hours = 8;

    public const int _10Minutes = 10;
    public const int _15Minutes = 15;
    public const int _30Minutes = 30;
    public const int _60Minutes = 60;

    #endregion Times

    public const int SqlServerTimeout = 30; //seconds
    public const int ApiCallerTimeout = 10000; //milliseconds = 0.166666667 minutes 

    public const int AccessFailedCount = 5;
    public const int ShowCaptcha = 3;

    public const string EncryptionKeys = "!Tvsi@2022#$%^&*";
    public const string SchemaAuth = "Bearer ";

    public const string ErrPrefix = "";
    public const string PasswordChar = "********";
    
    public const int FormWebsite = 3; // Open account Form Web 
    public const int EkycWebsite = 4; // Open account ekyc Web 
    public const int EkycMobile = 6; // Open account ekyc Mobile 


    public const string DateFormat = "dd/MM/yyyy";
    public const string DateTimeFormat = "dd/MM/yyyy HH:mm:ss";
    public const string? Branchiddefault = "01";
    public const string? Branchnamedefault = "HỘI SỞ CHÍNH";
    public const int BankNotRegister = -1;
}

public class CacheKeys
{
    public const string RootPath = "TVSI";
    public const string UserInfo = RootPath + "USER_INFO:";
    public const string AccessFailedCount = RootPath + "ACCESS_FAILED_COUNT:";
    public const string TokenRevoked = RootPath + "TOKEN_REVOKED:";
    public const string TokenCurrDevice = RootPath + "TOKEN_CURR_DEVICE:";
    public const string SysConfigParam = RootPath + "SYS:SYS_CONFIG_PARAM";
    public const string SysErrCode = RootPath + "SYS:SYS_ERR_CODE";
}

/// <summary>
/// Trạng thái hồ sơ Color
/// </summary>
public class ProfileStatusColorConst
{
    public const string ProfileDebt = "#C20E11"; // Nợ hồ sơ
    public const string Process = "#0573ff"; // Chờ xu lý
    public const string ProfileNo = "#34A853"; // Ko hồ sơ
    public const string Pending = "#FBBC05"; // Chờ duyệt
    public const string Finish = "#34A853"; // Hoàn thành
    public const string Reject = "#C20E11"; // Từ chối	
}

/// <summary>
/// Trạng thái tài khoản Color
/// </summary>
public class AccountStatusColorConst
{
    public const string WaitingBuMen = "#0573ff"; // Chờ BU MAN duyệt	
    public const string WaitingFs = "#0573ff"; // Chờ FS duyệt	
    public const string Process = "#0573ff"; //  Chờ xu lý
    public const string Processing = "#FBBC05"; // Đang xử lý	
    public const string WatingActive = "#FBBC05"; // Chờ kích hoạt	
    public const string Active = "#34A853"; // Kích hoạt	
    public const string Finish = "#34A853"; // Hoàn thành	
    public const string Reject = "#C20E11"; // Từ chối
    public const string Cancell = "#C20E11"; // Đã hủy	
}
/// <summary>
/// Trạng thái tương tác Color
/// </summary>
public class ActionsStatusColorConst
{
    public const string Process = "#0573ff"; // Chờ xu lý
    public const string Contaced = "#34A853"; // Đã liên hệ
    public const string ContacedFailFirst = "#C20E11"; //  Không nghe máy lần 1
    public const string ContacedFailSecond = "#C20E11"; //  Không nghe máy lần 2
    public const string ContacedFailMul = "#C20E11"; // Không nghe máy lần 3	
}

/// <summary>
/// Mã loại hình tài khoản
/// </summary>
public class TypeAccountConst
{
    public const int INDIVIDUAL_DOMESTIC_INVEST = 1;    // CN trong nuoc
    public const int ORGANIZATION_DOMESTIC_INVEST = 2;  // TC trong nuoc
    public const int INDIVIDUAL_FOREIGN_INVEST = 3;     // CN nuoc ngoai
    public const int ORGANIZATION_FOREIGN_INVEST = 4;   // TC nuoc ngoai

}

/// <summary>
/// Mã trạng thái tài khoản
/// </summary>
public class AccountStatusConst
{
    public const string Process = "000"; //  Chờ xu lý
    public const string WaitingBuMen = "001"; // Chờ BU MAN duyệt	
    public const string WaitingFs = "002"; // Chờ FS duyệt	
    public const string BDSaleApprove = "003"; // GDMG duyệt
    public const string BuMenReject = "099"; // BU_MAN từ chối
    public const string BDSaleReject = "033"; // GDMG từ chối
    public const string Processing = "100"; // Đang xử lý	
    public const string ProcessingSecond = "101"; // Đang xử lý	
    public const string WatingActive = "102"; // Chờ kích hoạt	
    public const string Finish = "200"; // Hoàn thành	
    public const string FsReject = "399"; // FS từ chối
    public const string Reject = "199"; // từ chối
    public const string RejectSecond = "299"; // từ chối
    public const string Cancell = "999"; // Đã hủy	
}
public class ProfileStatusConst
{
    public const string Process = "000"; //  Chờ xử lý
    public const string Pending = "100"; //  Chờ duyệt
    public const string ProfileDebt = "101"; //  Nợ hồ sơ
    public const string ProfileNo = "102"; //  Không hồ sơ
    public const string Finish = "200"; // Hoàn thành
    public const string Reject = "199"; // Từ chối
    public const string RejectSecond = "299"; // Từ chối

}

/// <summary>
/// Mã trạng thái tương tác
/// </summary>
public class ActionStatusConst
{
    public const string Process = "000"; //  Chờ xu lý
    public const string Contaced = "100"; //  Đã liên hệ
    public const string ContacedFailFirst = "200"; //  Không nghe máy lần 1
    public const string ContacedFailSecond = "300"; //  Không nghe máy lần 2
    public const string ContacedFailMul = "400"; //  Không nghe máy lần 4
}

/// <summary>
/// Quốc tịch
/// </summary>
public class Nationality
{
    public const string NationalityName = "Việt Nam";
    public const string NationalityCode = "000";
}
