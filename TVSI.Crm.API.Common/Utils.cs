﻿using System.Globalization;

namespace TVSI.Crm.API.Common;

public static class Utils
{
    public static string ErrorCodeFormat(this int errCode)
    {
        return !string.IsNullOrEmpty(CommonConstants.ErrPrefix)
            ? errCode != 1 ? $"{CommonConstants.ErrPrefix}-{errCode}" : errCode.ToString()
            : errCode.ToString();
    }

    public static string AccountRenderColors(this string statusCode)
    {
        switch (statusCode)
        {
            case AccountStatusConst.Process:
                return AccountStatusColorConst.Process;
                    
            case AccountStatusConst.WaitingBuMen:
                return AccountStatusColorConst.WaitingBuMen;
                    
            case AccountStatusConst.WaitingFs:
                return AccountStatusColorConst.WaitingFs;  
                    
            case AccountStatusConst.Processing:
                return AccountStatusColorConst.Processing;      
                    
            case AccountStatusConst.WatingActive:
                return AccountStatusColorConst.WatingActive;  
                    
            case AccountStatusConst.Finish:
                return AccountStatusColorConst.Finish;    
                                          
            case AccountStatusConst.FsReject:
                return AccountStatusColorConst.Reject;  
                    
            case AccountStatusConst.Reject:
                return AccountStatusColorConst.Reject;     
                    
            case AccountStatusConst.RejectSecond:
                return AccountStatusColorConst.Reject;  
                    
            case AccountStatusConst.Cancell:
                return AccountStatusColorConst.Cancell;
            default:
                return "";
        }
    }

    public static string ProfileRenderColors(this string statusCode)
    {
        switch (statusCode)
        {
            case ProfileStatusConst.Process:
                return ProfileStatusColorConst.Process;
            
            case ProfileStatusConst.Pending:
                return ProfileStatusColorConst.Pending; 
            
            case ProfileStatusConst.ProfileDebt:
                return ProfileStatusColorConst.ProfileDebt;
            
            case ProfileStatusConst.ProfileNo:
                return ProfileStatusColorConst.ProfileNo;    
            
            case ProfileStatusConst.Finish:
                return ProfileStatusColorConst.Finish;
            
            case ProfileStatusConst.Reject:
                return ProfileStatusColorConst.Reject; 
            
            case ProfileStatusConst.RejectSecond:
                return ProfileStatusColorConst.Reject;
            
            default:
                return "";
        }
    }

    public static string ActionsRenderColors(this string statusCode)
    {
        switch (statusCode)
        {
            case ActionStatusConst.Process:
                return ActionsStatusColorConst.Process;
            
            case ActionStatusConst.Contaced:
                return ActionsStatusColorConst.Contaced;   
            
            case ActionStatusConst.ContacedFailFirst:
                return ActionsStatusColorConst.ContacedFailFirst;
            
            case ActionStatusConst.ContacedFailSecond:
                return ActionsStatusColorConst.ContacedFailSecond;  
            
            case ActionStatusConst.ContacedFailMul:
                return ActionsStatusColorConst.ContacedFailMul;
            default:
                return "";
        }
    }
    
    public static string ToTitleCase(this string title)
    {
        return CultureInfo.CurrentCulture.TextInfo.ToTitleCase(title.ToLower()); 
    }
    
    public static bool IsValidDateTime(string value, string formatTime = "dd/MM/yyyy")
    {
        try
        {
            DateTime timeValue;
            return DateTime.TryParseExact(value, formatTime,CultureInfo.InvariantCulture,
                DateTimeStyles.None, out timeValue);
        }
        catch (Exception e)
        {
            return false;
        }

    }
    public static bool CheckIssueDateEkyc(string cardID, int type, DateTime issueDate)
    {
        try
        {
            var currentDate = DateTime.Now;
            if (type == 0 || type == 1)
            {
                    
                if (cardID.Length == 9)
                {
                    return currentDate <= issueDate.AddYears(15);
                }

                if (cardID.Length == 12)
                {
                    return true;
                }
            }
            if (type == 3)
            {
                return currentDate <= issueDate.AddYears(12);
            }
            if (type == 7)
            {
                return currentDate <= issueDate.AddYears(12);
            }
            return true;
        }
        catch (Exception e)
        {
            return false;
        }
    }

 
}